//
//  Layout.m
//  Shop
//
//  Created by ike on 16/12/5.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "Layout.h"

@implementation Layout


+(NSDictionary *)order{
    return @{@(0):@{@"Rows":@1,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"NameTextViewCell"}},
             @(1):@{@"Rows":@1,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"NameTextViewCell"}},
             @(2):@{@"Rows":@1,
                    @"Header":@{@"Bool":@"Yes",@"Height":@"40",@"NameCell":@"moneLabelCell"},
                    @(0):@{@"NameCell":@"mmImglabelCell"}},
             @(3):@{@"Rows":@1,
                    @(0):@{@"NameCell":@"SelendBtnCell",@"data":@"去支付"}}};
}

//+(Layout *)sharedLayout{
//    static Layout *layout;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken,^{
//        layout = [[self alloc]init];
//    });
//    return layout;
//
//}

+(NSDictionary *)aorder{
    return @{@(0):@{@"Rows":@0,
                    @"Header":@{@"Bool":@"Yes"},
                    @"Footer":@{@"Bool":@"Yes",@"Height":@"25"},
                    @(0):@{@"NameCell":@"msevenLabelCell"}
                    }};
}



+ (NSDictionary *)border{
    return @{@(0):@{@"Rows":@4,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"NameTextViewCell"}},
             @(1):@{@"Rows":@2,
                    @"Header":@{@"Bool":@"Yes",@"Height":@"40",@"NameCell":@"moneLabelCell"},
                    @(0):@{@"NameCell":@"NameTextViewCell"}},
             @(2):@{@"Rows":@1,
                    @"Footer":@{@"Bool":@"Yes",@"Height":@"40",@"NameCell":@"moneLabelCell"},
                    @(0):@{@"NameCell":@"SelendBtnCell",@"data":@"提交申请"}}};

}

+(NSDictionary *)corder{

   return  @{@(0):@{@"Rows":@1,
                    @(0):@{@"NameCell":@"mQRCell",@"Type":@"0"}}};

}




//    待验证： 酒店0 KTV1  商城2 美食3 外卖4
//    待发货   10
//    待退款   100
//    已完成   已退款1000   商城 10


#pragma mark  0酒店
+(NSDictionary *)hotelOrder{
    
    return @{@(0):@{@"Rows":@2,
                    @(0):@{@"NameCell":@"mImglabelCell",@"Type":@"1"},
                    @(1):@{@"NameCell":@"mTwoBtnCell"}},
             @(1):@{@"Rows":@3,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"mtwolabelCell",@"Type":@"0"},
                    @(1):@{@"NameCell":@"mmimaCell"},
                    @(2):@{@"NameCell":@"mtwolabelnoCell"}},
             @(2):@{@"Rows":@4,//4
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"mtwolabelnoCell",@"Type":@"0"}},
             @(3):@{@"Rows":@2,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"myonelabelCell",@"Type":@"1"},
                    @(1):@{@"NameCell":@"LinkCell"}
                    },
             @(4):@{@"Rows":@5,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"mmonelabelCell",@"Type":@"0"},
                    @(1):@{@"NameCell":@"mhuajiaCell"},
                    @(2):@{@"NameCell":@"OldCell"},
                    @(3):@{@"NameCell":@"SignCell"},
                    @(4):@{@"NameCell":@"moneBtnCell"}
                    },
             @(5):@{@"Rows":@2,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"myonelabelCell",@"Type":@"0"},
                    @(1):@{@"NameCell":@"mfivelabelCell"}}};
    
}

#pragma mark 1KTV
+(NSDictionary *)KTVOrder{
    
    return @{@(0):@{@"Rows":@2,
                    @(0):@{@"NameCell":@"mImglabelCell",@"Type":@"1"},
                    @(1):@{@"NameCell":@"mTwoBtnCell"}},
             @(1):@{@"Rows":@3,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"mtwolabelCell",@"Type":@"1"},
                    @(1):@{@"NameCell":@"mmimaCell"},
                    @(2):@{@"NameCell":@"mtwolabelnoCell"}},
             @(2):@{@"Rows":@2,//2
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"mtwolabelnoCell",@"Type":@"0"}},
             @(3):@{@"Rows":@2,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"myonelabelCell",@"Type":@"1"},
                    @(1):@{@"NameCell":@"LinkCell"}
                    },
             @(4):@{@"Rows":@5,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"mmonelabelCell",@"Type":@"0"},
                    @(1):@{@"NameCell":@"mhuajiaCell"},
                    @(2):@{@"NameCell":@"OldCell"},
                    @(3):@{@"NameCell":@"SignCell"},
                    @(4):@{@"NameCell":@"moneBtnCell"}
                    },
             @(5):@{@"Rows":@2,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"myonelabelCell",@"Type":@"0"},
                    @(1):@{@"NameCell":@"mfivelabelCell"}}};
    
}

#pragma mark  2商城
+(NSDictionary *)clothesOrder{

    return @{@(0):@{@"Rows":@2,
                    @(0):@{@"NameCell":@"mImglabelCell",@"Type":@"1"},
                    @(1):@{@"NameCell":@"mTwoBtnCell"}},
             @(1):@{@"Rows":@3,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"mtwolabelnoCell",@"Type":@"0"},
                    @(1):@{@"NameCell":@"mtwolabelnoCell"},
                    @(2):@{@"NameCell":@"mmimaCell"}},
             @(2):@{@"Rows":@1,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"mmtwolabelCell"}},
             @(3):@{@"Rows":@2,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"myonelabelCell",@"Type":@"1"},
                    @(1):@{@"NameCell":@"LinkCell"}
                    },
             @(4):@{@"Rows":@5,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"mmonelabelCell",@"Type":@"0"},
                    @(1):@{@"NameCell":@"mhuajiaCell"},
                    @(2):@{@"NameCell":@"OldCell"},
                    
                    @(3):@{@"NameCell":@"SignCell"},
                    @(4):@{@"NameCell":@"moneBtnCell"}
                    },
             @(5):@{@"Rows":@2,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"myonelabelCell",@"Type":@"0"},
                    @(1):@{@"NameCell":@"mfivelabelCell"}}};

}

#pragma mark  3美食
+(NSDictionary *)foodOrder{
    
    return @{@(0):@{@"Rows":@2,
                    @(0):@{@"NameCell":@"mImglabelCell",@"Type":@"1"},
                    @(1):@{@"NameCell":@"mTwoBtnCell"}},
             @(1):@{@"Rows":@2,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"mtwolabelCell",@"Type":@"1"},
                    @(1):@{@"NameCell":@"mmimaCell"}
                
                    },
             @(2):@{@"Rows":@2,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"myonelabelCell",@"Type":@"1"},
                    @(1):@{@"NameCell":@"LinkCell"}
                    },
             @(3):@{@"Rows":@5,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"mmonelabelCell",@"Type":@"0"},
                    @(1):@{@"NameCell":@"mhuajiaCell"},
                    @(2):@{@"NameCell":@"OldCell"},
                    @(3):@{@"NameCell":@"SignCell"},
                    @(4):@{@"NameCell":@"moneBtnCell"}
                    },
             @(4):@{@"Rows":@2,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"myonelabelCell",@"Type":@"0"},
                    @(1):@{@"NameCell":@"mfivelabelCell"}}};

}

#pragma mark  4外卖
+(NSDictionary *)orderOrder{
    
    return @{@(0):@{@"Rows":@2,
                    @(0):@{@"NameCell":@"mTakeOutCell",@"Type":@"0"}
                    },
             @(1):@{@"Rows":@2,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"mtwolabelCell",@"Type":@"1"},
                    @(1):@{@"NameCell":@"mmimaCell"}
                    
                    },
             @(2):@{@"Rows":@2,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"myonelabelCell",@"Type":@"1"},
                    @(1):@{@"NameCell":@"LinkCell"}
                    },
           
             @(3):@{@"Rows":@2,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"myonelabelCell",@"Type":@"0"},
                    @(1):@{@"NameCell":@"mfivelabelCell"}}};
    
}

#pragma mark  10 待发货---10已完成 商城

+(NSDictionary *)fahuoOrder{
 
    return @{@(0):@{@"Rows":@2,
                    @(0):@{@"NameCell":@"mImglabelCell",@"Type":@"1"},
                    @(1):@{@"NameCell":@"mTwoBtnCell"}},
             @(1):@{@"Rows":@2,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"mtwolabelnoCell",@"Type":@"0"},
//                    @(1):@{@"NameCell":@"mtwolabelnoCell"}
                    },
             @(2):@{@"Rows":@3,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"myonelabelCell",@"Type":@"1"},
                    @(1):@{@"NameCell":@"mThreeLabelCell"},
                    @(2):@{@"NameCell":@"mtwolabelnoCell"}
                    },
             @(3):@{@"Rows":@2,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"myonelabelCell",@"Type":@"1"},
                    @(1):@{@"NameCell":@"LinkCell"}
                    },
             @(4):@{@"Rows":@5,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"mmonelabelCell",@"Type":@"0"},
                    @(1):@{@"NameCell":@"mhuajiaCell"},
                    @(2):@{@"NameCell":@"OldCell"},
                    @(3):@{@"NameCell":@"SignCell"},
                    @(4):@{@"NameCell":@"moneBtnCell"}
                    },
             @(5):@{@"Rows":@2,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"myonelabelCell",@"Type":@"0"},
                    @(1):@{@"NameCell":@"mfivelabelCell"}}};
    
}


#pragma mark  100 待退款
+(NSDictionary *)tuikuanOrder{
    
    return @{@(0):@{@"Rows":@1,
                     @(0):@{@"NameCell":@"mImglabelCell"}},
             @(1):@{@"Rows":@2,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"myonelabelCell",@"Type":@"0"},
                    @(1):@{@"NameCell":@"mfivelabelCell"}},
             @(2):@{@"Rows":@1,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"myonelabelCell",@"Type":@"0"}},
             @(3):@{@"Rows":@7,
                    @(0):@{@"NameCell":@"mtwolabelnoCell",@"Type":@"0"}},
             @(4):@{@"Rows":@1,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"mtuikuanbtnCell"}}};
    
}

#pragma mark 1000 已完成已退款
+(NSDictionary *)overOrder{
    
    return @{@(0):@{@"Rows":@1,
                    @(0):@{@"NameCell":@"mImglabelCell"}},
             
             @(1):@{@"Rows":@2,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"myonelabelCell",@"Type":@"0"},
                    @(1):@{@"NameCell":@"mfivelabelCell"}},
             
             @(2):@{@"Rows":@1,
                    @"Header":@{@"Bool":@"Yes"},
                    
                    @(0):@{@"NameCell":@"myonelabelCell",@"Type":@"0"}},
             
             @(3):@{@"Rows":@7,
                    
                    @(0):@{@"NameCell":@"mtwolabelnoCell",@"Type":@"0"}}
             };
    
}


+(NSDictionary *)eorder{
    return @{@(0):@{@"Rows":@0,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"yanzhengCell"}
                    }};
    
}
+(NSDictionary *)forder{
    
    return @{@(0):@{@"Rows":@1,
                    @"Header":@{@"Bool":@"Yes",@"Height":@"45",@"NameCell":@"CTimeCell"},
                    @(0):@{@"NameCell":@"yanzhengnumCell"}},
             @(1):@{@"Rows":@0,
                    @"Footer":@{@"Bool":@"Yes",@"Height":@"25"},
                    @(0):@{@"NameCell":@"yanzhengCell"}}};
    
}
+(NSDictionary *)gorder{
    
    return @{@(0):@{@"Rows":@1,
                    @(0):@{@"NameCell":@"qianbaoheadCell"}},
             @(1):@{@"Rows":@0,
                    @"Header":@{@"Bool":@"Yes",@"Height":@"45",@"NameCell":@"CTimeCell"},
                    @"Footer":@{@"Bool":@"Yes",@"Height":@"25"},
                    @(0):@{@"NameCell":@"qianbaoCell"}}};
}
+(NSDictionary *)horder{
    
    return @{@(0):@{@"Rows":@0,
                    @"Header":@{@"Bool":@"Yes"},
                    @"Footer":@{@"Bool":@"Yes",@"Height":@"25"},
                    @(0):@{@"NameCell":@"TuanduiCell"}}};
}

+(NSDictionary *)iorder{
    return @{@(0):@{@"Rows":@0,
                    @"Header":@{@"Bool":@"Yes",@"Height":@"45",@"NameCell":@"CTimeCell"},
                    @"Footer":@{@"Bool":@"Yes",@"Height":@"25"},
                    @(0):@{@"NameCell":@"mdaodianCell"}}};

}

+(NSDictionary *)jorder{
    return @{@(0):@{@"Rows":@1,
                    @"Footer":@{@"Bool":@"Yes"},
                    @"Header":@{@"Bool":@"Yes",@"Height":@"45",@"NameCell":@"CTimeCell"},
                    @(0):@{@"NameCell":@"MoneyCell"}},
             @(1):@{@"Rows":@0,
                    @"Header":@{@"Bool":@"Yes",@"Height":@"90",@"NameCell":@"mCaiwuheadCell"},
                    @"Footer":@{@"Bool":@"Yes",@"Height":@"25"},
                    @(0):@{@"NameCell":@"mcaiwuCell"}}};
}

+(NSDictionary *)korder{
    return @{@(0):@{@"Rows":@0,
                    @"Header":@{@"Bool":@"Yes"},
                    @"Footer":@{@"Bool":@"Yes",@"Height":@"20"},
                    @(0):@{@"NameCell":@"OrderCell"}}};

}

+(NSDictionary *)kkorder{
    return @{@(0):@{@"Rows":@0,
                    @"Header":@{@"Bool":@"Yes"},
                    @"Footer":@{@"Bool":@"Yes",@"Height":@"25"},
                    @(0):@{@"NameCell":@"kkOrderCell"},
                    }};
    
}

//+(NSDictionary *)kkkorder:(NSDictionary *)dict{
//    return @{@(0):@{@"Rows":@2,
//                    @"Header":@{@"Bool":@"Yes"},
//                    @(0):@{@"NameCell":@"OrderCell",@"data":dict}
//                }};
//    
//}
+(NSDictionary *)kkkorder{
    return @{@(0):@{@"Rows":@0,
                    @"Header":@{@"Bool":@"Yes"},
                    @"Footer":@{@"Bool":@"Yes",@"Height":@"25"},
                    @(0):@{@"NameCell":@"kkkOrderCell"}
                }};

}


+(NSDictionary *)lorder{
   return  @{@(0):@{@"Rows":@4,
                    @(0):@{@"NameCell":@"AdressCell"}}};
    
}

//+(NSDictionary *)morder{
//    
//    return @{@(0):@{@"Rows":@(1),
//                    @"Header":@{@"Bool":@"Yes"},
//                    @(0):@{@"NameCell":@"HeadInfoCell"}},
//             @(1):@{@"Rows":@(1),
//                    @"Header":@{@"Bool":@"Yes"},
//                    @(0):@{@"NameCell":@"NamesCell"}},
//             @(2):@{@"Rows":@(2),
//                    @"Header":@{@"Bool":@"Yes"},
//                    @(0):@{@"NameCell":@"NamesCell"}},
//             @(3):@{@"Rows":@(1),
//                   @"Header":@{@"Bool":@"Yes"},
//                   @(0):@{@"NameCell":@"NamesCell"}},
//             @(4):@{@"Rows":@(1),
//                    @"Header":@{@"Bool":@"Yes"},
//                    @(0):@{@"NameCell":@"SelendBtnCell",@"data":@"退出登录"}}};
//}

//+(NSDictionary *)norder{
//    
//    return @{@(0):@{@"left":@"昵称",@"right":@"取个好听的名字"},
//             @(1):@{@"left":@"卡号",@"right":@"1234566456",@"Push":@"Yes"},
//             @(2):@{@"left":@"手机号码",@"right":@"15168220912",@"Push":@"Yes"},
//             @(3):@{@"left":@"性别",@"right":@"选择性别"},
//             @(4):@{@"left":@"出生日期",@"right":@"选择出生日期"}};
//}

+(NSDictionary *)oorder{
    
    return @{@(0):@{@"Rows":@(2),
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"PayPassCell",@"data":@"请确认支付密码"},
                    @(1):@{@"NameCell":@"SelendBtnCell",@"data":@"确认修改"}}};
    
}

+(NSDictionary *)porder{
    
    return @{@(0):@{@"Rows":@(2),
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"PayPassCell",@"data":@"请设置支付密码"},
                    @(1):@{@"NameCell":@"SelendBtnCell",@"data":@"确认修改"}}};
    
}

+(NSDictionary *)qorder{
    
    return @{@(0):@{@"Rows":@(3),
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"CodeCell",@"data":@"输入绑定手机号码"},
                    @(1):@{@"NameCell":@"TFViewCell",@"data":@"请输入验证码"},
                    @(2):@{@"NameCell":@"SelendBtnCell",@"data":@"确认修改"}}};
}

+(NSDictionary *)rorder{
    
    return @{@(0):@{@"Rows":@(3),
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"CodeCell",@"data":@"输入绑定手机号码"},
                    @(1):@{@"NameCell":@"TFViewCell",@"data":@"请输入验证码"},
                    @(2):@{@"NameCell":@"SelendBtnCell",@"data":@"下一步"}}};
    
}

+(NSDictionary *)sorder{
    return @{@(0):@{@"Rows":@(4),
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"TFViewCell",@"data":@{@(0):@"请输入旧密码",
                                                               @(1):@"请输入新密码",
                                                               @(2):@"请输入新密码确认"}},
                    @(3):@{@"NameCell":@"SelendBtnCell",@"data":@"确认修改"}}};
    
}

+(NSDictionary *)torder{
    
    return @{@(0):@{@"Rows":@(10),
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"NewsSystemCell"}}};
    
}

+(NSDictionary *)uorder{
    return @{@(0):@{@"Rows":@(3),
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"NamesCell",@"Type":@"1",@"data":@{@(0):@{@"left":@"登录密码",@"right":@"修改"},
                                                                           @(1):@{@"left":@"手机号码",@"right":@"修改"},
                                                                           @(2):@{@"left":@"支付密码",@"right":@"未设置"}}}},
             @(1):@{@"Rows":@(1),
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"NamesCell",@"data":@{@(0):@{@"left":@"清空缓存"}}}},
             @(2):@{@"Rows":@(1),
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"SelendBtnCell",@"data":@"退出登录"}}};
    
}

+(NSDictionary *)vorder{
    return @{@(0):@{@"Rows":@4,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"mtwolabelnoCell",@"Type":@"0"}},
             @(1):@{@"Rows":@1,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"mmImglabelCell",@"Type":@"0"}},
             @(2):@{@"Rows":@1,
                    @"Header":@{@"Bool":@"Yes"},
                    @(0):@{@"NameCell":@"SelendBtnCell",@"Type":@"0",@"data":@"确认支付"}}
             };
}


@end
