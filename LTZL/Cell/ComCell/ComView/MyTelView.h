//
//  MyTelView.h
//  Shop
//
//  Created by ike on 16/12/10.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MyTelView : UIView<UICollectionViewDataSource,UICollectionViewDelegate>
@property(nonatomic,strong)void(^VerifyBtn)(long);
@property (weak, nonatomic) IBOutlet UICollectionView *myCollection;
@end
