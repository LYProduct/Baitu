//
//  TelView.h
//  Shop
//
//  Created by mc on 16/9/30.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TelView : UIView<UICollectionViewDataSource,UICollectionViewDelegate>
@property(nonatomic,strong)void(^VerifyBtn)(long);
@property (weak, nonatomic) IBOutlet UICollectionView *myCollection;
@end
