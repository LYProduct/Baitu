//
//  ClassView.m
//  FC
//
//  Created by mc on 16/9/23.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "ClassView.h"

@interface ClassView (){
    NSArray *leftarray;
    NSArray *rightarray;
    long leftselnd;
    long rightselnd;
}

@end

@implementation ClassView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.leftTableView.delegate = self;
    self.rightTableView.delegate = self;
    self.rightTableView.dataSource = self;
    self.leftTableView.dataSource = self;
    leftselnd = 1000;
    rightselnd = 1000;
    
}
-(void)lefeTableView:(NSArray *)leftArray rightTableView:(NSArray *)rightArray{
    self.tableView_W.constant = 150;
    self.rightTableView.hidden = NO;
    leftarray = [NSArray arrayWithArray:leftArray];
    [self.leftTableView reloadData];
    rightarray = [NSArray arrayWithArray:rightArray];
    [self.rightTableView reloadData];
}
-(void)lefeTableView:(NSArray *)leftArray{
    leftselnd = 1000;
    leftarray = [NSArray arrayWithArray:leftArray];
    [self.leftTableView reloadData];
}
-(void)rightTableView:(NSArray *)rightArray{
    rightarray = [NSArray arrayWithArray:rightArray];
    [self.rightTableView reloadData];
}
-(void)tableViewCount:(NSArray *)leftArray{
    leftselnd = 1000;
    self.tableView_W.constant = WIDTH_K;
    self.rightTableView.hidden = YES;
    leftarray = [NSArray arrayWithArray:leftArray];
    [self.leftTableView reloadData];
}
TableViewHeightHeader{
    return 0.0001;
}
TableViewHeightFooter{
    return 0.0001;
}
TableViewNumberSectionRows{
    if(tableView == self.leftTableView){
        return leftarray.count;
    }else{
        return rightarray.count;
    }
}

TableViewSelfCell{
    idCell_K
    OldCell initCell("OldCell")
    cell.line.hidden = NO;
    cell.count.hidden = YES;
    
    if(tableView == self.leftTableView){
        cell.name.text = leftarray[indexPath.row];
        cell.backgroundColor = [UIColor whiteColor];
        if(leftselnd == indexPath.row){
            cell.name.textColor = ColorBlue_k;
            cell.backgroundColor = COLORPCT_K(235.0, 234.0, 241.0,1);
        }
    }else if(tableView == self.rightTableView){
        cell.backgroundColor = COLORPCT_K(235.0, 234.0, 241.0,1);
        cell.name.text = rightarray[indexPath.row];
        cell.name.textColor = [UIColor redColor];
        cell.rightLabel.hidden = NO;
    }
    return cell;
}
TableViewCellHeightRow{
    UITableViewCell *cell = (UITableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}
TableViewSelectSectionRow{
    if(self.rightTableView.hidden == YES){
        leftselnd = indexPath.row;
        leftselnd = 1000;
        rightselnd = 1000;
    }else{
        if(tableView == self.rightTableView){
             rightselnd = indexPath.row;
            leftselnd = 1000;
            rightselnd = 1000;
        }else{
            leftselnd = indexPath.row;
        }
    }
    [_leftTableView reloadData];
    self.idCell(leftselnd,rightselnd);
}
@end
