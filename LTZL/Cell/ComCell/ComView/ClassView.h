//
//  ClassView.h
//  FC
//
//  Created by mc on 16/9/23.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClassView : UIView<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *rightTableView;
@property (weak, nonatomic) IBOutlet UITableView *leftTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableView_W;

@property (nonatomic,copy)void(^idCell)(long leftSlend,long rightSlend);

-(void)tableViewCount:(NSArray *)leftArray;
-(void)lefeTableView:(NSArray *)leftArray rightTableView:(NSArray *)rightArray;
-(void)lefeTableView:(NSArray *)leftArray;
-(void)rightTableView:(NSArray *)rightArray;
@end
