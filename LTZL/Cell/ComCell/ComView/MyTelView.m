
//
//  TelView.m
//  Shop
//
//  Created by mc on 16/9/30.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "MyTelView.h"
#import "MyNumberCell.h"
#import "MyVerifyCell.h"

@interface MyTelView () {
    NSMutableArray *AllColorArr;
}

@end

@implementation MyTelView

-(void)awakeFromNib{
    [super awakeFromNib];
    AllColorArr = [NSMutableArray array];
    self.myCollection.delegate = self;
    self.myCollection.dataSource = self;
    [self.myCollection registerNib:[UINib nibWithNibName:@"MyNumberCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"cellID"];
    [self.myCollection registerNib:[UINib nibWithNibName:@"MyVerifyCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"cellTop"];
}
//每个区有几个item
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 11;
}
//重用cell
#pragma mark CollectionSelfCell
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 10){
        MyVerifyCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellTop" forIndexPath:indexPath];
        cell.selendBtn.layer.cornerRadius = ((WIDTH_K-24-72)/3-24)/2;
        [cell.selendBtn addTarget:self action:@selector(selendBtn) forControlEvents:(UIControlEventTouchUpInside)];
        return cell;
    }else{
        MyNumberCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellID" forIndexPath:indexPath];
        cell.number.layer.cornerRadius = ((WIDTH_K-24-72)/3 - 24)/2;
        NSLog(@"%f-----is a %f", cell.number.width, ((WIDTH_K-24-72)/3 - 24)/2);
        cell.backgroundColor = [UIColor whiteColor];
        if (indexPath.row == 9) {
            cell.number.text = @"0";
        }else{
            cell.number.text = [NSString stringWithFormat:@"%d",(int)indexPath.row + 1];
        }
        return cell;
    }
    
}
-(void)selendBtn{
    self.VerifyBtn(100);
}
//-(void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath{
//    if (indexPath.row != 10) {
//        NumberCell *cell = (NumberCell *)[self.myCollection cellForItemAtIndexPath:indexPath];
//        cell.number.backgroundColor = ColorBlue_k;
//        cell.number.textColor = [UIColor whiteColor];
//    }
//}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row != 10) {
        MyNumberCell *cell = (MyNumberCell *)[self.myCollection cellForItemAtIndexPath:indexPath];
        cell.number.backgroundColor = ColorBlue_k;
        [AllColorArr addObject:cell.number];
        [self performSelector:@selector(changeColor) withObject:cell.number afterDelay:0.1f];
        cell.number.textColor = [UIColor blackColor];
    }
    if (indexPath.row == 9) {
        self.VerifyBtn(0);
    }else{
        self.VerifyBtn(indexPath.row + 1);
    }
}

- (void)changeColor {
    for (int i = 0; i < AllColorArr.count; i++) {
        UILabel *label = AllColorArr[i];
        label.backgroundColor = [UIColor whiteColor];
    }
    
}

//定义每个UICollectionViewCell 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 10) {
        return CGSizeMake((WIDTH_K-24-72)/3 * 2+36,(WIDTH_K-24-72)/3-24);
    }
    else{
        return CGSizeMake((WIDTH_K-24-72)/3, (WIDTH_K-24-72)/3);
    }
}

//定义每个Section 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    
    return UIEdgeInsetsMake(12, 12, 12, 12);//分别为上、左、下、右
}
//每个item之间的间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
//每个section中不同的行之间的行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
@end
