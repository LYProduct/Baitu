
//
//  TelView.m
//  Shop
//
//  Created by mc on 16/9/30.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "TelView.h"

@implementation TelView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.myCollection.delegate = self;
    self.myCollection.dataSource = self;
    [self.myCollection registerNib:[UINib nibWithNibName:@"NumberCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"cellID"];
    [self.myCollection registerNib:[UINib nibWithNibName:@"VerifyCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"cellTop"];
}
//每个区有几个item
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 11;
}
//重用cell
#pragma mark CollectionSelfCell
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 10){
        VerifyCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellTop" forIndexPath:indexPath];
        [cell.selendBtn addTarget:self action:@selector(selendBtn) forControlEvents:(UIControlEventTouchUpInside)];
        return cell;
    }else{
        NumberCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellID" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
        if (indexPath.row == 9) {
            cell.number.text = @"0";
        }else{
            cell.number.text = [NSString stringWithFormat:@"%d",(int)indexPath.row + 1];
        }
        return cell;
    }
    
}
-(void)selendBtn{
    self.VerifyBtn(100);
}
-(void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row != 10) {
        NumberCell *cell = (NumberCell *)[self.myCollection cellForItemAtIndexPath:indexPath];
//        cell.number.backgroundColor = ColorBlue_k;
        cell.number.textColor = [UIColor whiteColor];
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row != 10) {
        NumberCell *cell = (NumberCell *)[self.myCollection cellForItemAtIndexPath:indexPath];
        cell.number.backgroundColor = [UIColor whiteColor];
        cell.number.textColor = [UIColor blackColor];
    }
    if (indexPath.row == 9) {
        self.VerifyBtn(0);
    }else{
        self.VerifyBtn(indexPath.row + 1);
    }
}
//定义每个UICollectionViewCell 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 10) {
        return CGSizeMake(WIDTH_K /3 * 2,HEIGHT_K / 3 * 2 / 4.5);
    }
    else{
        return CGSizeMake(WIDTH_K/3 -1, HEIGHT_K / 3 * 2 / 4.5);
    }
}

//定义每个Section 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);//分别为上、左、下、右
}
//每个item之间的间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
//每个section中不同的行之间的行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
@end
