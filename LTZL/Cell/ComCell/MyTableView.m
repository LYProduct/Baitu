//
//  MyTableView.m
//  FC
//
//  Created by mc on 16/9/20.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "MyTableView.h"
#import "LayoutCell.h"
//#import "UITableViewCell+data.h"

@implementation MyTableView
-(void)RetuenidCellText:(ReturnCellText)block{
    self.CellText = block;
}
-(void)RetuenidCellBtnClick:(ReturnCellBtn)block{
    self.btnTag = block;
}
//更改指定header的布局
-(void)showSectionHeader:(long)section uploadHeight:(id)data{
    NSMutableDictionary *headerDict = [NSMutableDictionary dictionaryWithDictionary:self.showSectionRow[@(section)]];
    if (data == nil) {
        [headerDict removeObjectForKey:@"Header"];
    }else{
        [headerDict setObject:data forKey:@"Header"];
    }
    [self.showSectionRow setObject:headerDict forKey:@(section)];
}

//更改指定Footer的布局
-(void)showSectionFooter:(long)section uploadHeight:(id)data{
    NSMutableDictionary *headerDict = [NSMutableDictionary dictionaryWithDictionary:self.showSectionRow[@(section)]];
    if (data == nil){
        [headerDict removeObjectForKey:@"Footer"];
    }else{
        [headerDict setObject:data forKey:@"Footer"];
    }
    [self.showSectionRow setObject:headerDict forKey:@(section)];
}
/**
 多重布局某一个区
 */
-(void)showLayoutSection:(long)section Type:(int)type Cells:(NSArray *)NameCells{
    NSMutableDictionary *headerDict = [NSMutableDictionary dictionaryWithDictionary:self.showSectionRow[@(section)]];
    [headerDict setObject:@([NameCells count]) forKey:@"Rows"];
    [headerDict setObject:@(type) forKey:@"Type"];
    for (int i = 0;  i < NameCells.count; i ++) {
        [headerDict setObject:NameCells[i] forKey:@(i)];
    }
    [self.showSectionRow setObject:headerDict forKey:@(section)];
}
-(void)RetuenidRemoveCell:(ReturnRemoveCell)block{
    self.RmoveCell = block;
}

-(void)RetuenidSelendCell:(ReturnSelendCell)block{
    self.selendCell = block;
}
//区头，区尾点击事件
-(void)RetuenidSelendHeaderFooter:(ReturnSelendHeaderFooter)block{
    self.selendHeaderFooter = block;
}
-(void)showCell:(NSDictionary *)dictCell title:(NSString *)title{
    self.titleString = title;
    self.showSectionRow = [[NSMutableDictionary alloc]initWithDictionary:dictCell];
}
-(void)RetuenidScroll:(ReturnTableViewScroll)block{
    self.scroll = block;
}
-(void)LayoutshowCell:(long)section Rows:(long)rows StringCell:(NSString *)cellString{
    NSMutableDictionary *sectionDict = [NSMutableDictionary dictionaryWithDictionary:self.showSectionRow[@(section)]];
    
    int row = [sectionDict[@"Rows"] intValue];//总数为6行
    if (row - rows != 0){
        [sectionDict setObject:@(row + (row - rows)) forKey:@"Rows"];
        NSLog(@"%ld-------%d",rows,row);
        for (int i = (unsigned int)rows; i < row; i ++){
            [sectionDict setObject:@{@"NameCell":sectionDict[@(i)][@"NameCell"]} forKey:@(i + 1)];
        }
    }
    [sectionDict setObject:@{@"NameCell":cellString} forKey:@(rows)];
}
//更改指定rowsNumber的值
-(void)showRowsNumber:(long)rows uploadDate:(long)date{
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.showSectionRow[@(rows)]];
    [dict setObject:@(date) forKey:@"Rows"];
    [self.showSectionRow setObject:dict forKey:@(rows)];
}
//动态改变Rows
-(void)showSectionTypeNameCellDict:(long)section{
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.showSectionRow[@(section)]];
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:dict[@(0)]];
    if ([dic[@"Type"]intValue] == 1) {
        [dic removeObjectForKey:@"Type"];
    }else{
        [dic setObject:@"1" forKey:@"Type"];
    }
    [dict setObject:dic forKey:@(0)];
    [self.showSectionRow setObject:dict forKey:@(section)];
}
//更改指定header的值
-(void)showSectionHeader:(long)section uploadDate:(id)date{
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.showSectionRow[@(section)]];
    NSMutableDictionary *HeaderDic = [NSMutableDictionary dictionaryWithDictionary:dict[@"Header"]];
    [HeaderDic setObject:date forKey:@"data"];
    [dict setObject:HeaderDic forKey:@"Header"];
    [self.showSectionRow setObject:dict forKey:@(section)];
}
//更改指定footer的值
-(void)showSectionFooter:(long)section uploadDate:(id)date{
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.showSectionRow[@(section)]];
    NSMutableDictionary *HeaderDic = [NSMutableDictionary dictionaryWithDictionary:dict[@"Footer"]];
    [HeaderDic setObject:date forKey:@"data"];
    [dict setObject:HeaderDic forKey:@"Footer"];
    [self.showSectionRow setObject:dict forKey:@(section)];
}
//更改Cell的值
-(void)showSection:(long)section uploadRows:(long)row uploadDate:(id)date{
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.showSectionRow[@(section)]];
    NSMutableDictionary *rowDict = [NSMutableDictionary dictionaryWithDictionary:dict[@(row)]];
    [rowDict setObject:date forKey:@"data"];
    [dict setObject:rowDict forKey:@(row)];
    [self.showSectionRow setObject:dict forKey:@(section)];
}
-(void)showSection:(long)section uploadRows:(long)row layouatCell:(id)date{
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.showSectionRow[@(section)]];
    NSMutableDictionary *rowDict = [NSMutableDictionary dictionaryWithDictionary:dict[@(row)]];
    [rowDict setObject:date forKey:@"NameCell"];
    [dict setObject:rowDict forKey:@(row)];
    [self.showSectionRow setObject:dict forKey:@(section)];
}
-(void)awakeFromNib{
    [super awakeFromNib];
    self.delegate = self;
    self.dataSource = self;
    self.showSectionRow = [[NSMutableDictionary alloc]initWithCapacity:0];
}
-(void)SingleTap{
    [self endEditing:YES];
}
-(void)showDate:(id)data{
    [self.showSectionRow setObject:data forKey:@"data"];
}
TableViewSelfCell{//@[@"img06", @"img07", @"img08",@"img09"]
    idCell_K;
    NSString *cellString = [NSString stringWithFormat:@"%@",self.showSectionRow[@(indexPath.section)][@(indexPath.row)][@"NameCell"]];
    if([cellString rangeOfString:@"Cell"].location == NSNotFound){
        if([self.showSectionRow[@"Type"] rangeOfString:@"null"].location != NSNotFound){
            cellString = [NSString stringWithFormat:@"%@",self.showSectionRow[@(indexPath.section)][@(0)][@"NameCell"]];
        }else{
            cellString = [NSString stringWithFormat:@"%@",self.showSectionRow[@(indexPath.section)][@([self.showSectionRow[@"Type"] intValue])][@"NameCell"]];
        }
    }
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idCell];
    if(!cell){
        cell = [[[NSBundle mainBundle]loadNibNamed:cellString owner:nil options:nil]objectAtIndex:0];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    id data;
    //showSectionRow 属性就是写在 里面的 为了布局 对于头尾视图不用复用
    if(![[self.showSectionRow allKeys]containsObject:@"data"]){
        if([[self.showSectionRow[@(indexPath.section)][@(indexPath.row)] allKeys] containsObject:@"data"]){
            data =self.showSectionRow[@(indexPath.section)][@(indexPath.row)][@"data"];
        }else{
            data = self.showSectionRow[@(indexPath.section)][@(0)][@"data"];
        }
    }else{
        data = self.showSectionRow[@"data"];
    }
    
//    [cell scrollViewShowImgs:data selendIndex:indexPath titleString:self.titleString];
    
    //根据数据进行再次布局采用此方法，该方法可以写在cell中
    [cell scrollViewShowImgs:data selendIndex:indexPath tableView:tableView titleString:self.titleString];
    
    [cell RetuenBtnClick:^(NSInteger btnTag){
        self.btnTag(indexPath,btnTag,data);
    }];
    [cell RetuenTextClick:^(NSInteger btnTag, id dataa) {
        self.CellText(indexPath,btnTag,dataa);
    }];
    return cell;
}
TableViewNumberSection{
    return [[self.showSectionRow allKeys] count];
}
TableViewNumberSectionRows{
    if([self.showSectionRow[@(section)][@"Type"] rangeOfString:@"null"].location != NSNotFound){
        if([self.showSectionRow[@(section)][@(0)][@"Type"] rangeOfString:@"null"].location != NSNotFound){
            if([self.showSectionRow[@(section)][@(0)][@"data"] isKindOfClass:[NSDictionary class]]){
                return 1;
            }else if([self.showSectionRow[@(section)][@(0)][@"data"] isKindOfClass:[NSArray class]]){
                NSArray *array = [NSArray arrayWithArray:self.showSectionRow[@(section)][@(0)][@"data"]];
                return [array count];
            }else{
                return [self.showSectionRow[@(section)][@"Rows"] intValue];
            }
        }else{
            return [self.showSectionRow[@(section)][@"Rows"] intValue];
        }
    }else{
        return [self.showSectionRow[@(section)][@"Rows"] intValue];
    }
}
TableViewHeightHeader{
    NSString *string = self.showSectionRow[@(section)][@"Header"][@"Bool"];
    if ([string isEqualToString:@"Yes"]){
        if([self.showSectionRow[@(section)][@"Header"][@"Height"] intValue] >= 5){
            return [self.showSectionRow[@(section)][@"Header"][@"Height"] floatValue];
        }else{
            return 10;
        }
    }else{
        if([string isEqualToString:@"Yes"]){
            return 10;
        }else{
            return 0.0001;
        }
    }
}
TableViewHeightFooter{
    NSString *string = self.showSectionRow[@(section)][@"Footer"][@"Bool"];
    if ([string isEqualToString:@"Yes"]){
        if([self.showSectionRow[@(section)][@"Footer"][@"Height"] intValue] >= 5){
            return [self.showSectionRow[@(section)][@"Footer"][@"Height"] floatValue];
        }else{
            return 10;
        }
    }else{
        return 0.0001;
    }
}
TableViewCellHeightRow{
    TableViewCell *cell = (TableViewCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"idCell------>%ld---------->%ld",(long)indexPath.section,indexPath.row);
    if(self.SelendIdCell == 0){
        if(self.allDate == 0){
            self.selendCell(indexPath,self.showSectionRow[@(indexPath.section)][@(0)][@"data"]);
        }else{
            self.selendCell(indexPath,self.showSectionRow);
        }
        
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    NSString *string = self.showSectionRow[@(section)][@"Footer"][@"Bool"];
    NSString *nameCell = self.showSectionRow[@(section)][@"Footer"][@"NameCell"];
    if ([string isEqualToString:@"Yes"]){
        if([self.showSectionRow[@(section)][@"Footer"][@"Height"] intValue] >= 30){
            TableViewCell *cell = [[[NSBundle mainBundle]loadNibNamed:nameCell owner:nil options:nil]objectAtIndex:0];
            [cell secrollShow:self.showSectionRow[@(section)][@"Footer"][@"data"] Footer:section title:self.titleString];
            cell.tag = section;
            if(self.headerFooter == 1){
                UITapGestureRecognizer* singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(footerSingleTap:)];
                [cell addGestureRecognizer:singleTap];
            }
            [cell RetuenBtnClick:^(NSInteger btnTag){
                self.btnTag([NSIndexPath indexPathForRow:200 inSection:section],btnTag,_showSectionRow);
            }];
            return cell;
        }else{
            UIView *view = [[UIView alloc]init];
            view.backgroundColor = [UIColor clearColor];
            return view;
        }
    }else{
        UIView *view = [[UIView alloc]init];
        view.backgroundColor = [UIColor clearColor];
        return view;
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    NSString *string = self.showSectionRow[@(section)][@"Header"][@"Bool"];
    NSString *nameCell = self.showSectionRow[@(section)][@"Header"][@"NameCell"];
    if ([string isEqualToString:@"Yes"]){
        if([self.showSectionRow[@(section)][@"Header"][@"Height"] intValue] >= 30){
            TableViewCell *cell = [[[NSBundle mainBundle]loadNibNamed:nameCell owner:nil options:nil]objectAtIndex:0];
            [cell secrollShow:self.showSectionRow[@(section)][@"Header"][@"data"] Header:section title:self.titleString];
            cell.tag = section;
            if(self.headerFooter == 1){
                UITapGestureRecognizer* singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headerSingleTap:)];
                [cell addGestureRecognizer:singleTap];
            }
            [cell RetuenBtnClick:^(NSInteger btnTag){
                self.btnTag([NSIndexPath indexPathForRow:100 inSection:section],btnTag,_showSectionRow);
            }];
            return cell;
        }else{
            return nil;
        }
    }else{
        return nil;
    }
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.RemoveCell != 0) {
        self.RmoveCell(indexPath);
    }
}
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCellEditingStyle result = UITableViewCellEditingStyleNone;//默认没有编辑风格
    if (self.RemoveCell != 0) {
        result = UITableViewCellEditingStyleDelete;
    }
    return result;
}
//设置删除按钮提示的文字
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(self.RemoveCell != 0){
        return @"删除";
    }else{
        return nil;
    }
}
-(void)footerSingleTap:(UITapGestureRecognizer *)sender{
    self.selendHeaderFooter(0,[sender view].tag);
}
-(void)headerSingleTap:(UITapGestureRecognizer *)sender{
    self.selendHeaderFooter([sender view].tag,0);
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (self.scrollSign > 0) {
        self.scroll(scrollView.contentOffset.y);
    }
}
@end




@implementation UITableViewCell (data)
-(void)awakeFromNib{
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}


@end

@implementation TableViewCell
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}
-(void)RetuenBtnClick:(ReturnBtnClick)block{
    self.btnClickTag = block;
}
-(void)RetuenTextClick:(ReturnTextClick)block{
    self.TextTag = block;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (void)setFrame:(CGRect)frame {
    if (self.ButtomHeight != 0) {
        self.separatorInset = UIEdgeInsetsMake(0, self.frame.size.width, 0, 0);
    }else{
//        self.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        self.separatorInset = UIEdgeInsetsMake(0, self.lw, 0, self.rw);
    }
    frame.size.height -= self.ButtomHeight;    // 减掉的值就是分隔线的高度
    [super setFrame:frame];
}
-(void)tableViewCellSize:(CGFloat)height{
    self.height = height;
}

//Cell共类方法，调用方法[LayoutCell sharedLayout] 进行再次布局数据
-(void)scrollViewShowImgs:(id)date selendIndex:(NSIndexPath *)indexPath tableView:(UITableView *)tableView titleString:(NSString *)title{
    NSMutableArray *labelArr = [NSMutableArray arrayWithCapacity:0];
    for (int i = 0; i  < [self.contentView subviews].count; i ++) {
        if ([[self.contentView subviews][i] isKindOfClass:[UILabel class]]) {
            [labelArr addObject:[self.contentView subviews][i]];
        }
    }
    NSMutableArray *imgArr = [NSMutableArray arrayWithCapacity:0];
    for (int i = 0; i  < [self.contentView subviews].count; i ++) {
        if ([[self.contentView subviews][i] isKindOfClass:[UIImageView class]]) {
            [imgArr addObject:[self.contentView subviews][i]];
        }
    }
    NSMutableArray *btnArr = [NSMutableArray arrayWithCapacity:0];
    for (int i = 0; i  < [self.contentView subviews].count; i ++) {
        if ([[self.contentView subviews][i] isKindOfClass:[UIButton class]]) {
            [btnArr addObject:[self.contentView subviews][i]];
        }
    }
    NSMutableArray *viewArr = [NSMutableArray arrayWithCapacity:0];
    for (int i = 0; i  < [self.contentView subviews].count; i ++) {
        if ([[self.contentView subviews][i] isKindOfClass:[UIImageView class]]) {
            [viewArr addObject:[self.contentView subviews][i]];
        }
    }
    NSMutableArray *tfArr = [NSMutableArray arrayWithCapacity:0];
    for (int i = 0; i  < [self.contentView subviews].count; i ++) {
        if ([[self.contentView subviews][i] isKindOfClass:[UITextField class]]) {
            [tfArr addObject:[self.contentView subviews][i]];
        }
    }
    
    [[LayoutCell sharedLayout] showCell:date tableViewCell:self label:labelArr btns:btnArr TFViews:tfArr imgView:imgArr showView:viewArr title:title indexPath:indexPath];
}
-(void)secrollShow:(id)date Header:(NSInteger)header title:(NSString *)title{
    NSMutableArray *labelArr = [NSMutableArray arrayWithCapacity:0];
    for (int i = 0; i  < [self.contentView subviews].count; i ++) {
        if ([[self.contentView subviews][i] isKindOfClass:[UILabel class]]) {
            [labelArr addObject:[self.contentView subviews][i]];
        }
    }
    NSMutableArray *imgArr = [NSMutableArray arrayWithCapacity:0];
    for (int i = 0; i  < [self.contentView subviews].count; i ++) {
        if ([[self.contentView subviews][i] isKindOfClass:[UIImageView class]]) {
            [imgArr addObject:[self.contentView subviews][i]];
        }
    }
    NSMutableArray *btnArr = [NSMutableArray arrayWithCapacity:0];
    for (int i = 0; i  < [self.contentView subviews].count; i ++) {
        if ([[self.contentView subviews][i] isKindOfClass:[UIButton class]]) {
            [btnArr addObject:[self.contentView subviews][i]];
        }
    }
    NSMutableArray *viewArr = [NSMutableArray arrayWithCapacity:0];
    for (int i = 0; i  < [self.contentView subviews].count; i ++) {
        if ([[self.contentView subviews][i] isKindOfClass:[UIImageView class]]) {
            [viewArr addObject:[self.contentView subviews][i]];
        }
    }
    NSMutableArray *tfArr = [NSMutableArray arrayWithCapacity:0];
    for (int i = 0; i  < [self.contentView subviews].count; i ++) {
        if ([[self.contentView subviews][i] isKindOfClass:[UITextField class]]) {
            [tfArr addObject:[self.contentView subviews][i]];
        }
    }
    [[LayoutCell sharedLayout] showViewCell:date tableViewCell:self label:labelArr btns:btnArr TFViews:tfArr imgView:imgArr showView:viewArr title:title header:header];
}
-(void)secrollShow:(id)date Footer:(NSInteger)footer title:(NSString *)title{
    NSMutableArray *labelArr = [NSMutableArray arrayWithCapacity:0];
    for (int i = 0; i  < [self.contentView subviews].count; i ++) {
        if ([[self.contentView subviews][i] isKindOfClass:[UILabel class]]) {
            [labelArr addObject:[self.contentView subviews][i]];
        }
    }
    NSMutableArray *imgArr = [NSMutableArray arrayWithCapacity:0];
    for (int i = 0; i  < [self.contentView subviews].count; i ++) {
        if ([[self.contentView subviews][i] isKindOfClass:[UIImageView class]]) {
            [imgArr addObject:[self.contentView subviews][i]];
        }
    }
    NSMutableArray *btnArr = [NSMutableArray arrayWithCapacity:0];
    for (int i = 0; i  < [self.contentView subviews].count; i ++) {
        if ([[self.contentView subviews][i] isKindOfClass:[UIImageView class]]) {
            [btnArr addObject:[self.contentView subviews][i]];
        }
    }
    NSMutableArray *viewArr = [NSMutableArray arrayWithCapacity:0];
    for (int i = 0; i  < [self.contentView subviews].count; i ++) {
        if ([[self.contentView subviews][i] isKindOfClass:[UIImageView class]]) {
            [viewArr addObject:[self.contentView subviews][i]];
        }
    }
    NSMutableArray *tfArr = [NSMutableArray arrayWithCapacity:0];
    for (int i = 0; i  < [self.contentView subviews].count; i ++) {
        if ([[self.contentView subviews][i] isKindOfClass:[UITextField class]]) {
            [tfArr addObject:[self.contentView subviews][i]];
        }
    }
    [[LayoutCell sharedLayout] showViewCell:date tableViewCell:self label:labelArr btns:btnArr TFViews:tfArr imgView:imgArr showView:viewArr title:title footer:footer];
}
@end

