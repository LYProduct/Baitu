//
//  HomeCategoryCVCell.h
//  意通
//
//  Created by ike on 16/8/12.
//  Copyright © 2016年 MIFAN. All rights reserved.
//

#import <UIKit/UIKit.h>

//首页分类collectionViewCell

@interface HomeCategoryCVCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *Img;

@property (weak, nonatomic) IBOutlet UILabel *label;

@end
