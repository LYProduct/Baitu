//
//  SelendTwoCell.h
//  Shop
//
//  Created by mc on 16/9/30.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelendTwoCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;

@property (weak, nonatomic) IBOutlet UILabel *rightLabel;

@end
