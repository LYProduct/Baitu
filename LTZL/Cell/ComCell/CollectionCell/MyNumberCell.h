//
//  MyNumberCell.h
//  Shop
//
//  Created by ike on 16/12/10.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyNumberCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *number;
@end
