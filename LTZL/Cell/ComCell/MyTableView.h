//
//  MyTableView.h
//  FC
//
//  Created by mc on 16/9/26.
//  Copyright © 2016年 mc. All rights reserved.
//
/**
 ike:
 更新日期 2016.11.28
 */

#import <UIKit/UIKit.h>

typedef void(^ReturnSelendCell)(NSIndexPath *indexPath,id data);
typedef void(^ReturnSelendHeaderFooter)(long header,long footer);
typedef void(^ReturnTableViewScroll)(CGFloat scroll);
typedef void(^ReturnRemoveCell)(NSIndexPath *indexPath);
typedef void(^ReturnCellBtn)(NSIndexPath *indexPath,NSInteger btnTag,id data);
typedef void(^ReturnCellText)(NSIndexPath *indexPath,NSInteger tag,id data);
@interface MyTableView : UITableView<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic, copy)ReturnSelendCell selendCell;
@property(nonatomic, copy)ReturnSelendHeaderFooter selendHeaderFooter;
@property(nonatomic, copy)ReturnTableViewScroll scroll;
@property(nonatomic, copy)ReturnRemoveCell RmoveCell;
@property(nonatomic, copy)ReturnCellBtn btnTag;
@property(nonatomic, copy)ReturnCellText CellText;
#pragma mark 操作处理
/**
 文本框传输
 */
-(void)RetuenidCellText:(ReturnCellText)block;
/**
 Cell上的按钮点击事件,如果是header(row:100) footer(200)
 */
-(void)RetuenidCellBtnClick:(ReturnCellBtn)block;

/**
 删除
 默认是 0
 删除是 1
 */
@property int  RemoveCell;
/**
 删除Cell事件
 */
-(void)RetuenidRemoveCell:(ReturnRemoveCell)block;
/**
 滑动
 默认是 0
 监听是 1
 */
@property int  scrollSign;
/**
 监听滑动
 */
-(void)RetuenidScroll:(ReturnTableViewScroll)block;
/**
 监听点击Cell 事件
 默认是 1
 监听是 0
 */
@property int SelendIdCell;
/**
 Cell点击事件
 */
-(void)RetuenidSelendCell:(ReturnSelendCell)block;

/**
 点击区头,区尾
 默认是 0
 监听是 1
 */
@property int  headerFooter;
/**
 区头，区尾点击事件
 */
-(void)RetuenidSelendHeaderFooter:(ReturnSelendHeaderFooter)block;
/**
 表示符
 */
@property(nonatomic,strong)NSString *titleString;
/**
 返回所有数据
 否 0
 是 1
 */
@property int  allDate;


//把布局的字典赋值进去，就可以显示了。该字典在cellForRowAt等一系列布局方法中使用，取出里面的值。
//在一系列更新的接口中布局传入用的字典实际就是这个
@property(nonatomic,strong)NSMutableDictionary *showSectionRow;


-(void)LayoutshowCell:(long)section Rows:(long)rows StringCell:(NSString *)cellString;

#pragma mark 布局
-(void)showCell:(NSDictionary *)dictCell title:(NSString *)title;
/**
 更改一个区的Rows
 */
-(void)showRowsNumber:(long)rows uploadDate:(long)date;

/**
 更改指定HeaderView
 */
-(void)showSectionHeader:(long)section uploadHeight:(id)data;
/**
 更改指定FooterView
 */
-(void)showSectionFooter:(long)section uploadHeight:(id)data;

/**
 更改Cell布局
 */
-(void)showSection:(long)section uploadRows:(long)row layouatCell:(id)date;
/**
 根据布局动态改变Rows 改变Type 默认取第一个item 例如：1变多
 */
-(void)showSectionTypeNameCellDict:(long)section;

/**
 多重布局某一个区 Type(重复布局指定item)
 */
-(void)showLayoutSection:(long)section Type:(int)type Cells:(NSArray *)NameCells;
#pragma mark 数据
/**
 更改指定footer的值
 */
-(void)showSectionFooter:(long)section uploadDate:(id)date;
/**
 更改指定header的值
 */
-(void)showSectionHeader:(long)section uploadDate:(id)date;
/**
 更改Cell的值 “data”
 */
-(void)showSection:(long)section uploadRows:(long)row uploadDate:(id)date;

/**
 全部数据 “data”
*/
-(void)showDate:(id)data;
@end


@interface UITableViewCell (data)

@end

//在Cell总类中的block。用于btn，TextField传值。在Cell中写入Block作为属性，将值传到controller中
typedef void(^ReturnBtnClick)(NSInteger btnTag);
typedef void(^ReturnTextClick)(NSInteger btnTag,id data);
@interface TableViewCell : UITableViewCell
@property CGFloat cheight;
/**
 底部的高
 */
@property int ButtomHeight;
/**
 line left.width
 */
@property CGFloat lw;
/**
 line left.right
 */
@property CGFloat rw;

@property(nonatomic)ReturnBtnClick btnClickTag;
@property(nonatomic)ReturnTextClick TextTag;
/**
 按钮点击接收
 */
-(void)RetuenBtnClick:(ReturnBtnClick)block;
/**
 当前Cell的高
 */
-(void)tableViewCellSize:(CGFloat)height;
/**
 文本框结束插入此方法
 */
-(void)RetuenTextClick:(ReturnTextClick)block;

//cell共用方法，可以在里面拿到cell中的控件，进行例如textField赋值之类的操作这里是数据的操作
//底层写在了CellForRowAtIndex中，进行赋值
//NSObject（改变数据类）LayoutCell的showCell---方法写入其中，使得进入cellForRowAtIndex方法的数据方法后先进入本方法，再获取LayoutCell中的方法，进行赋值，数据的传递
-(void)scrollViewShowImgs:(id)date selendIndex:(NSIndexPath *)indexPath tableView:(UITableView *)tableView titleString:(NSString *)title;

-(void)secrollShow:(id)date Header:(NSInteger)header title:(NSString *)title;
-(void)secrollShow:(id)date Footer:(NSInteger)footer title:(NSString *)title;
@end
