//
//  FoodCell.h
//  FC
//
//  Created by mc on 16/9/21.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodCell : TableViewCell
@property (weak, nonatomic) IBOutlet UILabel *name;

@end
