//
//  HeadInfoCell.h
//  FC
//
//  Created by mc on 16/9/27.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeadInfoCell : TableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headInfo;

@end
