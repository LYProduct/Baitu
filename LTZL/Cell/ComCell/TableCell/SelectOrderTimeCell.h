//
//  SelectOrderTimeCell.h
//  IT
//
//  Created by wyp on 16/11/24.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectOrderTimeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UILabel *centerLabel;

@end
