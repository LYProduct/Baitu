//
//  NameTextViewCell.m
//  TS
//
//  Created by mc on 16/1/12.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "NameTextViewCell.h"
#import "LayoutCell.h"

@interface NameTextViewCell (){
    NSString *titles;
}

@end

@implementation NameTextViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    self.TFView.delegate = self;
}
-(void)scrollViewShowImgs:(id)date selendIndex:(NSIndexPath *)indexPath tableView:(UITableView *)tableView titleString:(NSString *)title{
    titles = title;
    if (titles.length != 0 && ![titles isEqualToString:@"注册"] && ![titles isEqualToString:@"登录"]){
        [[LayoutCell sharedLayout] showCell:date tableViewCell:self label:@[self.name] btns:@[] TFViews:@[self.TFView] imgView:@[] showView:@[] title:title indexPath:indexPath];
    }
    
    if ([title isEqualToString:@"提现填写"]) {
        if ((indexPath.section == 0 && indexPath.row == 2)||(indexPath.section == 1 && indexPath.row == 0) ){
            self.TFView.keyboardType = UIKeyboardTypeNumberPad;
        }
    }else if ([title isEqualToString:@"买单"]){
        if (indexPath.section == 0 || indexPath.section == 1) {
            self.TFView.keyboardType = UIKeyboardTypeNumberPad;
        }
    
    }
    
   
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    self.TextTag(textField.tag,textField.text);
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    NSLog(@"6");//返回BOOL值，指定是否允许文本字段结束编辑，当编辑结束，文本字段会让出第一响应者
    if (![titles isEqualToString:@"注册"] && ![titles isEqualToString:@"登录"]){
        if (textField.text.length != 0) {
           self.TextTag(textField.tag,textField.text); 
        }
        
    }
    
    return YES;
}
@end
