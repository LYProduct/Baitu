//
//  SelectDateCell.h
//  MC
//
//  Created by wyp on 16/9/21.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectDateCell : TableViewCell

@property (nonatomic, strong) void(^reloadVC)(NSString *);


@property(nonatomic,copy)NSString *begin;
@property(nonatomic,copy)NSString *end;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@end
