//
//  PayPassCell.m
//  FC
//
//  Created by mc on 16/9/26.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "PayPassCell.h"

@interface PayPassCell (){
    int pay;
}

@end

@implementation PayPassCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.TFView.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state ●
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    UILabel *label = [self.lViews viewWithTag:10 + textField.text.length];
    label.text = @"●";
    if (textField.text.length == 5){
        self.TFView.text = [NSString stringWithFormat:@"%@%@",textField.text,string];
        self.TextTag(textField.tag,[NSString stringWithFormat:@"%@%@",textField.text,string]);
        [textField endEditing:YES];
    }
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    for (int i = 0; i < 6; i ++) {
        UILabel *label =[self.lViews viewWithTag:10 + i];
        label.text = @"";
    }
    self.TFView.text = @"";
}
@end
