//
//  HomeCategoryCell.h
//  意通
//
//  Created by ike on 16/8/12.
//  Copyright © 2016年 MIFAN. All rights reserved.
//

#import <UIKit/UIKit.h>

//首页分类tableViewCell

@interface HomeCategoryCell : TableViewCell
@property(nonatomic,strong)void(^SelendBtnCell)(NSIndexPath * indexPath,long  selend);
-(void)scrollViewShowImgs:(id)date selendIndex:(NSIndexPath *)indexPath;
@end
