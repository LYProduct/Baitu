//
//  ScrollViewCell.h
//  FC
//
//  Created by mc on 16/9/20.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScrollViewCell : TableViewCell{
    NSIndexPath *index;
}

-(void)scrollViewShowImgs:(id)date selendIndex:(NSIndexPath *)indexPath tableView:(UITableView *)tableView titleString:(NSString *)title;

-(void)secrollShowHeaderFooter:(NSInteger)headerFooter title:(NSString *)title;
@end
