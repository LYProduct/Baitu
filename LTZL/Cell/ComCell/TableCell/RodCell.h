//
//  RodCell.h
//  FC
//
//  Created by mc on 16/9/21.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RodCell : TableViewCell
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;
@property (weak, nonatomic) IBOutlet UILabel *hlabel;
@property (weak, nonatomic) IBOutlet UILabel *mlabel;
@property (weak, nonatomic) IBOutlet UILabel *sLabel;
@end
