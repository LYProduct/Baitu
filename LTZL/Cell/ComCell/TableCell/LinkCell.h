//
//  LinkCell.h
//  FC
//
//  Created by mc on 16/9/21.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LinkCell : TableViewCell
@property (weak, nonatomic) IBOutlet UILabel *Label_T;
@property (weak, nonatomic) IBOutlet UILabel *label_C;
@property (weak, nonatomic) IBOutlet UIImageView *img_B;
@property (weak, nonatomic) IBOutlet UILabel *label_B;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn_T;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn_B;

@end
