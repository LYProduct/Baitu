//
//  ShopsCell.m
//  FC
//
//  Created by mc on 16/9/21.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "ShopsCell.h"

@implementation ShopsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.myCollection.delegate = self;
    self.myCollection.dataSource = self;
    [self.myCollection registerNib:[UINib nibWithNibName:@"ImgPriceCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"cellID"];
}//SimgCell
-(void)scrollViewShowImgs:(id)date selendIndex:(NSIndexPath *)indexPath title:(NSString *)title{
    [self.myCollection registerNib:[UINib nibWithNibName:@"SimgCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"cellID"];
}
-(void)secrollShowHeaderFooter:(NSInteger)headerFooter title:(NSString *)title{
    
}
//每个区有几个item
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 10;
}
//有几个区
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
//重用cell
#pragma mark CollectionSelfCell
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ImgPriceCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellID" forIndexPath:indexPath];
    [cell scrollViewShowImgs:@"" selendIndex:indexPath];
    return cell;
}
//定义每个UICollectionViewCell 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(WIDTH_K / 3, self.height);
}
//定义每个Section 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);//分别为上、左、下、右
}
//每个section中不同的行之间的行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
//每个item之间的间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
//选择了某个cell
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    self.SelendBtnCell(indexPath,indexPath.row);
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
