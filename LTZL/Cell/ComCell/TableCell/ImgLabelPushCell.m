//
//  ImgLabelPushCell.m
//  FC
//
//  Created by mc on 16/9/26.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "ImgLabelPushCell.h"

@interface ImgLabelPushCell ()
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *name;

@end

@implementation ImgLabelPushCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)scrollViewShowImgs:(id)date selendIndex:(NSIndexPath *)indexPath tableView:(UITableView *)tableView titleString:(NSString *)title{
    self.img.image = [UIImage imageNamed:date[@(indexPath.row)][@"img"]];
    self.name.text =date[@(indexPath.row)][@"name"];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
