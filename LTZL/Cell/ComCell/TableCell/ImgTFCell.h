//
//  ImgTFCell.h
//  FC
//
//  Created by mc on 16/9/27.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImgTFCell : TableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UITextField *TFView;

@end
