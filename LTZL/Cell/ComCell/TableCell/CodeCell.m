//
//  CodeCell.m
//  FC
//
//  Created by mc on 16/9/26.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "CodeCell.h"

@implementation CodeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.TFView.delegate = self;
    
}
- (IBAction)codeButton:(UIButton *)sender {
    self.btnClickTag(sender.tag);
}
-(void)scrollViewShowImgs:(id)date selendIndex:(NSIndexPath *)indexPath tableView:(UITableView *)tableView titleString:(NSString *)title{
    self.TFView.placeholder = date;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    self.TextTag(textField.tag,textField.text);
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    NSLog(@"6");//返回BOOL值，指定是否允许文本字段结束编辑，当编辑结束，文本字段会让出第一响应者
    //    if (![titles isEqualToString:@"注册"] && ![titles isEqualToString:@"登录"]){
    if (textField.text.length != 0) {
        self.TextTag(textField.tag,textField.text);
    }
    
    //    }
    
    return YES;
}


@end
