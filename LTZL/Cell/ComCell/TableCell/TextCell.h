//
//  TextCell.h
//  PT
//
//  Created by ike on 2016/12/1.
//  Copyright © 2016年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextCell : TableViewCell<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *textPler;
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end
