//
//  LbWholeRecordCell.m
//  MT
//
//  Created by ike on 17/1/11.
//  Copyright © 2017年 ike. All rights reserved.
//

#import "LbWholeRecordCell.h"

@implementation LbWholeRecordCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (CGFloat)height{
    
    
    CGSize size = [self.reasonLabel.attributedText boundingRectWithSize:CGSizeMake(WIDTH_K-24, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin context:nil].size;
    CGFloat newheight = size.height + 20;
    
//    NSString *resonText = [NSString stringWithFormat:@"%@",self.reasonLabel.text];
//
//    CGRect rect = [resonText boundingRectWithSize:CGSizeMake(kWidth - 24, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} context:nil];
//
//    self.reasonLabel.frame = CGRectMake(0, 0, kWidth - 24 , rect.size.height);
    
//    CGFloat newHeight = self.reasonLabel.height + 16;
    return newheight;
}
@end
