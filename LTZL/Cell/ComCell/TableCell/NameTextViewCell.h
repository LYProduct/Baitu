//
//  NameTextViewCell.h
//  TS
//
//  Created by mc on 16/1/12.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NameTextViewCell : TableViewCell<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UITextField *TFView;
@property (weak, nonatomic) IBOutlet UILabel *line;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *laft_WC;

@end
