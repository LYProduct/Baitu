//
//  TextCell.m
//  PT
//
//  Created by ike on 2016/12/1.
//  Copyright © 2016年 Apple. All rights reserved.
//

#import "TextCell.h"

@implementation TextCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.textView.delegate = self;
}
-(void)scrollViewShowImgs:(id)date selendIndex:(NSIndexPath *)indexPath tableView:(UITableView *)tableView titleString:(NSString *)title{
    [[LayoutCell sharedLayout] showCell:date tableViewCell:self label:@[self.textPler] btns:@[] TFViews:@[self.textView] imgView:@[] showView:@[] title:title indexPath:indexPath];
}
-(void)textViewDidBeginEditing:(UITextView *)textView{
    self.textPler.text = @"";
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@""]){
        self.textPler.text = @"请输入内容";
    }else{
        self.textPler.text = @"";
    }
    self.TextTag(0,textView.text);
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //隐藏键盘
    [self.textView resignFirstResponder];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
