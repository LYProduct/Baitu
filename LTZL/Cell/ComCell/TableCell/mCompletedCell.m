//
//  mCompletedCell.m
//  Shop
//
//  Created by ike on 16/12/9.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "mCompletedCell.h"

@implementation mCompletedCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)deleteBtn:(UIButton *)sender {
    
    self.btnClickTag(sender.tag);
}


@end
