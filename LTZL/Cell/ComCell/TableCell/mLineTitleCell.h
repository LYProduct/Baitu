//
//  mLineTitleCell.h
//  Shop
//
//  Created by BWD on 16/12/5.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface mLineTitleCell : TableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
