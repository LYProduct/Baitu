//
//  LbWholeRecordCell.h
//  MT
//
//  Created by ike on 17/1/11.
//  Copyright © 2017年 ike. All rights reserved.
////整个一个label的cell

#import <UIKit/UIKit.h>

@interface LbWholeRecordCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *reasonLabel;


@end
