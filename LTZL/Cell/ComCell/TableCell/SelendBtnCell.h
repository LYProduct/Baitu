//
//  SelendBtnCell.h
//  FC
//
//  Created by mc on 16/9/23.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelendBtnCell : TableViewCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *label_TH;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *label_BH;
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
