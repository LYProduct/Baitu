//
//  picDetailCell.h
//  Shop
//
//  Created by ike on 16/12/24.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface picDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img;

@end
