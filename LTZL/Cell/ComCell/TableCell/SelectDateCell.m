//
//  SelectDateCell.m
//  MC
//
//  Created by wyp on 16/9/21.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "SelectDateCell.h"

@interface SelectDateCell () {

   // __weak IBOutlet UIDatePicker *datePicker;
    
}

@end

@implementation SelectDateCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _datePicker.backgroundColor = [UIColor whiteColor];
    NSDate *maxDate = [NSDate date];
    
    [_datePicker setMaximumDate:maxDate];
    // Initialization code
}
- (IBAction)cancel:(UIButton *)sender {
    
    self.reloadVC(@"");
}
- (IBAction)ok:(UIButton *)sender {
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息。
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormatter stringFromDate:_datePicker.date];
    
    
    
    self.reloadVC(dateString);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
