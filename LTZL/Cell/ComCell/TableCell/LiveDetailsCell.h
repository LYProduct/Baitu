//
//  LiveDetailsCell.h
//  FC
//
//  Created by mc on 16/9/21.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LiveDetailsCell : TableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *km;
//简介
@property (weak, nonatomic) IBOutlet UILabel *about;
//已售
@property (weak, nonatomic) IBOutlet UILabel *shon;
//门市价
@property (weak, nonatomic) IBOutlet UILabel *fact;

@property (weak, nonatomic) IBOutlet UILabel *nPrict;
@end
