//
//  LabelTFCell.h
//  IT
//
//  Created by wyp on 16/11/25.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LabelTFCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;

@property (weak, nonatomic) IBOutlet UITextField *rightTF;

@end
