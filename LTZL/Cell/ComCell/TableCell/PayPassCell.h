//
//  PayPassCell.h
//  FC
//
//  Created by mc on 16/9/26.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PayPassCell : TableViewCell<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *TFView;
@property (weak, nonatomic) IBOutlet UIView *lViews;

@end
