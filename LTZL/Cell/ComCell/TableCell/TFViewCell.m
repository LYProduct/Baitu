//
//  TFViewCell.m
//  FC
//
//  Created by mc on 16/9/26.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "TFViewCell.h"

@implementation TFViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.TFView.delegate = self;
}
-(void)scrollViewShowImgs:(id)date selendIndex:(NSIndexPath *)indexPath tableView:(UITableView *)tableView titleString:(NSString *)title{
        if([date isKindOfClass:[NSDictionary class]]){
            self.TFView.placeholder = date[@(indexPath.row)];
            }
        else{
            self.TFView.placeholder =date;
        }
        if ([title isEqualToString:@"找回密码"]||[title isEqualToString:@"修改支付密码"]||[title isEqualToString:@"手机号码修改"]||[title isEqualToString:@"输入新号码"]) {
            if (indexPath.section == 0) {
                if (indexPath.row == 1) {
                    self.TFView.keyboardType = UIKeyboardTypeNumberPad;
                }
            }
        }else
    
        if ([title isEqualToString:@"注册"]) {
            if (indexPath.section == 1) {
                self.TFView.secureTextEntry = YES;

                }
        }else if ([title isEqualToString:@"手机号码修改"]){
            
            if (indexPath.section == 0) {
                if (indexPath.row == 1) {
                    self.TFView.placeholder = @"请输入验证码";
                }
            }
            
        }else if ([title isEqualToString:@"密码修改"]){
            if (indexPath.section == 0) {
                if (indexPath.row == 0) {
                    self.TFView.placeholder = @"请输入旧密码";
                    self.TFView.secureTextEntry = YES;
                    if ([date containsObject:@"oldPw"]) {
                        self.TFView.text = date[@"oldPw"];
                    }
                }else if (indexPath.row == 1){
                    self.TFView.placeholder = @"请输入新密码";
                     self.TFView.secureTextEntry = YES;
                    if ([date containsObject:@"newPw"]) {
                        self.TFView.text = date[@"newPw"];
                    }
                }else if (indexPath.row == 2)
                    self.TFView.placeholder = @"请输入新密码确认";
                    self.TFView.secureTextEntry = YES;
                    if ([date containsObject:@"confirmNewpw"]) {
                        self.TFView.text = date[@"confirmNewpw"];
                    }
            }
        
        }
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    self.TextTag(textField.tag,textField.text);
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    NSLog(@"6");//返回BOOL值，指定是否允许文本字段结束编辑，当编辑结束，文本字段会让出第一响应者
//    if (![titles isEqualToString:@"注册"] && ![titles isEqualToString:@"登录"]){
        if (textField.text.length != 0) {
            self.TextTag(textField.tag,textField.text);
        }
        
//    }
    
    return YES;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
