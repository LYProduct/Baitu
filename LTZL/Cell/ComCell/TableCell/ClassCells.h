//
//  ClassCells.h
//  FC
//
//  Created by mc on 16/9/21.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClassCells : TableViewCell
//特价
@property (weak, nonatomic) IBOutlet UILabel *sele;
@property (weak, nonatomic) IBOutlet UIImageView *seleImg;
@property (weak, nonatomic) IBOutlet UILabel *leftTitle;
@property (weak, nonatomic) IBOutlet UILabel *leftAbout;
@property (weak, nonatomic) IBOutlet UIImageView *leftImg;
@property (weak, nonatomic) IBOutlet UILabel *rightTitle_T;
@property (weak, nonatomic) IBOutlet UILabel *rightAbout_T;
@property (weak, nonatomic) IBOutlet UIImageView *rightImg_T;
@property (weak, nonatomic) IBOutlet UILabel *rightTitle_C;
@property (weak, nonatomic) IBOutlet UILabel *rightAbout_C;
@property (weak, nonatomic) IBOutlet UIImageView *rightImg_C;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightTitle;
@property (weak, nonatomic) IBOutlet UILabel *rightAbout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view_TH;
@property (weak, nonatomic) IBOutlet UIImageView *rightImg;
@end
