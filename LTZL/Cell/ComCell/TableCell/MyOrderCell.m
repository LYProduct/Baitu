//
//  OrderCell.m
//  IT
//
//  Created by wyp on 16/11/24.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "MyOrderCell.h"

@interface MyOrderCell () {
    
    __weak IBOutlet UILabel *titleLabel;
    
    __weak IBOutlet UILabel *orderNum;
    __weak IBOutlet UILabel *money;
    
    __weak IBOutlet UILabel *time;
}

@end

@implementation MyOrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)reloadTheCell:(NSDictionary *)dict {
    titleLabel.text = dict[@"title"];
    orderNum.text = [NSString stringWithFormat:@"订单编号: %@", [dict[@"threePayId"] description]];
    money.text = [NSString stringWithFormat:@"充值金额: %@", [dict[@"amount"] description]];
    if ([[dict[@"gmtDatetime"] description] length] == 12) {
        time.text = [NSString stringWithFormat:@"充值时间: %@", [TheGlobalMethod dateStrss:[[dict[@"gmtDatetime"] description] substringToIndex:9]]];
    } else {
        time.text = [NSString stringWithFormat:@"充值时间: %@", [TheGlobalMethod dateStrss:[[dict[@"gmtDatetime"] description] substringToIndex:10]]];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
