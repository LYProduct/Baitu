//
//  MyOrderCell.h
//  Shop
//
//  Created by ike on 16/12/10.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyOrderCell : UITableViewCell

- (void)reloadTheCell:(NSDictionary *)dict;
@property (weak, nonatomic) IBOutlet UIView *bottomView;

@end
