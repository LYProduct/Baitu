//
//  TxFViewCell.h
//  FC
//
//  Created by mc on 16/9/23.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^returnTextView)(NSInteger tag,id data);

@interface TxFViewCell : TableViewCell<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view_TH;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view_BH;
@property (weak, nonatomic) IBOutlet UITextView *textView;

@property (copy,nonatomic)returnTextView textViewText;

@end
