//
//  mtuikuanbtnCell.h
//  Shop
//
//  Created by ike on 16/12/3.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface mtuikuanbtnCell : TableViewCell
@property (weak, nonatomic) IBOutlet UIButton *refuseBtn;
@property (weak, nonatomic) IBOutlet UIButton *agreeBtn;

@end
