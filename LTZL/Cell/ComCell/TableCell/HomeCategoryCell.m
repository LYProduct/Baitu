//
//  HomeCategoryCell.m
//  意通
//
//  Created by ike on 16/8/12.
//  Copyright © 2016年 MIFAN. All rights reserved.
//

#import "HomeCategoryCell.h"
#import "HomeCategoryCVCell.h"

#import "PagingCollectionViewLayout.h"


@interface HomeCategoryCell ()<UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout> {
    
    UICollectionView *myCollectionView;//collectionView
    NSArray *ImgArr;//图片数组
    NSArray *labelArr;//字符串数组
    
    BOOL isVertical;//是否垂直,YES为垂直
    
    UIPageControl *page;//分页控件
    
    NSIndexPath *selendIndexPath;
    
}

@end

@implementation HomeCategoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)secrollShowHeaderFooter:(NSInteger)headerFooter{
    
}

-(void)scrollViewShowImgs:(id)date selendIndex:(NSIndexPath *)indexPath{
    selendIndexPath = indexPath;
    isVertical = NO;//控制是否垂直
    
    ImgArr = [NSMutableArray array];
    labelArr = [NSMutableArray array];
    
//    for (int i = 0; i < gateArr.count; i++) {
//        [ImgArr addObject:gateArr[i][@"imgUrl"]];
//        [labelArr addObject:gateArr[i][@"name"]];
//    }
    ImgArr = @[@"yong_index01", @"yong_index01", @"yong_index01", @"yong_index01", @"yong_index01", @"yong_index01", @"yong_index01", @"yong_index01", @"yong_index01", @"yong_index01", @"yong_index01",@"yong_index01", @"yong_index01", @"yong_index01", @"yong_index01", @"yong_index01", @"yong_index01", @"yong_index01", @"yong_index01", @"yong_index01", @"yong_index01", @"yong_index01"];
    labelArr = @[@"品牌街", @"旗舰店", @"分红区", @"特价专区", @"O2O直营", @"美食", @"洗车", @"超市", @"酒店", @"KTV", @"电影", @"休闲娱乐", @"自助餐", @"足疗按摩", @"甜点饮品", @"美容美体", @"品牌街", @"旗舰店", @"分红区", @"特价专区", @"O2O直营", @"美食", @"洗车", @"超市", @"酒店", @"KTV", @"电影", @"休闲娱乐", @"自助餐",@"品牌街", @"旗舰店", @"分红区", @"特价专区", @"O2O直营", @"美食", @"洗车", @"超市", @"酒店", @"KTV", @"电影", @"休闲娱乐", @"自助餐", @"足疗按摩", @"甜点饮品", @"美容美体", @"品牌街", @"旗舰店", @"分红区", @"特价专区", @"O2O直营", @"美食", @"洗车", @"超市", @"酒店", @"KTV", @"电影", @"休闲娱乐", @"自助餐"];
    
    if (!isVertical) {//如果非垂直,用自定义横向布局
        PagingCollectionViewLayout *layout = [[PagingCollectionViewLayout alloc]init];
        layout.itemSize = CGSizeMake(WIDTH_K/4, WIDTH_K/4);
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        myCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, 0, 0) collectionViewLayout:layout];
    } else {//如果垂直布局,用系统
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        //设置每一个item大小
        layout.itemSize = CGSizeMake(WIDTH_K/4, WIDTH_K/4);
        //设置分区的缩进量
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        //设置行间距
        layout.minimumLineSpacing = 0;
        //设置最小行间距
        layout.minimumInteritemSpacing = 0;
        layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        myCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, 0, 0) collectionViewLayout:layout];
    }
    
    
    
    //设置cell自身的高并设置collectionView在垂直和竖向的高度
    if (ImgArr.count % 4 > 0) {
        if (isVertical) {
            self.height = (WIDTH_K/4)*(ImgArr.count/4 + 1) + 8;
            myCollectionView.height = self.height;
        } else {
            self.height = (WIDTH_K/4)*2 + 16;
            myCollectionView.height = WIDTH_K/4 * 2;
            
        }
    } else {
        if (isVertical) {
            self.height = (WIDTH_K/4)*(ImgArr.count/4) + 8;
            myCollectionView.height = self.height;
        } else {
            self.height = (WIDTH_K/4)*2 + 16;
            myCollectionView.height = WIDTH_K/4 * 2;
        }
    }
    
    //设置collectionView的frame
    myCollectionView.left = 0;
    myCollectionView.top = 0;
    myCollectionView.width = WIDTH_K;
    
    //设置collectionView的contentSize是无效的的,需要将cell的个数填充才可整页滑动
    //    myCollectionView.contentSize = CGSizeMake(6 * kWidth, kHeight);
    
    //设置collectionView的属性
    myCollectionView.delegate = self;
    myCollectionView.dataSource = self;
    myCollectionView.backgroundColor = [UIColor whiteColor];
    [myCollectionView registerNib:[UINib nibWithNibName:@"HomeCategoryCVCell" bundle:nil] forCellWithReuseIdentifier:@"cellID"];
    myCollectionView.pagingEnabled = YES;//整屏滑动
    myCollectionView.showsVerticalScrollIndicator = NO;
    myCollectionView.showsHorizontalScrollIndicator = NO;
    [self addSubview:myCollectionView];
    
    
    if (!isVertical) {//如水平添加一个page
        CGRect frame = CGRectMake(0, self.height-16, 320, 16);
        page = [[UIPageControl alloc] init];
        page.frame = frame;
        if (ImgArr.count%8 > 0) {
            page.numberOfPages = ImgArr.count/8 + 1;
        } else {
            page.numberOfPages = ImgArr.count/8;
        }
        page.currentPageIndicatorTintColor = NAVCOLOR_C(13.0, 95.0, 255.0);;
        page.pageIndicatorTintColor = [UIColor lightGrayColor];
        
        [self addSubview:page];
    }

}

//监测collectionView的滑动控制分页控件
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat index = myCollectionView.contentOffset.x;
    page.currentPage = index / WIDTH_K;
    
}

//有几个区
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

//每个区有几个item
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
        return ImgArr.count;
}
//重用cell
#pragma mark CollectionSelfCell
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HomeCategoryCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellID" forIndexPath:indexPath];
    cell.Img.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",ImgArr[indexPath.row]]];
    cell.label.text = labelArr[indexPath.row];
    return cell;
}
//定义每个Section 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);//分别为上、左、下、右
}
//选择了某个cell
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"点击了");
    self.SelendBtnCell(selendIndexPath,indexPath.row);
}
//每个item之间的间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
