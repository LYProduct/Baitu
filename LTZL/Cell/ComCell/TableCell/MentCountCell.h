//
//  MentCountCell.h
//  FC
//
//  Created by mc on 16/9/21.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MentCountCell : TableViewCell
@property (weak, nonatomic) IBOutlet UILabel *mentCount;
@property (weak, nonatomic) IBOutlet UILabel *label_T;
@property (weak, nonatomic) IBOutlet UILabel *money;
@property (weak, nonatomic) IBOutlet UILabel *people;

@end
