//
//  ScrollViewCell.m
//  FC
//
//  Created by mc on 16/9/20.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "ScrollViewCell.h"

@implementation ScrollViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)scrollViewShowImgs:(id)date selendIndex:(NSIndexPath *)indexPath tableView:(UITableView *)tableView titleString:(NSString *)title{
    index = indexPath;
    self.height = HEIGHT_K / 4;
    NSArray *imgs = (NSArray *)[date mutableCopy];
    SDCycleScrollView *cycleView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, WIDTH_K, self.height) imageNamesGroup:imgs];
    cycleView.currentPageDotColor = NAVCOLOR_C(13.0, 95.0, 255.0);
    [self addSubview:cycleView];
}
-(void)secrollShowHeaderFooter:(NSInteger)headerFooter title:(NSString *)title{
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
