//
//  BtnPayCell.h
//  FC
//
//  Created by mc on 16/9/24.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BtnPayCell : TableViewCell
@property (weak, nonatomic) IBOutlet UIButton *payBtn;

@end
