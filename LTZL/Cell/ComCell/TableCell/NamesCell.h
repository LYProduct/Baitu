//
//  NamesCell.h
//  FC
//
//  Created by mc on 16/9/21.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NamesCell : TableViewCell
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *count;
@property (weak, nonatomic) IBOutlet UILabel *rightlabel;
@property (weak, nonatomic) IBOutlet UIImageView *pushImg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view_BT;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *view_TH;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *label_RW;
@property (weak, nonatomic) IBOutlet UILabel *line;
@end
