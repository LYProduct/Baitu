//
//  ShowImgCell.h
//  FC
//
//  Created by mc on 16/9/21.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowImgCell : TableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label_T;
@property (weak, nonatomic) IBOutlet UILabel *label_B;
@property (weak, nonatomic) IBOutlet UILabel *imgCount;
@property (weak, nonatomic) IBOutlet UIView *imgCoutView;
@end
