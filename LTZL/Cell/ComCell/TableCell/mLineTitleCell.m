//
//  mLineTitleCell.m
//  Shop
//
//  Created by BWD on 16/12/5.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "mLineTitleCell.h"

@implementation mLineTitleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)scrollViewShowImgs:(id)date selendIndex:(NSIndexPath *)indexPath tableView:(UITableView *)tableView titleString:(NSString *)title{
    
    if ([title isEqualToString:@"我的二维码"]) {
        if (indexPath.section == 0) {
            if (indexPath.row == 3) {
                self.label.text = @"如何赚钱";
            }
        }
    }
}

@end
