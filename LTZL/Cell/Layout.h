//
//  Layout.h
//  Shop
//
//  Created by ike on 16/12/5.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Layout : NSObject
#pragma mark 买单
//+(NSDictionary *)order;
//+(Layout *)sharedLayout;
+(NSDictionary *)order;

#pragma mark 提现记录
+(NSDictionary *)aorder;

#pragma mark 提现填写
+(NSDictionary *)border;

#pragma mark 我的二维码
+(NSDictionary *)corder;


//    待验证： 酒店0 KTV1  商城2 美食3 外卖4
//    待发货   10
//    待退款   100
//    已完成   已退款1000   商城 10

#pragma mark 订单详情之酒店0
+(NSDictionary *)hotelOrder;

#pragma mark 订单详情之KTV1
+(NSDictionary *)KTVOrder;

#pragma mark 订单详情之商城2
+(NSDictionary *)clothesOrder;

#pragma mark 订单详情之美食3
+(NSDictionary *)foodOrder;

#pragma mark 订单详情之外卖4
+(NSDictionary *)orderOrder;

#pragma mark 订单详情之待发货10 --已完成的商城
+(NSDictionary *)fahuoOrder;

#pragma mark 订单详情之待退款100
+(NSDictionary *)tuikuanOrder;

#pragma mark 订单详情之已完成  已退款1000
+(NSDictionary *)overOrder;


#pragma mark  验券结果
+(NSDictionary *)eorder;

#pragma mark 验证记录
+(NSDictionary *)forder;

#pragma mark 我的钱包
+(NSDictionary *)gorder;

#pragma mark 我的团队
+(NSDictionary *)horder;

#pragma mark 到店付记录
+(NSDictionary *)iorder;

#pragma mark 财务管理
+(NSDictionary *)jorder;

#pragma mark 订单管理
+(NSDictionary *)korder;
+(NSDictionary *)kkorder;//待退款
//+(NSDictionary *)kkkorder:(NSDictionary *)dict;//已完成
+(NSDictionary *)kkkorder;//已完成


#pragma mark 我的地址
+(NSDictionary *)lorder;


//#pragma mark 我的
//+(NSDictionary *)morder;

#pragma mark 我的
//+(NSDictionary *)norder;

#pragma mark 确认支付密码
+(NSDictionary *)oorder;

#pragma mark  设置支付密码
+(NSDictionary *)porder;

#pragma mark  输入新号码
+(NSDictionary *)qorder;

#pragma mark 手机号码修改
+(NSDictionary *)rorder;

#pragma mark  修改密码
+(NSDictionary *)sorder;


#pragma mark  消息中心
+(NSDictionary *)torder;

#pragma mark  设置
+(NSDictionary *)uorder;

#pragma mark 支付确认
+(NSDictionary *)vorder;


@end
