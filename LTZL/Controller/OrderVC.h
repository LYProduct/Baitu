//
//  OrderVC.h
//  Shop
//
//  Created by ike on 16/12/30.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface OrderVC : UIViewController<CLLocationManagerDelegate>

@property(nonatomic,strong)NSString *titleString;

@end
