//
//  ScanningVC.m
//  MC
//
//  Created by wyp on 16/10/26.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "ScanningVC.h"
#import "InfoVC.h"
#import "TopUpVC.h"

#import <AVFoundation/AVFoundation.h>

@interface ScanningVC ()<AVCaptureMetadataOutputObjectsDelegate> {
    NSString *isNext;
}

@property (strong,nonatomic)AVCaptureDevice * device;
@property (strong,nonatomic)AVCaptureDeviceInput * input;
@property (strong,nonatomic)AVCaptureMetadataOutput * output;
@property (strong,nonatomic)AVCaptureSession * session;
@property (strong,nonatomic)AVCaptureVideoPreviewLayer * preview;

@property (strong, nonatomic) UIView *boxView;
@property (strong, nonatomic) CALayer *scanLayer;
@property (nonatomic, strong) NSTimer *timer;

@end

@implementation ScanningVC

@synthesize boxView,scanLayer,timer;

- (void)viewDidLoad {
    [super viewDidLoad];
  
   isNext = @"1";
    
    // Do any additional setup after loading the view.
    // Device
    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    // Input
    _input = [AVCaptureDeviceInput deviceInputWithDevice:self.device error:nil];
    
    // Output
    _output = [[AVCaptureMetadataOutput alloc]init];
    [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    
//    _output.rectOfInterest = CGRectMake(0.2f, 0.2f, 0.8f, 0.8f);

    // Session
    _session = [[AVCaptureSession alloc]init];
    [_session setSessionPreset:AVCaptureSessionPresetHigh];
//    连接输入和输出
    if ([_session canAddInput:self.input])
    {
        [_session addInput:self.input];
    }
    
    if ([_session canAddOutput:self.output])
    {
        [_session addOutput:self.output];
    }
//    设置条码类型
    _output.metadataObjectTypes =@[AVMetadataObjectTypeQRCode];
//    添加扫描画面
    _preview =[AVCaptureVideoPreviewLayer layerWithSession:_session];
    _preview.videoGravity =AVLayerVideoGravityResizeAspectFill;
    _preview.frame =self.view.bounds;
    [self.view.layer insertSublayer:_preview atIndex:0];
    
    //10.1.扫描框
    boxView = [[UIView alloc] initWithFrame:CGRectMake(30, 100, WIDTH_K-60, WIDTH_K-60)];
    boxView.layer.borderColor = ColorBlue_k.CGColor;
    boxView.layer.borderWidth = 3.0f;
    boxView.layer.cornerRadius = 3;
    boxView.layer.masksToBounds = YES;
    
    [self.view addSubview:boxView];
    
    //10.2.扫描线
    scanLayer = [[CALayer alloc] init];
    scanLayer.frame = CGRectMake(0, 0, boxView.bounds.size.width, 3);
    scanLayer.backgroundColor = ColorBlue_k.CGColor;
    
    [boxView.layer addSublayer:scanLayer];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(moveScanLayer:) userInfo:nil repeats:YES];
    
    [timer fire];
    
  
//    开始扫描
    [_session startRunning];
    
}

- (void)moveScanLayer:(NSTimer *)timer
{
    CGRect frame = scanLayer.frame;
    if (boxView.frame.size.height < scanLayer.frame.origin.y) {
        frame.origin.y = 0;
        scanLayer.frame = frame;
    }else{
        
        frame.origin.y += 10;
        
        [UIView animateWithDuration:0.1 animations:^{
            scanLayer.frame = frame;
        }];
    }
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    NSString *stringValue;
    if ([metadataObjects count] >0){
        AVMetadataMachineReadableCodeObject * metadataObject = [metadataObjects objectAtIndex:0];
        stringValue = metadataObject.stringValue;
        
        
        if (self.index == 1) {
            
        NSRange range1 = [stringValue rangeOfString:@"goodsBuyNumber="];
        NSRange range2 = [stringValue rangeOfString:@"yx_userid="];
        //http://www.yuexiang.com?sh_useri
//        NSRange range3 = [stringValue rangeOfString:@"http://www.yuexiang.com?sh_userid="];
            
        NSLog(@"strv == %@",stringValue);
        if (range1.location != NSNotFound) {
            
            NSString *str222 = [stringValue substringFromIndex: range1.length + range1.location ];
            NSLog(@"%@===str222",str222);
            //以下没用，前后不同的，要注意一下先测试这部分
  //加打印
                [_session stopRunning];
                [timer invalidate];
                 isNext = @"2";
                InfoVC *Controller = [self.storyboard instantiateViewControllerWithIdentifier:@"InfoVC"];
                Controller.titleString = @"验券结果";
                Controller.title = @"验券结果";
                //这里的数字
                Controller.goodsBuyNumber = str222;
                [self.navigationController pushViewController:Controller animated:YES];
                
        }
//        else if (range2.location != NSNotFound){
//            
//            NSString *str222 = [stringValue substringFromIndex: range2.length + range2.location ];
//            NSLog(@"%@===str222",str222);
//            //以下没用，前后不同的，要注意一下先测试这部分
//            
//            [_session stopRunning];
//            [timer invalidate];
//            isNext = @"2";
//            InfoVC *Controller = [self.storyboard instantiateViewControllerWithIdentifier:@"InfoVC"];
//            Controller.titleString = @"买单";
//            Controller.title = @"买单";
//            //这里的数字
//            Controller.numberCard = str222;
//            [self.navigationController pushViewController:Controller animated:YES];
//            
//            
//        }
        else if (range2.location != NSNotFound){
            
            NSString *str222 = [stringValue substringFromIndex: range2.length + range2.location ];
            NSLog(@"%@===str222",str222);
            //以下没用，前后不同的，要注意一下先测试这部分
            
            [_session stopRunning];
            [timer invalidate];
            isNext = @"2";
            
            [HttpUrl LTPOST:@"/user/binding/user/superior" dict:@{@"numberCard":str222} WithSuccessBlock:^(id data) {
                

                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:data[@"msg"]preferredStyle: UIAlertControllerStyleAlert];
                    
                    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                        kNavPop;
                    }]];
                    [self presentViewController:alert animated:true completion:nil];
             
            }];
          
            
            
        }else{
            
//                    NSString *str222 = [stringValue substringFromIndex: range2.length + range2.location ];
//                    NSLog(@"%@===str222",str222);
            
            int a = 2;
            unichar b ;
            for (int i = 0; i < stringValue.length; i++) {
                b=[stringValue characterAtIndex:i];
                if (!isdigit(b)){
                    
                    a= 1;
                }
            }
            
            if (a == 2) {
                NSLog(@"%@----sssss",stringValue);
                //以下没用，前后不同的，要注意一下先测试这部分
                [_session stopRunning];
                [timer invalidate];
                isNext = @"2";
                InfoVC *Controller = [self.storyboard instantiateViewControllerWithIdentifier:@"InfoVC"];
                Controller.titleString = @"买单";
                Controller.title = @"买单";
                //这里的数字
                Controller.numberCard = stringValue;
                
                [HttpUrl LTPOST:@"membership/card/find/user" dict:@{@"numberCard":stringValue} WithSuccessBlock:^(id data) {
                    if ([data[@"success"]intValue] == 1) {
                        
                        [self.navigationController pushViewController:Controller animated:YES];
                        
                    }else{
                        
                        [TheGlobalMethod popAlertView:data[@"msg"] controller:self];
                        
                        NSLog(@"222");
                    }
                }];
              
            
            }
          
            
        
        }
            
        }
            
        else{
            
            [_session stopRunning];
            [timer invalidate];
            isNext = @"2";
            //否则跳转充值
            NSString *stringValue;
            TopUpVC *vc = [TheGlobalMethod setstoryboard:@"TopUpVC" controller:self];
            vc.userCard = stringValue;
            [self.navigationController pushViewController:vc animated:YES];
            
        }
        
    }
}


- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = NO;
    if ([isNext isEqualToString:@"2"]) {
        kNavPop
    }
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
