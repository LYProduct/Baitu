//
//  HomeVC.m
//  FC
//
//  Created by mc on 16/9/20.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "MoneyLeftVC.h"
#import "CenterLabelCell.h"
#import "MyOrderCell.h"
#import "SelectOrderTimeCell.h"
//#import "MineVC.h"
#import "SelectDateCell.h"
#import "ScanningVC.h"
#import "TopUpVC.h"
#import "MyTelView.h"

@interface MoneyLeftVC ()<UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>{
    NSArray *ImgArr;
    NSArray *labelArr;
    NSString *tfString;
    __weak IBOutlet MyTelView *telView;
    __weak IBOutlet UILabel *TFView;
    __weak IBOutlet UIButton *cenu;
    __weak IBOutlet UITableView *myTableView;
    
    __weak IBOutlet UIButton *myMoney;
    __weak IBOutlet UIButton *headBtn;
    
    NSMutableArray *orderListArr;
    
    NSDictionary *userDic;
    NSInteger page;
    NSString *beganTime;//
    NSString *endTime;//
    NSString *upMoney;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *telView_BT;

@end

@implementation MoneyLeftVC
@synthesize telView_BT;
-(void)viewWillAppear:(BOOL)animated{
    
//时间--
    [HttpUrl LTPOST:@"user/amount/record/merchant/recharge/record" dict:@{@"beginTime":beganTime, @"endTime":[endTime stringByAppendingString:@" 23:59"] }WithSuccessBlock:^(id data) {
         if ([data[@"success"] intValue] == 1) {
             userDic = data[@"data"];
             [myMoney setTitle:[NSString stringWithFormat:@"  ¥%.2f", [data[@"data"][@"rechargeAmount"] floatValue]] forState:(UIControlStateNormal)];
         }
     }];
//    [self getUserData];
    
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication] setStatusBarStyle:(UIStatusBarStyleLightContent)];
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    headBtn.imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    page = 1;
    myTableView.delegate = self;
    myTableView.dataSource = self;
    //设置标头
    CenterLabelCell *headerView = [TheGlobalMethod setCell:@"CenterLabelCell"];
    headerView.backgroundColor = [UIColor whiteColor];
    headerView.frame = CGRectMake(0, 0, WIDTH_K, 44);
    myTableView.tableHeaderView = headerView;
    
    myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        page = 1;
        [orderListArr removeAllObjects];
        [self getOrderList];
    }];
    
    myTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        page++;
        [self getOrderList];
    }];
    
    orderListArr = [NSMutableArray array];
    
    beganTime = [[TheGlobalMethod dateStr:[NSString stringWithFormat:@"%ld", time(NULL)-3600*24*31]]substringToIndex:10 ];//
    endTime = [[TheGlobalMethod dateStr:[NSString stringWithFormat:@"%ld", time(NULL)]]substringToIndex:10];//
    
    
    //请求订单
    [self getOrderList];
    
    
    // Do any additional setup after loading the view.
    telView_BT.constant = -(HEIGHT_K / 3 * 2);
    labelArr = @[@"订单管理",@"财务服务",@"我的钱包",@"到店付记录",@"我的二维码",@"我的团队"];
    ImgArr = @[@"shanghu_index01",@"shanghu_index02",@"shanghu_index03",@"shanghu_index04",@"shanghu_index05",@"shanghu_index06"];
    
    UITapGestureRecognizer* singleRecognizer;
    singleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(SingleTap)];
    //点击的次数
    singleRecognizer.numberOfTapsRequired = 1; // 单击
    TFView.userInteractionEnabled = YES;
    //给self.view添加一个手势监测；
    [TFView addGestureRecognizer:singleRecognizer];
    
    tfString =@"";
    telView.VerifyBtn=^(long Selend){
        
        if (Selend == 100) {
            
            if ([TFView.text isEqualToString:@"请输入卡号充值"]) {
                [TheGlobalMethod xianShiAlertView:@"请输入卡号" controller:self];
                return;
            }
            
            
            [HttpUrl LTPOST:@"membership/card/find/user" dict:@{@"numberCard":TFView.text} WithSuccessBlock:^(id data) {
                if ([data[@"success"] intValue] == 1) {
                    TopUpVC *vc = [TheGlobalMethod setstoryboard:@"TopUpVC" controller:self];
                    vc.userCard = TFView.text;
                    [self.navigationController pushViewController:vc animated:YES];
                } else {
                    [TheGlobalMethod xianShiAlertView:@"请输入正确的卡号" controller:self];
                }
            }];
            
            
            [TFView endEditing:YES];
            [UIView animateWithDuration:1.0 // 动画时长
                                  delay:0.01 // 动画延迟
                 usingSpringWithDamping:1.0 // 类似弹簧振动效果 0~1
                  initialSpringVelocity:1.0 // 初始速度
                                options:UIViewAnimationOptionCurveEaseInOut // 动画过渡效果
                             animations:^{
                                 // code...
                                 CGPoint point = telView.center;
                                 point.y += HEIGHT_K/ 3 * 2;
                                 [telView setCenter:point];
                                 telView_BT.constant -= HEIGHT_K / 3 * 2;
                             } completion:^(BOOL finished){
                                 telView_BT.constant = -(HEIGHT_K / 3 * 2);
                             }];
        }else{
            TFView.textColor = [UIColor blackColor];
            cenu.hidden = NO;
            tfString = [NSString stringWithFormat:@"%@%ld",tfString,Selend];
            TFView.text = tfString;
        }
        
    };
}
-(void)SingleTap{
    [UIView animateWithDuration:0.8 // 动画时长
                          delay:0.1 // 动画延迟
         usingSpringWithDamping:0.8 // 类似弹簧振动效果 0~1
          initialSpringVelocity:1.0 // 初始速度
                        options:UIViewAnimationOptionCurveEaseInOut // 动画过渡效果
                     animations:^{
                         // code...
                         CGPoint point = telView.center;
                         point.y = HEIGHT_K / 3 * 2;
                         [telView setCenter:point];
                     } completion:^(BOOL finished){
                         telView_BT.constant = 0;
                     }];
}
#pragma mark 个人中心

- (IBAction)ImeBtnClick:(id)sender {
//    MineVC *Controller = [self.storyboard instantiateViewControllerWithIdentifier:@"MineVC"];
//    [self.navigationController pushViewController:Controller animated:YES];
    SetPop;
    
}
- (IBAction)removeTfView:(id)sender {
    tfString = @"";
    cenu.hidden = YES;
    TFView.text = @"请输入卡号充值";
    TFView.textColor = GrayColor_K;
}
#pragma mark 消息
- (IBAction)newsBtn:(id)sender {
}
- (IBAction)hideTelView:(id)sender{
    if (tfString.length == 0) {
        cenu.hidden = YES;
        TFView.text = @"请输入卡号充值";
        TFView.textColor = GrayColor_K;
    }
    [UIView animateWithDuration:0.8 // 动画时长
                          delay:0.1 // 动画延迟
         usingSpringWithDamping:0.8 // 类似弹簧振动效果 0~1
          initialSpringVelocity:1.0 // 初始速度
                        options:UIViewAnimationOptionCurveEaseInOut // 动画过渡效果
                     animations:^{
                         // code...
                         CGPoint point = telView.center;
                         point.y += HEIGHT_K/ 3 * 2;
                         [telView setCenter:point];
                         telView_BT.constant -= HEIGHT_K / 3 * 2;
                     } completion:^(BOOL finished){
                         telView_BT.constant = -(HEIGHT_K / 3 * 2);
                     }];
}


#pragma TableView

TableViewNumberOfRowsInSection {
    return orderListArr.count;
}

TableViewCellForRowAtIndexPath {
    MyOrderCell *cell = [TheGlobalMethod setCell:@"MyOrderCell"];
    [cell reloadTheCell:orderListArr[indexPath.row]];
    if (indexPath.row == orderListArr.count - 1) {
        cell.bottomView.hidden = YES;
    }
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    SelectOrderTimeCell *view = [TheGlobalMethod setCell:@"SelectOrderTimeCell"];
    
    if (![upMoney isEqual:[NSNull null]] && upMoney) {
        view.centerLabel.text = [NSString stringWithFormat:@"已充值金额:%@",upMoney];
    }else{
         view.centerLabel.text = [NSString stringWithFormat:@"已充值金额:0"];
    }
    
    
    [view.leftBtn setTitle:beganTime forState:(UIControlStateNormal)];
    [view.rightBtn setTitle:endTime forState:(UIControlStateNormal)];
    
    [view.leftBtn addTarget:self action:@selector(selectDate:) forControlEvents:(UIControlEventTouchUpInside)];
    [view.rightBtn addTarget:self action:@selector(selectDate:) forControlEvents:(UIControlEventTouchUpInside)];
    view.backgroundColor = kBackGroudColor;
    return view;
}

- (void)getOrderList {
//  user/amount/record/center/recharge/record
    [HttpUrl LTPOST:@"user/amount/record/merchant/recharge/record" dict:@{@"beginTime":beganTime, @"endTime":[endTime stringByAppendingString:@" 23:59"]} WithSuccessBlock:^(id data) {
        
        [myTableView.mj_header endRefreshing];
        [myTableView.mj_footer endRefreshing];
        if ([data[@"success"] intValue] == 1) {
            
            upMoney = data[@"data"][@"totalAmount"];
            NSArray *arr = data[@"data"][@"userAmountRecordPageInfo"][@"list"];
            if ([data[@"data"][@"pages"] intValue] <= page) {
                [myTableView.mj_footer setHidden:YES];
            }
            [orderListArr addObjectsFromArray:arr];
            
        }
        
        [myTableView reloadData];
        
    }];
}

TableViewHeightForHeaderInSection {
    return 88;
}

TableViewCellHeight

//选择日期
- (void)selectDate:(UIButton *)sender {
    UIView *aView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, HEIGHT_K)];
    aView.userInteractionEnabled = YES;
    UITapGestureRecognizer *removeView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeView:)];
    [aView addGestureRecognizer:removeView];
    aView.tag = 10010;
    SelectDateCell *cell = [TheGlobalMethod setCell:@"SelectDateCell"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSLog(@"begggg--%@",[beganTime substringToIndex:10]);
    NSDate *dateBegin = [dateFormatter dateFromString:[beganTime substringToIndex:10]];
    NSDate *dateEnd = [dateFormatter dateFromString:[endTime substringToIndex:10]];
    //左侧
    if (sender.tag == 300) {
       
         cell.datePicker.maximumDate = dateEnd;
    }else{
        cell.datePicker.minimumDate = dateBegin;
    }
    
    cell.tag = 10086;
    cell.frame = CGRectMake(0, HEIGHT_K-HEIGHT_K/3, WIDTH_K, HEIGHT_K/3);
    cell.reloadVC = ^void(NSString *str1){
        
        if (str1.length > 0) {
            
            if (sender.tag == 300) {
                NSLog(@"这是左边的");
                beganTime = str1;
                [sender setTitle:beganTime forState:(UIControlStateNormal)];
            } else {
                endTime = str1;
                [sender setTitle:endTime forState:(UIControlStateNormal)];
                NSLog(@"这是右边的");
            }
            [orderListArr removeAllObjects];
            [self getOrderList];
            NSLog(@"shijian%@", str1);
        }
        [[[UIApplication sharedApplication].keyWindow viewWithTag:10086] removeFromSuperview];
        [[[UIApplication sharedApplication].keyWindow viewWithTag:10010] removeFromSuperview];
        self.view.userInteractionEnabled = YES;
    };
    [[UIApplication sharedApplication].keyWindow addSubview:aView];
    aView.alpha = 0.3;
    aView.backgroundColor = [UIColor blackColor];
    [[UIApplication sharedApplication].keyWindow addSubview:cell];
    
    self.view.userInteractionEnabled = NO;
}
- (void)removeView:(UITapGestureRecognizer *)sender {
    [[[UIApplication sharedApplication].keyWindow viewWithTag:10086] removeFromSuperview];
    [[[UIApplication sharedApplication].keyWindow viewWithTag:10010] removeFromSuperview];
    self.view.userInteractionEnabled = YES;
}

//扫描验证按钮
- (IBAction)scanning:(UIButton *)sender {
    ScanningVC *vc = [TheGlobalMethod setstoryboard:@"ScanningVC" controller:self];
    [self.navigationController pushViewController:vc animated:YES];
}

//- (void)getUserData {
//    [HttpUrl POST:@"user/find/user" dict:@{} WithSuccessBlock:^(id data) {
//        if (![data[@"data"][@"headUrl"] isEqual:[NSNull null]]) {
//            [headBtn sd_setImageWithURL:[NSURL URLWithString:data[@"data"][@"headUrl"]] forState:(UIControlStateNormal) placeholderImage:[UIImage imageNamed:@"toux"]];
//        }
//        
//    }];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
