//
//  TopUpVC.m
//  IT
//
//  Created by wyp on 16/11/25.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "TopUpVC.h"
#import "TwoLabelCell.h"
#import "LabelTFCell.h"
#import "MyBalanceCell.h"
#import "ZCTradeView.h"
#import "SetPayPasswordVC.h"

@interface TopUpVC ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate> {

    __weak IBOutlet UITableView *myTableView;
    
    NSString *topupMoney;
    
    CGFloat userMoney;
    NSString *myMoney;
    
    NSString *userId;
}

@end

@implementation TopUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    myTableView.delegate = self;
    myTableView.dataSource = self;
    // Do any additional setup after loading the view.
    
    [self getNetWorking];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [HttpUrl LTPOST:@"user/find/user" dict:@{} WithSuccessBlock:^(id data) {
        if ([data[@"data"][@"payPassword"] intValue] == 1) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"您未设置过支付密码"preferredStyle: UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"暂不设置" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                //点击按钮的响应事件；
                kNavPop
            }]];
            [alert addAction:[UIAlertAction actionWithTitle:@"去设置" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                //点击按钮的响应事件；
                SetPayPasswordVC *vc = [TheGlobalMethod setstoryboard:@"SetPayPasswordVC" controller:self];
                vc.isNum = @"1";
                vc.lol = @"lol";
                [self.navigationController pushViewController:vc animated:YES];
            }]];
            //弹出提示框；
            [self presentViewController:alert animated:true completion:nil];
        }
        
    }];
    
    self.navigationController.navigationBarHidden = NO;
    [super viewWillAppear:animated];
}

- (void)getNetWorking {
    [HttpUrl LTPOST:@"membership/card/find/user" dict:@{@"numberCard":self.userCard} WithSuccessBlock:^(id data) {
        userMoney = [data[@"data"][@"balance"] floatValue];
        userId = [data[@"data"][@"id"] description];
        [HttpUrl POST:@"user/find/user" dict:nil sign:nil WithSuccessBlock:^(id data) {
            myMoney = [NSString stringWithFormat:@"%.2f",[data[@"data"][@"balance"] floatValue]];
            [myTableView reloadData];
        }];
    }];
    
}

TableViewNumberOfSectionsInTableView {
    return 2;
}

TableViewNumberOfRowsInSection {
    if (section == 0) {
        return 3;
    } else if (section==1) {
        return 3;
    }
    return 0;
}

TableViewCellForRowAtIndexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            TwoLabelCell *cell = [TheGlobalMethod setCell:@"TwoLabelCell"];
            cell.backgroundColor = kBackGroudColor;
            cell.leftLabel.textColor = [UIColor lightGrayColor];
            cell.rightLabel.hidden = YES;
            cell.leftLabel.text = @"用户账户明细";
            cell.bottomView.hidden = YES;
            return cell;
        } else if (indexPath.row == 1) {
            TwoLabelCell *cell = [TheGlobalMethod setCell:@"TwoLabelCell"];
            cell.leftLabel.text = @"用户卡号";
            cell.rightLabel.text = self.userCard;
            return cell;
        } else if (indexPath.row == 2) {
            TwoLabelCell *cell = [TheGlobalMethod setCell:@"TwoLabelCell"];
            cell.leftLabel.text = @"用户余额";
            if (userMoney > 0) {
            cell.rightLabel.text = [NSString stringWithFormat:@"¥%.2f", userMoney];
            } else {
                cell.rightLabel.text = [NSString stringWithFormat:@"¥0"];
            }
            cell.rightLabel.textColor = [UIColor redColor];
            cell.bottomView.hidden = YES;
            return cell;
        }
    } else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            LabelTFCell *cell = [TheGlobalMethod setCell:@"LabelTFCell"];
            cell.rightTF.delegate = self;
            if (topupMoney.length > 0) {
                cell.rightTF.text = topupMoney;
            }
            return cell;
        } else if (indexPath.row == 1) {
            MyBalanceCell *cell = [TheGlobalMethod setCell:@"MyBalanceCell"];
            if ([myMoney floatValue] > 0) {
            cell.money.text = [NSString stringWithFormat:@"¥%@", myMoney];
            } else {
                cell.money.text = [NSString stringWithFormat:@"¥0"];
            }
            cell.backgroundColor = kBackGroudColor;
            return cell;
        } else if (indexPath.row == 2) {
            SelendBtnCell *cell = [TheGlobalMethod setCell:@"SelendBtnCell"];
            if ([topupMoney floatValue] > 0) {
            cell.label.text = [NSString stringWithFormat:@"确认充值%@", topupMoney];
            } else {
                cell.label.text = [NSString stringWithFormat:@"确认充值"];
            }
            return cell;
        }
    }
    return nil;
}

TableViewDidSelectRowAtIndexPath {
    if (indexPath.section == 1 && indexPath.row == 2) {
        if ([topupMoney floatValue] > [myMoney floatValue]) {
            [TheGlobalMethod xianShiAlertView:@"您的余额不足" controller:self];
        } else if ([topupMoney floatValue] <= 0) {
            [TheGlobalMethod xianShiAlertView:@"请输入充值余额" controller:self];
        } else {
            
            ZCTradeView *ZCT = [[ZCTradeView alloc] init];
            [ZCT show];
            ZCT.finish = ^void (NSString *str1) {
                
                [HttpUrl LTPOST:@"user/service/centre/recharge" dict:@{@"money":topupMoney, @"numberCard":self.userCard, @"payPassword":str1} WithSuccessBlock:^(id data) {
                    if ([data[@"success"] integerValue] == 1) {
                        [TheGlobalMethod popAlertView:@"充值成功" controller:self];
                    } else {
                        [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
                    }
                }];
                
            };
            
            
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    topupMoney = textField.text;
}

TableViewHeightForFooterInSection {
    return 12;
}

TableViewViewForFooterInSection {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kWidth, 12)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

TableViewCellHeight

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)pop:(UIBarButtonItem *)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
