//
//  OrderDetailVC.h
//  Shop
//
//  Created by ike on 16/12/12.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^ReturnBlock)(void);

@interface OrderDetailVC : UIViewController


@property(nonatomic,strong)NSString *titleString;

//新界面获取经纬度
@property(nonatomic,assign)float longitudeF ;//经度
@property(nonatomic,assign)float latitudeF ;//纬度

//类别，根据不同类别加载不同的视图


@property(nonatomic,assign)NSInteger orderClassifyName;
@property(nonatomic,copy)NSString *orderNum;//订单号

@property(nonatomic,assign)NSInteger QRRecord;
//回调block，界面刷新
@property(nonatomic,strong)ReturnBlock block;

//
@property (nonatomic,copy)NSString *eva;
@end
