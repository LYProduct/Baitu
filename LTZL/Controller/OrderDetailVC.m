//
//  OrderDetailVC.m
//  Shop
//
//  Created by ike on 16/12/12.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "OrderDetailVC.h"
//#import "PicDetailVC.h"
#import "Layout.h"
#import "ZCTradeView.h"




@interface OrderDetailVC (){

    NSMutableDictionary *dateDict;
    NSMutableDictionary *httpDict;
    
    NSArray *goodsAttList;
    
    NSMutableDictionary *content;
   
}

@property (weak, nonatomic) IBOutlet MyTableView *myTableView;

@end

@implementation OrderDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSLog(@"%f aa== %f",self.longitudeF,self.latitudeF);
    dateDict = [[NSMutableDictionary alloc]initWithCapacity:0];
    httpDict = [[NSMutableDictionary alloc]initWithCapacity:0];
    
    content = [[NSMutableDictionary alloc]initWithCapacity:0];
    
//    [self.myTableView showSection:0 uploadRows:0 layouatCell:@"BtnImgCell"];
//    [self.myTableView showLayoutSection:0 Type:0 Cells:@[@"BtnImgCell",@"LogoCell",@"PassWordCell"]];
//    [self.myTableView showSectionFooter:0 uploadDate:nil];
    
#pragma mark 布局订单详情
    if ([self.titleString isEqualToString:@"订单详情"]) {
        
        
        NSLog(@"Classify === %@ == Num  ==== %li ===",self.orderNum,(long)self.orderClassifyName);
//        self.myTableView.SelendIdCell = 1;
        
        //    待验证： 酒店0 KTV1  商城2 美食3 外卖4
        //    待发货   10
        //    待退款   100
        //    已完成   已退款1000   商城 10
        if (self.orderClassifyName == 0) {
            [dateDict addEntriesFromDictionary:[Layout hotelOrder]];
        }else if (self.orderClassifyName == 1){
            [dateDict addEntriesFromDictionary:[Layout KTVOrder]];
        }else if (self.orderClassifyName == 2){
            [dateDict addEntriesFromDictionary:[Layout clothesOrder]];
        }else if (self.orderClassifyName == 3){
            [dateDict addEntriesFromDictionary:[Layout foodOrder]];
        }else if (self.orderClassifyName == 4){
            [dateDict addEntriesFromDictionary:[Layout orderOrder]];//外卖
        }else if (self.orderClassifyName == 10 || self.orderClassifyName == 11 ){
            [dateDict addEntriesFromDictionary:[Layout fahuoOrder]];
        }else if (self.orderClassifyName == 100){
            [dateDict addEntriesFromDictionary:[Layout tuikuanOrder]];
        }else if(self.orderClassifyName == 1000){
             [dateDict addEntriesFromDictionary:[Layout overOrder]];
        }
        
        [self.myTableView showCell:dateDict title:self.titleString];
        self.myTableView.hidden = YES;
        [self uploadHttp];
       
        
    }
    
    
#pragma mark Btn
    [self.myTableView RetuenidCellBtnClick:^(NSIndexPath *indexPath, NSInteger btnTag, id data) {
        
        NSMutableDictionary *dictt = [NSMutableDictionary dictionaryWithCapacity:0];
        NSString *httpString = @"goods/order/user/order/list";
        [dictt setObject:@1 forKey:@"pageNum"];
        [dictt setObject:[NSNumber numberWithInt:Limit] forKey:@"pageSize"];
        [dictt setObject:@1 forKey:@"typeId"];
        
            if (btnTag == 800) {
//                PicDetailVC  *controller =[self.storyboard instantiateViewControllerWithIdentifier:@"PicDetailVC"];
//                controller.title = @"图文详情";
//                [self.navigationController pushViewController:controller animated:YES];
        
            }
            else if (btnTag == 1000) {
                
                //拒绝退款 弹窗 取消恢复 确定调改变接口改变成退款失败，刷新tableView
                //删除订单 调改变接口，刷新tableView
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"确定拒绝退款?"preferredStyle: UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    [dictt setObject:@"REFUND_APPLICATION" forKey:@"status"];
                     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    //请求1，看id
                    [HttpUrl LTPOST:httpString dict:dictt WithSuccessBlock:^(id data) {
                        if ([data[@"success"]integerValue] == 1) {
                            NSArray *arr = [NSArray arrayWithArray:data[@"data"][@"list"]] ;
                            NSString *str  = arr[indexPath.row][@"id"];
                            NSMutableDictionary *dicttt = [NSMutableDictionary dictionaryWithCapacity:0];
                            [dicttt setObject:@"REFUND_FAILED" forKey:@"status"];
                            [dicttt setObject:str forKey:@"id"];
                            
                            //请求2 更新
                            [HttpUrl LTPOST:@"goods/order/update" dict:dicttt WithSuccessBlock:^(id data) {
                                if ([data[@"success"]intValue]== 1) {
                                    
                                    SetPop;
//                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                    self.block();

                                }else{
                                    [TheGlobalMethod popAlertView:data[@"msg"] controller:self];
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                    
                                }
                            }];
                            
                        }
                        
                    }];
                    
                }]];
                [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                }]];
                //弹出提示框；
                [self presentViewController:alert animated:true completion:nil];
                
            }
        
            else if (btnTag == 1001){
                
                //同意退款 跳支付界面 输完调改变接口已退款，刷新tableView
                ZCTradeView *ZCT = [[ZCTradeView alloc] init];
                [ZCT show];
                
                NSString *find = @"/user/find/user";
                
                [HttpUrl LTPOST:find dict:nil WithSuccessBlock:^(id data) {
                    if ([data[@"success"]intValue] == 1) {
                        
                      NSString  *payPassword = data[@"data"][@"payPassword"];
                        
                        if ( [payPassword intValue] == 1) {
                            
                            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"请设置支付密码"]}];
                        }else{
                            NSLog(@"%@---pay",payPassword);
                            
                            
                            ZCT.finish = ^void (NSString *str1) {

                                
                                NSLog(@"%@---payyyy",payPassword);
                                [dictt setObject:@"REFUND_APPLICATION" forKey:@"status"];
                                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                
                                //请求1 看id
                                [HttpUrl LTPOST:httpString dict:dictt WithSuccessBlock:^(id data) {
                                    if ([data[@"success"]integerValue] == 1) {
                                        NSArray *arr = [NSArray arrayWithArray:data[@"data"][@"list"]] ;
                                        
                                        //请求2 update
                                        NSMutableDictionary *updateDict = [NSMutableDictionary dictionaryWithCapacity:0];
                                        NSString *strId  = arr[indexPath.row][@"id"];
                                        [updateDict setObject:@"REFUNDED" forKey:@"status"];
                                        [updateDict setObject:strId forKey:@"id"];
                                        [updateDict setObject:str1 forKey:@"payPassword"];
                                        NSLog(@"%@---payyyyl",updateDict);
                                        [HttpUrl LTPOST:@"goods/order/update" dict:updateDict WithSuccessBlock:^(id data) {
                                            
                                            if ([data[@"success"]intValue] == 1) {
//                                                NSString *str  = arr[indexPath.row][@"number"];
//                                                NSMutableDictionary *dicttt = [NSMutableDictionary dictionaryWithCapacity:0];
//                                                [dicttt setObject:str forKey:@"number"];
//                                                [dicttt setObject:str1 forKey:@"payPassword"];
                                                
//                                                NSLog(@"%@---payyyy，%@-----lll",str,str1);
//                                                //请求3 调余额支付接口
//                                                [HttpUrl LTPOST:@"goods/order/balance/pay" dict:dicttt WithSuccessBlock:^(id data) {
//                                                    if ([data[@"success"]intValue]== 1) {
//                                                        
                                                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                        [TheGlobalMethod popAlertView:@"退款成功" controller:self];
                                                        SetPop;
                                                        self.block();
//                                                    }
//                                                }];
                                            }else{
                                                [TheGlobalMethod popAlertView:data[@"msg"] controller:self];
                                                [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                
                                            }
                                        }];
                                        
                                    }
                                    
                                }];
                                
                                
//                                    [dictt setObject:@"REFUND_APPLICATION" forKey:@"status"];
//                                    [HttpUrl LTPOST:httpString dict:dictt WithSuccessBlock:^(id data) {
//                                        if ([data[@"success"]integerValue] == 1) {
//                                            NSArray *arr = [NSArray arrayWithArray:data[@"data"][@"list"]] ;
//                                            NSString *str  = arr[indexPath.row][@"number"];
//                                            NSMutableDictionary *dicttt = [NSMutableDictionary dictionaryWithCapacity:0];
//                                            [dicttt setObject:str forKey:@"id"];
//                                            [dicttt setObject:str1 forKey:@"number"];
//                                            //请求2 更新
//                                            [HttpUrl LTPOST:@"goods/order/balance/pay" dict:dicttt WithSuccessBlock:^(id data) {
//                                                if ([data[@"success"]intValue]== 1) {
//
//                                                    SetPop;
//                                                    self.block();
//                                                }
//                                            }];
//                                            
//                                        }
//                                        
//                                    }];

                                
                            };
                            
                            
                        }
                        
                    }else{
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":data[@"msg"]}];
                    }
                }];
                

            }//btntag
    }];
 
    
    
    
#pragma mark  cell点击事件
    //到图文详情，位置不同，图文详情不同
    [self.myTableView RetuenidSelendCell:^(NSIndexPath *indexPath, id data) {
        
        
        NSLog(@"2223");
        if([self.titleString isEqualToString:@"订单详情"]){
            if (_orderClassifyName == 0 ||_orderClassifyName == 10||_orderClassifyName == 1||_orderClassifyName == 2 ||_orderClassifyName == 11) {
                if (indexPath.section == 4) {
                    if (indexPath.row == 3 + goodsAttList.count) {
//                        PicDetailVC *controller =[self.storyboard instantiateViewControllerWithIdentifier:@"PicDetailVC"];
//                        controller.title = @"图文详情";
//                        controller.dataDic = content;
//                        [self.navigationController pushViewController:controller animated:YES];
                    }
                }
            }else if(_orderClassifyName == 3){
                if (indexPath.section == 3) {
                    if (indexPath.row == 3 + goodsAttList.count) {
//                        PicDetailVC *controller =[self.storyboard instantiateViewControllerWithIdentifier:@"PicDetailVC"];
//                        controller.title = @"图文详情";
//                        controller.dataDic = content;
//                        [self.navigationController pushViewController:controller animated:YES];
                    }
                }
                
            }

            }
    }];
}



#pragma mark 网络订单详情 
-(void)uploadHttp{

     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if ([self.titleString isEqualToString:@"订单详情"]) {
        
        //    待验证： 酒店0 KTV1  商城2 美食3 外卖4
        //    待发货   10
        //    待退款   100
        //    已完成   已退款1000   商城 10
        [httpDict setObject:[NSString stringWithFormat:@"%f",self.longitudeF] forKey:@"longitude"];
        [httpDict setObject:[NSString stringWithFormat:@"%f",self.latitudeF] forKey:@"latitude"];
        [httpDict setObject:self.orderNum forKey:@"orderNumber"];
        NSLog(@"cccc%@",httpDict);
        [HttpUrl LTPOST:@"goods/order/info/ios" dict:httpDict WithSuccessBlock:^(id data){
            
               NSLog(@"Classify === %@ == bum  ==== %li ===",self.orderNum,(long)self.orderClassifyName);
            if ([[data[@"data"][@"store"]allKeys] containsObject:@"content"]) {
                if ( data[@"data"][@"store"][@"content"] && ![data[@"data"][@"store"][@"content"] isEqual:[NSNull null]]) {
                     [content setObject:data[@"data"][@"store"][@"content"] forKey:@"content"];
                }
            }
            
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            NSMutableDictionary *dicti = [NSMutableDictionary dictionaryWithCapacity:0];
            [dicti setObject:data[@"data"] forKey:@"dataa"];
            [dicti setObject:[NSNumber numberWithInteger:_orderClassifyName] forKey:@"myClassfiy"];
            
            
            //这里
            NSDictionary *goodsList = [NSDictionary dictionaryWithDictionary: data[@"data"][@"GoodsOrderMeasuresList"][0]];
            //goods
            NSDictionary *goods = [NSDictionary dictionaryWithDictionary:goodsList[@"goods"]];
            //  NSDictionary *goodsAtt;
            
            if ([[goods allKeys]containsObject:@"goodsAttributeList"]) {
                goodsAttList = goods[@"goodsAttributeList"];
            }
            
            
            
            //商城,待发货,KTV,酒店
            if (self.orderClassifyName == 2 || self.orderClassifyName == 10 || self.orderClassifyName == 1 ||self.orderClassifyName == 0 ||self.orderClassifyName == 11) {
                if (_QRRecord == 1) {
                    [dicti setObject:@"已验证" forKey:@"QR"];
                }
                if ([self.eva isEqualToString:@"EVALUATION"]) {
                     [dicti setObject:@"待评价" forKey:@"EVALUATION"];
                }
               
                [self.myTableView showRowsNumber:4 uploadDate:(4 + goodsAttList.count)];//更改rows数
                
                for (int i = 0; i < goodsAttList.count; i++) {
                    [self.myTableView showSection:4 uploadRows:(i+3) layouatCell:@"OldCell"];
                }
                [self.myTableView showSection:4 uploadRows:(goodsAttList.count + 2) layouatCell:@"SignCell"];
                [self.myTableView showSection:4 uploadRows:(goodsAttList.count + 3) layouatCell:@"moneBtnCell"];
                
                for (int i = 0; i < 6; i++) {
                    [self.myTableView showSection:i uploadRows:0 uploadDate:dicti];
                    
                }
                self.myTableView.hidden  = NO;
             //美食
            }else if (self.orderClassifyName == 3 ){
                if (_QRRecord == 1) {
                    [dicti setObject:@"已验证" forKey:@"QR"];
                }
                
                [self.myTableView showRowsNumber:3 uploadDate:(4 + goodsAttList.count)];//更改rows数
                
                for (int i = 0; i < goodsAttList.count; i++) {
                    [self.myTableView showSection:3 uploadRows:(i+3) layouatCell:@"OldCell"];
                }
                [self.myTableView showSection:3 uploadRows:(goodsAttList.count + 2) layouatCell:@"SignCell"];
                [self.myTableView showSection:3 uploadRows:(goodsAttList.count + 3) layouatCell:@"moneBtnCell"];
                
                
                for (int i = 0; i < 5; i++) {
                    [self.myTableView showSection:i uploadRows:0 uploadDate:dicti];
                }
                self.myTableView.hidden  = NO;
            //外卖
            }else if (self.orderClassifyName == 4){
                if (_QRRecord == 1) {
                    [dicti setObject:@"已验证" forKey:@"QR"];
                }
                

                if ([[dicti[@"dataa"] allKeys] containsObject:@"GoodsOrderMeasuresList"]) {
                     NSArray *goodsList = dicti[@"dataa"][@"GoodsOrderMeasuresList"];
                      [dicti setObject:[NSNumber numberWithInteger: goodsList.count] forKey:@"rowsCount"];
                    [self.myTableView showRowsNumber:0 uploadDate:goodsList.count];
                }
               
                
                for (int i = 0; i < 4; i++) {
                    NSLog(@"--dicti-%@",dicti);
                    [self.myTableView showSection:i uploadRows:0 uploadDate:dicti];
                }
                
                self.myTableView.hidden  = NO;
            //待退款要第二个接口 已完成，已退款
            }else if (self.orderClassifyName == 100 ||self.orderClassifyName == 1000){
                [self.myTableView showSection:0 uploadRows:0 uploadDate:dicti];
                [self.myTableView showSection:1 uploadRows:0 uploadDate:dicti];
                [self.myTableView showSection:2 uploadRows:0 uploadDate:dicti];
                
                [HttpUrl LTPOST:@"refund/reason/find" dict:@{@"number":self.orderNum} WithSuccessBlock:^(id data) {
                    
                    [self.myTableView showSection:3 uploadRows:0 uploadDate:@{@"tuik":data[@"data"],@"classify":@100}];
                    [self.myTableView reloadData];
                    self.myTableView.hidden  = NO;
                }];
                //已完成 的已退款
            }

            [self.myTableView reloadData];
        }];

        
    }
}




#pragma mark 其他
- (IBAction)pop:(UIBarButtonItem *)sender {
    SetPop;
}




@end
