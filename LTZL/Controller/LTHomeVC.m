//
//  HomeVC.m
//  FC
//
//  Created by mc on 16/9/20.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "LTHomeVC.h"
#import "ScanningVC.h"
#import "UIButton+WebCache.h"
#import "mMoneyCell.h"
#import "OrderVC.h"
#import "MoneyLeftVC.h"
//#import "MessageVC.h"

@interface LTHomeVC ()<UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate>{
    NSArray *ImgArr;
    NSArray *labelArr;
    NSString *tfString;
    NSMutableDictionary *httpDict;
    __weak IBOutlet UIButton *mineBtn;
    __weak IBOutlet TelView *telView;
    __weak IBOutlet UILabel *TFView;
    __weak IBOutlet UIButton *cenu;
    __weak IBOutlet UIButton *messageBtn;
}
@property (weak, nonatomic) IBOutlet UICollectionView *myCollection;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *telView_BT;
@property (weak, nonatomic) IBOutlet UIButton *saomiao;
@property (weak, nonatomic) IBOutlet UIButton *yanzhengjilu;

@property (weak, nonatomic) IBOutlet UILabel *name;


@property (nonatomic,strong)NSMutableDictionary *dataDict;

@property (nonatomic,copy)NSString *headInfo;
@end

@implementation LTHomeVC
@synthesize myCollection,telView_BT;
-(void)viewWillAppear:(BOOL)animated{
    if(self.dataDict != nil){
        [self uploadHttp];
    }
    self.navigationController.navigationBarHidden = YES;
    
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self uploadHttp];
    
    
    
    telView_BT.constant = -(HEIGHT_K / 3 * 2);
    myCollection.delegate = self;
    myCollection.dataSource = self;
    [myCollection registerNib:[UINib nibWithNibName:@"HomeCategoryCVCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"cellID"];
    [myCollection registerNib:[UINib nibWithNibName:@"mMoneyCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"mMoneyCell"];
    [myCollection registerNib:[UINib nibWithNibName:@"SelendTwoCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"cellTop"];
    labelArr = @[@"订单管理",@"财务服务",@"我的钱包",@"到店付记录",@"我的二维码",@"我的团队"];
    ImgArr = @[@"shanghu_index01",@"shanghu_index02",@"shanghu_index03",@"shanghu_index04",@"shanghu_index05",@"shanghu_index06"];
    
    
    UITapGestureRecognizer* singleRecognizer;
    singleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(SingleTap)];
    //点击的次数
    singleRecognizer.numberOfTapsRequired = 1; // 单击
    TFView.userInteractionEnabled = YES;
    //给self.view添加一个手势监测；
    [TFView addGestureRecognizer:singleRecognizer];
    
    tfString =@"";
    telView.VerifyBtn=^(long Selend){
        
        if (Selend == 100) {
            
            if ([TFView.text isEqualToString:@"请输入消费券验证码"]) {
                [TheGlobalMethod xianShiAlertView:@"请输入验证码号" controller:self];
                return;
            }
            
            [HttpUrl LTPOST:@"goods/order/info/merchant" dict:@{@"goodsBuyNumber":TFView.text} WithSuccessBlock:^(id data) {
                if ([data[@"success"] intValue] == 1) {
                    
                    InfoVC *vc = [TheGlobalMethod setstoryboard:@"InfoVC" controller:self];
                    vc.titleString = @"验券结果";
                    [self.navigationController pushViewController:vc animated:YES];
                } else {
                    [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
                }
            }];
            
            
            [TFView endEditing:YES];
            [UIView animateWithDuration:1.0 // 动画时长
                                  delay:1.0 // 动画延迟
                 usingSpringWithDamping:1.0 // 类似弹簧振动效果 0~1
                  initialSpringVelocity:1.0 // 初始速度
                                options:UIViewAnimationOptionCurveEaseInOut // 动画过渡效果
                             animations:^{
                                 // code...
                                 CGPoint point = telView.center;
                                 point.y += HEIGHT_K/ 3 * 2;
                                 [telView setCenter:point];
                                 telView_BT.constant -= HEIGHT_K / 3 * 2;
                             } completion:^(BOOL finished){
                                 telView_BT.constant = -(HEIGHT_K / 3 * 2);
                             }];
        }else{
            TFView.textColor = [UIColor blackColor];
            cenu.hidden = NO;
            tfString = [NSString stringWithFormat:@"%@%ld",tfString,Selend];
            TFView.text = tfString;
        }
        
    };
}
-(void)SingleTap{
    [UIView animateWithDuration:0.8 // 动画时长
                          delay:0.1 // 动画延迟
         usingSpringWithDamping:0.8 // 类似弹簧振动效果 0~1
          initialSpringVelocity:1.0 // 初始速度
                        options:UIViewAnimationOptionCurveEaseInOut // 动画过渡效果
                     animations:^{
                         // code...
                         CGPoint point = telView.center;
                         point.y = HEIGHT_K / 3 * 2;
                         [telView setCenter:point];
                     } completion:^(BOOL finished){
                         telView_BT.constant = 0;
                     }];
}
#pragma mark 我的
- (IBAction)ImeBtnClick:(id)sender {
    
//    InfoVC *Controller = [self.storyboard instantiateViewControllerWithIdentifier:@"InfoVC"];
//    Controller.titleString = @"我的";
//    Controller.title = @"我的";
//    Controller.headIcon = self.headInfo;
//    Controller.block = ^(void){
//        [self uploadHttp];
//        
//    };
//    [self.navigationController pushViewController:Controller animated:NO];
    
}
- (IBAction)removeTfView:(id)sender {
    tfString = @"";
    cenu.hidden = YES;
    TFView.text = @"请输入消费券优惠码";
    TFView.textColor = GrayColor_K;
}
#pragma mark 消息
- (IBAction)newsBtn:(id)sender {
    NSLog(@"消息");
    //    EaseMessageViewController *chatController = [[EaseMessageViewController alloc] initWithConversationChatter:@"8001" conversationType:EMConversationTypeChat];
    //    chatController.title = @"8001";
    
//    EaseConversationListViewController *listViewController = [[EaseConversationListViewController alloc] init];
//    [self.navigationController pushViewController:listViewController animated:YES];
    
}
- (IBAction)hideTelView:(id)sender{
    if (tfString.length == 0) {
        cenu.hidden = YES;
        TFView.text = @"请输入消费券优惠码";
        TFView.textColor = GrayColor_K;
    }
    [UIView animateWithDuration:0.8 // 动画时长
                          delay:0.1 // 动画延迟
         usingSpringWithDamping:0.8 // 类似弹簧振动效果 0~1
          initialSpringVelocity:1.0 // 初始速度
                        options:UIViewAnimationOptionCurveEaseInOut // 动画过渡效果
                     animations:^{
                         // code...
                         CGPoint point = telView.center;
                         point.y += HEIGHT_K/ 3 * 2;
                         [telView setCenter:point];
                         telView_BT.constant -= HEIGHT_K / 3 * 2;
                     } completion:^(BOOL finished){
                         telView_BT.constant = -(HEIGHT_K / 3 * 2);
                     }];
}

//有几个区
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 3;
}

//每个区有几个item
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(section == 0 || section == 1){
        return 1;
    }else{
        return labelArr.count;
    }
}
//重用cell
#pragma mark CollectionSelfCell
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 1) {
        mMoneyCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"mMoneyCell" forIndexPath:indexPath];
        return cell;
    }else if(indexPath.section == 0){
        SelendTwoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellTop" forIndexPath:indexPath];
        NSLog(@"%@----",self.dataDict);
        cell.leftLabel.text = [NSString stringWithFormat:@"%d",[self.dataDict[@"orderNumber"] intValue]];
        cell.rightLabel.text = [NSString stringWithFormat:@"%.2f",[self.dataDict[@"sum"] floatValue]];
        return cell;
    }else{
        HomeCategoryCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellID" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
        cell.Img.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",ImgArr[indexPath.row]]];
        cell.label.text = labelArr[indexPath.row];
        return cell;
    }
    
}
//定义每个UICollectionViewCell 的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return CGSizeMake(WIDTH_K,60);
    }else if (indexPath.section == 1){
        return CGSizeMake(WIDTH_K, 50);
    }
    else{
        return CGSizeMake(WIDTH_K/3 - 1, WIDTH_K/3-1);
    }
}
//定义每个Section 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);//分别为上、左、下、右
}

///-----
//选择了某个cell
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        
        MoneyLeftVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MoneyLeftVC"];
        vc.title = @" 充值";
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [hud hideAnimated:YES afterDelay:5];
            
            [HttpUrl LTPOST:@"goods/order/user/order/list" dict:@{@"typeId":@"1"} WithSuccessBlock:^(id data) {
                if (![data[@"code"]isEqualToString:@"EMPTY"] ) {
                    
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    OrderVC *Controller = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderVC"];
                    Controller.titleString = @"订单管理";
                    Controller.title = @"订单管理";
                    [self.navigationController pushViewController:Controller animated:YES];
                }else{
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    [TheGlobalMethod popAlertView:@"没有订单管理记录" controller:self];
                }
                
            }];
            
            
        }else
            if (indexPath.row == 1) {
                InfoVC *Controller = [self.storyboard instantiateViewControllerWithIdentifier:@"InfoVC"];
                Controller.titleString = @"财务管理";
                Controller.title = @"财务管理";
                [self.navigationController pushViewController:Controller animated:YES];
            }else
                if (indexPath.row == 2) {
                    InfoVC *Controller = [self.storyboard instantiateViewControllerWithIdentifier:@"InfoVC"];
                    Controller.titleString = @"钱包";
                    Controller.title = @"钱包";
                    [self.navigationController pushViewController:Controller animated:YES];
                }else
                    
                    if (indexPath.row == 3) {
                        
                        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                        [hud hideAnimated:YES afterDelay:5];
                        
                        [HttpUrl LTPOST:@"user/amount/record/merchant/pay/record" dict:nil WithSuccessBlock:^(id data) {
                            if ([data[@"success"]intValue] == 1) {
                                [MBProgressHUD hideHUDForView:self.view animated:YES];
                                if ([data[@"data"][@"size"] intValue]!= 0) {
                                    InfoVC *Controller = [self.storyboard instantiateViewControllerWithIdentifier:@"InfoVC"];
                                    Controller.titleString = @"到店付记录";
                                    Controller.title = @"到店付记录";
                                    [self.navigationController pushViewController:Controller animated:YES];
                                }else{
                                    
                                    [TheGlobalMethod popAlertView:@"没有到店付记录" controller:self];
                                }
                            }
                        }];
                        
                        
                    }else
                        if (indexPath.row == 4) {
                            InfoVC *Controller = [self.storyboard instantiateViewControllerWithIdentifier:@"InfoVC"];
                            Controller.titleString = @"我的二维码";
                            Controller.title = @"我的二维码";
                            [self.navigationController pushViewController:Controller animated:YES];
                        }else
                            if (indexPath.row == 5) {
                                InfoVC *Controller = [self.storyboard instantiateViewControllerWithIdentifier:@"InfoVC"];
                                Controller.titleString = @"我的团队";
                                Controller.title = @"我的团队";
                                [self.navigationController pushViewController:Controller animated:YES];
                            }
    }
    
    
}
//每个item之间的间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
//返回头footerView的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    CGSize size;
    if (section == 0) {
        size = CGSizeMake(WIDTH_K, 8);
    }else{
        size=CGSizeMake(WIDTH_K, 8);
    }
    return size;
}
//每个section中不同的行之间的行间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 1;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark 按钮事件
- (IBAction)yanzhengjilu:(UIButton *)sender {
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud hideAnimated:YES afterDelay:5];
    [HttpUrl LTPOST:@"goods/order/buy/number/record" dict:nil WithSuccessBlock:^(id data) {
        if ([data[@"success"]intValue] == 1) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if ([data[@"data"][@"goodsOrderListPage"][@"size"]intValue] != 0) {
                InfoVC *Controller = [self.storyboard instantiateViewControllerWithIdentifier:@"InfoVC"];
                Controller.titleString = @"验证记录";
                Controller.title = @"验证记录";
                [self.navigationController pushViewController:Controller animated:YES];
            }else{
                [ TheGlobalMethod popAlertView:@"没有验证记录" controller: self];
            }
            
        }
        
    }];
    
    
}


- (IBAction)saomiaoyanzheng:(UIButton *)sender {
    ScanningVC *Controller = [self.storyboard instantiateViewControllerWithIdentifier:@"ScanningVC"];
    Controller.title = @"扫一扫";
    Controller.index = 1;
    [self.navigationController pushViewController:Controller animated:YES];
}

-(void)uploadHttp{
    
    [HttpUrl POST:@"goods/order/merchant/index" dict:nil sign:nil WithSuccessBlock:^(id data) {
        
        [self.dataDict addEntriesFromDictionary:data[@"data"]];
        [mineBtn sd_setImageWithURL:[self.dataDict objectForKey:@"headUrl"] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"moren_touxiang44"]];
        
        self.headInfo = [self.dataDict objectForKey:@"headUrl"];
        self.name.text = [self.dataDict objectForKey:@"name"];
        [myCollection reloadData];
        
    }];
    
}




-(NSMutableDictionary *)dataDict{
    if (!_dataDict) {
        _dataDict = [NSMutableDictionary dictionaryWithCapacity:0];
    }
    return _dataDict;
}
@end
