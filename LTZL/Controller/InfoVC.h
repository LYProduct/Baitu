//
//  InfoVC.h
//  FC
//
//  Created by mc on 16/9/26.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

typedef void(^headBlock)(void);

@interface InfoVC : UIViewController<CLLocationManagerDelegate,SRActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property(nonatomic,strong)NSString *titleString;
@property(nonatomic,strong)NSDictionary *userDict;

//消费券验证码号
@property(nonatomic,copy)NSString *goodsBuyNumber;
@property(nonatomic,copy)NSString *numberCard;

@property(nonatomic,copy)NSString *number;
@property(nonatomic,copy)NSString *storeClasslyName;


@property(nonatomic,assign)float totalMoney;//总价
@property(nonatomic,assign)float noMoney;//不参与优惠金额


//头像获取
@property(nonatomic,copy)NSString *headIcon;

@property(nonatomic,copy)headBlock block;

@end
