
//
//  InfoVC.m
//  FC
//
//  Created by mc on 16/9/26.
//  Copyright © 2016年 mc. All rights reserved.
//
#import "CTimeCell.h"
#import "InfoVC.h"
//#import "ChangeVC.h"//修改昵称界面
#import "SelectDateCell.h"//选择出生日期cell
#import "qianbaoheadCell.h"
#import "Layout.h"
#import "ScanningVC.h"
#import "HeadInfoCell.h"
#import "ZCTradeView.h"
//#import "OrderDetailVC.h"
#import "SetPayPasswordVC.h"
#import "HomeVC.h"
#import "SRActionSheet.h"
//#import "VerifyVC.h"
//#import <ShareSDK/ShareSDK.h>


@interface InfoVC (){
    NSMutableDictionary *dateDict;
    int pageNum;
//    NSMutableDictionary *dateTixianDict;
    NSMutableDictionary *httpDict;
    
    NSMutableDictionary *caiwuDict;
    NSArray *mjArray;
    NSArray *dataArr;
    
    NSMutableArray *dataArray;//分页array
    long typeBtn;

    float latitude;//纬度
    float longitude;//经度
    
    NSString *leftMoney;//余额
    NSString *userName;//用户名
    NSString *password;//密码
    NSString *payPassword;//支付密码
    NSString *phone;//电话
    NSInteger success;
    
    NSString *beginTime;
    NSString *endTime;
    
    //财务管理的
    NSInteger change;//换，到店付1 团购2
    
    NSString *oneC;
    NSString *twoC;
    NSString *threeC;
    
    //支付，优惠金额
    float youhui;
    float payMoney;//最后支付金额
    
    //用户的钱 支付用
     NSString *userMoney;
   
    NSString *stringUrl;
}

@property(nonatomic,strong)UIImageView *navigationImageView;

@property (strong,nonatomic)CLLocationManager *locManager;//获取经纬度的属性

//@property (nonatomic, assign) SROtherActionItemAlignment otherActionItemAlignment;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableView_TH;
@property (weak, nonatomic) IBOutlet MyTableView *myTableView;
//分类view
@property (weak, nonatomic) IBOutlet UIView *typeView;
//验证记录view
@property (weak, nonatomic) IBOutlet UIView *teamView;
//团队view
@property (weak, nonatomic) IBOutlet UIView *myTeamView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bView_H;
//验证记录
@property (weak, nonatomic) IBOutlet UIButton *moneyAllBtn;
@property (weak, nonatomic) IBOutlet UIButton *moneyLingqianBtn;
@property (weak, nonatomic) IBOutlet UIButton *moneyLoadBtn;
@property (weak, nonatomic) IBOutlet UIButton *moneyFailBtn;

//订单管理
@property (weak, nonatomic) IBOutlet UIButton *daiyanzhengBtn;
@property (weak, nonatomic) IBOutlet UIButton *daifahuoBtn;
@property (weak, nonatomic) IBOutlet UIButton *daituikuanBtn;
@property (weak, nonatomic) IBOutlet UIButton *yiwanchengBtn;
//团队
@property (weak, nonatomic) IBOutlet UIButton *oneClass;
@property (weak, nonatomic) IBOutlet UIButton *twoClass;
@property (weak, nonatomic) IBOutlet UIButton *threeClass;

//买单的，支付确认的
@property(nonatomic,copy)NSString *man;//
@property(nonatomic,copy)NSString *jian;//


//点击cell的订单号(订单列表)
@property(nonatomic,copy)NSString *orderNum;
@property(nonatomic,copy)NSString *orderClassifyName;

@end

@implementation InfoVC
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = NO;
    
    if ([self.titleString isEqualToString:@"钱包"]) {
        self.navigationImageView.hidden = YES;
    }
    [super viewWillAppear:animated];
    
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    if ([self.titleString isEqualToString:@"钱包"]) {
        self.navigationImageView.hidden = NO;
    }


}
- (void)viewDidLoad {
    [super viewDidLoad];


    dateDict = [[NSMutableDictionary alloc]initWithCapacity:0];
    httpDict = [[NSMutableDictionary alloc]initWithCapacity:0];
    
    beginTime = [[TheGlobalMethod dateStr:[NSString stringWithFormat:@"%ld", time(NULL)-3600*24*31]]substringToIndex:10 ];
    endTime = [[TheGlobalMethod dateStr:[NSString stringWithFormat:@"%ld", time(NULL)]]substringToIndex:10];
    
#pragma mark 支付确认
    if ([self.titleString isEqualToString:@"支付确认"]) {
        //        self.myTableView.SelendIdCell = 1;
        [dateDict addEntriesFromDictionary:[Layout vorder]];
        [self.myTableView showCell:dateDict title:self.titleString];
        [self uploaderHttp];

    }else
#pragma mark 买单
    if ([self.titleString isEqualToString:@"买单"]) {
        [dateDict addEntriesFromDictionary:[Layout order]];
        self.myTableView.scrollEnabled = NO;
        [self.myTableView showCell:dateDict title:self.titleString];
        [self uploaderHttp];
    }else

#pragma mark 提现填写
        if ([self.titleString isEqualToString:@"提现填写"]) {
            [dateDict addEntriesFromDictionary:[Layout border]];
            self.myTableView.scrollEnabled = NO;
            [self.myTableView showCell:dateDict title:self.titleString];
//            [self uploaderHttp];
        }else
#pragma mark 我的二维码
    if ([self.titleString isEqualToString:@"我的二维码"]) {
        [dateDict addEntriesFromDictionary:[Layout corder]];
//        self.myTableView.scrollEnabled = NO;
        self.myTableView.SelendIdCell = 1;
        self.teamView.hidden = YES;
        self.myTeamView.hidden  =YES;
        self.typeView .hidden =YES;
        self.myTableView.hidden = YES;
        self.myTableView.backgroundColor = [UIColor whiteColor];
        [self.myTableView showCell:dateDict title:self.titleString];
        [self uploaderHttp];
    }else

#pragma mark 验券结果
    if ([self.titleString isEqualToString:@"验券结果"]) {
        self.locManager = [[CLLocationManager alloc] init];
        self.locManager.delegate = self;
        self.locManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.locManager.distanceFilter = 5.0;
        [self.locManager startUpdatingLocation];
        
        [dateDict addEntriesFromDictionary:[Layout eorder]];
        [self.myTableView showCell:dateDict title:self.titleString];
        //ss加打印
        
        [self uploaderHttp];
    }else

#pragma mark 验证记录
    if ([self.titleString isEqualToString:@"验证记录"]) {
        self.locManager = [[CLLocationManager alloc] init];
        self.locManager.delegate = self;
        self.locManager.desiredAccuracy = kCLLocationAccuracyBest;
        self.locManager.distanceFilter = 5.0;
        [self.locManager startUpdatingLocation];
        
        [dateDict addEntriesFromDictionary:[Layout forder]];
//        self.myTableView.SelendIdCell = 1;
        [self.myTableView showCell:dateDict title:self.titleString];
        [self uploaderHttp];
        [self mjPageViewDidLoad];
    }else
  
#pragma mark 我的钱包
    if ([self.titleString isEqualToString:@"钱包"]) {
//        UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc]initWithTitle:@"提现记录" style:UIBarButtonItemStyleDone target:self action:@selector(doClickTixian)];
//        self.navigationItem.rightBarButtonItem = rightBarItem;
//        self.navigationItem.rightBarButtonItem.tintColor = [UIColor whiteColor];
        self.myTableView.SelendIdCell = 1;
        
        UIImageView *navigationImageView = [self findHairlineImageViewUnder:self.navigationController.navigationBar];
        self.navigationImageView = navigationImageView;
        
        [dateDict addEntriesFromDictionary:[Layout gorder]];
        [self.myTableView showCell:dateDict title:self.titleString];
        [self uploaderHttp];
        [self mjPageViewDidLoad];
    }else
#pragma mark 我的团队
    if ([self.titleString isEqualToString:@"我的团队"]) {
        [dateDict addEntriesFromDictionary:[Layout horder]];
        self.tableView_TH.constant = 50;
        self.myTeamView.hidden = NO;
        self.teamView.hidden = YES;
        self.typeView.hidden = YES;
        typeBtn = 80;
        [self.myTableView showCell:dateDict title:self.titleString];
        [self uploaderHttp];
    
    }else
#pragma mark 到店付记录
    if ([self.titleString isEqualToString:@"到店付记录"]) {
        self.myTableView.SelendIdCell = 1;
        [dateDict addEntriesFromDictionary:[Layout iorder]];
        [self.myTableView showCell:dateDict title:self.titleString];
//        self.myTableView.hidden = YES;
//        self.myTeamView.hidden = YES;
//        self.teamView.hidden = YES;
//        self.typeView.hidden = YES;
        [self uploaderHttp];
        [self mjPageViewDidLoad];
    }else
    
#pragma mark 财务管理
    if ([self.titleString isEqualToString:@"财务管理"]){
        [dateDict addEntriesFromDictionary:[Layout jorder]];
        self.myTableView.SelendIdCell = 1;
        caiwuDict = [[NSMutableDictionary alloc]initWithCapacity:0];
        [self.myTableView showCell:dateDict title:self.titleString];
        [self uploaderHttp];
        [self mjPageViewDidLoad];
        
    }else

#pragma mark 我的
    if ([self.titleString isEqualToString:@"我的"]) {
        [dateDict addEntriesFromDictionary:@{@(0):@{@"Rows":@(1),
                                                    @"Header":@{@"Bool":@"Yes"},
                                                    @(0):@{@"NameCell":@"HeadInfoCell"}},
                                             @(1):@{@"Rows":@(1),
                                                    @"Header":@{@"Bool":@"Yes"},
                                                    @(0):@{@"NameCell":@"mphoneNumCell"}},
                                             @(2):@{@"Rows":@(2),
                                                    @"Header":@{@"Bool":@"Yes"},
                                                    @(0):@{@"NameCell":@"mNamesCell"}},
                                             @(3):@{@"Rows":@(1),
                                                    @"Header":@{@"Bool":@"Yes"},
                                                    @(0):@{@"NameCell":@"mNamesCell"}},
                                             @(4):@{@"Rows":@(1),
                                                    @"Header":@{@"Bool":@"Yes"},
                                                    @(0):@{@"NameCell":@"SelendBtnCell",@"data":@"退出登录"}}}];

        [self.myTableView showCell:dateDict title:self.titleString];
        [self uploaderHttp];
    }else
#pragma mark 消息中心
    if([self.titleString isEqualToString:@"消息中心"]){
        [dateDict addEntriesFromDictionary:[Layout torder]];
        [self.myTableView showCell:dateDict title:self.titleString];
#pragma mark 设置
    }else{
        [dateDict addEntriesFromDictionary:[Layout uorder]];
        [self.myTableView showCell:dateDict title:@"设置"];
    }
    
#pragma mark -------------textField
    [self.myTableView RetuenidCellText:^(NSIndexPath *indexPath, NSInteger tag, id data) {
       
        if ([self.titleString isEqualToString:@"提现填写"]) {
            if (indexPath.section == 0) {
                if (indexPath.row == 0) {
                     [httpDict setObject:data forKey:@"storeName"];
                }else if (indexPath.row == 1){
                     [httpDict setObject:data forKey:@"userName"];
                }else if (indexPath.row == 2){
                     [httpDict setObject:data forKey:@"phone"];
                }else if (indexPath.row == 3){
                     [httpDict setObject:data forKey:@"idCard"];
                }
                [self.myTableView showSection:0 uploadRows:0 uploadDate:httpDict];//?
            }else if (indexPath.section == 1){
                if (indexPath.row == 0) {
                     [httpDict setObject:data forKey:@"money"];
                }else if (indexPath.row == 1){
                     [httpDict setObject:data forKey:@"remark"];
                }
                [self.myTableView showSection:1 uploadRows:0 uploadDate:httpDict];
            }

        }else if ([self.titleString isEqualToString:@"买单"]){
        
            if (indexPath.section == 0) {
                _totalMoney = [data floatValue];
                [self.myTableView showSection:0 uploadRows:0 uploadDate:[NSNumber numberWithFloat: _totalMoney]];//总价

            }else if (indexPath.section == 1){
                _noMoney = [data floatValue];
                [self.myTableView showSection:1 uploadRows:0 uploadDate:[NSNumber numberWithFloat: _noMoney]];//不参与优惠金额
                
            }
            
        }
        
        
    }];
    __weak typeof(self)weakSelf = self;//
#pragma mark -------------  Btn-----------loading
    [self.myTableView RetuenidCellBtnClick:^(NSIndexPath *indexPath, NSInteger btnTag, id data) {
 
        if ([self.titleString isEqualToString:@"我的二维码"]) {
            
        }else if ([self.titleString isEqualToString:@"财务管理"]) {
            if(indexPath.section == 0){
                [self selectNormalDate:btnTag];}
            else if (indexPath.section == 1){
                if (btnTag == 200) {
                    
                    //左边接的是 带团购的接口
                    [self.myTableView showSection:1 uploadRows:0 layouatCell:@"mcaiwuCell"];
                     change = 2;//订单管理的参数
                    [self uploaderHttp];
                    [caiwuDict setObject:@(0) forKey:@"selend"];
                    [self.myTableView showSectionHeader:1 uploadDate:caiwuDict];
                    [self mjPageViewDidLoad];
                    
                }else if (btnTag == 201){
                    
                    //右边接的是到店付记录
                    [caiwuDict setObject:@(1) forKey:@"selend"];
                    [self.myTableView showSection:1 uploadRows:0 layouatCell:@"mdaodianCell"];
                    [self.myTableView showSectionHeader:1 uploadDate:caiwuDict];
                    change = 1;//订单流水的参数
                    [self uploaderHttp];
                    
                    [self mjPageViewDidLoad];

                
                }
            }
        }else if([self.titleString isEqualToString:@"钱包"])
        {
            if (indexPath.section == 0 && indexPath.row == 0) {
                
                InfoVC *controller =[self.storyboard instantiateViewControllerWithIdentifier:@"InfoVC"];
                controller.title = @"提现填写";
                controller.titleString = @"提现填写";
                [self.navigationController pushViewController:controller animated:YES];
            }else if(indexPath.section == 1 ){
                [self selectNormalDate:btnTag];}
            
            }else if([self.titleString isEqualToString:@"验证记录"]){
                [self selectNormalDate:btnTag];
            }else if([self.titleString isEqualToString:@"记录"]){
                [self selectNormalDate:btnTag];
            }else if ([self.titleString isEqualToString:@"到店付记录"]){
                [self selectNormalDate:btnTag];
            }
    }];
  
#pragma mark ------------- 点击cell事件
    [self.myTableView RetuenidSelendCell:^(NSIndexPath *indexPath, id data) {
        
#pragma mark  支付确认cell点击
        if ([self.titleString isEqualToString:@"支付确认"]) {
            if (indexPath.section == 2) {
                
            ZCTradeView *ZCT = [[ZCTradeView alloc] init];
            [ZCT show];
                
                NSString *find = @"/user/find/user";
                
                [HttpUrl LTPOST:find dict:nil WithSuccessBlock:^(id data) {
                    if ([data[@"success"]intValue] == 1) {
                        leftMoney = data[@"data"][@"balance"];
                        password = data[@"data"][@"password"];
                        payPassword = data[@"data"][@"payPassword"];
//                        NSString *numberCard = data[@"data"][@"membershipCard"][@"numberCard"];
                        NSLog(@"pay ===%@",payPassword);
                        if ([payPassword intValue] ==1) {
                            
                            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"请设置支付密码"]}];
                        }else{
                            
                            ZCT.finish = ^void (NSString *str1){
                                
//                                if (! [str1 isEqualToString:payPassword ]) {
//                                    [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"请输入正确的支付密码!"]}];
//                                    
//                                }else{
                                
                                    //输入了正确的密码
                                    NSMutableDictionary *dictt = [NSMutableDictionary dictionaryWithCapacity:0];

                                    [dictt setObject:[NSString stringWithFormat:@"%@",self.numberCard] forKey:@"numberCard"];//卡号查找得
                                    
                                    if ( _noMoney != 0) {
                                        [dictt setObject:[NSString stringWithFormat:@"%f",_noMoney]  forKey:@"noMoney"];}//不参与优惠push得
                                
                                    if (youhui != 0) {
                                         [dictt setObject:[NSString stringWithFormat:@"%f",youhui]  forKey:@"discountMoney"];//优惠计算得
                                    }
                                  
                                    [dictt setObject:[NSString stringWithFormat:@"%f",payMoney] forKey:@"money"];//支付金额计算得
                                    [dictt setObject:str1 forKey:@"payPassword"];//支付密码查找得
                                    [dictt setObject:[NSString stringWithFormat:@"%f",_totalMoney]  forKey:@"totalMoney"];//总价push得
                                
                                    NSLog(@"zhifu dictt:%@",dictt);
                                
                                    [HttpUrl LTPOST:@"user/merchent/pay" dict:dictt WithSuccessBlock:^(id data) {
                                        if ([data[@"success"] integerValue] == 1) {
                                          
//到店付打印ss
                                            
                                            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"付款成功" preferredStyle:(UIAlertControllerStyleAlert)];
                                          
                                            UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                                                
                                                [self.navigationController popToRootViewControllerAnimated:YES];
                                            

                                            }];
                                            [alert addAction:action];
                                            [self presentViewController:alert animated:YES completion:nil];

                                          
                                        } else {
                                              NSLog(@"jjjjjj");
                                            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
                                        }
                                    }];
                                    
                                    

                            
                            };
                        }
                        
                    }else{
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":data[@"msg"]}];
                    }
                }];
                
        
        }
        }else if([self.titleString isEqualToString:@"买单"]){
            if([self.titleString isEqualToString:@"买单"]){
                [self iselendIdCell:indexPath data:data];
            }
        }else if([self.titleString isEqualToString:@"提现填写"]){
            if([self.titleString isEqualToString:@"提现填写"]){
                [self hselendIdCell:indexPath data:data];
            }
        }else if([self.titleString isEqualToString:@"验券结果"]){
            [self selendIdCell:indexPath data:data];

        }else if([self.titleString isEqualToString:@"验证记录"]){
            [self bselendIdCell:indexPath data:data];
        }else if([self.titleString isEqualToString:@"订单管理"]){
            
//            [self cselendIdCell:indexPath data:data];
            
        }else if([self.titleString isEqualToString:@"我的团队"]){
            
        }
        else if ([self.titleString isEqualToString:@"我的"]) {
            
            [self dselendIdCell:indexPath data:data];
            
        }
    }];
    
}


#pragma mark -----------Cell点击事件--------------
#pragma mark Cell点击买单
-(void)iselendIdCell:(NSIndexPath *)indexPath data:(id)data{
    if (indexPath.section == 3) {
        
        InfoVC *controller =[self.storyboard instantiateViewControllerWithIdentifier:@"InfoVC"];
        controller.title = @"支付确认";
        controller.titleString = @"支付确认";
        controller.noMoney = _noMoney;//不参与优惠金额
        controller.totalMoney = _totalMoney;//消费总金额
        controller.man = _man;//满
        controller.jian = _jian;//减
        controller.numberCard = self.numberCard;
        if (_totalMoney == 0) {
              [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":@"请输入金额"}];
        }else{
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}


#pragma mark Cell点击 提现填写
-(void)hselendIdCell:(NSIndexPath *)indexPath data:(id)data{
    if (indexPath.section == 2) {

        if ([self.titleString isEqualToString:@"提现填写"])
        {
            //1.获取正确的个人信息
            NSString *find = @"/user/find/user";
            
            [HttpUrl LTPOST:find dict:nil WithSuccessBlock:^(id data) {
                if ([data[@"success"]intValue] == 1) {
                    leftMoney = data[@"data"][@"balance"];
                    
                    password = data[@"data"][@"password"];
                    payPassword = data[@"data"][@"payPassword"];
                    phone = data[@"data"][@"phone"];
                    if ( [payPassword intValue]==1) {
                        
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"请设置支付密码"]}];
                    }else{
                        
                        //2.做判断
                        if (![httpDict objectForKey:@"storeName"]) {
                            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"请输入正确店铺名称!"]}];
                        }else if (![httpDict objectForKey:@"userName"] ) {
                            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"请输入正确用户名称!"]}];
                        }else if (![[httpDict objectForKey:@"phone"] isEqualToString:[TheGlobalMethod getUserDefault:@"phone"]]) {
                            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"请输入正确手机号!"]}];
                        }else if ([[httpDict objectForKey:@"idCard"]length] != 18 ) {
                            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"请输入正确身份证号!"]}];
                        }else if ([[httpDict objectForKey:@"money"]floatValue] > [leftMoney floatValue] ) {
                            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"余额不足!"]}];
                        }else
                            
                        {
                            
                            //1.弹出框
                            ZCTradeView *ZCT = [[ZCTradeView alloc] init];
                            [ZCT show];
                            ZCT.finish = ^void (NSString *str1) {
                                
                                //2.判断支付密码是否正确
//                                if (! [str1 isEqualToString:payPassword ]) {
//                                    [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"请输入正确的支付密码!"]}];}
//                                else{
                                    [httpDict setObject:str1 forKey:@"payPassword"];
                                    //3.POST
                                    [HttpUrl LTPOST:@"user/present/table/insert" dict:httpDict WithSuccessBlock:^(id data) {
                                        if ([data[@"success"] integerValue] == 1) {
                                            //                                  [TheGlobalMethod popAlertView:@"提现成功" controller:self];
                                            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"提现成功"]}];
                                        } else {
                                            //                                  [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
                                            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"%@", data[@"msg"]]}];
                                        }
                                    }];

                            };
                        }
                    }
                    
                }else{
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":data[@"msg"]}];
                }
            }];
                if (success == 1) {
          
            }//success
            
            
        }//提现填写
    }//section == 2
}

#pragma mark Cell点击 验券结果=====
-(void)selendIdCell:(NSIndexPath *)indexPath data:(id)data{
    if (indexPath.section == 0) {
        
        
//        OrderDetailVC *controller =[self.storyboard instantiateViewControllerWithIdentifier:@"OrderDetailVC"];
//        controller.title = @"订单详情";
//        controller.titleString = @"订单详情";
//        controller.longitudeF = longitude;
//        controller.latitudeF = latitude;
////
//        
//        controller.orderNum = self.number;
//        controller.QRRecord = 1;
        
//        NSLog(@"goods--%@",self.goodsBuyNumber);
//        [HttpUrl POST:@"goods/order/info/merchant" dict:@{@"goodsBuyNumber":self.goodsBuyNumber} WithSuccessBlock:^(id data) {
//            
        
//            if ([data[@"success"]intValue] == 1) {
        
//                NSDictionary *dict = data[@"data"];
//                if([[dict allKeys]containsObject:@"number"] &&[[dict allKeys]containsObject:@"storeClasslyName"] ){
        
//                        controller.orderNum = dict[@"number"];
//                        _orderClassifyName = dict[@"storeClasslyName"];//类别
//                        if ([self.storeClasslyName isEqualToString:@"酒店"]){
//                            controller.orderClassifyName = 0;
//                        }else if ([self.storeClasslyName isEqualToString:@"KTV"]){
//                            controller.orderClassifyName = 1;
//                        }else if([self.storeClasslyName isEqualToString:@"商城"]){
//                           
//                            controller.orderClassifyName = 2;
//                            
//                        }else if([self.storeClasslyName isEqualToString:@"美食"]){
//                            controller.orderClassifyName = 3;
//                        }else if([self.storeClasslyName isEqualToString:@"外卖"]){
//                            controller.orderClassifyName = 4;
//                        }
//                        NSLog(@"%@,%@---numbbbb",self.storeClasslyName,self.number);
//                        [self.navigationController pushViewController:controller animated:YES];
        
//                }
//            }
//            else{
//                [TheGlobalMethod popAlertView:data[@"msg"] controller:self];
//            }
//        }];
        
    }
}

#pragma mark Cell点击 验证记录  -----------loading
-(void)bselendIdCell:(NSIndexPath *)indexPath data:(id)data{
//    if (indexPath.section == 1) {
//        
//        NSInteger selected = indexPath.row % 10 ;
//        NSInteger pageNumm = indexPath.row / 10 + 1;
//        //1.获取经纬度？
//        self.locManager = [[CLLocationManager alloc] init];
//        self.locManager.delegate = self;
//        self.locManager.desiredAccuracy = kCLLocationAccuracyBest;
//        self.locManager.distanceFilter = 5.0;
//        [self.locManager startUpdatingLocation];
//        
////        OrderDetailVC *controller =[self.storyboard instantiateViewControllerWithIdentifier:@"OrderDetailVC"];
////        controller.title = @"订单详情";
////        controller.titleString = @"订单详情";
////        controller.longitudeF = longitude;
////        controller.latitudeF = latitude;
//        
//        //2.Post 根据点击的cell来Post相应的界面,获取相应的类别
//        NSMutableDictionary *dictt = [NSMutableDictionary dictionaryWithCapacity:0];
//        [dictt setObject:beginTime forKey:@"beginTime"];
//        [dictt setObject:[endTime stringByAppendingString:@" 23:59"] forKey:@"endTime"];
//        [dictt setObject:@(pageNumm) forKey:@"pageNum"];
//        [dictt setObject:[NSNumber numberWithInt:Limit] forKey:@"pageSize"];
//        
//        [HttpUrl LTPOST:@"goods/order/buy/number/record" dict:dictt WithSuccessBlock:^(id data) {
//          
//            if ([[data[@"data"] allKeys]containsObject:@"goodsOrderListPage"]) {
//                NSLog(@"aaa");
//                NSDictionary *dict = data[@"data"][@"goodsOrderListPage"];
//                if([[dict allKeys]containsObject:@"list"] ){
//                    NSArray *arr = dict[@"list"];
//                    if (arr.count > 0 ) {
//
//                       controller.orderNum = arr[selected][@"number"];
//                        
//                        _orderClassifyName = arr[selected][@"storeClasslyName"];//类别
//                        if ([_orderClassifyName isEqualToString:@"酒店"]){
//                            controller.orderClassifyName = 0;
//                        }else if ([_orderClassifyName isEqualToString:@"KTV"]){
//                            controller.orderClassifyName = 1;
//                        }else if([_orderClassifyName isEqualToString:@"商城"]){
//                            controller.orderClassifyName = 2;
//                        }else if([_orderClassifyName isEqualToString:@"美食"]){
//                            controller.orderClassifyName = 3;
//                        }else if([_orderClassifyName isEqualToString:@"外卖"]){
//                            controller.orderClassifyName = 4;
//                        }
//                        controller.QRRecord = 1;
//                      [self.navigationController pushViewController:controller animated:YES];
//
//
//    
//                        
//                        
//                    }
//                }
//            }
//        }];
//        
//        
//        
//  
//        
//    }
}



#pragma mark 我的界面
-(void)dselendIdCell:(NSIndexPath *)indexPath data:(id)data{
//登陆密码，支付密码
    if (indexPath.section == 0) {
        [self headPhoto];
    
    }
    else
    if(indexPath.section == 2){
        
        if (indexPath.row == 0) {
        
        }else if (indexPath.row == 1){
        
        }}else
        
            if (indexPath.section == 1) {
            
            }else if (indexPath.section == 3) {
            
            } else if (indexPath.section == 4) {//退出登陆
            
           
            [HttpUrl LTPOST:@"user/logout" dict:nil WithSuccessBlock:^(id data) {
                if ([data[@"success"] intValue] == 1) {
                    
                    
                    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
                    [user removeObjectForKey:@"password"];
                    
                    //环信退出登录
//                    EMError *error = [[EMClient sharedClient] logout:YES];
//                    if (!error) {
//                        NSLog(@"退出成功");
//                    }
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                    UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"loginNA"];
                    [self presentViewController:controller animated:NO completion:nil];
                }else{
                     [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"%@!",data[@"msg"]]}];
                }
   
            }];
            
            
            
        }
}


#pragma mark  确认支付密码界面
-(void)eselendIdCell:(NSIndexPath *)indexPath data:(id)data{

    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[InfoVC class]]) {
            InfoVC *revise =(InfoVC *)controller;
            [self.navigationController popToViewController:revise animated:YES];
        }
    }
}


#pragma mark 设置支付密码界面
-(void)fselendIdCell:(NSIndexPath *)indexPath data:(id)data{
    
    if(indexPath.row == 1){
        
        InfoVC *controller =[self.storyboard instantiateViewControllerWithIdentifier:@"InfoVC"];
        controller.title = @"确认支付密码";
        controller.titleString = @"确认支付密码";
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark 手机号码修改界面
-(void)gselendIdCell:(NSIndexPath *)indexPath data:(id)data{
    if (indexPath.row == 2) {
        InfoVC *controller =[self.storyboard instantiateViewControllerWithIdentifier:@"InfoVC"];
        controller.title = @"输入新号码";
        controller.titleString = @"输入新号码";
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark MJrefresh------------------网络请求
//MJ方法中的请求网络
-(void)updateWithType:(NSString *)type{

    [httpDict setObject:type forKey:@"pageNum"];
    [httpDict setObject:[NSNumber numberWithInt:Limit] forKey:@"pageSize"];
    

        if ([self.titleString isEqualToString:@"财务管理"])
        {
            [httpDict setObject:beginTime forKey:@"beginTime"];
            [httpDict setObject:[endTime stringByAppendingString:@" 23:59"] forKey:@"endTime"];
            
            NSString *myhttpString = @"user/amount/record/merchant/finance";
            
            //第一次进来,让其显示订单管理
            if (change == 0) {
                change = 2;
            }
            
            if (change == 1) {
                [httpDict setObject:@1 forKey:@"orderTypeNumber"];
            }else if (change == 2){
                [httpDict setObject:@2 forKey:@"orderTypeNumber"];
            }
            
    
            [HttpUrl LTPOST:myhttpString dict:httpDict WithSuccessBlock:^(id data) {
               if ([data[@"success"]intValue] == 1) {
                
            mjArray = [NSArray arrayWithArray:data[@"data"][@"userAmountRecordPageInfo"][@"list"]];
            
            [dataArray addObjectsFromArray:mjArray];
            
            NSArray *arr = [NSArray arrayWithArray:dataArray];
//            [self.myTableView showSection:1 uploadRows:0 uploadDate:@{@"type":[NSNumber numberWithInteger:change],@"data":arr}];
            [self.myTableView showSection:1 uploadRows:0 uploadDate:arr];
            [self.myTableView showSection:0 uploadRows:1 uploadDate:data[@"data"]];
            [self.myTableView showSectionHeader:0 uploadDate:@{@"beginTime":beginTime,@"endTime":endTime}];
            [caiwuDict setObject:data[@"data"] forKey:@"data"];
                   
            [self.myTableView showSectionHeader:1 uploadDate:caiwuDict];
                   
            [self mjFresh];

               }
                
            }];
        }else if ([self.titleString isEqualToString:@"到店付记录"])
        {
            [httpDict setObject:beginTime forKey:@"beginTime"];
            [httpDict setObject:[endTime stringByAppendingString:@" 23:59"] forKey:@"endTime"];
            NSString *myhttpString = @"user/amount/record/merchant/pay/record";
            [HttpUrl LTPOST:myhttpString dict:httpDict WithSuccessBlock:^(id data) {
                if ([data[@"success"]intValue] == 1) {
                
                mjArray = [NSArray arrayWithArray:data[@"data"][@"list"]];
                
                [dataArray addObjectsFromArray:mjArray];
                
                
                NSArray *arr = [NSArray arrayWithArray:dataArray];
                
               
                [self.myTableView showSection:0 uploadRows:0 uploadDate:arr];
                [self.myTableView showSectionHeader:0 uploadDate:@{@"beginTime":beginTime,@"endTime":endTime}];
                
                [self mjFresh];}
            }];
        }else if ([self.titleString isEqualToString:@"验证记录"])
        {
            [httpDict setObject:beginTime forKey:@"beginTime"];
            [httpDict setObject:[endTime stringByAppendingString:@" 23:59"] forKey:@"endTime"];
            NSString *myhttpString = @"goods/order/buy/number/record";
            [HttpUrl LTPOST:myhttpString dict:httpDict WithSuccessBlock:^(id data) {
                if ([data[@"success"]intValue] == 1) {
                
            mjArray = [NSArray arrayWithArray:data[@"data"][@"goodsOrderListPage"][@"list"]];
            
            [dataArray addObjectsFromArray:mjArray];
            
            NSArray *arr = [NSArray arrayWithArray:dataArray];
                
            [self.myTableView showSection:0 uploadRows:1 uploadDate:data[@"data"][@"sum"]];
             [self.myTableView showSectionHeader:0 uploadDate:@{@"beginTime":beginTime,@"endTime":endTime}];
            [self.myTableView showSection:1 uploadRows:0 uploadDate:arr];
            
            [self mjFresh];}
            }];
        }else if ([self.titleString isEqualToString:@"钱包"])
        {
            [httpDict setObject:[beginTime stringByAppendingString:@" 00:00:00"]forKey:@"beginTime"];
            [httpDict setObject:[endTime stringByAppendingString:@" 23:59:59"] forKey:@"endTime"];
            NSLog(@"%@---aaaa--",httpDict);
            NSString *myhttpString = @"user/amount/record/my/data/by/condition/page";
            [HttpUrl LTPOST:myhttpString dict:httpDict WithSuccessBlock:^(id data) {
            if ([data[@"success"]intValue] == 1) {
                
                 NSLog(@"kkkkkkk%@-",dataArray);
                
                mjArray = [NSArray arrayWithArray:data[@"data"][@"list"]];
                    
                [dataArray addObjectsFromArray:mjArray];
                    
                NSArray *arr = [NSArray arrayWithArray:dataArray];
                
                NSLog(@"aaaarrrrr%@----%@",arr,dataArray);
                [self.myTableView showSectionHeader:1 uploadDate:@{@"beginTime":beginTime,@"endTime":endTime}];
                [self.myTableView showSection:1 uploadRows:0 uploadDate:arr];
                [self mjFresh];
            }
            }];
        }
    
}
//有分页的界面调用的MJ方法
//viewDidload调该接口
-(void)mjPageViewDidLoad{

    self.myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        pageNum = 1;
        dataArray = [NSMutableArray arrayWithCapacity:0];
        [self.myTableView.mj_footer resetNoMoreData];//闲置状态
        [self updateWithType:[NSString stringWithFormat:@"%d",pageNum]];
    }];
    [self.myTableView.mj_header beginRefreshing];
    
    self.myTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        if (mjArray.count == Limit) {
            pageNum ++;
            [self updateWithType:[NSString stringWithFormat:@"%d",pageNum]];
        }else{
            self.myTableView.mj_footer.ignoredScrollViewContentInsetBottom = -30;
            [self.myTableView.mj_footer endRefreshingWithNoMoreData];/////1111

        }
        
    }];
    self.myTableView.mj_footer.ignoredScrollViewContentInsetBottom = 0;

}

//MJ公用的方法提出
-(void)mjFresh{
    
    if (pageNum == 1){
        [self.myTableView.mj_header endRefreshing];
    }else{
        if (mjArray.count == Limit) {
//            [self.myTableView.mj_footer resetNoMoreData];
            [self.myTableView.mj_footer endRefreshingWithNoMoreData];
        }
        [self.myTableView.mj_footer endRefreshing];
    }
    [self.myTableView reloadData];
    
    [self.myTableView.mj_header endRefreshing];

}


-(void)uploaderHttp{
    NSString *httpString = @"";
    
    if ([self.titleString isEqualToString:@"买单"]) {
     // 优惠券
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [HttpUrl LTPOST:@"store/coupon/find/my/coupon" dict:nil WithSuccessBlock:^(id data) {
            
            if ([data[@"success"]intValue] == 1) {
                [MBProgressHUD hideHUDForView :self.view animated:YES];
                [self.myTableView showSection:2 uploadRows:0 uploadDate:data[@"data"]];
                //如果有满减
                if ([[data[@"data"] allKeys]containsObject:@"preferentialAmount"] && [[data[@"data"] allKeys]containsObject:@"fullAmount"]) {
                    if ([data[@"data"][@"preferentialAmount"] isEqual:[NSNull null]]) {
                        self.jian = @"0";
                    }else{
                      self.jian = [NSString stringWithFormat:@"%f", [data[@"data"][@"preferentialAmount"] floatValue]] ;

                    }
                    //如果没有满减，满减是0
                    if ([data[@"data"][@"fullAmount"] isEqual:[NSNull null]]) {
                        self.man = @"0";
                    }else{
                        self.man =[NSString stringWithFormat:@"%f", [data[@"data"][@"fullAmount"] floatValue]];
        
                    }
                   
                }
                
                 [self.myTableView reloadData];
            }else{
                //如果没有满减
                 [MBProgressHUD hideHUDForView :self.view animated:YES];
//                 [TheGlobalMethod popAlertView:data[@"msg"] controller:self];
                self.man = @"0";
                self.jian = @"0";
                [self.myTableView reloadData];
            }
        }];
    }else if ([self.titleString isEqualToString:@"我的二维码"]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [HttpUrl POST:@"user/find/user" dict:nil sign:nil WithSuccessBlock:^(id data) {
           
            
            
            if ([data[@"success"]intValue] == 1) {
//                if ([[data[@"data"] allKeys] containsObject:@"name"]) {
//                    if (![data[@"data"][@"name"]isEqual:[NSNull null]] && data[@"data"][@"name"]){
//                       [httpDict setObject:data[@"data"][@"name"] forKey:@"name"];
//                    }
//                    
//                }
                
                //二维码
                if([[data[@"data"] allKeys]containsObject:@"membershipCard"]){
                     [httpDict setObject:data[@"data"][@"membershipCard"] forKey:@"membershipCard"];
                    
                }
                
                //logo
                [HttpUrl POST:@"user/find/store/logo" dict:nil sign:nil WithSuccessBlock:^(id data) {
                    
                            if ([data[@"success"]intValue] == 1) {
                                if ([[data allKeys]containsObject:@"data"]) {
                                    if (![data[@"data"]isEqual:[NSNull null]] && data[@"data"]) {
                                          [httpDict setObject:data[@"data"] forKey:@"headUrl"];

                                    }
                                }
                            }
                    
                            //名字
                            [HttpUrl POST:@"goods/order/merchant/index" dict:nil sign:nil WithSuccessBlock:^(id data) {
                                if ([data[@"success"]intValue] == 1) {
                                    
                                    if ([[data[@"data"] allKeys] containsObject:@"name"]) {
                                        if (![data[@"data"][@"name"]isEqual:[NSNull null]] && data[@"data"][@"name"]){
                                            [httpDict setObject:data[@"data"][@"name"] forKey:@"name"];
                                        }
                                        
                                    }
                                }
                                    [self.myTableView showSection:0 uploadRows:0 uploadDate:httpDict];
                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                    self.myTableView.hidden = NO;
                                    [self.myTableView reloadData];
                            }];
                    
                    
                    


                }];
                
            }
            
        }];
        
        
    }else
    if ([self.titleString isEqualToString:@"我的"]) {
         NSMutableDictionary *mydict  = [NSMutableDictionary dictionary];
       
        NSString *myphone = [TheGlobalMethod getUserDefault:@"phone"];
        if ([myphone length] > 0) {
            [mydict setObject:myphone forKey:@"phone"];
            
            [HttpUrl LTPOST:@"user/find/store/logo" dict:@{} WithSuccessBlock:^(id data) {
                if ([data[@"success"]intValue] == 1) {
                    [mydict setObject:data[@"data"] forKey:@"icon"];
                    [self.myTableView showSection:0 uploadRows:0 uploadDate:mydict];
                    [self.myTableView showSection:1 uploadRows:0 uploadDate:mydict];
                    [self.myTableView reloadData];
                    
                }else{
                    [self.myTableView showSection:1 uploadRows:0 uploadDate:mydict];
                    [self.myTableView reloadData];
                }
                
             
                
            }];
            
        }
        
       

        

    }else

    
#pragma mark 网络支付确认 到店付。。
    if ([self.titleString isEqualToString:@"支付确认"])
    {
      [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.myTableView.scrollEnabled = NO;
//        NSString *leftMoneyString = @"user/amount/record/merchant/recharge/record";
        
        //查看的是用户余额
        [HttpUrl LTPOST:@"membership/card/find/user" dict:@{@"numberCard":self.numberCard} WithSuccessBlock:^(id data) {
            if ([data[@"success"]intValue] == 1) {
                [MBProgressHUD hideHUDForView :self.view animated:YES];
                if ([[data[@"data"] allKeys] containsObject:@"balance"]) {
                    if (![data[@"data"][@"balance"] isEqual:[NSNull null]]) {
                        [self.myTableView showSection:1 uploadRows:0 uploadDate:@{@"left":data[@"data"][@"balance"]}];
                        NSLog(@"余额,%@",data[@"data"][@"balance"]);
                    }
                }
                
                // 可以使用的是 : 总金额_totalMoney 满_man  减_jian 不可优惠金额_noMoney
                // 计算出来    :优惠金额 youhui 实付金额payMoney
                
                NSLog(@"man---%@",_man);
                NSLog(@"jian---%@",_jian);
                
                //如果总金额减去不可优惠金额大于满的金额的话，有优惠的
                if ((_totalMoney - _noMoney)>= [_man floatValue]) {
                    youhui  = [_jian floatValue]  ;//优惠金额就是减的金额啊
                    
//                    if (youhui < 0 ) {
//                        youhui = 0;
//                    }
                    payMoney = _totalMoney - youhui;
                    
                }else{
                    
                    payMoney = _totalMoney;
                    youhui = 0;
                }
                
                NSMutableDictionary *mDic = [NSMutableDictionary dictionaryWithCapacity:0];
                
                [mDic setObject:[NSNumber numberWithFloat:_totalMoney] forKey:@"totalmoney"];
                [mDic setObject:[NSNumber numberWithFloat:_noMoney] forKey:@"noMoney"];
                [mDic setObject:[NSNumber numberWithFloat:youhui] forKey:@"youhui"];
                [mDic setObject:[NSNumber numberWithFloat:payMoney] forKey:@"payMoney"];
                
                [self.myTableView showSection:0 uploadRows:0 uploadDate:mDic];

                //                [self.myTableView showSection:2 uploadRows:0 uploadDate: @{@"pay": [NSNumber numberWithFloat: payMoney]}];
                [self.myTableView reloadData];
                
            }else{
                 [MBProgressHUD hideHUDForView :self.view animated:YES];
                 [TheGlobalMethod popAlertView:data[@"msg"] controller:self];

            }
        }];
    }else
#pragma mark 网络验券结果

    if ([self.titleString isEqualToString:@"验券结果"])
    {
//        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        httpString = @"goods/order/info/merchant";

        [httpDict setObject:self.goodsBuyNumber forKey:@"goodsBuyNumber"];
        [HttpUrl LTPOST:httpString dict:httpDict WithSuccessBlock:^(id data) {

            if ([data[@"success"]intValue] ==1) {
//                [MBProgressHUD hideHUDForView :self.view animated:YES];
                 [self.myTableView showSection:0 uploadRows:0 uploadDate:data[@"data"]];
                NSDictionary *dict = data[@"data"];
                if([[dict allKeys]containsObject:@"number"] &&[[dict allKeys]containsObject:@"storeClasslyName"] ){
                    self.storeClasslyName = dict[@"storeClasslyName"];
                    self.number = dict[@"number"];
                    
                }
                 [self.myTableView reloadData];
            }else{
                [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"%@", data[@"msg"]]}];
            }
       
            
        }];
        
    }else

#pragma mark 网络财务管理
        if ([self.titleString isEqualToString:@"财务管理"])
        {
//            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            httpString = @"user/amount/record/merchant/finance";
            [httpDict setObject:@1 forKey:@"pageNum"];
            [httpDict setObject:[NSNumber numberWithInt:Limit] forKey:@"pageSize"];
            [httpDict setObject:[beginTime stringByAppendingString:@" 00:00:00" ] forKey:@"beginTime"];
            [httpDict setObject:[endTime stringByAppendingString:@" 23:59:59"] forKey:@"endTime"];
            
            NSLog(@"%@----httdddd-",httpDict);
            //第一次进来,让其显示订单管理
            if (change == 0) {
                change = 2;
            }
            
            if (change == 1) {
                [httpDict setObject:@1 forKey:@"orderTypeNumber"];
            }else if (change == 2){
                [httpDict setObject:@2 forKey:@"orderTypeNumber"];
            }
            
            [HttpUrl LTPOST:httpString dict:httpDict WithSuccessBlock:^(id data) {
//             [MBProgressHUD hideHUDForView :self.view animated:YES];
            
//            [self.myTableView showSection:1 uploadRows:0 uploadDate:@{@"type":[NSNumber numberWithInteger:change],@"data":data[@"data"][@"userAmountRecordPageInfo"][@"list"]}];
                if ([data[@"success"]intValue] == 1) {
                    [self.myTableView showSection:1 uploadRows:0 uploadDate:data[@"data"][@"userAmountRecordPageInfo"][@"list"]];
                    
                    [self.myTableView showSectionHeader:0 uploadDate:@{@"beginTime":beginTime,@"endTime":endTime}];
                    [self.myTableView showSection:0 uploadRows:0 uploadDate:data[@"data"]];
                    
                    [caiwuDict setObject:data[@"data"] forKey:@"data"];
                    [self.myTableView showSectionHeader:1 uploadDate:caiwuDict];
                        
                    [self.myTableView.mj_header endRefreshing];
                        
                   
                    [self.myTableView reloadData];
                }
            }];
            
        }else
            
#pragma mark 网络到店付记录
            if ([self.titleString isEqualToString:@"到店付记录"])
            {
                httpString = @"user/amount/record/merchant/pay/record";
                [httpDict setObject:@1 forKey:@"pageNum"];
                [httpDict setObject:[NSNumber numberWithInt:Limit] forKey:@"pageSize"];
                [httpDict setObject:beginTime  forKey:@"beginTime"];
                [httpDict setObject:[endTime stringByAppendingString:@" 23:59"] forKey:@"endTime"];
                [HttpUrl LTPOST:httpString dict:httpDict WithSuccessBlock:^(id data) {
                    
                    if ([data[@"success"]intValue] == 1) {
                        [self.myTableView showSectionHeader:0 uploadDate:@{@"beginTime":beginTime,@"endTime":endTime}];
                        [self.myTableView showSection:0 uploadRows:0 uploadDate:data[@"data"][@"list"]];

                        [self.myTableView reloadData];
                        [self.myTableView.mj_header endRefreshing];}
                }];
            }else
                
#pragma mark 网络钱包
                if ([self.titleString isEqualToString:@"钱包"])
                {

                    httpString = @"user/amount/record/my/data/by/condition/page";
                    [httpDict setObject:@1 forKey:@"pageNum"];
                    [httpDict setObject:[NSNumber numberWithInt:Limit] forKey:@"pageSize"];
                    [httpDict setObject:[beginTime stringByAppendingString:@" 00:00:00"]  forKey:@"beginTime"];
                    [httpDict setObject:[endTime stringByAppendingString:@" 23:59:59"] forKey:@"endTime"];
                    [HttpUrl LTPOST:httpString dict:httpDict WithSuccessBlock:^(id data) {
//                        [MBProgressHUD hideHUDForView :self.view animated:YES];
                        if ([data[@"success"]intValue] == 1) {
 
                            NSLog(@"%@,---bbbb--===%@",beginTime,endTime);
                            [self.myTableView showSectionHeader:1 uploadDate:@{@"beginTime":beginTime,@"endTime":endTime}];
                            [self.myTableView showSection:1 uploadRows:0 uploadDate:data[@"data"][@"list"]];
                            [self.myTableView reloadData];
                        
                            [self.myTableView.mj_header endRefreshing];
                      
                        }else{
                            [self.myTableView.mj_header endRefreshing];
                        }
                        
                    }];
                    
                    NSString *leftMoneyString = @"/user/find/user";
                    [HttpUrl LTPOST:leftMoneyString dict:nil WithSuccessBlock:^(id data) {
                        if ([data[@"success"]intValue] == 1) {
                            [self.myTableView showSection:0 uploadRows:0 uploadDate:data[@"data"][@"balance"]];

                        }else{
                            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"获取个人信息失败"]}];
                            
                        }
                     }];
                    

                }else

                        
#pragma mark 网络我的团队
                    if ([self.titleString isEqualToString:@"我的团队"] && (_oneClass.selected == YES)){

                        NSString *httpString = @"user/steps/member";
                        [httpDict setObject:@1 forKey:@"pageNum"];
                        [httpDict setObject:[NSNumber numberWithInt:Limit] forKey:@"pageSize"];
                        
                        [HttpUrl LTPOST:httpString dict:httpDict WithSuccessBlock:^(id data) {
                            
                            if ([data[@"success"]intValue]==1) {
                                

                                NSArray *team = [NSArray arrayWithArray:data[@"data"][@"level"]];
                
                                if (team.count > 0) {
                                    NSArray *oneClass = team[0];
                                     [_oneClass setTitle:[NSString stringWithFormat:@"一级(%li)",(unsigned long)oneClass.count] forState:UIControlStateNormal];
                                    [self.myTableView showSection:0 uploadRows:0 uploadDate:oneClass];
                                    [self.myTableView reloadData];
                                    [self.myTableView.mj_header endRefreshing];
                                    if (team.count > 1) {
                                        NSArray *twoClass = team[1];
                                        [_twoClass setTitle:[NSString stringWithFormat:@"二级(%li)",(unsigned long)twoClass.count] forState:UIControlStateNormal];
                
                                        if (team.count > 2) {
                                            NSArray *threeClass = team[2];
                                             [_threeClass setTitle:[NSString stringWithFormat:@"一级(%li)",(unsigned long)threeClass.count] forState:UIControlStateNormal];
                                        }else{
                                            [_threeClass setTitle:@"三级(0)" forState:UIControlStateNormal];
                                        }
                                        
                                    }else{
                                        [_threeClass setTitle:@"三级(0)" forState:UIControlStateNormal];
                                        [_twoClass setTitle:@"二级(0)" forState:UIControlStateNormal];
                                    
                                    }
                                    
                                    
                                }else{
                                    [_oneClass setTitle:@"一级(0)" forState:UIControlStateNormal];
                                    [_threeClass setTitle:@"三级(0)" forState:UIControlStateNormal];
                                    [_twoClass setTitle:@"二级(0)" forState:UIControlStateNormal];
                                
                                }
                   
                            }

                            
                            
                        }];
                    
                    
                    }
                    else
#pragma mark 网络验证记录
                        if ([self.titleString isEqualToString:@"验证记录"])
                        {
 
                            NSLog(@"%@----,ccc---%@",beginTime,endTime);
                            httpString = @"goods/order/buy/number/record";
                            [httpDict setObject:beginTime   forKey:@"beginTime"];
                            [httpDict setObject:[endTime stringByAppendingString:@" 23:59"] forKey:@"endTime"];
                            [httpDict setObject:@1 forKey:@"pageNum"];
                            [httpDict setObject:[NSNumber numberWithInt:Limit] forKey:@"pageSize"];
                            NSLog(@"%@----,ccc",httpDict);
                            [HttpUrl LTPOST:httpString dict:httpDict WithSuccessBlock:^(id data) {

                            if ([data[@"success"]intValue] == 1) {

                                [self.myTableView showSection:0 uploadRows:0 uploadDate:data[@"data"][@"goodsOrderListPage"][@"total"]];
                           
                                [self.myTableView showSection:1 uploadRows:0 uploadDate:data[@"data"][@"goodsOrderListPage"][@"list"]];

                                [self.myTableView reloadData];
                                [self.myTableView.mj_header endRefreshing];
                                }
                            }];
                            
                        }
}


#pragma mark - 选择日期
//选择日期
-(void)selectNormalDate:(NSInteger)tag{
    
    UIView *aView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, HEIGHT_K)];
    aView.userInteractionEnabled = YES;
    UITapGestureRecognizer *removeView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeView:)];
    [aView addGestureRecognizer:removeView];
    aView.tag = 10010;
    SelectDateCell *cell = [TheGlobalMethod setCell:@"SelectDateCell"];
   
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSLog(@"begggg--%@",[beginTime substringToIndex:10]);
    NSDate *dateBegin = [dateFormatter dateFromString:[beginTime substringToIndex:10]];
     NSDate *dateEnd = [dateFormatter dateFromString:[endTime substringToIndex:10]];
    //右侧
    if (tag == 401) {
        cell.datePicker.minimumDate = dateBegin;
    }else if (tag == 400){
        cell.datePicker.maximumDate = dateEnd;
    }
    
    cell.tag = 10086;
    cell.frame = CGRectMake(0, HEIGHT_K-HEIGHT_K/3, WIDTH_K, HEIGHT_K/3);
    cell.reloadVC = ^void(NSString *str1){
    
        if (str1.length > 0) {
            if ([self.titleString isEqualToString:@"到店付记录"] || [self.titleString isEqualToString:@"财务管理"]||[self.titleString isEqualToString:@"验证记录"] ) {
                
                if (tag == 400) {
                    beginTime = str1;
                } else {
                    endTime = str1;

                }
                [self.myTableView showSectionHeader:0 uploadDate:@{@"beginTime":beginTime,@"endTime":endTime}];
              
            }
            //钱包
            else{

                if (tag == 400) {
                    beginTime = str1;

                } else {
                    endTime = str1;
                }
                 [self.myTableView showSectionHeader:0 uploadDate:@{@"beginTime":beginTime,@"endTime":endTime}];
            
            }
             [self mjPageViewDidLoad];
         
        }
        [[[UIApplication sharedApplication].keyWindow viewWithTag:10086] removeFromSuperview];
        [[[UIApplication sharedApplication].keyWindow viewWithTag:10010] removeFromSuperview];
        self.view.userInteractionEnabled = YES;
    };
    [[UIApplication sharedApplication].keyWindow addSubview:aView];
    aView.alpha = 0.3;
    aView.backgroundColor = [UIColor blackColor];
    [[UIApplication sharedApplication].keyWindow addSubview:cell];
    
    self.view.userInteractionEnabled = NO;
}



#pragma mark --- SB点击事件 1.

- (IBAction)teamButton:(UIButton *)sender {
    
    if (typeBtn != sender.tag){
        UIButton *button = [self.view viewWithTag:typeBtn];
        button.selected = NO;
        
        UIView *view = [self.myTeamView viewWithTag:typeBtn + 10];
        view.hidden = YES;
        sender.selected = YES;
        UIView *view1 = [self.myTeamView viewWithTag:sender.tag + 10];
        view1.hidden = NO;
        typeBtn = sender.tag;
    }
    
}

#pragma mark 一级
- (IBAction)oneClass:(UIButton *)sender {
    
    NSString *httpString = @"user/steps/member";
    [httpDict setObject:@1 forKey:@"pageNum"];
    [httpDict setObject:[NSNumber numberWithInt:Limit] forKey:@"pageSize"];
//      [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HttpUrl LTPOST:httpString dict:httpDict WithSuccessBlock:^(id data) {
        
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([data[@"success"]intValue] == 1) {
        NSMutableArray *arrNew = [NSMutableArray array];
        NSArray *team = [NSArray arrayWithArray:data[@"data"][@"level"]];
        for (NSArray *oneClass in team) {
            if (oneClass.count > 0 ) {
                
                NSArray *array = [NSArray arrayWithArray:data[@"data"][@"level"][0]];
                for (int i = 0; i < array.count; i++) {
                    NSDictionary *dic = [NSDictionary dictionaryWithDictionary:array[i]];
                        [arrNew addObject:dic];
                }
                
            }
        }
        
        NSArray *arr = [NSArray arrayWithArray:arrNew];
        
        [self.myTableView showSection:0 uploadRows:0 uploadDate:arr];
        
        [self.myTableView.mj_footer resetNoMoreData];//闲置状态
        [self.myTableView reloadData];
        [self.myTableView.mj_header endRefreshing];
        }
        
    }];
//    [self mjPageViewDidLoad];
    
    
}

#pragma mark 二级
- (IBAction)twoClass:(UIButton *)sender {

    NSString *httpString = @"user/steps/member";
    [httpDict setObject:@1 forKey:@"pageNum"];
    [httpDict setObject:[NSNumber numberWithInt:Limit] forKey:@"pageSize"];
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HttpUrl LTPOST:httpString dict:httpDict WithSuccessBlock:^(id data) {
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([data[@"success"]intValue] == 1) {
        NSMutableArray *arrNew = [NSMutableArray array];
        NSArray *team = [NSArray arrayWithArray:data[@"data"][@"level"]];
        for (NSArray *oneClass in team) {
            if (oneClass.count > 1 ) {
                NSArray *array = [NSArray arrayWithArray:data[@"data"][@"level"][1]];
            
               
                for (int i = 0; i < array.count; i++) {
                    NSDictionary *dic = [NSDictionary dictionaryWithDictionary:array[i]];
                    [arrNew addObject:dic];
                }
                
               
                
            }
        }
        NSArray *arr = [NSArray arrayWithArray:arrNew];
        
        [self.myTableView showSection:0 uploadRows:0 uploadDate:arr];
        
        [self.myTableView.mj_footer resetNoMoreData];//闲置状态
        [self.myTableView reloadData];
        [self.myTableView.mj_header endRefreshing];
        }
    }];
//    [self mjPageViewDidLoad];
    
}


#pragma mark 三级
- (IBAction)threeClass:(UIButton *)sender {
    
    NSString *httpString = @"user/steps/member";
    [httpDict setObject:@1 forKey:@"pageNum"];
    [httpDict setObject:[NSNumber numberWithInt:Limit] forKey:@"pageSize"];
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HttpUrl LTPOST:httpString dict:httpDict WithSuccessBlock:^(id data) {
        if ([data[@"success"]intValue] == 1) {
//          [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSMutableArray *arrNew = [NSMutableArray array];
        NSArray *team = [NSArray arrayWithArray:data[@"data"][@"level"]];
        for (NSArray *oneClass in team) {
            if (oneClass.count > 2 ) {
                
                NSArray *array = [NSArray arrayWithArray:data[@"data"][@"level"][2]];
              
                for (int i = 0; i < array.count; i++) {
                    NSDictionary *dic = [NSDictionary dictionaryWithDictionary:array[i]];
                    [arrNew addObject:dic];
                }
                
              
                
            }
        }
        NSArray *arr = [NSArray arrayWithArray:arrNew];
        
        [self.myTableView showSection:0 uploadRows:0 uploadDate:arr];
        
        [self.myTableView.mj_footer resetNoMoreData];//闲置状态
        [self.myTableView reloadData];
        [self.myTableView.mj_header endRefreshing];
        }
    }];
//    [self mjPageViewDidLoad];
}

#pragma mark - 点击右侧的提现记录
-(void)doClickTixian{
//msevenLabelCell

//    VerifyVC *controller  = [self.storyboard instantiateViewControllerWithIdentifier:@"VerifyVC"];
//    controller.title = @"提现记录";
//    controller.titleString = @"提现记录";
//    [self.navigationController pushViewController:controller animated:YES];

}


#pragma mark - 获取照片
//选择取消调用的方法
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

//选择完照相机或图片调用的方法
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{

        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSString *string = [HTTP_IP stringByAppendingString:@"file/batch"];

        [manager POST:string parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {

            NSData *data=UIImageJPEGRepresentation(info[UIImagePickerControllerEditedImage], 1.0);
            if (data.length>100*1024) {
                if (data.length>1024*1024) {//1M以及以上
                    data=UIImageJPEGRepresentation(info[UIImagePickerControllerEditedImage], 0.1);
                }else if (data.length>512*1024) {//0.5M-1M
                    data=UIImageJPEGRepresentation(info[UIImagePickerControllerEditedImage], 0.5);
                }else if (data.length>200*1024) {//0.25M-0.5M
                    data=UIImageJPEGRepresentation(info[UIImagePickerControllerEditedImage], 0.9);
                }
            }
        [formData appendPartWithFileData:data name:@"files" fileName:@"1234.jpg" mimeType:@"image/jpg"];
        } progress:^(NSProgress * _Nonnull uploadProgress) {

        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            NSLog(@"dictg:--%@",dict);

                if ([dict[@"success"]intValue] == 1) {
                [HttpUrl LTPOST:@"user/update/store/logo" dict:@{@"logoUrl":dict[@"data"][0]} WithSuccessBlock:^(id data) {

                    [self uploaderHttp];
                    self.block();
                }];
            }
            [self dismissViewControllerAnimated:YES completion:nil];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             [TheGlobalMethod xianShiAlertView:@"上传图片失败,请重试" controller:self];
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
    
    
}


-(void)headPhoto{

}

- (void)removeView:(UITapGestureRecognizer *)sender {
    [[[UIApplication sharedApplication].keyWindow viewWithTag:10086] removeFromSuperview];
    [[[UIApplication sharedApplication].keyWindow viewWithTag:10010] removeFromSuperview];
    self.view.userInteractionEnabled = YES;
}
- (IBAction)setPop:(id)sender{
    if ([self.titleString isEqualToString:@"买单"]||[self.titleString isEqualToString:@"验券结果"]) {
         [self.navigationController popToRootViewControllerAnimated:YES];
    }else{
        SetPop
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIImageView *)findHairlineImageViewUnder:(UIView *)view {
    
    //在navBar中找到那条线
    if ([view isKindOfClass:UIImageView.class] && view.bounds.size.height <= 1.0) {
        return (UIImageView *)view;
    }
    for (UIView *subview in view.subviews) {
        UIImageView *imageView = [self findHairlineImageViewUnder:subview];
        if (imageView) {
            return imageView;
        }
    }
    return nil;
}
#pragma mark 分享平台


#pragma mark -获取位置代理方法
// 代理方法实现
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    longitude = newLocation.coordinate.longitude;
    latitude = newLocation.coordinate.latitude;
    NSLog(@"%f,hhhh,%f",newLocation.coordinate.latitude,newLocation.coordinate.longitude);
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"%@",error);
}

@end
