//
//  OrderVC.m
//  Shop
//
//  Created by ike on 16/12/30.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "OrderVC.h"
#import "OrderDetailVC.h"
#import "Layout.h"
#import "ZCTradeView.h"

@interface OrderVC (){
    NSMutableDictionary *httpDict;
    NSMutableDictionary *layoutDict;
    
    NSInteger mybtnTag;
    NSMutableArray *dataArray;
    NSArray *dataArr;
    NSInteger page;
    NSString *payPassword;
    
    float latitude;//纬度
    float longitude;//经度
    
}
@property (strong,nonatomic)CLLocationManager *locManager;//获取经纬度的属性
@property (weak, nonatomic) IBOutlet MyTableView *myTableView;
//点击cell的订单号(订单列表)
@property(nonatomic,copy)NSString *orderNum;
@property(nonatomic,copy)NSString *orderClassifyName;

@end

@implementation OrderVC


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;

}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    //1.获取经纬度？
    self.locManager = [[CLLocationManager alloc] init];
    self.locManager.delegate = self;
    self.locManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locManager.distanceFilter = 5.0;
    [self.locManager startUpdatingLocation];
    
    page = 1;
    httpDict = [NSMutableDictionary dictionaryWithCapacity:0];
    layoutDict  = [NSMutableDictionary dictionaryWithCapacity:0];
    dataArray = [NSMutableArray arrayWithCapacity:0];
    
    [layoutDict addEntriesFromDictionary:[Layout korder]];
    [self.myTableView showCell:layoutDict title:self.titleString];
    
    [httpDict setObject:@"1" forKey:@"typeId"];
    [httpDict setObject:@"SINCE" forKey:@"distributedType"];
    [httpDict setObject:@"TO_BE_SHIPPED" forKey:@"status"];
    mybtnTag = 10;
    
    [httpDict setObject:[NSNumber numberWithInt:Limit] forKey:@"pageSize"];
    self.myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        [httpDict setObject:@"1" forKey:@"pageNum"];
        page = 1;
        [self uploaderHttp];
    }];
    [self.myTableView.mj_header beginRefreshing];
    
    self.myTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        if (dataArr.count == Limit) {//最下了，刷新
            NSLog(@"httpddd----%@-----",httpDict);
//            [httpDict setObject:@([httpDict[@"pageNum"] intValue] + 1) forKey:@"pageNum"];
            page ++;
            [self uploaderHttp];
        }else{//
            self.myTableView.mj_footer.ignoredScrollViewContentInsetBottom = -40;
            [self.myTableView.mj_footer endRefreshingWithNoMoreData];
        }
    }];
    self.myTableView.mj_footer.ignoredScrollViewContentInsetBottom = 0;
    
    [self.myTableView RetuenidSelendCell:^(NSIndexPath *indexPath, id data) {
        [self selendIdCell:indexPath data:data];
    }];
    
    [self.myTableView RetuenidCellBtnClick:^(NSIndexPath *indexPath, NSInteger btnTag, id data) {
        [self btnSelected:indexPath data:data btnTag:btnTag];
    }];
}
#pragma mark 网络请求
-(void)uploaderHttp{
    
    [httpDict setObject:@(page) forKey:@"pageNum"];
    [HttpUrl LTPOST:@"goods/order/user/order/list" dict:httpDict WithSuccessBlock:^(id data) {
        if ([data[@"success"] intValue] == 1) {//未刷新
            
            dataArr = [NSArray arrayWithArray:[data[@"data"][@"list"] mutableCopy]];
            if ([httpDict[@"pageNum"] intValue] == 1) {
                [dataArray removeAllObjects];
                [dataArray addObjectsFromArray:dataArr];
                [self.myTableView.mj_header endRefreshing];//上部不刷新
                [self.myTableView.mj_footer resetNoMoreData];//闲置状态
                if ([data[@"data"][@"pages"] intValue] == page) {
                    [self.myTableView.mj_footer endRefreshingWithNoMoreData];//无数据
                    self.myTableView.mj_footer.ignoredScrollViewContentInsetBottom = -40;
                }
            }else{
                [dataArray addObjectsFromArray:dataArr];
                [self.myTableView.mj_footer endRefreshing];
                if ([data[@"data"][@"pages"] intValue] == page) {
//                    [self.myTableView.mj_footer endRefreshingWithNoMoreData];//无数据
                    self.myTableView.mj_footer.ignoredScrollViewContentInsetBottom = -40;
                }
              
            }
            [self.myTableView showSection:0 uploadRows:0 uploadDate:dataArray];
            [self.myTableView showRowsNumber:0 uploadDate:dataArray.count];
            [self.myTableView reloadData];
        }else{
            [self.myTableView.mj_header endRefreshing];
            [self.myTableView.mj_footer endRefreshingWithNoMoreData];//无数据
            self.myTableView.mj_footer.ignoredScrollViewContentInsetBottom = -40;
//            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"%@", data[@"msg"]]}];
//            if ([data[@"code"]isEqualToString:@"EMPTY"]) {
//                [self.myTableView.mj_header endRefreshing];
//            }
        }
    }];
}

#pragma mark btn点击分类
- (IBAction)typeBtnClick:(UIButton *)sender {
    if (mybtnTag != sender.tag) {
        NSMutableDictionary *newDict = [NSMutableDictionary dictionaryWithCapacity:0];
        sender.selected = YES;
        UIView *view = [self.view viewWithTag:sender.tag + 10];
        view.hidden = NO;
        UIButton *button = [self.view viewWithTag:mybtnTag];
        button.selected = NO;
        UIView *view1 = [self.view viewWithTag:mybtnTag + 10];
        view1.hidden = YES;
        if (sender.tag == 10) {
            [httpDict setObject:@"SINCE" forKey:@"distributedType"];
            [httpDict setObject:@"TO_BE_SHIPPED" forKey:@"status"];
           
            [newDict addEntriesFromDictionary:[Layout korder]];
            [self.myTableView showCell:newDict title:self.titleString];
            [self.myTableView reloadData];
            
        }else if (sender.tag == 11){
            [httpDict setObject:@"DGTYD" forKey:@"distributedType"];
            [httpDict setObject:@"TO_BE_SHIPPED" forKey:@"status"];
            
            [newDict addEntriesFromDictionary:[Layout korder]];
            [self.myTableView showCell:newDict title:self.titleString];
            [self.myTableView reloadData];
            
        }else if (sender.tag == 12){
            [httpDict removeObjectForKey:@"distributedType"];
            [httpDict setObject:@"REFUND_APPLICATION" forKey:@"status"];
        
            [newDict addEntriesFromDictionary:[Layout kkorder]];
            [self.myTableView showCell:newDict title:self.titleString];
            [self.myTableView reloadData];
            
        }else{
            [httpDict removeObjectForKey:@"distributedType"];
            [httpDict setObject:@"COMPLETE" forKey:@"status"];
            
            [newDict addEntriesFromDictionary:[Layout kkkorder]];
            [self.myTableView showCell:newDict title:self.titleString];
            [self.myTableView reloadData];
        }
        mybtnTag = sender.tag;//点击的时候btnTag改变
        [self.myTableView.mj_header beginRefreshing];
    }
}
-(void)btnSelected:(NSIndexPath*)indexPath data:(id)data btnTag:(NSInteger)bTag{

    NSInteger selected = indexPath.row % 10 ;
    NSInteger pageNumm = indexPath.row / 10 + 1;
#pragma mark 删除订单
        NSMutableDictionary *dictt = [NSMutableDictionary dictionaryWithCapacity:0];
        NSString *httpString = @"goods/order/user/order/list";
        [dictt setObject:@(pageNumm) forKey:@"pageNum"];
        [dictt setObject:[NSNumber numberWithInt:Limit] forKey:@"pageSize"];
        [dictt setObject:@1 forKey:@"typeId"];
        
        if (bTag == 1002) {
            
            //删除订单 调改变接口，刷新tableView
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"确定删除订单?"preferredStyle: UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"确定删除" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [dictt setObject:@"COMPLETE" forKey:@"status"];
                
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                
                //1.全部订单，看id
                [HttpUrl LTPOST:httpString dict:dictt WithSuccessBlock:^(id data) {
                    if ([data[@"success"]integerValue] == 1) {
                        NSArray *arr = [NSArray arrayWithArray:data[@"data"][@"list"]] ;
                        NSString *str  = arr[selected][@"id"];
                        NSMutableDictionary *dicttt = [NSMutableDictionary dictionaryWithCapacity:0];
                        [dicttt setObject:@"DELETE" forKey:@"status"];
                        [dicttt setObject:str forKey:@"id"];
                        //2.更新
                        [HttpUrl LTPOST:@"goods/order/update" dict:dicttt WithSuccessBlock:^(id data) {
                            if ([data[@"success"]intValue]== 1) {
                                //3.请求全部订单，刷新
                                [HttpUrl LTPOST:httpString dict:dictt WithSuccessBlock:^(id data) {
                                    if ([data[@"success"]intValue] == 1) {
                                        [MBProgressHUD hideHUDForView: self.view animated:YES];
                                        
                                        NSArray *arr = [NSArray arrayWithArray:data[@"data"][@"list"]];
                                        
                                        [self.myTableView showSection:0 uploadRows:0 uploadDate:arr];
                                        
                                        [self.myTableView.mj_header endRefreshing];
                                        [self.myTableView reloadData];
                                    }else{
                                        NSLog(@"datt---%@",data[@"msg"]);
                                        if ([data[@"code"]isEqualToString:@"EMPTY"]) {
                                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                                            NSArray *arr = [NSArray array];
                                            [self.myTableView showSection:0 uploadRows:0 uploadDate:arr];
                                            [self.myTableView reloadData];
                                        }
                                    }
                                    
                                }];
                            }else{
                                
                                [MBProgressHUD hideHUDForView:self.view animated:YES];
                                [TheGlobalMethod popAlertView:data[@"msg"] controller:self];
                                NSLog(@"hahhahhaha");
                            }
                        }];
                        
                    }else{
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        [TheGlobalMethod popAlertView:data[@"msg"] controller:self];
                        NSLog(@"bbbbbbbbbbb");
                        
                    }
                    
                }];
                
            }]];
            [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
            //弹出提示框；
            [self presentViewController:alert animated:true completion:nil];
            
            
            
        }
        
        else if (bTag == 1000) {
            
            //拒绝退款 弹窗 取消恢复 确定调改变接口改变成退款失败，刷新tableView
            //删除订单 调改变接口，刷新tableView
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"确定拒绝退款?"preferredStyle: UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                
                [dictt setObject:@"REFUND_APPLICATION" forKey:@"status"];
                //请求1，看id
                [HttpUrl LTPOST:httpString dict:dictt WithSuccessBlock:^(id data) {
                    if ([data[@"success"]integerValue] == 1) {
                        NSArray *arr = [NSArray arrayWithArray:data[@"data"][@"list"]] ;
                        NSString *str  = arr[selected][@"id"];
                        
                        NSMutableDictionary *dicttt = [NSMutableDictionary dictionaryWithCapacity:0];
                        [dicttt setObject:@"REFUND_FAILED" forKey:@"status"];
                        [dicttt setObject:str forKey:@"id"];
                        
                        //请求2 更新
                        [HttpUrl LTPOST:@"goods/order/update" dict:dicttt WithSuccessBlock:^(id data) {
                            if ([data[@"success"]intValue]== 1) {
                                //3 请求3 刷新
                                
                                [HttpUrl LTPOST:httpString dict:dictt WithSuccessBlock:^(id data) {
                                    
                                    NSLog(@"dadadada -----> %@",data);
                                    if ([data[@"success"]intValue]==1) {
                                        [MBProgressHUD hideHUDForView: self.view animated:YES];
                                        
                                        NSArray *arr = [NSArray arrayWithArray:data[@"data"][@"list"]];
                                        [self.myTableView showSection:0 uploadRows:0 uploadDate:arr];
                                        [self.myTableView.mj_header endRefreshing];
                                        [self.myTableView reloadData];
                                    }else{
                                        NSLog(@"datt---%@",data[@"msg"]);
                                        if ([data[@"code"]isEqualToString:@"EMPTY"]) {
                                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                                            NSArray *arr = [NSArray array];
                                            [self.myTableView showSection:0 uploadRows:0 uploadDate:arr];
                                            [self.myTableView reloadData];
                                        }
                                    }
                                    
                                    
                                }];
                            }
                        }];
                        
                    }
                    
                }];
                
            }]];
            [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
            //弹出提示框；
            [self presentViewController:alert animated:true completion:nil];
            
        }
        
        else if (bTag == 1001){
            
            //同意退款 跳支付界面 输完调改变接口已退款，刷新tableView
            ZCTradeView *ZCT = [[ZCTradeView alloc] init];
            [ZCT show];
            
            NSString *find = @"/user/find/user";
            
            [HttpUrl LTPOST:find dict:nil WithSuccessBlock:^(id data) {
                if ([data[@"success"]intValue] == 1) {
                    
                    payPassword = data[@"data"][@"payPassword"];
                    
                    if ([payPassword intValue] == 1) {
                        
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"请设置支付密码"]}];
                    }else{
                        NSLog(@"%@---pay",payPassword);
                        
                        
                        ZCT.finish = ^void (NSString *str1) {
                            //                             NSLog(@"%@---payyyy",payPassword);
                            [dictt setObject:@"REFUND_APPLICATION" forKey:@"status"];
                            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                            
                            //请求1 看id
                            [HttpUrl LTPOST:httpString dict:dictt WithSuccessBlock:^(id data) {
                                if ([data[@"success"]integerValue] == 1) {
                                    NSArray *arr = [NSArray arrayWithArray:data[@"data"][@"list"]] ;
                                    
                                    //请求2 update
                                    NSMutableDictionary *updateDict = [NSMutableDictionary dictionaryWithCapacity:0];
                                    NSString *strId  = arr[selected][@"id"];
                                    [updateDict setObject:@"REFUNDED" forKey:@"status"];
                                    [updateDict setObject:strId forKey:@"id"];
                                    [updateDict setObject:str1 forKey:@"payPassword"];
                                    [HttpUrl LTPOST:@"goods/order/update" dict:updateDict WithSuccessBlock:^(id data) {
                                        
                                        if ([data[@"success"]intValue] == 1) {
                            
                                            //请求4 刷新
//                                            [TheGlobalMethod popAlertView:@"退款成功" controller:self];
                                            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"退款成功" preferredStyle: UIAlertControllerStyleAlert];
                                            
                                            [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                
                                                //                                                kNavPop;
                                            }]];
                                            [HttpUrl LTPOST:httpString dict:dictt WithSuccessBlock:^(id data) {
                                                if ([data[@"success"]intValue]==1) {
                                                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                    
                                                    NSArray *arr = [NSArray arrayWithArray:data[@"data"][@"list"]];
                                                    [self.myTableView showSection:0 uploadRows:0 uploadDate:arr];
                                                    [self.myTableView.mj_header endRefreshing];
                                                    [self.myTableView reloadData];
                                                }else{
                                                    NSLog(@"datt---%@",data[@"msg"]);
                                                    if ([data[@"code"]isEqualToString:@"EMPTY"]) {
                                                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                        NSArray *arr = [NSArray array];
                                                        [self.myTableView showSection:0 uploadRows:0 uploadDate:arr];
                                                        [self.myTableView reloadData];
                                                    }
                                                }
                                                
                                                
                                            }];
                                        }else{
//                                            [TheGlobalMethod popAlertView:data[@"msg"] controller:self];
                                            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:data[@"msg"]preferredStyle: UIAlertControllerStyleAlert];
                                            
                                            [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                                
//                                                kNavPop;
                                            }]];
                                            [self presentViewController:alert animated:true completion:nil];
                                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                                        }
                                    }];
                           
                                }
                                
                            }];
                            
                        };
                        
                        
                    }
                    
                }else{
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":data[@"msg"]}];
                }
            }];
            
    
        }//btntag
    
}

-(void)selendIdCell:(NSIndexPath *)indexPath data:(id)data{
    
    NSInteger selected = indexPath.row % 10 ;
    NSInteger pageNumm = indexPath.row / 10 + 1;
    if (indexPath.section == 0) {
  
       
        OrderDetailVC *controller =[self.storyboard instantiateViewControllerWithIdentifier:@"OrderDetailVC"];
        controller.title = @"订单详情";
        controller.titleString = @"订单详情";
        controller.longitudeF = longitude;
        controller.latitudeF = latitude;
        
        //2.Post，根据分类传类型，利于布局
        //3.获取不同的tableView的cell的row对应的订单号
        NSString *httpString = @"goods/order/user/order/list";//订单管理
        [httpDict setObject:@(pageNumm) forKey:@"pageNum"];
        [httpDict setObject:[NSNumber numberWithInt:Limit] forKey:@"pageSize"];
        [httpDict setObject:@1 forKey:@"typeId"];
        
        //验券,待验证： 酒店0 KTV1  商城2 美食3
        // 外卖4
        if (mybtnTag == 10) {
            
            [httpDict setObject:@"TO_BE_SHIPPED" forKey:@"status"];
            [httpDict setObject:@"SINCE" forKey:@"distributedType"];
            [HttpUrl LTPOST:httpString dict:httpDict WithSuccessBlock:^(id data) {
                
                
                NSArray *arr = [NSArray arrayWithArray:data[@"data"][@"list"]];
                
                
                _orderNum = arr[selected][@"number"];//订单号
                controller.orderNum = _orderNum;
                
                _orderClassifyName = arr[selected][@"store"][@"classify"][@"name"];//类别
                if ([_orderClassifyName rangeOfString:@"酒店"].location != NSNotFound){
                    controller.orderClassifyName = 0;
                }else if ([_orderClassifyName rangeOfString:@"KTV"].location != NSNotFound){
                    controller.orderClassifyName = 1;
                }else if([_orderClassifyName rangeOfString:@"商城"].location != NSNotFound){
                    controller.orderClassifyName = 2;
                }else if([_orderClassifyName rangeOfString:@"美食"].location != NSNotFound){
                    controller.orderClassifyName = 3;
                }else if([_orderClassifyName rangeOfString:@"外卖"].location != NSNotFound){
                    controller.orderClassifyName = 4;
                }
                
                [self.navigationController pushViewController:controller animated:YES];
            }];
            
            //待发货  10
        }else if (mybtnTag == 11){
            
            [httpDict setObject:@"TO_BE_SHIPPED" forKey:@"status"];
            [httpDict setObject:@"DGTYD" forKey:@"distributedType"];
            [HttpUrl LTPOST:httpString dict:httpDict WithSuccessBlock:^(id data) {
                
                
                NSArray *arr = [NSArray arrayWithArray:data[@"data"][@"list"]];
                _orderNum =  arr[selected][@"number"];//订单号
                controller.orderNum = _orderNum;
                _orderClassifyName = arr[selected][@"store"][@"classify"][@"name"];//类别
                if ([_orderClassifyName rangeOfString:@"外卖"].location != NSNotFound) {
                    controller.orderClassifyName = 4;
                    
                }else{
                    controller.orderClassifyName = 10;}
                
                [self.navigationController pushViewController:controller animated:YES];
                
            }];
            
        }
        //待退款 100
        else if (mybtnTag == 12){
            
            [httpDict setObject:@"REFUND_APPLICATION" forKey:@"status"];
            [HttpUrl LTPOST:httpString dict:httpDict WithSuccessBlock:^(id data) {
                
                
                NSArray *arr = [NSArray arrayWithArray:data[@"data"][@"list"]];
                _orderNum =  arr[selected][@"number"];//订单号
                controller.orderNum = _orderNum;
                _orderClassifyName =  arr[selected][@"status"];
                if ([_orderClassifyName isEqualToString:@"REFUND_APPLICATION"]) {
                    controller.orderClassifyName = 100;
                }
                controller.block = ^(){
                    
                    [HttpUrl LTPOST:httpString dict:httpDict WithSuccessBlock:^(id data) {
                        NSLog(@"httpDictttt-----%@",httpDict);
                        if ([data[@"success"]intValue]==1) {
                            NSArray *arr = [NSArray arrayWithArray:data[@"data"][@"list"]];
                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                            
                            [self.myTableView showSection:0 uploadRows:0 uploadDate:arr];
                            
                            [self.myTableView.mj_header endRefreshing];
                            [self.myTableView reloadData];
                        }else{
                            NSLog(@"dattt---%@",data[@"msg"]);
                            if ([data[@"code"]isEqualToString:@"EMPTY"]) {
                                [MBProgressHUD hideHUDForView:self.view animated:YES];
                                NSArray *arr = [NSArray array];
                                [self.myTableView showSection:0 uploadRows:0 uploadDate:arr];
                                [self.myTableView reloadData];
                            }
                        }
                        
                    }];
                    
                };
                [self.navigationController pushViewController:controller animated:YES];
            }];
            
            //已完成 已退款1000
            //      其他同10
            
        }else if (mybtnTag == 13){
            [httpDict setObject:@"COMPLETE" forKey:@"status"];
            [HttpUrl LTPOST:httpString dict:httpDict WithSuccessBlock:^(id data) {
                
                NSArray *arr = [NSArray arrayWithArray:data[@"data"][@"list"]];
                if ([arr[selected][@"status"] isEqualToString:@"EVALUATION"]) {
                    controller.eva = @"EVALUATION";
                }
                _orderNum =  arr[selected][@"number"];//订单号
                _orderClassifyName = arr[selected][@"status"];
                controller.orderNum = _orderNum;
                controller.QRRecord = 1;
                if ([_orderClassifyName isEqualToString:@"REFUNDED"]) {
                    controller.orderClassifyName = 1000;
                }else{
                    NSString *name = arr[selected][@"store"][@"classify"][@"name"];
                    if ([name rangeOfString:@"酒店"].location != NSNotFound){
                        controller.orderClassifyName = 0;
                    }else if ([name rangeOfString:@"KTV"].location != NSNotFound){
                        controller.orderClassifyName = 1;
                    }else if([name rangeOfString:@"商城"].location != NSNotFound){
                        controller.orderClassifyName = 2;
                    }else if([name rangeOfString:@"美食"].location != NSNotFound){
                        controller.orderClassifyName = 3;
                    }else if([name rangeOfString:@"外卖"].location != NSNotFound){
                        controller.orderClassifyName = 4;
                    }else{
                        controller.orderClassifyName = 11;
                    }
                }
                
                [self.navigationController pushViewController:controller animated:YES];
            }];
        }
        
        
    }
}


#pragma mark -获取位置代理方法
// 代理方法实现
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    longitude = newLocation.coordinate.longitude;
    latitude = newLocation.coordinate.latitude;
    NSLog(@"%f,%f",newLocation.coordinate.latitude,newLocation.coordinate.longitude);
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"%@",error);
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)pop:(UIBarButtonItem *)sender {
    
    SetPop;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
