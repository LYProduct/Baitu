//
//  NavController.m
//  BS
//
//  Created by mc on 16/8/5.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "NavController.h"

@interface NavController ()

@end

@implementation NavController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(AlerView:) name:@"AlerView" object:nil];
}
-(void)AlerView:(NSNotification *)string{
    if([string.userInfo[@"text"] isEqualToString:@"111"]){
        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(networkingStatesFromStatebar) userInfo:nil repeats:NO];
    }else{
        [self AlerViewNSNotification:string.userInfo[@"text"]];
    }
}
-(void)networkingStatesFromStatebar{
    [self AlerViewNSNotification:[NetworkStatus networkingStatesFromStatebar]];
}
-(void)AlerViewNSNotification:(NSString *)string{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:string message:nil preferredStyle:  UIAlertControllerStyleAlert];
    //弹出提示框；
    [self presentViewController:alert animated:YES completion:nil];
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(creatAlert:) userInfo:alert repeats:NO];
}
- (void)creatAlert:(NSTimer *)timer{
    UIAlertController *alert = [timer userInfo];
    [alert dismissViewControllerAnimated:YES completion:nil];
    alert = nil;
    [timer invalidate];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
