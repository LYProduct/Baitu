//
//  SetPayPasswordVC.m
//  IT
//
//  Created by wyp on 16/11/26.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "SetPayPasswordVC.h"
#import "MineVC.h"
#import "TopUpVC.h"

@interface SetPayPasswordVC ()<UITextFieldDelegate> {
    
    
    __weak IBOutlet UITextField *TF;
    
    
}

@property (nonatomic, strong) NSMutableArray *arr;

@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UIButton *okBtn;


@end

@implementation SetPayPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    TF.delegate = self;
    self.arr = [NSMutableArray array];
    
    if ([self.isNum intValue] == 2) {
        _topLabel.text = _topStr;
        [_okBtn setTitle:_bottomString forState:(UIControlStateNormal)];
    }
    
    // Do any additional setup after loading the view.
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (![string isEqualToString:@""]) {
        if (_arr.count >= 6) {
            
        } else {
            [self.arr addObject:string];
        }
    } else {
        [self.arr removeLastObject];
        [self.arr addObject:@""];
    }
    
    
    for (int i = 101; i < 107; i++) {
        for (int j = 0; j < self.arr.count; j++) {
            if (i - 100 == self.arr.count) {
                UILabel *label = [self.view viewWithTag:i];
                label.text = self.arr[j];
            }
        }
    }
    if ([string isEqualToString:@""]) {
        [self.arr removeLastObject];
    }
    NSLog(@"array = %@", _arr);
    
    return YES;
}

- (IBAction)okbtn:(UIButton *)sender {
    if (_arr.count != 6) {
        [TheGlobalMethod xianShiAlertView:@"请设置6位密码" controller:self];
        return;
    }
    
    
    if ([self.isNum intValue] == 1) {
        SetPayPasswordVC *vc = [TheGlobalMethod setstoryboard:@"SetPayPasswordVC" controller:self];
        vc.topStr = @"确认支付密码";
        vc.bottomString = @"确定设置";
        vc.isNum = @"2";
        vc.phoneChange = self.phoneChange;
        vc.firstPW = [_arr componentsJoinedByString:@""];;
        vc.lol = self.lol;
        vc.title = @"确定支付密码";
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        
        NSString *str = [_arr componentsJoinedByString:@""];
        
        NSLog(@"string is %@, first is %@", str, _firstPW);
        if (![str isEqualToString:self.firstPW]) {
            [TheGlobalMethod xianShiAlertView:@"两次密码不同" controller:self];
            return;
        }
        
        if ([self.phoneChange length] > 0) {
            [HttpUrl LTPOST:@"user/update/pay/password" dict:@{@"payPassword":str, @"phone":[TheGlobalMethod getUserDefault:@"phone"], @"code":self.phoneChange} WithSuccessBlock:^(id data) {
                if ([data[@"success"] intValue] == 1) {
                    //提示框
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"设置成功"preferredStyle: UIAlertControllerStyleAlert];
                    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        //点击按钮的响应事件；
                        for (UIViewController *controller in self.navigationController.viewControllers) {
                            if ([controller isKindOfClass:[MineVC class]]) {
                                MineVC *revise =(MineVC *)controller;
                                [self.navigationController popToViewController:revise animated:YES];
                            }
                        }
                    }]];
                    //弹出提示框；
                    [self presentViewController:alert animated:true completion:nil];
                } else {
                    [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
                }
                NSLog(@"我设置了密码成功%@", data);
            }];
        } else {
        
        [HttpUrl LTPOST:@"user/shezhi/pay/password" dict:@{@"payPassword":str} WithSuccessBlock:^(id data) {
            
            if ([data[@"success"] intValue] == 1) {
                //提示框
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"设置成功"preferredStyle: UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    //点击按钮的响应事件；
                    if (self.lol.length > 0) {
                        for (UIViewController *controller in self.navigationController.viewControllers) {
                            if ([controller isKindOfClass:[TopUpVC class]]) {
                                TopUpVC *revise =(TopUpVC *)controller;
                                [self.navigationController popToViewController:revise animated:YES];
                            }
                        }
                    } else {
                        for (UIViewController *controller in self.navigationController.viewControllers) {
                            if ([controller isKindOfClass:[MineVC class]]) {
                                MineVC *revise =(MineVC *)controller;
                                [self.navigationController popToViewController:revise animated:YES];
                            }
                        }
                    }
                    
                }]];
                //弹出提示框；
                [self presentViewController:alert animated:true completion:nil];
            } else {
                [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
            }
            
            
            NSLog(@"我设置了密码成功%@", data);
        }];
        }
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
