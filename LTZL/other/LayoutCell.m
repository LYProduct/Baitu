//
//  LayoutCell.m
//  Shop
//
//  Created by ike on 16/11/18.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "LayoutCell.h"
#import <Masonry.h>

@interface LayoutCell(){
    
    
}


@end

@implementation LayoutCell
/**
 0、白色
 1、红色
 2、黄色s
 3、绿色
 4、蓝色
 5、橙色
 6、紫色
 */

-(void)commonCellNSInteger:(NSInteger)index label:(NSArray<UILabel *>*)labels btns:(NSArray<UIButton *>*)btns indexPath:(NSIndexPath *)indexPath myData:(NSDictionary *)dict{
    
    NSDictionary *goodsList = [NSDictionary dictionaryWithDictionary: dict[@"GoodsOrderMeasuresList"][0]];
    //goods
    NSDictionary *goods = [NSDictionary dictionaryWithDictionary:goodsList[@"goods"]];
    NSDictionary *goodsOrder = [NSDictionary dictionaryWithDictionary:dict[@"goodsOrder"]];
    NSDictionary *store = [NSDictionary dictionaryWithDictionary:dict[@"store"]];
    //  NSDictionary *goodsAtt;
    NSArray *goodsAttList;
    if ([[goods allKeys]containsObject:@"goodsAttributeList"]) {
        goodsAttList = [NSArray arrayWithArray:goods[@"goodsAttributeList"]];
        //        if (goodsAttList.count > 0 ) {
        //            goodsAtt = [NSDictionary dictionaryWithDictionary:goodsAttList[0]];
        //        }
    }
    
    if (indexPath.section == index){
        
        if (indexPath.row == 0) {
            [self label:labels index:0].text = @"商家信息";
        }else if (indexPath.row == 1){
            [self label:labels index:0].text =  goods[@"name"];
            [self label:labels index:2].text =  store[@"detailedAddress"];
            if ([store[@"distance"]floatValue] <= 1000) {
                [self label:labels index:3].text =  [NSString stringWithFormat:@"%.1fm",[store[@"distance"]floatValue]];
            }else{
                [self label:labels index:3].text =  [NSString stringWithFormat:@"%.1fkm",[store[@"distance"]floatValue]/1000];
            }
        }
    }else if (indexPath.section == index + 1 ){
        if (indexPath.row == 0){
            [self label:labels index:0].text =@"商品详情";
        }else if (indexPath.row == 1){
            [self label:labels index:0].text = goods[@"name"];
        }else if (indexPath.row == goodsAttList.count + 2){
            [self label:labels index:1].text =goods[@"attribute"];
        }else if (indexPath.row == goodsAttList.count + 3){
            [[self btn:btns index:0] setTitle:@"查看图文详情" forState:0];
        }
        else{
            
            [self label:labels index:0].text = goodsAttList[indexPath.row -2][@"name"];
            [self label:labels index:1].text = [NSString stringWithFormat:@"%i份", [goodsAttList[indexPath.row -2][@"attr"] intValue]] ;
            [self label:labels index:2].hidden = NO;
            [self label:labels index:2].text = [NSString stringWithFormat:@"￥%.2f",[goodsAttList[indexPath.row -2][@"unitPrice"] floatValue]];
        }
        
    }else if (indexPath.section == index  + 2){
        if (indexPath.row == 0) {
            [self label:labels index:0].text = @"订单详情";
        }else if (indexPath.row == 1){
            [self label:labels index:4].text =  [NSString stringWithFormat:@"订单编号:%@",goodsOrder[@"number"]];
            
            NSString *stringPhone;
            
            NSLog(@"goods00  %@ ",goodsOrder);
            if ([[goodsOrder allKeys]containsObject:@"userPhone"]) {
                
                if ([goodsOrder[@"userPhone"] isEqual:[NSNull null]] || !goodsOrder[@"userPhone"] ) {
                    stringPhone = @"";
                }else{
                    stringPhone = goodsOrder[@"userPhone"];
                }
            }else{
                stringPhone = @"";
            }
            NSLog(@"----ppppp%@",stringPhone);
            [self label:labels index:0].text =  [NSString stringWithFormat:@"购买手机号:%@",stringPhone];
            
            NSString *time = [[NSString stringWithFormat:@"%@",[goodsList objectForKey: @"gmtDatetime"]]substringToIndex:10];
            [self label:labels index:3].text =  [NSString stringWithFormat:@"付款时间:%@",[TheGlobalMethod dateStr:time]];
            [self label:labels index:1].text =  [NSString stringWithFormat:@"数量:%i", [goodsList[@"sum"] intValue]];
            [self label:labels index:2].text =  [NSString stringWithFormat:@"总价:￥%.2f",[goodsList[@"unitPrice"] floatValue]];
            
        }
        
    }
    
}


-(void)showCell:(id)data tableViewCell:(TableViewCell *)cell label:(NSArray<UILabel *>*)labels btns:(NSArray<UIButton *>*)btns TFViews:(NSArray<UITextField *>*)TFViews imgView:(NSArray<UIImageView *>*)imgs showView:(NSArray<UIView *>*)views title:(NSString *)title indexPath:(NSIndexPath *)indexPath{
#pragma mark 我的二维码
    if ([title isEqualToString:@"我的二维码"]) {
        
        NSLog(@"%@--headUrls--",data);
        if ([[data allKeys] containsObject:@"headUrl"]) {
            if (![data[@"headUrl"]isEqual:[NSNull null]] && data[@"headUrl"]) {
                [[self img:imgs index:0]sd_setImageWithURL:[NSURL URLWithString:data[@"headUrl"]] placeholderImage:[UIImage imageNamed:@"moren_touxiang44"]];
            }
            
        }
        if (indexPath.section == 0) {
            if (indexPath.row == 0) {
                if ([[data allKeys] containsObject:@"name"]) {
                    if (![data[@"name"]isEqual:[NSNull null]] && data[@"name"]){
                        [self label:labels index:1].text = data[@"name"];
                    }
                }
            }
        }
        
        
        if ([[data allKeys]containsObject:@"membershipCard"]) {
            
            //生成自己的二维码
            CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
            
            // 2.恢复滤镜的默认属性 (因为滤镜有可能保存上一次的属性)
            [filter setDefaults];
            
            // 3.将字符串转换成NSdata yx_
            //
            //http://www.yuexiang.com?yx_userid=%@
            NSString *urlString = [NSString stringWithFormat:@"https://itunes.apple.com/us/app/yue-xiang-lin-tao/id1187784455?l=zh&ls=1&mt=8?yx_userid=%@",data[@"membershipCard"][@"numberCard"]];
            NSData *data  = [urlString dataUsingEncoding:NSUTF8StringEncoding];
            
            // 4.通过KVO设置滤镜, 传入data, 将来滤镜就知道要通过传入的数据生成二维码
            [filter setValue:data forKey:@"inputMessage"];
            
            // 5.生成二维码
            CIImage *outputImage = filter.outputImage;
            //        CGFloat scale = CGRectGetWidth(cell.img.bounds) / CGRectGetWidth(outputImage.extent);
            CGAffineTransform transform = CGAffineTransformMakeScale(10, 10); // scale 为放大倍数
            CIImage *transformImage = [outputImage imageByApplyingTransform:transform];
            
            // 保存
            CIContext *context = [CIContext contextWithOptions:nil];
            CGImageRef imageRef = [context createCGImage:transformImage fromRect:transformImage.extent];
            UIImage *qrCodeImage = [UIImage imageWithCGImage:imageRef];
            
            // 6.设置生成好得二维码到imageView上
            [self img:imgs index:1].image = qrCodeImage;
            
        }
    }else
#pragma mark  我的团队
        if ([title isEqualToString:@"我的团队"]) {
            
            if(indexPath.section == 0){
                if ([data isKindOfClass:[NSArray class]]) {
                    NSDictionary *dict = [NSDictionary dictionaryWithDictionary:data[indexPath.row]];
                    if (![dict[@"name"] isEqual:[NSNull null]] && dict[@"name"]) {
                        [self label:labels index:0].text = [NSString stringWithFormat:@"%@",dict[@"name"]];
                        
                    }else{
                        [self label:labels index:0].text =@"未命名";
                    }
                    
                    NSString *timee = [[NSString stringWithFormat:@"%@",dict[@"gmtDatetime"]] substringToIndex:10];
                    [self label:labels index:1].text = [NSString stringWithFormat:@"注册日期 %@",[[TheGlobalMethod dateStr:timee]substringToIndex:10]];
                    [self label:labels index:2].text = [NSString stringWithFormat:@"+￥%.2f",[dict[@"balance"]floatValue]];
                    if ([[dict allKeys]containsObject:@"headUrl"]) {
                        if(![dict[@"headUrl"] isEqual:[NSNull null]] && dict[@"headUrl"]){
                            [[self img:imgs index:0]sd_setImageWithURL:[NSURL URLWithString:dict[@"headUrl"]] placeholderImage:[UIImage imageNamed:@"touxiang666"]];
                        }
                        //
                    }
                    
                    [self label:labels index:3].text = [NSString stringWithFormat:@"%i个成员",[dict[@"memberSum"]intValue]];
                }
                
                
            }
        }
    
        else
#pragma mark 验券结果
            if ([title isEqualToString:@"验券结果"]) {
                
                if (indexPath.section == 0) {
                    
                    [self label:labels index:0].text = [NSString stringWithFormat:@"%@",data[@"goodsBuyNumber"]];
                    [self label:labels index:1].text = [NSString stringWithFormat:@"消费金额:￥%.2f",[data[@"totalPrice"]floatValue]];
                    [self label:labels index:2].text = [NSString stringWithFormat:@"顾客:%@",data[@"user"][@"phone"]];
                    [self label:labels index:3].text = [NSString stringWithFormat:@"团购数量:%i",[[data[@"goodsOrderMeasuresList"][0] objectForKey:@"sum"]intValue]];
                    
                    [self label:labels index:4].text = [NSString stringWithFormat:@"团购价:¥%.2f",[[data[@"goodsOrderMeasuresList"][0] objectForKey:@"unitPrice"]floatValue]];
                    
                    if ([[data allKeys]containsObject:@"uptDatetime"]) {
                        NSString *time = [[NSString stringWithFormat:@"%@",[data objectForKey: @"uptDatetime"]]substringToIndex:10];
                        [self label:labels index:5].text = [NSString stringWithFormat:@"消费时间:%@",[TheGlobalMethod dateStr:time]];
                    }
                    
                    //            [self img:imgs index:0].hidden = YES;
                    
                }
            }else
                
#pragma mark 订单管理
                if ([title isEqualToString:@"订单管理"]) {
                    cell.ButtomHeight = 10;
                    if (indexPath.section == 0) {
                        if ([data isKindOfClass:[NSArray class]]) {
                            NSArray *array = [NSArray arrayWithArray:data];
                            NSDictionary *dict = [NSDictionary dictionaryWithDictionary:array[indexPath.row]];
                            
                            NSString *pic;
                            NSDictionary *dict1 = dict[@"goodsOrderMeasuresList"][0];
                            if ([[dict1 allKeys]containsObject:@"goods"]) {
                                NSArray *arr  = dict1[@"goods"][@"carousel"];
                                if (arr.count > 0) {
                                    pic = arr[0][@"imgUrl"];
                                    [[self img:imgs index:0] sd_setImageWithURL:[NSURL URLWithString:pic]];
                                }
                            }
                            
                            
                            [self label:labels index:0].text = [NSString stringWithFormat:@"订单编号:%@",dict[@"number"]];
                            //title
                            [self label:labels index:2].text = [NSString stringWithFormat:@"%@",[[dict[@"goodsOrderMeasuresList"][0]objectForKey:@"goods"] objectForKey:@"name"]];
                            //时间，待改正，加字段, 在classify里面的name判断是否是ktv 酒店..
                            NSString *timeStr  = [dict objectForKey:@"gmtDatetime"];
                            NSString *time = [[NSString stringWithFormat:@"%@",timeStr]substringToIndex:10];
                            [self label:labels index:3].text = [NSString stringWithFormat:@"时间:%@",[TheGlobalMethod dateStr:time]];
                            [self label:labels index:4].text = [NSString stringWithFormat:@"￥%.2f",[dict[@"totalPrice"]floatValue]];
                            [self label:labels index:5].text = [NSString stringWithFormat:@"*%@",[dict[@"goodsOrderMeasuresList"][0] objectForKey:@"sum"]];
                            //当选择待认证的时候
                            if ([dict[@"status"] isEqualToString:@"TO_BE_SHIPPED"]&&[dict[@"distributedType"]isEqualToString:@"SINCE"]) {
                                [self label:labels index:1].text = [NSString stringWithFormat:@"待验证"];
                            }else if ([dict[@"status"] isEqualToString:@"TO_BE_SHIPPED"]&&[dict[@"distributedType"]isEqualToString:@"DGTYD"]){
                                [self label:labels index:1].text = [NSString stringWithFormat:@"待发货"];
                            }else if ([dict[@"status"] isEqualToString:@"TO_BE_SHIPPED"]&&[dict[@"distributedType"]isEqualToString:@"SINCE"]){
                                [self label:labels index:1].text = [NSString stringWithFormat:@"待上门"];
                            }else if ([dict[@"status"] isEqualToString:@"REFUND_APPLICATION"]){
                                [self label:labels index:1].text = [NSString stringWithFormat:@"待退款"];
                            }else if ([dict[@"status"] isEqualToString:@"COMPLETE"]){
                                [self label:labels index:1].text = [NSString stringWithFormat:@"已完成"];
                            }else if ([dict[@"status"] isEqualToString:@"EVALUATION"]){
                                [self label:labels index:1].text = [NSString stringWithFormat:@"待评价"];
                                [self btn:btns index:0].hidden = YES;
                                
                                //                            cell.height = 118;
                                //                             cell.ButtomHeight = 1;
                                
                                [cell tableViewCellSize:145];
                            }
                            
                            
                        }
                    }
                    
                }else
                    
                    
#pragma mark 提现记录
                    if ([title isEqualToString:@"提现记录"]) {
                        if (indexPath.section == 0) {
                            if ([data isKindOfClass:[NSArray class]]) {
                                
                                NSArray *array = [NSArray arrayWithArray:data];
                                //            NSLog(@"%f----hhhhh",[array[indexPath.row][@"money"] floatValue]);
                                
                                if ([[array[indexPath.row]allKeys] containsObject:@"money"]) {
                                    if (![array[indexPath.row][@"money"] isEqual:[NSNull null]] && array[indexPath.row][@"money"]) {
                                        [self label:labels index:0].text = [NSString stringWithFormat:@"￥%.2f",[array[indexPath.row][@"money"] floatValue]];
                                    }else{
                                        [self label:labels index:0].text = @"";
                                    }
                                    
                                }else{
                                    [self label:labels index:0].text = @"";
                                }
                                
                                
                                if ([array[indexPath.row][@"status"] isEqualToString:@"IN_TREATMENT"]) {
                                    [self label:labels index:6].text = @"处理中";
                                    [self label:labels index:6].textColor = [UIColor greenColor];
                                }else if ([array[indexPath.row][@"status"]isEqualToString:@"WITHDRAWALS_FAIL"]){
                                    [self label:labels index:6].text = @"失败";
                                    [self label:labels index:6].textColor = [UIColor redColor];
                                }else if ([array[indexPath.row][@"status"]isEqualToString:@"WITHDRAWALS_SUCCESS"]){
                                    [self label:labels index:6].textColor = [UIColor yellowColor];
                                    [self label:labels index:6].text = @"可领钱";
                                }else if([array[indexPath.row][@"status"]isEqualToString:@"COMPLETE"]){
                                    [self label:labels index:6].text = @"已领钱";
                                }
                                [self label:labels index:1].text = [NSString stringWithFormat:@"店铺名称:%@",array[indexPath.row][@"storeName"]];
                                
                                
                                if ([[array[indexPath.row] allKeys]containsObject:@"userName"]) {
                                    
                                    NSString *name = array[indexPath.row][@"userName"];
                                    if (![name isEqual:[NSNull null]] && name && name.length > 1) {
                                        [self label:labels index:3].text = [NSString stringWithFormat:@"姓名:%@",[name stringByReplacingCharactersInRange:NSMakeRange(1, 1) withString:@"*"]];
                                    }else{
                                        [self label:labels index:3].text= @"姓名:*";
                                    }
                                }
                                
                                
                                NSString *phone = array[indexPath.row][@"phone"];
                                [self label:labels index:2].text = [NSString stringWithFormat:@"手机号码:%@",[phone stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"]];
                                
                                NSString *idCard = array[indexPath.row][@"idCard"];
                                [self label:labels index:5].text = [NSString stringWithFormat:@"身份证号:%@", [idCard stringByReplacingCharactersInRange:NSMakeRange(6, 8) withString:@"********"]];
                                
                                NSString *remark = array[indexPath.row][@"remark"];
                                if (![remark isEqual:[NSNull null]] && remark) {
                                    [self label:labels index:4].text = [NSString stringWithFormat:@"备注:%@",array[indexPath.row][@"remark"]];
                                }else{
                                    [self label:labels index:4].text = @"备注:无";
                                }
                                
                                
                                
                                NSString *time = [[NSString stringWithFormat:@"%@",[array[indexPath.row]objectForKey: @"gmtDatetime"]]substringToIndex:10];
                                [self label:labels index:7].text = [NSString stringWithFormat:@"时间:%@",[TheGlobalMethod dateStr:time]];
                                
                            }
                        }
                    }else
                        
#pragma mark 支付确认
                        if ([title isEqualToString:@"支付确认"]) {
                            
                            if (indexPath.section == 0) {
                                if(indexPath.row == 0){
                                    [self label:labels index:0].text = @"消费金额";
                                    [self label: labels index:0].textColor = [UIColor blackColor];
                                    [self label:labels index:1].text = [NSString stringWithFormat:@"￥%.2f",[data[@"totalmoney"]floatValue]];
                                    [self label:labels index:1].textColor = [UIColor redColor];
                                }else if (indexPath.row == 1){
                                    [self label:labels index:0].text = @"不参与优惠金额";
                                    [self label: labels index:0].textColor = [UIColor blackColor];
                                    [self label:labels index:1].text = [NSString stringWithFormat:@"￥%.2f",[data[@"noMoney"]floatValue]];
                                    [self label:labels index:1].textColor = [UIColor redColor];
                                }else if (indexPath.row == 2){
                                    [self label:labels index:0].text = @"优惠金额";
                                    [self label: labels index:0].textColor = [UIColor blackColor];
                                    [self label:labels index:1].text = [NSString stringWithFormat:@"￥%.2f",[data[@"youhui"]floatValue]];
                                    [self label:labels index:1].textColor = [UIColor redColor];
                                }else if (indexPath.row == 3){
                                    [self label:labels index:0].text = @"实付金额";
                                    [self label: labels index:0].textColor = [UIColor blackColor];
                                    [self label:labels index:1].text = [NSString stringWithFormat:@"￥%.2f",[data[@"payMoney"]floatValue]];
                                    [self label:labels index:1].textColor = [UIColor redColor];
                                }
                            }
                            else if(indexPath.section == 1){
                                if (indexPath.row == 0) {
                                    [self img:imgs index:0].image = [UIImage imageNamed:@"jine"];
                                    NSLog(@"%@aaaa",data[@"left"]);
                                    [self label:labels index:0].text = [NSString stringWithFormat:@"用户余额￥%.2f",[data[@"left"]floatValue]];
                                }
                                
                            }else if (indexPath.section == 2){
                                if (indexPath.row == 0) {
                                    
#warning --??
                                    [[self btn:btns index:0]setTitle:[NSString stringWithFormat:@"确认支付(%.2f)",[data[@"pay"]floatValue] ]forState:UIControlStateNormal];
                                }
                                
                            }
                        }
                        else
#pragma mark 买单
                            if ([title isEqualToString:@"买单"]) {
                                if (indexPath.section == 0) {
                                    [self label:labels index:0].text = @"消费金额";
                                    TFViews[0].placeholder = @"请输入金额";
                                    if (data != NULL) {
                                        TFViews[0].text = [NSString stringWithFormat:@"%.2f",[data floatValue]];
                                    }
                                }else if(indexPath.section == 1){
                                    [self label:labels index:0].text = @"不可优惠金额";
                                    TFViews[0].placeholder = @"询问后输入";
                                    if (data != NULL) {
                                        TFViews[0].text = [NSString stringWithFormat:@"%.2f",[data floatValue]];
                                    }
                                    
                                }else if(indexPath.section == 2){
                                    if ([[data allKeys]containsObject:@"preferentialAmount"] && [[data allKeys]containsObject:@"fullAmount"]) {
                                        NSString *man = [NSString stringWithFormat:@"%.2f",[data[@"fullAmount"]floatValue]];
                                        NSString *jian = [NSString stringWithFormat:@"%.2f",[data[@"preferentialAmount"]floatValue]];
                                        [self label:labels index:0].text = [NSString stringWithFormat:@"满%@减%@",man,jian];
                                    }else{
                                        [self label:labels index:0].text = @"";
                                    }
                                }
                            }else
                                
#pragma mark 提现填写
                                if ([title isEqualToString:@"提现填写"]) {
                                    if (indexPath.section == 0) {
                                        if(indexPath.row == 0){
                                            [self label:labels index:0].text = @"店铺名字";
                                            TFViews[0].placeholder = @"请输入店铺名字";
                                            if ([[data allKeys] containsObject:@"storeName"]) {
                                                TFViews[0].text = data[@"storeName"];
                                            }
                                        }else if (indexPath.row == 1){
                                            [self label:labels index:0].text = @"姓名";
                                            TFViews[0].placeholder = @"请输入姓名";
                                            if ([[data allKeys] containsObject:@"userName"]) {
                                                TFViews[0].text = data[@"userName"];
                                            }
                                        }else if (indexPath.row == 2){
                                            [self label:labels index:0].text = @"手机号码";
                                            TFViews[0].placeholder = @"请输入手机号码";
                                            if ([[data allKeys] containsObject:@"phone"]) {
                                                TFViews[0].text = data[@"phone"];
                                            }
                                        }else if (indexPath.row == 3){
                                            [self label:labels index:0].text = @"身份证号";
                                            TFViews[0].placeholder = @"请输入身份证号";
                                            if ([[data allKeys] containsObject:@"idCard"]) {
                                                TFViews[0].text = data[@"idCard"];
                                            }
                                        }
                                    }else if(indexPath.section == 1){
                                        if(indexPath.row == 0){
                                            [self label:labels index:0].text = @"提现金额";
                                            TFViews[0].placeholder = @"请输入提现金额";
                                            if ([[data allKeys] containsObject:@"money"]) {
                                                TFViews[0].text = [NSString stringWithFormat:@"%.2f",[data[@"money"]floatValue]];
                                            }
                                        }else if (indexPath.row == 1){
                                            [self label:labels index:0].text = @"备注";
                                            TFViews[0].placeholder = @"请输入备注";
                                            if ([[data allKeys] containsObject:@"remark"]) {
                                                TFViews[0].text = data[@"remark"];
                                            }else{
                                                TFViews[0].text = @"";
                                            }
                                        }}
                                    
                                    
#pragma mark 验证记录
                                }else if ([title isEqualToString:@"验证记录"]) {
                                    if (indexPath.section == 0) {
                                        
                                        if (indexPath.row == 0) {
                                            
                                            NSMutableAttributedString *dealStr = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"已验券%i张",[data intValue]]];
                                            NSUInteger dealLen1= [[dealStr string] rangeOfString:@"张"].location ;
                                            NSUInteger dealLen2 = [[dealStr string]rangeOfString:@"券"].location ;
                                            NSRange dealRange = NSMakeRange([[dealStr string]rangeOfString:@"券"].location + 1,dealLen1 - dealLen2 - 1);
                                            //需要设置的位置
                                            [dealStr addAttribute:NSForegroundColorAttributeName value:ColorBlue_k range:dealRange];
                                            //设置颜色
                                            [[self label:labels index:0] setAttributedText : dealStr];
                                        }
                                    }else if (indexPath.section == 1) {
                                        
                                        if ([data isKindOfClass:[NSArray class]]) {
                                            
                                            NSArray *array = [NSArray arrayWithArray:data];
                                            NSDictionary *dict = [NSDictionary dictionaryWithDictionary:array[indexPath.row]];
                                            if([[dict allKeys] containsObject:@"goodsBuyNumber"]){
                                                [self label:labels index:0].text = [NSString stringWithFormat:@"%@",array[indexPath.row][@"goodsBuyNumber"]] ;
                                            }
                                            if([[dict allKeys] containsObject:@"goodsOrderMeasuresList"]){
                                                [self label:labels index:1].text =   [NSString stringWithFormat:@"%@", [[array[indexPath.row][@"goodsOrderMeasuresList"][0] objectForKey:@"goods"]objectForKey:@"name"]] ;
                                            }
                                            if([[dict allKeys] containsObject:@"user"]){
                                                NSString *phone  = [array[indexPath.row][@"user"] objectForKey: @"phone"];
                                                [self label:labels index:2].text = [NSString stringWithFormat:@"顾客:%@",[phone stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"]];
                                            }
                                            if([[dict allKeys]containsObject:@"goodsOrderMeasuresList"]){
                                                [self label:labels index:3].text = [NSString stringWithFormat:@"团购数量:%i",[[array[indexPath.row][@"goodsOrderMeasuresList"][0] objectForKey:@"sum"]intValue]];
                                            }
                                            if ([[dict allKeys]containsObject:@"goodsOrderMeasuresList"]) {
                                                
                                                [self label:labels index:4].text = [NSString stringWithFormat:@"团购价:￥%.2f",[[array[indexPath.row][@"goodsOrderMeasuresList"][0] objectForKey:@"unitPrice"]floatValue]];
                                            }
                                            if ([[dict allKeys]containsObject:@"uptDatetime"]) {
                                                
                                                NSString *time = [[NSString stringWithFormat:@"%@",[array[indexPath.row]objectForKey:@"uptDatetime"]]substringToIndex:10];
                                                
                                                [self label:labels index:5].text = [NSString stringWithFormat:@"%@",[TheGlobalMethod dateStr:time]];
                                            }
                                            
                                        }
                                    }}else
                                        
#pragma mark 到店付记录
                                        if ([title isEqualToString:@"到店付记录"]) {
                                            
                                            if (indexPath.section == 0) {
                                                
                                                if ([data isKindOfClass:[NSArray class]]) {
                                                    
                                                    NSArray *array = [NSArray arrayWithArray:data];
                                                    NSDictionary *dataDict = [NSDictionary dictionaryWithDictionary:array[indexPath.row]];
                                                    if (dataDict.count > 0) {
                                                        [self label:labels index:6].text = [NSString stringWithFormat:@"订单编号:%@",dataDict[@"threePayId"]];
                                                        if (![dataDict[@"totalMoney"] isEqual:[NSNull null]]) {
                                                            [self label:labels index:2].text = [NSString stringWithFormat:@"消费金额:￥%.2f",[dataDict[@"totalMoney"] floatValue]];
                                                        }else{
                                                            [self label:labels index:2].text = @"消费金额:0.00";
                                                        }
                                                        
                                                        if (![dataDict[@"noMoney"] isEqual:[NSNull null]]) {
                                                            [self label:labels index:0].text = [NSString stringWithFormat:@"不参与优惠金额:￥%.1f",[array[indexPath.row][@"noMoney"] floatValue]];
                                                        }else{
                                                            [self label:labels index:0].text = @"不参与优惠金额:0.0";
                                                        }
                                                        
                                                        if (![dataDict[@"discountMoney"] isEqual:[NSNull null]]) {
                                                            [self label:labels index:3].text = [NSString stringWithFormat:@"优惠金额:￥%.2f",[array[indexPath.row][@"discountMoney"]floatValue]];
                                                        }else{
                                                            [self label:labels index:3].text = @"优惠金额:0.00";
                                                        }
                                                        
                                                        if (![dataDict[@"amount"] isEqual:[NSNull null]]) {
                                                            [self label:labels index:1].text = [NSString stringWithFormat:@"实付金额:￥%.2f",[array[indexPath.row][@"amount"] floatValue]];
                                                        }else{
                                                            [self label:labels index:1].text = @"实付金额:0.00";
                                                        }
                                                        
                                                        NSString *phone = [NSString stringWithFormat:@"%@",[array[indexPath.row][@"devoteUser"] objectForKey: @"phone"]];
                                                        [self label:labels index:4].text = [NSString stringWithFormat:@"消费用户:%@",[phone stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"]];
                                                        NSString *time = [[NSString stringWithFormat:@"%@",[array[indexPath.row]objectForKey: @"gmtDatetime"]]substringToIndex:10];
                                                        [self label:labels index:5].text = [NSString stringWithFormat:@"消费时间:%@",[TheGlobalMethod dateStr:time]];
                                                    }
                                                }
                                            }
                                        }else
#pragma mark 财务管理
                                            if ([title isEqualToString:@"财务管理"]) {
                                                if (indexPath.section == 0) {
                                                    if (indexPath.row == 0) {
                                                        NSDictionary *dic = [NSDictionary dictionaryWithDictionary:data];
                                                        if (![dic[@"sum"]isEqual:[NSNull null]] && dic[@"sum"]) {
                                                            [self label:labels index:0].text = [NSString stringWithFormat:@"%.2f",[[dic objectForKey:@"sum"]floatValue]];
                                                        }
                                                        if (![dic[@"sum"]isEqual:[NSNull null]] && dic[@"settled"]) {
                                                            [self label:labels index:1].text = [NSString stringWithFormat:@"%.2f",[[dic objectForKey:@"settled"]floatValue]];
                                                        }
                                                        if (![dic[@"sum"]isEqual:[NSNull null]] && dic[@"pendingSettlement"]) {
                                                            [self label:labels index:2].text = [NSString stringWithFormat:@"%.2f",[[dic objectForKey:@"pendingSettlement"]floatValue]];
                                                        }
                                                        
                                                        
                                                        
                                                    }
                                                }
                                                else if (indexPath.section == 1) {
                                                    
                                                    
                                                    if ([data isKindOfClass:[NSArray class]]) {
                                                        NSArray *array = [NSArray arrayWithArray:data];
                                                        if (array.count >0) {
                                                            
                                                            if ([array[indexPath.row][@"title"]isEqualToString:@"到店付款"]) {
                                                                
                                                                if ([data isKindOfClass:[NSArray class]]) {
                                                                    
                                                                    NSDictionary *dataDict = [NSDictionary dictionaryWithDictionary:array[indexPath.row]];
                                                                    if (dataDict.count > 0) {
                                                                        [self label:labels index:6].text = [NSString stringWithFormat:@"订单编号:%@",dataDict[@"threePayId"]];
                                                                        if (![dataDict[@"totalMoney"] isEqual:[NSNull null]]) {
                                                                            [self label:labels index:2].text = [NSString stringWithFormat:@"消费金额:￥%.2f",[dataDict[@"totalMoney"] floatValue]];
                                                                        }else{
                                                                            [self label:labels index:2].text = @"消费金额:0.00";
                                                                        }
                                                                        
                                                                        if (![dataDict[@"noMoney"] isEqual:[NSNull null]]) {
                                                                            [self label:labels index:0].text = [NSString stringWithFormat:@"不参与优惠金额:￥%.2f",[array[indexPath.row][@"noMoney"] floatValue]];
                                                                        }else{
                                                                            [self label:labels index:0].text = @"不参与优惠金额:0.0";
                                                                        }
                                                                        
                                                                        if (![dataDict[@"discountMoney"] isEqual:[NSNull null]]) {
                                                                            [self label:labels index:3].text = [NSString stringWithFormat:@"优惠金额:￥%.2f",[array[indexPath.row][@"discountMoney"]floatValue]];
                                                                        }else{
                                                                            [self label:labels index:3].text = @"优惠金额:0.00";
                                                                        }
                                                                        
                                                                        if (![dataDict[@"amount"] isEqual:[NSNull null]]) {
                                                                            [self label:labels index:1].text = [NSString stringWithFormat:@"实付金额:￥%.2f",[array[indexPath.row][@"amount"] floatValue]];
                                                                        }else{
                                                                            [self label:labels index:1].text = @"实付金额:0.00";
                                                                        }
                                                                        
                                                                        NSString *phone = [NSString stringWithFormat:@"%@",[array[indexPath.row][@"devoteUser"] objectForKey: @"phone"]];
                                                                        [self label:labels index:4].text = [NSString stringWithFormat:@"消费用户:%@",[phone stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"]];
                                                                        NSString *time = [[NSString stringWithFormat:@"%@",[array[indexPath.row]objectForKey: @"gmtDatetime"]]substringToIndex:10];
                                                                        [self label:labels index:5].text = [NSString stringWithFormat:@"消费时间:%@",[TheGlobalMethod dateStr:time]];
                                                                    }
                                                                }
                                                                
                                                                
                                                                //                        到店付
                                                                //                             [self label:labels index:6].text = [NSString stringWithFormat:@"订单编号:%@",array[indexPath.row][@"threePayId"]];
                                                                //                             [self label:labels index:2].text = [NSString stringWithFormat:@"消费金额:￥%@",array[indexPath.row][@"totalMoney"]];
                                                                //                             [self label:labels index:0].text = [NSString stringWithFormat:@"不参与优惠金额:￥%@",array[indexPath.row][@"noMoney"]];
                                                                //                             [self label:labels index:3].text = [NSString stringWithFormat:@"优惠金额:￥%@",array[indexPath.row][@"discountMoney"]];
                                                                //                             [self label:labels index:1].text = [NSString stringWithFormat:@"实付金额:￥%@",array[indexPath.row][@"totalMoney"]];
                                                                //                             NSString *phone = [NSString stringWithFormat:@"%@",[array[indexPath.row][@"devoteUser"] objectForKey: @"phone"]];
                                                                //                             [self label:labels index:4].text = [NSString stringWithFormat:@"消费用户:%@",[phone stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"]];
                                                                //                             NSString *time = [[NSString stringWithFormat:@"%@",[array[indexPath.row]objectForKey: @"gmtDatetime"]]substringToIndex:10];
                                                                //                             [self label:labels index:5].text = [NSString stringWithFormat:@"消费时间:%@",[TheGlobalMethod dateStr:time]];
                                                                
                                                            }else {
                                                                
                                                                [self label:labels index:0].text = [NSString stringWithFormat:@"订单编号:%@",array[indexPath.row][@"goodsOrderId"]];
                                                                [self label:labels index:1].text = array[indexPath.row][@"title"];
                                                                if ([[array[indexPath.row] allKeys]containsObject:@"orderSum"]) {
                                                                    if (array[indexPath.row][@"orderSum"] && ![array[indexPath.row][@"orderSum"]isEqual:[NSNull null]] ) {
                                                                        NSLog(@"%@ ---llllll",array[indexPath.row][@"orderSum"] );
                                                                        [self label:labels index:2].text= [NSString stringWithFormat:@"数量:%i", [array[indexPath.row][@"orderSum"]intValue]];
                                                                    }else
                                                                        [self label:labels index:2].text= [NSString stringWithFormat:@"数量:0"];
                                                                }
                                                                
                                                                [self label:labels index:3].text= [NSString stringWithFormat:@"金额:￥%.2f", [array[indexPath.row][@"amount"]floatValue]];
                                                                if ([[array[indexPath.row] allKeys]containsObject:@"uptDatetime"]) {
                                                                    NSString *upDateTime = [[NSString stringWithFormat:@"%@",[array[indexPath.row]objectForKey:@"uptDatetime"]]substringToIndex:10];
                                                                    [self label:labels index:4].text = [NSString stringWithFormat:@"下单时间:%@",[TheGlobalMethod dateStr:upDateTime]];
                                                                }
                                                                
                                                                
                                                                NSString *gmtDateTime = [[NSString stringWithFormat:@"%@",[array[indexPath.row]objectForKey: @"gmtDatetime"]]substringToIndex:10];
                                                                [self label:labels index:5].text = [NSString stringWithFormat:@"消费时间:%@",[TheGlobalMethod dateStr:gmtDateTime]];
                                                                
                                                                if ([[array[indexPath.row]allKeys] containsObject:@"devoteUser"]) {
                                                                    //
                                                                    NSDictionary *dict = [NSDictionary dictionaryWithDictionary:array[indexPath.row][@"devoteUser"]];
                                                                    NSString *string = dict[@"phone"];
                                                                    if (![string isEqual:[NSNull null]] && string) {
                                                                        //                                    NSString *phone = [NSString stringWithFormat:@"%@",array[indexPath.row][@"user"]] ;
                                                                        
                                                                        [self label:labels index:6].text = [NSString stringWithFormat:@"消费用户:%@",[string stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"]];
                                                                    }else{
                                                                        [self label:labels index:6].text = @"消费用户:";
                                                                    }
                                                                }
                                                                
                                                            }
                                                            
                                                            
                                                        }
                                                    }
                                                    
                                                }
                                            }else
                                                
#pragma mark 订单详情-----------------loading
                                                if([title isEqualToString:@"订单详情"]){
                                                    
                                                    if ([data isKindOfClass:[NSDictionary class]]) {
                                                        
                                                        if ([[data allKeys]containsObject:@"tuik"]) {
                                                            if ([data[@"classify"] intValue] == 100) {//待退款,已完成已退款
                                                                
                                                                if (indexPath.section == 3){
                                                                    
                                                                    NSDictionary *tuik = [NSDictionary dictionaryWithDictionary:data[@"tuik"]];
                                                                    
                                                                    if (indexPath.row == 0) {
                                                                        [self label:labels index:0].text = @"退款类型";
                                                                        if ([[tuik allKeys]containsObject:@"refundType"]) {
                                                                            if (![tuik[@"refundType"]isEqual:[NSNull  null]]) {
                                                                                if ([tuik[@"refundType"] isEqualToString:@"ORDER"]){
                                                                                    [self label:labels index:1].text = @"订单退款申请";
                                                                                }
                                                                            }
                                                                        }
                                                                    }else if (indexPath.row == 1){
                                                                        [self label:labels index:0].text = @"退款原因";
                                                                        if ([[tuik allKeys]containsObject:@"reason"]) {
                                                                            if (![tuik[@"reason"]isEqual:[NSNull  null]]) {
                                                                                [self label:labels index:1].text = tuik[@"reason"];
                                                                            }
                                                                        }
                                                                    }else if (indexPath.row == 2){
                                                                        [self label:labels index:0].text = @"退款金额";
                                                                        if ([[tuik allKeys]containsObject:@"amount"]) {
                                                                            if (![tuik[@"amount"]isEqual:[NSNull  null]]) {
                                                                                [self label:labels index:1].text = [NSString stringWithFormat:@"￥%.2f",[tuik[@"amount"]floatValue]];
                                                                            }
                                                                        }
                                                                        
                                                                    }else if (indexPath.row == 3){
                                                                        [self label:labels index:0].text = @"退款说明";
                                                                        if ([[tuik allKeys]containsObject:@"instructions"]) {
                                                                            if (![tuik[@"instructions"]isEqual:[NSNull  null]]) {
                                                                                [self label:labels index:1].text = tuik[@"instructions"];
                                                                            }else{
                                                                                [self label: labels index:1].text =@"";
                                                                            }
                                                                        }
                                                                        
                                                                    }else if (indexPath.row  == 4){
                                                                        [self label:labels index:0].text = @"退款编号";
                                                                        if ([[tuik allKeys]containsObject:@"number"]) {
                                                                            if (![tuik[@"number"]isEqual:[NSNull  null]]) {
                                                                                [self label:labels index:1].text = [NSString stringWithFormat:@"%li", [tuik[@"number"] longValue]];
                                                                            }
                                                                        }
                                                                    }else if (indexPath.row == 5){
                                                                        [self label:labels index:0].text = @"退款时间";
                                                                        NSString *time = [[NSString stringWithFormat:@"%@",[tuik objectForKey: @"gmtDatetime"]]substringToIndex:10];
                                                                        [self label:labels index:1].text = [NSString stringWithFormat:@"%@",[TheGlobalMethod dateStr:time]];
                                                                    }else if (indexPath.row == 6){
                                                                        [self label:labels index:0].text = @"退款状态";
                                                                        if ([[tuik allKeys]containsObject:@"status"]) {
                                                                            if (![tuik[@"status"]isEqual:[NSNull  null]]) {
                                                                                NSString *tuikuan;
                                                                                if ([tuik[@"status"]isEqualToString:@"IN_DISPOSE"]) {
                                                                                    
                                                                                    tuikuan = @"处理中";
                                                                                }else if ([tuik[@"status"]isEqualToString:@"DISPOSE"]){
                                                                                    tuikuan = @"已处理";
                                                                                }else{
                                                                                    
                                                                                    tuikuan = @"错误";
                                                                                }
                                                                                
                                                                                [self label:labels index:1].text = tuikuan;
                                                                            }
                                                                        }
                                                                        
                                                                    }//row
                                                                    
                                                                }//section
                                                                
                                                            }//100
                                                            
                                                        }else{
                                                            //如果请求的data没有tuik字段
                                                            NSInteger classify = [data[@"myClassfiy"] integerValue];
                                                            NSDictionary *dict = [NSDictionary dictionaryWithDictionary:data[@"dataa"]];
                                                            
                                                            NSLog(@"dingdan--%ld",(long)classify);
                                                            NSLog(@"dataaaa---%@",data);
                                                            
                                                            NSDictionary *goodsList = [NSDictionary dictionaryWithDictionary: dict[@"GoodsOrderMeasuresList"][0]];
                                                            //goods
                                                            NSDictionary *goods = [NSDictionary dictionaryWithDictionary:goodsList[@"goods"]];
                                                            NSDictionary *goodsOrder = [NSDictionary dictionaryWithDictionary:dict[@"goodsOrder"]];
                                                            NSDictionary *store = [NSDictionary dictionaryWithDictionary:dict[@"store"]];
                                                            NSDictionary *goodsAtt;
                                                            if ([[goods allKeys]containsObject:@"goodsAttributeList"]) {
                                                                NSArray *goodsAttList = goods[@"goodsAttributeList"];
                                                                if (goodsAttList.count > 0 ) {
                                                                    goodsAtt = [NSDictionary dictionaryWithDictionary:goodsAttList[0]];
                                                                }
                                                            }
#pragma mark 订单详情共有的第一部分
                                                            //共有的
                                                            if (indexPath.section == 0){
                                                                
                                                                if (classify != 4) {
                                                                    
                                                                    if ([[goodsList allKeys]containsObject:@"goods"]) {
                                                                        NSDictionary *good = [NSDictionary dictionaryWithDictionary:goodsList[@"goods"]];
                                                                        if (indexPath.row == 0) {
                                                                            [self label: labels index:0].text = good[@"name"];
                                                                            [self label: labels index:1].text = good[@"title"];
                                                                            [self label: labels index:2].text = [NSString stringWithFormat:@"￥%@",good[@"sellingPrice"]];
                                                                            if ([[good allKeys]containsObject:@"carousel"]) {
                                                                                NSArray *imgArr = good[@"carousel"];
                                                                                if (imgArr.count > 0 ) {
                                                                                    NSDictionary *imgDic = [NSDictionary dictionaryWithDictionary:imgArr[0]];
                                                                                    if ([[imgDic allKeys] containsObject:@"imgUrl"]) {
                                                                                        NSString *imgUrl = imgDic[@"imgUrl"];
                                                                                        [[self img:imgs index:0]sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
                                                                                    }
                                                                                    
                                                                                }
                                                                            }
                                                                        }else{
                                                                            //过期付等等  AUTOMATIC_BACK
                                                                            if ([[good allKeys]containsObject:@"goodsAttribute"]) {
                                                                                NSString *string = good[@"goodsAttribute"];
                                                                                if (string != nil && ![string isEqualToString:@""]) {
                                                                                    //                                     NSLog(@" stringgg %@",string);//BACK,
                                                                                    NSArray *arr = [string componentsSeparatedByString:@","];
                                                                                    
                                                                                    //
                                                                                    if ([arr[0] isEqualToString:@"BACK"]) {
                                                                                        [[self btn:btns index:0] setTitle:@"随时退" forState:UIControlStateNormal];
                                                                                    }else if([arr[0] isEqualToString:@"AUTOMATIC_BACK"]){
                                                                                        [[self btn:btns index:0] setTitle:@"过期自动退" forState:UIControlStateNormal];
                                                                                    }else{
                                                                                        [self btn:btns index:0].hidden = YES;
                                                                                    }
                                                                                    
                                                                                    if ([arr[1]isEqualToString:@"BACK"]) {
                                                                                        [[self btn:btns index:1] setTitle:@"随时退" forState:UIControlStateNormal];
                                                                                    }else if ([arr[1] isEqualToString:@"AUTOMATIC_BACK"]){
                                                                                        [[self btn:btns index:1] setTitle:@"过期自动退" forState:UIControlStateNormal];
                                                                                    }else{
                                                                                        [self btn:btns index:1].hidden = YES;
                                                                                    }
                                                                                    
                                                                                }else{
                                                                                    cell.height = 0;
                                                                                    [self btn:btns index:0].hidden = YES;
                                                                                    [self btn:btns index:1].hidden = YES;
                                                                                }
                                                                            }else{
                                                                                cell.height = 0;
                                                                                [self btn:btns index:0].hidden = YES;
                                                                                [self btn:btns index:1].hidden = YES;
                                                                            }
                                                                            
                                                                        }
                                                                        //外卖部分
                                                                    }
                                                                    
                                                                }else{
                                                                    
                                                                    if ([[data allKeys] containsObject:@"rowsCount"]) {
                                                                        NSLog(@"counttt -%i- ",[data[@"rowsCount"] intValue]);
                                                                        
                                                                        NSInteger rowCount  = [data[@"rowsCount"] intValue];
                                                                    }
                                                                    
                                                                    NSArray *arrW = [NSArray arrayWithArray:dict[@"GoodsOrderMeasuresList"]];
                                                                    
                                                                    for (int i = 0; i < arrW.count; i++) {
                                                                        NSDictionary *goodsListW = [NSDictionary dictionaryWithDictionary: dict[@"GoodsOrderMeasuresList"][i]];
                                                                        //goods
                                                                        NSDictionary *goodsW = [NSDictionary dictionaryWithDictionary:goodsListW[@"goods"]];
                                                                        //                            NSDictionary *goodsOrderW = [NSDictionary dictionaryWithDictionary:dict[@"goodsOrder"]];
                                                                        //                            NSDictionary *storeW = [NSDictionary dictionaryWithDictionary:dict[@"store"]];
                                                                        NSDictionary *goodsAttW;
                                                                        if ([[goodsW allKeys]containsObject:@"goodsAttributeList"]) {
                                                                            NSArray *goodsAttListW = goods[@"goodsAttributeList"];
                                                                            if (goodsAttListW.count > 0 ) {
                                                                                goodsAttW = [NSDictionary dictionaryWithDictionary:goodsAttListW[0]];
                                                                            }
                                                                        }
                                                                        
                                                                        if ([[goodsListW allKeys]containsObject:@"goods"]) {
                                                                            NSDictionary *goodW = [NSDictionary dictionaryWithDictionary:goodsListW[@"goods"]];
                                                                            if (indexPath.row == i) {
                                                                                [self label: labels index:0].text = goodW[@"name"];
                                                                                [self label: labels index:1].text = goodW[@"title"];
                                                                                [self label: labels index:2].text = [NSString stringWithFormat:@"￥%@",goodW[@"sellingPrice"]];
                                                                                if ([[goodW allKeys]containsObject:@"carousel"]) {
                                                                                    NSArray *imgArr = goodW[@"carousel"];
                                                                                    if (imgArr.count > 0 ) {
                                                                                        NSDictionary *imgDic = [NSDictionary dictionaryWithDictionary:imgArr[0]];
                                                                                        if ([[imgDic allKeys] containsObject:@"imgUrl"]) {
                                                                                            NSString *imgUrl = imgDic[@"imgUrl"];
                                                                                            [[self img:imgs index:0]sd_setImageWithURL:[NSURL URLWithString:imgUrl]];
                                                                                        }
                                                                                        
                                                                                    }
                                                                                }
                                                                                //                                }else{
                                                                                //过期付等等
                                                                                if ([[goodW allKeys]containsObject:@"goodsAttribute"]) {
                                                                                    NSString *string = goodW[@"goodsAttribute"];
                                                                                    if (string != nil && ![string isEqualToString:@""]) {
                                                                                        NSArray *arr = [string componentsSeparatedByString:@","];
                                                                                        if ([arr[0] isEqualToString:@"BACK"]) {
                                                                                            [[self btn:btns index:0] setTitle:@"随时退" forState:UIControlStateNormal];
                                                                                        }else if([arr[0] isEqualToString:@"AUTOMATIC_BACK"]){
                                                                                            [[self btn:btns index:0] setTitle:@"过期自动退" forState:UIControlStateNormal];
                                                                                        }else{
                                                                                            [self btn:btns index:0].hidden = YES;
                                                                                        }
                                                                                        
                                                                                        if ([arr[1]isEqualToString:@"BACK"]) {
                                                                                            [[self btn:btns index:1] setTitle:@"随时退" forState:UIControlStateNormal];
                                                                                        }else if([arr[1] isEqualToString:@"AUTOMATIC_BACK"]){
                                                                                            [[self btn:btns index:1] setTitle:@"过期自动退" forState:UIControlStateNormal];
                                                                                        }else{
                                                                                            [self btn:btns index:1].hidden = YES;
                                                                                        }
                                                                                        [self label:labels index:3].hidden = NO;
                                                                                    }else{
                                                                                        //
                                                                                        [self btn:btns index:0].hidden = YES;//height改变
                                                                                        [self btn:btns index:1].hidden = YES;
                                                                                        [self label:labels index:3].hidden = YES;
                                                                                        cell.ButtomHeight = 1;
                                                                                        [cell tableViewCellSize:100];
                                                                                        
                                                                                    }
                                                                                }else{
                                                                                    
                                                                                    [self btn:btns index:0].hidden = YES;
                                                                                    [self btn:btns index:1].hidden = YES;
                                                                                    [self label:labels index:3].hidden = YES;
                                                                                    [cell tableViewCellSize:100];
                                                                                    //                                        cell.height = 114;
                                                                                }
                                                                                
                                                                            }
                                                                            //外卖部分
                                                                        }
                                                                        
                                                                    }//for 循环
                                                                    
                                                                    
                                                                }//else
                                                                
                                                            }//section
#pragma mark 订单详情外卖部分
                                                            if (classify == 4) {
                                                                if (indexPath.section == 1) {
                                                                    if (indexPath.row == 0) {
                                                                        [self label:labels index:0].text = @"消费券码";
                                                                        NSString *time = [[NSString stringWithFormat:@"%@",[goods objectForKey: @"uptDatetime"]]substringToIndex:10];
                                                                        [self label:labels index:1].text = [NSString stringWithFormat:@"有效期至:%@",[TheGlobalMethod dateStr:time]];
                                                                    }else if (indexPath.row == 1){
                                                                        
                                                                        [self label:labels index:0].text = @"密码:";
                                                                        NSString *goodsOrderStr= [NSString stringWithFormat:@"%@",goodsOrder[@"goodsBuyNumber"]];
                                                                        [self label:labels index:1].text = [goodsOrderStr stringByReplacingCharactersInRange:NSMakeRange(4, 5) withString:@"*****"];
                                                                        
                                                                        if ([[data allKeys]containsObject:@"QR"]) {
                                                                            [[self btn:btns index:0]setTitle:@"已验证" forState:UIControlStateNormal];
                                                                        }else{
                                                                            [[self btn:btns index:0]setTitle:@"待验证" forState:UIControlStateNormal];
                                                                        }
                                                                    }
                                                                }else if (indexPath.section == 2){
                                                                    if (indexPath.row == 0) {
                                                                        [self label:labels index:0].text = @"商家信息";
                                                                    }else if (indexPath.row == 1){
                                                                        [self label:labels index:0].text =  goods[@"name"];
                                                                        [self label:labels index:2].text =  store[@"detailedAddress"];
                                                                        //                            [self label:labels index:3].text =  [NSString stringWithFormat:@"%@m",store[@"distance"]];
                                                                        if ([store[@"distance"]floatValue] <= 1000) {
                                                                            [self label:labels index:3].text =  [NSString stringWithFormat:@"%.1fm",[store[@"distance"]floatValue]];
                                                                        }else{
                                                                            [self label:labels index:3].text =  [NSString stringWithFormat:@"%.1fkm",[store[@"distance"]floatValue]/1000];
                                                                        }
                                                                    }
                                                                }else if (indexPath.section == 3){
                                                                    if (indexPath.row == 0) {
                                                                        [self label:labels index:0].text = @"订单详情";
                                                                    }else if (indexPath.row == 1){
                                                                        [self label:labels index:0].text =  [NSString stringWithFormat:@"订单编号:%@",goodsOrder[@"number"]];
                                                                        [self label:labels index:1].text =  [NSString stringWithFormat:@"购买手机号:%@",goodsOrder[@"phone"]];
                                                                        
                                                                        NSString *time = [[NSString stringWithFormat:@"%@",[goodsList objectForKey: @"gmtDatetime"]]substringToIndex:10];
                                                                        [self label:labels index:2].text =  [NSString stringWithFormat:@"付款时间:%@",[TheGlobalMethod dateStr:time]];
                                                                        [self label:labels index:3].text =  [NSString stringWithFormat:@"数量:%i",[goodsList[@"sum"]intValue]];
                                                                        [self label:labels index:4].text =  [NSString stringWithFormat:@"总价:￥%.2f",[goodsList[@"unitPrice"]floatValue]];
                                                                        
                                                                    }
                                                                    
                                                                    
                                                                    
                                                                }
                                                                
                                                            }else
                                                                
#pragma mark 订单详情酒店部分
                                                                if (classify == 0) {
                                                                    if (indexPath.section == 1) {
                                                                        if (indexPath.row == 0) {
                                                                            [self label:labels index:0].text = @"消费券码";
                                                                            NSString *time = [[NSString stringWithFormat:@"%@",[goods objectForKey: @"uptDatetime"]]substringToIndex:10];
                                                                            [self label:labels index:1].text = [NSString stringWithFormat:@"有效期至:%@",[TheGlobalMethod dateStr:time]];
                                                                        }else if (indexPath.row == 1){
                                                                            
                                                                            [self label:labels index:0].text = @"密码:";
                                                                            NSString *goodsOrderStr= [NSString stringWithFormat:@"%@",goodsOrder[@"goodsBuyNumber"]];
                                                                            if ([goodsOrderStr length] >= 10) {
                                                                                [self label:labels index:1].text = [goodsOrderStr stringByReplacingCharactersInRange:NSMakeRange(4, 5) withString:@"*****"];
                                                                            }else{
                                                                                [self label:labels index:1].text = goodsOrderStr;
                                                                            }
                                                                            //                            [self label:labels index:1].text = [goodsOrderStr stringByReplacingCharactersInRange:NSMakeRange(4, 5) withString:@"*****"];
                                                                            if ([[data allKeys]containsObject:@"QR"]) {
                                                                                [[self btn:btns index:0]setTitle:@"已验证" forState:UIControlStateNormal];
                                                                            }else{
                                                                                [[self btn:btns index:0]setTitle:@"待验证" forState:UIControlStateNormal];
                                                                            }
                                                                            
                                                                        }else if (indexPath.row == 2){
                                                                            
                                                                            [self label:labels index:0].text =@"验证时间:";
                                                                            NSString *time = [[NSString stringWithFormat:@"%@",[goods objectForKey:@"gmtDatetime"]]substringToIndex:10];
                                                                            [self label:labels index:1].text = [NSString stringWithFormat:@"%@",[TheGlobalMethod dateStr:time]];
                                                                        }
                                                                        
                                                                    }else if (indexPath.section == 2){
                                                                        if (indexPath.row == 0) {
                                                                            [self label:labels index:0].text = @"入住日期:";
                                                                            NSString *time = [[NSString stringWithFormat:@"%@",[goods objectForKey:@"gmtDatetime"]]substringToIndex:10];
                                                                            [self label:labels index:1].text =[NSString stringWithFormat:@"%@",[TheGlobalMethod dateStr:time]];
                                                                        }
                                                                        else if (indexPath.row == 1){
                                                                            [self label:labels index:0].text = @"离店日期:";
                                                                            NSString *time = [[NSString stringWithFormat:@"%@",[goods objectForKey:@"uptDatetime"]]substringToIndex:10];
                                                                            [self label:labels index:1].text =[NSString stringWithFormat:@"%@",[TheGlobalMethod dateStr:time]];
                                                                        }
                                                                        else if (indexPath.row == 2){
                                                                            [self label:labels index:0].text = @"房间数:";
                                                                            if ([[goodsList allKeys]containsObject:@"sum"]) {
                                                                                
                                                                                [self label:labels index:1].text =  [NSString stringWithFormat:@"%i", [goodsList[@"sum"] intValue]];
                                                                            }
                                                                            
                                                                            
                                                                        }else if (indexPath.row == 3){
                                                                            [self label:labels index:0].text = @"入住人:";
                                                                            if ([[goodsOrder allKeys]containsObject:@"consignee"]) {
                                                                                [self label:labels index:1].text = goodsOrder[@"consignee"];
                                                                            }
                                                                            
                                                                        }
                                                                    }else{
                                                                        
                                                                        [self commonCellNSInteger:3 label:labels btns:btns indexPath:indexPath myData:dict];
                                                                    }
                                                                    
                                                                }else
#pragma mark 订单详情KTV部分
                                                                    if (classify == 1) {
                                                                        if (indexPath.section == 1) {
                                                                            if (indexPath.row == 0) {
                                                                                [self label:labels index:0].text = @"消费券码";
                                                                                NSString *time = [[NSString stringWithFormat:@"%@",[goods objectForKey: @"uptDatetime"]]substringToIndex:10];
                                                                                [self label:labels index:1].text = [NSString stringWithFormat:@"有效期至:%@",[TheGlobalMethod dateStr:time]];
                                                                            }else if (indexPath.row == 1){
                                                                                
                                                                                [self label:labels index:0].text = @"密码:";
                                                                                NSString *goodsOrderStr= [NSString stringWithFormat:@"%@",goodsOrder[@"goodsBuyNumber"]];
                                                                                [self label:labels index:1].text = [goodsOrderStr stringByReplacingCharactersInRange:NSMakeRange(4, 5) withString:@"*****"];
                                                                                if ([[data allKeys]containsObject:@"QR"]) {
                                                                                    [[self btn:btns index:0]setTitle:@"已验证" forState:UIControlStateNormal];
                                                                                }else{
                                                                                    [[self btn:btns index:0]setTitle:@"待验证" forState:UIControlStateNormal];
                                                                                }
                                                                                
                                                                            }else if (indexPath.row == 2){
                                                                                
                                                                                [self label:labels index:0].text =@"验证时间:";
                                                                                NSString *time = [[NSString stringWithFormat:@"%@",[goods objectForKey:@"gmtDatetime"]]substringToIndex:10];
                                                                                [self label:labels index:1].text = [NSString stringWithFormat:@"%@",[TheGlobalMethod dateStr:time]];
                                                                                
                                                                            }
                                                                        }else if (indexPath.section == 2){
                                                                            if (indexPath.row == 0) {
                                                                                [self label:labels index:0].text = @"欢唱时间:";
                                                                                if ([[goodsAtt allKeys]containsObject:@"dateTime"]) {
                                                                                    [self label:labels index:1].text = goodsAtt[@"dateTime"];
                                                                                }
                                                                                
                                                                            }else if (indexPath.row == 1){
                                                                                [self label:labels index:0].text = @"包厢:";
                                                                                if ([[goodsAtt allKeys]containsObject:@"name"]) {
                                                                                    [self label:labels index:1].text = goodsAtt[@"name"];
                                                                                }
                                                                                
                                                                            }
                                                                        }else {
                                                                            
                                                                            [self commonCellNSInteger:3 label:labels btns:btns indexPath:indexPath myData:dict];
                                                                        }
                                                                        
                                                                    }else
#pragma mark 订单详情美食部分
                                                                        if (classify == 3) {
                                                                            if (indexPath.section == 1) {
                                                                                if (indexPath.row == 0) {
                                                                                    [self label:labels index:0].text = @"消费券码";
                                                                                    NSString *time = [[NSString stringWithFormat:@"%@",[goods objectForKey: @"uptDatetime"]]substringToIndex:10];
                                                                                    [self label:labels index:1].text = [NSString stringWithFormat:@"有效期至:%@",[TheGlobalMethod dateStr:time]];
                                                                                }else if (indexPath.row == 1){
                                                                                    
                                                                                    [self label:labels index:0].text = @"密码:";
                                                                                    NSString *goodsOrderStr= [NSString stringWithFormat:@"%@",goodsOrder[@"goodsBuyNumber"]];
                                                                                    if ([goodsOrderStr length] >= 10) {
                                                                                        [self label:labels index:1].text = [goodsOrderStr stringByReplacingCharactersInRange:NSMakeRange(4, 5) withString:@"*****"];
                                                                                    }else{
                                                                                        [self label:labels index:1].text = goodsOrderStr;
                                                                                    }
                                                                                    
                                                                                    if ([[data allKeys]containsObject:@"QR"]) {
                                                                                        [[self btn:btns index:0]setTitle:@"已验证" forState:UIControlStateNormal];
                                                                                    }else{
                                                                                        [[self btn:btns index:0]setTitle:@"待验证" forState:UIControlStateNormal];
                                                                                    }
                                                                                }
                                                                            }else {
                                                                                
                                                                                [self commonCellNSInteger:2 label:labels btns:btns indexPath:indexPath myData:dict];
                                                                                
                                                                            }
                                                                            
                                                                        }else
#pragma mark 订单详情待发货部分
                                                                            if (classify == 10 ||classify == 11) {
                                                                                
                                                                                if (indexPath.section == 1) {
                                                                                    if (indexPath.row == 0) {
                                                                                        [self label:labels index:0].text = @"订单状态:";
                                                                                        if (classify == 10) {
                                                                                            [self label:labels index:1].text = @"待发货";
                                                                                        }else{
                                                                                            if ([[data allKeys]containsObject:@"EVALUATION"]) {
                                                                                                [self label:labels index:1].text = @"待评价";
                                                                                            }else{
                                                                                                
                                                                                                [self label:labels index:1].text = @"已完成";
                                                                                            }
                                                                                        }
                                                                                    }else if(indexPath.row ==1){
                                                                                        [self label:labels index:0].text = @"规格:";
                                                                                        if ([[goods allKeys]containsObject:@"standard"]) {
                                                                                            [self label:labels index:1].text = goods[@"standard"];
                                                                                        }else{
                                                                                            [self label:labels index:1].text = @"";
                                                                                        }}}
                                                                                else if(indexPath.section == 2){
                                                                                    if (indexPath.row == 0) {
                                                                                        [self label:labels index:0].text = @"收货信息";
                                                                                    }else if(indexPath.row == 1){
                                                                                        
                                                                                        if ([[goodsOrder allKeys]containsObject:@"consignee"]) {
                                                                                            if (![goodsOrder[@"consignee"] isEqual:[NSNull null]]) {
                                                                                                [self label:labels index:0].text= goodsOrder[@"consignee"];
                                                                                            }else{
                                                                                                
                                                                                                [self label:labels index:0].text = @"";
                                                                                            }
                                                                                            
                                                                                        }
                                                                                        if ([[goodsOrder allKeys]containsObject:@"phone"]) {
                                                                                            if (![goodsOrder[@"phone"] isEqual:[NSNull null]]){
                                                                                                NSString *phone = goodsOrder[@"phone"];
                                                                                                [self label:labels index:1].text = [phone stringByReplacingCharactersInRange:NSMakeRange(3, 6) withString:@"****"];
                                                                                            }else{
                                                                                                
                                                                                                [self label:labels index:1].text = @"";
                                                                                            }
                                                                                        }
                                                                                        if ([[goodsOrder allKeys]containsObject:@"address"]) {
                                                                                            if (![goodsOrder[@"phone"] isEqual:[NSNull null]]){
                                                                                                if([[goodsOrder allKeys]containsObject:@"address"])
                                                                                                {
                                                                                                    if (![goodsOrder[@"address"]isEqual:[NSNull null]] &&goodsOrder[@"address"]) {
                                                                                                        [self label:labels index:2].text = goodsOrder[@"address"];
                                                                                                    }
                                                                                                    
                                                                                                }
                                                                                            }else{
                                                                                                
                                                                                                [self label:labels index:2].text = @"";
                                                                                            }
                                                                                        }
                                                                                        
                                                                                    }else if (indexPath.row == 2){
                                                                                        [self label:labels index:0].text = @"买家留言:无";
                                                                                        [self label:labels index:1].text = @"";
                                                                                    }
                                                                                }else
                                                                                {
                                                                                    [self commonCellNSInteger:3 label:labels btns:btns indexPath:indexPath myData:dict];
                                                                                }
                                                                            }else
                                                                                
#pragma mark 订单详情商城部分
                                                                                //2是商城
                                                                                if (classify == 2) {
                                                                                    if (indexPath.section == 1) {
                                                                                        if(indexPath.row == 0){
                                                                                            [self label:labels index:0].text = @"订单状态:";
                                                                                            if ([[data allKeys]containsObject:@"QR"]) {
                                                                                                [self label:labels index:1].text = @"已验证";
                                                                                                
                                                                                            }else{
                                                                                                [self label:labels index:1].text = @"待验证";
                                                                                            }
                                                                                            
                                                                                        }
                                                                                        else if(indexPath.row ==1){
                                                                                            [self label:labels index:0].text = @"规格:";
                                                                                            if ([[goods allKeys]containsObject:@"standard"]) {
                                                                                                [self label:labels index:1].text = goods[@"standard"];
                                                                                            }else{
                                                                                                [self label:labels index:1].text = @"";
                                                                                            }
                                                                                        }else if (indexPath.row == 2){
                                                                                            [self label:labels index:0].text = @"密码:";
                                                                                            NSString *goodsOrderStr= [NSString stringWithFormat:@"%@",goodsOrder[@"goodsBuyNumber"]];
                                                                                            
                                                                                            if ([goodsOrderStr length] >= 10) {
                                                                                                [self label:labels index:1].text = [goodsOrderStr stringByReplacingCharactersInRange:NSMakeRange(4, 5) withString:@"*****"];
                                                                                            }else{
                                                                                                [self label:labels index:1].text = goodsOrderStr;
                                                                                            }
                                                                                            //                                [self label:labels index:1].text = [goodsOrderStr stringByReplacingCharactersInRange:NSMakeRange(4, 5) withString:@"*****"];
                                                                                            if ([[data allKeys]containsObject:@"QR"]) {
                                                                                                [[self btn:btns index:0]setTitle:@"已验证" forState:UIControlStateNormal];
                                                                                                
                                                                                            }else{
                                                                                                [[self btn:btns index:0]setTitle:@"待验证" forState:UIControlStateNormal];
                                                                                            }
                                                                                        }
                                                                                    }else if (indexPath.section == 2){
                                                                                        if (indexPath.row == 0) {
                                                                                            [self label:labels index:0].text = @"买家留言:无";
                                                                                            [self label: labels index:1].text = @"";
                                                                                        }
                                                                                    }else{
                                                                                        
                                                                                        [self commonCellNSInteger:3 label:labels btns:btns indexPath:indexPath myData:dict];
                                                                                    }
                                                                                }else if (classify == 100 ){
#pragma mark 订单详情待退款，已完成的已退款
                                                                                    if (indexPath.section == 1) {
                                                                                        if (indexPath.row == 0) {
                                                                                            [self label:labels index:0].text = @"订单详情";
                                                                                        }else if (indexPath.row == 1){
                                                                                            [self label:labels index:4].text =  [NSString stringWithFormat:@"订单编号:%@",goodsOrder[@"number"]];
                                                                                            [self label:labels index:0].text =  [NSString stringWithFormat:@"购买手机号:%@",goodsOrder[@"phone"]];
                                                                                            
                                                                                            NSString *time = [[NSString stringWithFormat:@"%@",[goodsList objectForKey: @"gmtDatetime"]]substringToIndex:10];
                                                                                            [self label:labels index:3].text =  [NSString stringWithFormat:@"付款时间:%@",[TheGlobalMethod dateStr:time]];
                                                                                            [self label:labels index:1].text =  [NSString stringWithFormat:@"数量:%i",[goodsList[@"sum"]intValue]];
                                                                                            [self label:labels index:2].text =  [NSString stringWithFormat:@"总价:￥%.2f",[goodsList[@"unitPrice"]floatValue]];
                                                                                            
                                                                                        }
                                                                                    }else if (indexPath.section == 2){
                                                                                        if (indexPath.row == 0) {
                                                                                            [self label:labels index:0].text = @"退款详情";
                                                                                            
                                                                                        }
                                                                                    }
                                                                                    
                                                                                }//上面的
                                                        }//是否包含arr
                                                    }//是否是字典
                                                    
                                                    
                                                }else
#pragma mark 钱包
                                                    if([title isEqualToString:@"钱包"]){
                                                        
                                                        if (indexPath.section == 1) {
                                                            
                                                            if ([data isKindOfClass:[NSArray class]]) {
                                                                NSArray *array = [NSArray arrayWithArray:data];
                                                                if ([array[indexPath.row][@"amountType"]isEqualToString:@"ADMIN_MERCHANT_RECHARGE"]) {
                                                                    [self label:labels index:0].text = @"总后台商户充值";
                                                                }else if ([array[indexPath.row][@"amountType"]isEqualToString:@"RECHARGE_GIVE"]) {
                                                                    [self label:labels index:0].text = @"充值赠送";
                                                                }else if ([array[indexPath.row][@"amountType"]isEqualToString:@"GIVE_INTEGERAL_GOODS"]) {
                                                                    [self label:labels index:0].text = @"购买商品赠送积分";
                                                                }else if ([array[indexPath.row][@"amountType"]isEqualToString:@" MERCHANT_RECHARGE"]) {
                                                                    [self label:labels index:0].text = @"商户端充值";
                                                                }else if ([array[indexPath.row][@"amountType"]isEqualToString:@"WITHDRAWALS_FAIL"]) {
                                                                    [self label:labels index:0].text = @"提现失败";
                                                                }else if ([array[indexPath.row][@"amountType"]isEqualToString:@"WITHDRAWALS_SUCCESS"]) {
                                                                    [self label:labels index:0].text = @"提现成功";
                                                                }else if ([array[indexPath.row][@"amountType"]isEqualToString:@"ORDER_BUY"]) {
                                                                    [self label:labels index:0].text = @"订单已支付";
                                                                }else if ([array[indexPath.row][@"amountType"]isEqualToString:@"  RECHARGE_SUCCESS"]) {
                                                                    [self label:labels index:0].text = @"充值成功";
                                                                }else if ([array[indexPath.row][@"amountType"]isEqualToString:@"DISTRIBUTION"]) {
                                                                    [self label:labels index:0].text = @"分销奖金";
                                                                }else if ([array[indexPath.row][@"amountType"]isEqualToString:@"BUY_GOODS"]) {
                                                                    [self label:labels index:0].text = @"购买商品";
                                                                }else if ([array[indexPath.row][@"amountType"]isEqualToString:@"WITHDRAWALS"]) {
                                                                    [self label:labels index:0].text = @"提现";
                                                                }else if ([array[indexPath.row][@"amountType"]isEqualToString:@"RECHARGE"]) {
                                                                    [self label:labels index:0].text = @"充值";
                                                                }else if ([array[indexPath.row][@"amountType"]isEqualToString:@"CENTER_RECHARGE"]) {
                                                                    [self label:labels index:0].text = @"服务中心充值";
                                                                }else if ([array[indexPath.row][@"amountType"]isEqualToString:@"ADMIN_RECHARGE"]) {
                                                                    [self label:labels index:0].text = @"总后台充值中心充值";
                                                                }else  if ([array[indexPath.row][@"amountType"]isEqualToString:@"EXCHANGE"]) {
                                                                    [self label:labels index:0].text = @"积分兑换";
                                                                }else if ([array[indexPath.row][@"amountType"]isEqualToString:@"STORE_TO_PAY"]) {
                                                                    [self label:labels index:0].text = @"到店付";
                                                                }else
                                                                {
                                                                    NSLog(@"%@ amount",array[indexPath.row][@"amountType"]);
                                                                    [self label:labels index:0].text = @"错误";
                                                                }
                                                                
                                                                float money = [array[indexPath.row][@"amount"] floatValue] ;
                                                                
                                                                if (money <0) {
                                                                    [self label:labels index:1].text = [NSString stringWithFormat:@"%-.2f",money];
                                                                    [self label:labels index:1].textColor = [UIColor redColor];
                                                                }else{
                                                                    
                                                                    [self label:labels index:1].text = [NSString stringWithFormat:@"%+.2f",money];
                                                                }
                                                                NSString *time = [[NSString stringWithFormat:@"%@",[array[indexPath.row]objectForKey:@"gmtDatetime"]]substringToIndex:10];
                                                                [self label:labels index:2].text = [NSString stringWithFormat:@"%@",[TheGlobalMethod dateStr:time]];
                                                            }
                                                        }else if (indexPath.section == 0){
                                                            if (indexPath.row == 0) {
                                                                if (![data isEqual:[NSNull null]] && data) {
                                                                    
                                                                    [self label:labels index:1].text = [NSString stringWithFormat:@"¥ %@",[NSString stringWithFormat:@"%.2f",[data floatValue]]];
                                                                }else{
                                                                    
                                                                    [self label:labels index:1].text = @"￥0";
                                                                }
                                                                
                                                            }
                                                        }
                                                        
                                                        
                                                    }else
#pragma mark 我的团队
                                                        if([title isEqualToString:@"我的团队"]){
                                                            
                                                            [self label:labels index:0].text = @"瑞达路可";
                                                            [self label:labels index:1].text = @"注册日期 2016-06-30 12:30";
                                                        }else
#pragma mark 我的
                                                            if ([title isEqualToString:@"我的"]){
                                                                
                                                                
                                                                if ([data isKindOfClass:[NSDictionary class]]) {
                                                                    if ([[data allKeys]containsObject:@"phone"]) {
                                                                        if (indexPath.section == 0) {
                                                                            
                                                                            if (indexPath.row == 0) {
                                                                                
                                                                                [[self img:imgs index:0] sd_setImageWithURL:[NSURL URLWithString:data[@"icon"]] placeholderImage:[UIImage imageNamed:@"toux"]];
                                                                            }
                                                                        }else if(indexPath.section == 1){
                                                                            [self label:labels index:1].text = @"手机号码";
                                                                            [self label:labels index:0].text = data[@"phone"];
                                                                        }
                                                                        
                                                                    }
                                                                }
                                                                if (indexPath.section == 2){
                                                                    if (indexPath.row == 0) {
                                                                        [self label:labels index:0].text = @"登录密码";
                                                                        [self label:labels index:1].text = @"修改";
                                                                    }else if (indexPath.row == 1){
                                                                        [self label:labels index:0].text = @"支付密码";
                                                                        [self label:labels index:1].text = @"已设置";
                                                                        
                                                                    }
                                                                }else if (indexPath.section == 3){
                                                                    [self label:labels index:0].text = @"关于我们";
                                                                    [self label:labels index:1].text = @"";
                                                                }
                                                                
                                                            }if ([title isEqualToString:@"关于我们"]) {
                                                                
                                                                if (indexPath.section == 1) {
                                                                    if (indexPath.row == 0) {
                                                                        //用户协议
                                                                        //                self.topView.hidden = NO;
                                                                        [self label:labels index:0].text = @"用户协议";
                                                                    }else if (indexPath.row == 1){
                                                                        //公司介绍
                                                                        [self label:labels index:0].text = @"公司介绍";
                                                                    }else if (indexPath.row == 2){
                                                                        //客服介绍
                                                                        [self label:labels index:0].text  = @"客服电话";
                                                                        [self label:labels index:1].hidden = NO;
                                                                        [self img:imgs index:0].hidden = YES;
                                                                    }else if (indexPath.row == 3){
                                                                        //意见反馈
                                                                        [self label:labels index:0].text = @"意见反馈";
                                                                    }
                                                                }
                                                                
                                                                if ([[data allKeys] containsObject:@"value"]) {
                                                                    if (indexPath.section == 1) {
                                                                        if (indexPath.row == 2) {
                                                                            [self label:labels index:1].text = data[@"value"];
                                                                        }
                                                                    }
                                                                }
                                                                
                                                            }else if ([title isEqualToString:@"用户协议"]||[title isEqualToString:@"公司介绍"]){
                                                                if (indexPath.section == 0) {
                                                                    if (indexPath.row == 0) {
                                                                        [self label:labels index:0].text = data[@"value"];
                                                                    }
                                                                }
                                                                
                                                            }
    
    
    
    //#pragma mark 色
    //    for (int i = 0; i < labels.count; i ++){
    //        UILabel *label = [self label:labels index:i];
    //        if (i == 1) {
    //            label.backgroundColor = [UIColor redColor];
    //        }else if (i == 2){
    //            label.backgroundColor = [UIColor yellowColor];
    //        }else if (i == 3){
    //            label.backgroundColor = [UIColor greenColor];
    //        }else if (i == 4){
    //            label.backgroundColor = [UIColor blueColor];
    //        }else if (i == 5){
    //            label.backgroundColor = [UIColor orangeColor];
    //        }else if (i == 6){
    //            label.backgroundColor = [UIColor brownColor];
    //        }
    //    }
}

-(UILabel *)label:(NSArray *)labels index:(long)index{
    UILabel *label;
    if (index < labels.count){
        label = labels[index];
        return label;
    }else{
        return nil;
    }
}


-(UIButton *)btn:(NSArray *)btns index:(long)index{
    UIButton *btn;
    if (index < btns.count){
        btn = btns[index];
        return btn;
    }else{
        return nil;
    }
}


-(UIImageView *)img:(NSArray *)labels index:(long)index{
    UIImageView *label;
    if (index < labels.count){
        label = labels[index];
        return label;
    }else{
        return nil;
    }
}
-(UITextField *)tfView:(NSArray *)labels index:(long)index{
    UITextField *label;
    if (index < labels.count){
        label = labels[index];
        return label;
    }else{
        return nil;
    }
}
+(LayoutCell *)sharedLayout{
    static LayoutCell *layout;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        layout = [[self alloc]init];
    });
    return layout;
}
#pragma mark 区头的方法，比如两个label不一样，分别赋值，reloadData的时候会调用这个方法
-(void)showViewCell:(id)data tableViewCell:(TableViewCell *)cell label:(NSArray<UILabel *>*)labels btns:(NSArray<UIButton *>*)btns TFViews:(NSArray<UITextField *>*)TFViews imgView:(NSArray<UIImageView *>*)imgs showView:(NSArray<UIView *>*)views title:(NSString *)title header:(long)header{
    
    NSString *beginTime= [[TheGlobalMethod dateStr:[NSString stringWithFormat:@"%ld", time(NULL)-3600*24*31]]substringToIndex:10];
    NSString *endTime = [[TheGlobalMethod dateStr:[NSString stringWithFormat:@"%ld", time(NULL)]]substringToIndex:10];
    if ([title isEqualToString:@"买单"]) {
        if (header == 2) {
            [self label:labels index:0].text = @"注:不可优惠金额(其他，特价物品等)";
        }
    }else
        
        if ([title isEqualToString:@"提现填写"]) {
            if (header == 1) {
                [self label:labels index:0].text = @"注:姓名和身份证号填写必须是一致的";
            }
        }else
            if ([title isEqualToString:@"钱包"]){
                
                if (header == 1) {
                    //data可以传字段的
                    if (![data[@"beginTime"] isEqual:[NSNull null]]) {
                        [self label:labels index:0].text = data[@"beginTime"];
                    }else{
                        [self label:labels index:0].text = beginTime;
                    }
                    if(![data[@"endTime"] isEqual:[NSNull null]]){
                        [self label:labels index:1].text = data[@"endTime"];
                    }else{
                        [self label:labels index:1].text = endTime;
                    }
                }
            }else if ([title isEqualToString:@"到店付记录"] || [title isEqualToString:@"财务管理"]||[title isEqualToString:@"验证记录"]){
                if (header == 0) {
                    //data可以传字段的
                    if (![data[@"beginTime"] isEqual:[NSNull null]]) {
                        [self label:labels index:0].text = data[@"beginTime"];
                    }else{
                        [self label:labels index:0].text = beginTime;
                        
                    }
                    if(![data[@"endTime"] isEqual:[NSNull null]]){
                        [self label:labels index:1].text = data[@"endTime"];
                    }else{
                        [self label:labels index:1].text = endTime;
                    }
                    
                }else if (header == 1) {
                    if ([title isEqualToString:@"财务管理"]) {
                        if ([data[@"selend"] intValue] == 1) {
                            [self btn:btns index:0].selected = NO;
                            [self btn:btns index:1].selected = YES;
                        }else{
                            [self btn:btns index:0].selected = YES;
                            [self btn:btns index:1].selected = NO;
                        }
                        
                        NSLog(@"dddd,%@",data);
                        NSDictionary *dict = data[@"data"];
                        //                [@"userAmountRecordPageInfo"];
                        //exampleSum
                        if ([[dict allKeys] containsObject:@"exampleSum"]) {
                            
                            if(![dict[@"exampleSum"] isEqual:[NSNull null]] && dict[@"exampleSum"]){
                                [self label:labels index:1].text = [NSString stringWithFormat:@"%.2f",[dict[@"exampleSum"]floatValue]];
                                
                            }
                            
                        }
                    }
                }
                
                
            }
}

#pragma mark 区尾
-(void)showViewCell:(id)data tableViewCell:(TableViewCell *)cell label:(NSArray<UILabel *>*)labels btns:(NSArray<UIButton *>*)btns TFViews:(NSArray<UITextField *>*)TFViews imgView:(NSArray<UIImageView *>*)imgs showView:(NSArray<UIView *>*)views title:(NSString *)title footer:(long)footer{
    
    if ([title isEqualToString:@"提现填写"]) {
        if (footer == 2) {
            [self label:labels index:0].text = @"注:提现申请成功后需要拿着身份证去服务中心领钱";
        }
    }
}

//[phone stringByReplacingCharactersInRange:NSMakeRange(3, 6) withString:@"****"]

//NSString *time = [[NSString stringWithFormat:@"%@",[array[indexPath.row]objectForKey: @"gmtDatetime"]]substringToIndex:10];
//[TheGlobalMethod dateStr:time]
@end
