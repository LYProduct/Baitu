//
//  LayoutCell.h
//  Shop
//
//  Created by ike on 16/11/18.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MyTableView.h"
#import <Masonry.h>

@interface LayoutCell : NSObject
+(LayoutCell *)sharedLayout;
-(void)showCell:(id)data tableViewCell:(TableViewCell *)cell label:(NSArray<UILabel *>*)labels btns:(NSArray<UIButton *>*)btns TFViews:(NSArray<UITextField *>*)TFViews imgView:(NSArray<UIImageView *>*)imgs showView:(NSArray<UIView *>*)views title:(NSString *)title indexPath:(NSIndexPath *)indexPath;

-(void)showViewCell:(id)data tableViewCell:(TableViewCell *)cell label:(NSArray<UILabel *>*)labels btns:(NSArray<UIButton *>*)btns TFViews:(NSArray<UITextField *>*)TFViews imgView:(NSArray<UIImageView *>*)imgs showView:(NSArray<UIView *>*)views title:(NSString *)title header:(long)header;
-(void)showViewCell:(id)data tableViewCell:(TableViewCell *)cell label:(NSArray<UILabel *>*)labels btns:(NSArray<UIButton *>*)btns TFViews:(NSArray<UITextField *>*)TFViews imgView:(NSArray<UIImageView *>*)imgs showView:(NSArray<UIView *>*)views title:(NSString *)title footer:(long)footer;
@end
