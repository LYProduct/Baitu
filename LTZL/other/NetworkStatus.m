 //
//  NetworkStatus.m
//  BS
//
//  Created by mc on 16/8/29.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "NetworkStatus.h"

@implementation NetworkStatus
+ (NSString *)networkingStatesFromStatebar{
    // 状态栏是由当前app控制的，首先获取当前app
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *children = [[[app valueForKeyPath:@"statusBar"] valueForKeyPath:@"foregroundView"] subviews];
    int type = 0;
    for (id child in children) {
        if ([child isKindOfClass:[NSClassFromString(@"UIStatusBarDataNetworkItemView") class]]) {
            type = [[child valueForKeyPath:@"dataNetworkType"] intValue];
        }
    }    
    NSString *stateString = @"当前网络为";
    switch (type) {
        case 0:
            stateString = @"当前没有网络!";
            break;
            
        case 1:
            stateString = @"当前网络 2G";
            break;
            
        case 2:
            stateString = @"当前网络 3G";
            break;
            
        case 3:
            stateString = @"当前网络 4G";
            break;
            
        case 4:
            stateString = @"当前网络 LTE";
            break;
            
        case 5:
            stateString = @"当前网络 Wifi";
            break;
            
        default:
            break;
    }
    return stateString;
}
+(void)NetWorkStates{
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        // 当网络状态改变时调用
        switch (status) {
            case AFNetworkReachabilityStatusUnknown:
                NSLog(@"未知网络");
                break;
            case AFNetworkReachabilityStatusNotReachable:
                NSLog(@"没有网络");
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
                NSLog(@"手机网络");
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                NSLog(@"WIFI");
                break;
        }
         [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":@"111"}];
    }];
    //开始监控
    [manager startMonitoring];
}
@end
