//
//  FileSize.m
//  BS
//
//  Created by mc on 16/8/27.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "FileSize.h"

@implementation FileSize
+(NSString*)getDocumentsPath{
    //获取Documents路径
    NSArray*paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString*path=[paths objectAtIndex:0];
    if (!path) {
        return @"文件夹为空";
    }else{
       return path;
    }
}
+(long long)fileSizeAtPath:(NSString*) filePath{
    NSFileManager* manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:filePath]){
        return [[manager attributesOfItemAtPath:filePath error:nil] fileSize];
    }
    return 0;
}
+(float )folderSizeAtPath:(NSString*)folderPath{
    NSFileManager* manager = [NSFileManager defaultManager];
    if (![manager fileExistsAtPath:folderPath]) return 0;
    NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath:folderPath] objectEnumerator];
    NSString* fileName;
    long long folderSize = 0;
    while ((fileName = [childFilesEnumerator nextObject]) != nil){
        NSString* fileAbsolutePath = [folderPath stringByAppendingPathComponent:fileName];
        folderSize += [self fileSizeAtPath:fileAbsolutePath];
    }
    return folderSize/(1024.0);
}
//获取某个文件夹下的文件
+(id)attainAtPath:(NSString *)filePath attainFile:(NSString *)file{
    NSArray *array = [[NSArray alloc]initWithContentsOfFile:[filePath stringByAppendingPathComponent:file]];
    NSDictionary *dict = [[NSDictionary alloc]initWithContentsOfFile:[filePath stringByAppendingPathComponent:file]];
    if (array.count > 0) {
        return  array;
    }else{
        return dict;
    }
}
//删除某个文件夹
+(void)removeFilePath:(NSString *)filePath{
     NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:filePath error:nil];
}
//某个文件夹下的写入、修改文件内容
+(void)reviseAtPath:(NSString *)filePath reviseFile:(NSString *)file test:(id)text{
    NSString *filePa = [filePath stringByAppendingPathComponent:file];
    [text writeToFile:filePa atomically:YES];
}
//在某个文件夹下创建、获取文件夹
+(NSString *)createPath:(NSString *)filePath createFile:(NSString *)file{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *testDirectory = [filePath stringByAppendingPathComponent:file];
    // 创建目录
    [fileManager createDirectoryAtPath:testDirectory withIntermediateDirectories:YES attributes:nil error:nil];
    return  testDirectory;
}
//获取文件夹下的多个文件
+(NSArray *)acquisitionAtPath:(NSString *)filePath acquisitionFile:(NSString *)file{
    NSString *fileDirectory = [filePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",file]];
    NSArray *files = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:fileDirectory error:nil];
    return files;
}
@end
