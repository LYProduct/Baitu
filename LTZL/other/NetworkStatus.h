//
//  NetworkStatus.h
//  BS
//
//  Created by mc on 16/8/29.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkStatus : NSObject


//获取网络状态
+ (NSString *)networkingStatesFromStatebar;

/**
  AFNetWorking  网络实时监控
 要在AppDelegate 里面写
 - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
 */
+(void)NetWorkStates;
@end
