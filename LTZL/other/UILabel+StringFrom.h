//
//  UILabel+StringFrom.h
//  Carucature
//
//  Created by LinJie on 15/6/22.
//  Copyright (c) 2015年 LinJie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (StringFrom)
- (CGSize)boundingRectWithSize:(CGSize)size;
@end
