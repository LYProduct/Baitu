//
//  AppDelegate.m
//  RYJ
//
//  Created by wyp on 2017/7/18.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "AppDelegate.h"


//极光
// 引入JPush功能所需头文件
#import "JPUSHService.h"
// iOS10注册APNs所需头文件
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif

@interface AppDelegate ()<JPUSHRegisterDelegate>
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    if (launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey]) {
        
        [TheGlobalMethod insertUserDefault:launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey][@"content"] Key:@"pushContent"];
        [TheGlobalMethod insertUserDefault:launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey][@"aps"][@"alert"] Key:@"pushMsg"];
        
    }
    
    [self setIQKeyboardManager];//设置键盘
    [TheGlobalMethod insertUserDefault:@"" Key:@"lng"];
    
    [self setPush:launchOptions];//极光推送
    [self configUSharePlatforms];//友盟
    
    [JPUSHService registrationIDCompletionHandler:^(int resCode, NSString *registrationID) {
        NSLog(@"极光resCode : %d,registrationID: %@",resCode,registrationID);
    }];
    [self setRootViewController];
    return YES;
}

- (void)setRootViewController {
//    NSString * phone = @"15938546808";
//    NSString * pass = @"111111";
    NSString * phone = [TheGlobalMethod getUserDefault:@"phone"];
    NSString * pass = [TheGlobalMethod getUserDefault:@"password"];
    
//    @{@"phone":[TheGlobalMethod getUserDefault:@"phone"], @"password":[TheGlobalMethod getUserDefault:@"password"]}
    if ([[TheGlobalMethod getUserDefault:@"password"] length] > 0) {
        [HttpUrl GET:@"user/loginPhone" dict:@{@"phone":phone, @"password":pass} hud:[UIApplication sharedApplication].keyWindow isShow:NO WithSuccessBlock:^(id data) {
            
            if ([data[@"success"] integerValue] == 1) {
                
                [TheGlobalMethod insertUserDefault:data[@"data"][@"token"] Key:@"appToken"];
                [TheGlobalMethod insertUserDefault:[data[@"data"][@"token"] description] Key:@"token"];
                [self setUserID:[data[@"data"][@"id"] description]];
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                UITabBarController *tabBar = [storyboard instantiateViewControllerWithIdentifier:@"tabbar"];
                self.window.rootViewController = tabBar;
                [self.window makeKeyAndVisible];
//                UIView *contentView;
//                if ( [[tabBar.view.subviews objectAtIndex:0] isKindOfClass:[UITabBar class]] )
//                    
//                    contentView = [tabBar.view.subviews objectAtIndex:1];
//                else
//                    contentView = [tabBar.view.subviews objectAtIndex:0];
//                contentView.frame = CGRectMake(contentView.bounds.origin.x,  contentView.bounds.origin.y,  contentView.bounds.size.width, contentView.bounds.size.height + tabBar.tabBar.frame.size.height);
//                tabBar.tabBar.hidden = YES;
                
                
            } else {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                UINavigationController *tabBar = [storyboard instantiateViewControllerWithIdentifier:@"loginNA"];
                self.window.rootViewController = tabBar;
                [self.window makeKeyAndVisible];
                
                
                
                
                
                
            }
            
            NSLog(@"登录登录登录%@", data);
        }];
        
    } else {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        UINavigationController *tabBar = [storyboard instantiateViewControllerWithIdentifier:@"loginNA"];
        
        self.window.rootViewController = tabBar;
        [self.window makeKeyAndVisible];
        
    }
}

- (void)setUserID:(NSString *)userID {
    [JPUSHService setTags:nil alias:userID fetchCompletionHandle:^(int iResCode, NSSet *iTags, NSString *iAlias) {
        NSLog(@"iResCode:%d; iTags:%@; iAlias:%@", iResCode, iTags, iAlias);
    }];
}

- (void)configUSharePlatforms
{
    /* 设置微信的appKey和appSecret */
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:@"wx5779ac01abad41ac" appSecret:@"e8f6c8097564dba92bf2ffce9b643bde" redirectURL:@"http://mobile.umeng.com/social"];
    /*
     * 移除相应平台的分享，如微信收藏
     */
    //[[UMSocialManager defaultManager] removePlatformProviderWithPlatformTypes:@[@(UMSocialPlatformType_WechatFavorite)]];
    
    /* 设置分享到QQ互联的appID
     * U-Share SDK为了兼容大部分平台命名，统一用appKey和appSecret进行参数设置，而QQ平台仅需将appID作为U-Share的appKey参数传进即可。
     */
        [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_QQ appKey:@"1106380226"/*设置QQ平台的appID*/  appSecret:@"0THxtOIm0QN6ArGs" redirectURL:@"http://mobile.umeng.com/social"];
    
    /* 设置新浪的appKey和appSecret */
        [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_Sina appKey:@"453985678"  appSecret:@"0c9cc03c6e6c93d315f0004d3b66263d" redirectURL:@"https://sns.whalecloud.com/sina2/callback"];
    
}


- (void)setIQKeyboardManager {
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    [IQKeyboardManager sharedManager].toolbarTintColor = appDefaultColor;
    [IQKeyboardManager sharedManager].toolbarDoneBarButtonItemText = @"完成";
    manager.enable = YES;
    manager.shouldResignOnTouchOutside = YES;//控制点击背景是否收起键盘
}


- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    if (!url) {
        return NO;
    }
    NSString *urlStr = [url absoluteString];
    NSLog(@"浏览器打开的网址%@", urlStr);
    if ([urlStr isEqualToString:@"xinyidai://zm"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"芝麻认证回调" object:nil userInfo:@{@"url":urlStr}];
    }
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    // 将URL转成字符串
    NSString *urlString = url.absoluteString;
    
    NSLog(@"支付宝跳转回来:%@", urlString);
    if ([urlString containsString:@"xinyidai://zm"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"芝麻认证回调" object:nil userInfo:@{@"url":urlString}];
    }
    
    return YES;
}


- (void)setPush:(NSDictionary *)launchOptions {
    //Required
    //notice: 3.0.0及以后版本注册可以这样写，也可以继续用之前的注册方式
    JPUSHRegisterEntity * entity = [[JPUSHRegisterEntity alloc] init];
    entity.types = JPAuthorizationOptionAlert|JPAuthorizationOptionBadge|JPAuthorizationOptionSound;
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        // 可以添加自定义categories
        // NSSet<UNNotificationCategory *> *categories for iOS10 or later
        // NSSet<UIUserNotificationCategory *> *categories for iOS8 and iOS9
    }
    [JPUSHService registerForRemoteNotificationConfig:entity delegate:self];
    
    // Optional
    // 获取IDFA
    // 如需使用IDFA功能请添加此代码并在初始化方法的advertisingIdentifier参数中填写对应值
    //    NSString *advertisingId = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    
    // Required
    // init Push
    // notice: 2.1.5版本的SDK新增的注册方法，改成可上报IDFA，如果没有使用IDFA直接传nil
    // 如需继续使用pushConfig.plist文件声明appKey等配置内容，请依旧使用[JPUSHService setupWithOption:launchOptions]方式初始化。
    [JPUSHService setupWithOption:launchOptions appKey:@"574dce151d2c5c7445e2dfea"
                          channel:@"Publish channel"
                 apsForProduction:1];

}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"deviceToken:%@", [NSJSONSerialization JSONObjectWithData:deviceToken options:0 error:nil]);
    /// Required - 注册 DeviceToken
    
    [JPUSHService registerDeviceToken:deviceToken];
}

#pragma mark- JPUSHRegisterDelegate

// iOS 10 Support
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler {
    
    NSLog(@"%@", notification);
    
    // Required
    NSDictionary * userInfo = notification.request.content.userInfo;
    
    NSLog(@"userInfo--%@", userInfo);
    
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
    }
    completionHandler(UNNotificationPresentationOptionAlert); // 需要执行这个方法，选择是否提醒用户，有Badge、Sound、Alert三种类型可以选择设置
}


// iOS 10 Support
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    // Required
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    
    NSLog(@"dianjiuserInfo--%@", userInfo);
    
    //    [TheGlobalMethod insertUserDefault:userInfo[@"aps"][@"alert"] Key:@"pushMsg"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"跳转信息页面" object:nil userInfo:@{@"msg":userInfo}];
    //[@"aps"][@"alert"]
    
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
    }
    completionHandler();  // 系统要求执行这个方法
}



- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    // Required, iOS 7 Support
    [JPUSHService handleRemoteNotification:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    // Required,For systems with less than or equal to iOS6
    [JPUSHService handleRemoteNotification:userInfo];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    
    
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
