//
//  BasicInformationCell.m
//  RYJ
//
//  Created by wyp on 2017/7/21.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "BasicInformationCell.h"

@interface BasicInformationCell () {

    __weak IBOutlet UIImageView *img;
    __weak IBOutlet UILabel *tl;
    __weak IBOutlet UILabel *bl;
    
}

@end

@implementation BasicInformationCell

@synthesize rightImg;

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc indexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *viewDic = dict[@"viewDic"];
    NSDictionary *authDic = dict[@"authDic"];
    
    img.image = [UIImage imageNamed:viewDic[@"img"]];
    tl.text = viewDic[@"tl"];
    bl.text = viewDic[@"bt"];
    if (indexPath.row == 0) {
        [self changeRightImg:@"baiscAuth" authDic:authDic];
    } else if (indexPath.row==1) {
        [self changeRightImg:@"identityAuth" authDic:authDic];
    } else if (indexPath.row==2) {
        [self changeRightImg:@"taobaoAuth" authDic:authDic];
    } else if (indexPath.row==3) {
        [self changeRightImg:@"phoneAuth" authDic:authDic];
    } else if (indexPath.row==4) {
        [self changeRightImg:@"bankAuth" authDic:authDic];
    }
}

- (void)changeRightImg:(NSString *)typeStr authDic:(NSDictionary *)authDic {
    if ([authDic[typeStr] integerValue] == 0) {
        rightImg.image = [UIImage imageNamed:@"dairenzheng"];
    } else if ([authDic[typeStr] integerValue] == 1) {
        rightImg.image = [UIImage imageNamed:@"yirenzheng"];
    } else if ([authDic[typeStr] integerValue] == 2) {
        rightImg.image = [UIImage imageNamed:@"shenghezhong"];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
