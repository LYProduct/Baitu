//
//  CerCardBtnCell.m
//  RYJ
//
//  Created by wyp on 2017/7/26.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "CerCardBtnCell.h"

@interface CerCardBtnCell () {
    UIViewController *theVC;
}

@end

@implementation CerCardBtnCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc {
    theVC = vc;
}

- (IBAction)lookAgreement:(UIButton *)sender {
    


    [HttpUrl GET:@"loanOrder/selectAgreementTwo" dict:@{@"id":@"1"} hud:theVC.view isShow:YES WithSuccessBlock:^(id data) {
        if ([data[@"success"] integerValue] == 1) {
            NSLog(@"协议数据:%@", data);
            [Methods pushUrl:data[@"msg"] seleVC:theVC];
        } else {
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:theVC];
        }
    }];
}

- (IBAction)agree:(UIButton *)sender {
    sender.selected = !sender.isSelected;
    self.returnSelect(sender.selected);
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
