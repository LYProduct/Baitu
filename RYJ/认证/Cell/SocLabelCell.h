//
//  SocLabelCell.h
//  RYJ
//
//  Created by wyp on 2017/7/27.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocLabelCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
