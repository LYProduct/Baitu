//
//  PickerView.h
//  RYJ
//
//  Created by wyp on 2017/7/27.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickerView : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIPickerView *myPickerView;

@property (nonatomic, strong) void(^ReturnString)(NSString *string);

@end
