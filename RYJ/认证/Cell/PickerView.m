//
//  PickerView.m
//  RYJ
//
//  Created by wyp on 2017/7/27.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "PickerView.h"

@interface PickerView ()<UIPickerViewDataSource,UIPickerViewDelegate> {
    NSArray *dataArr;
    __weak IBOutlet UIView *backView;
}

@end

@implementation PickerView

@synthesize title;
@synthesize leftBtn;
@synthesize rightBtn;
@synthesize myPickerView;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchBack)];
    [backView addGestureRecognizer:tap];
    
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = self.frame;
        frame.origin.y = 0;
        self.frame = frame;
    } completion:^(BOOL finished) {
        backView.hidden = NO;
        [UIView animateWithDuration:0.2 animations:^{
            backView.alpha = 0.7;
        } completion:^(BOOL finished) {
            
        }];
    }];
    
    dataArr = dict[@"arr"];
    //指定数据源和委托
    myPickerView.delegate = self;
    myPickerView.dataSource = self;
}

- (void)touchBack {
    [self removeSelf];
}

#pragma mark UIPickerView DataSource Method 数据源方法

//指定pickerview有几个表盘
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;//第一个展示字母、第二个展示数字
}

//指定每个表盘上有几行数据
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return dataArr.count;
}

#pragma mark UIPickerView Delegate Method 代理方法

//指定每行如何展示数据（此处和tableview类似）
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return dataArr[row];
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    UILabel *lbl = (UILabel *)view;
    
    if (lbl == nil) {
        
        lbl = [[UILabel alloc]init];
        
        //在这里设置字体相关属性
        
        lbl.font = [UIFont systemFontOfSize:16];
        
        lbl.textColor = NAVCOLOR_C(51.0, 51.0, 51.0);
        lbl.textAlignment = NSTextAlignmentCenter;
        [lbl setBackgroundColor:[UIColor clearColor]];
        
    }
    
    //重新加载lbl的文字内容
    
    lbl.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    
    ((UIView *)[myPickerView.subviews objectAtIndex:1]).backgroundColor = [NAVCOLOR_C(229.0, 229.0, 229.0) colorWithAlphaComponent:1];
    
    ((UIView *)[myPickerView.subviews objectAtIndex:2]).backgroundColor = [NAVCOLOR_C(229.0, 229.0, 229.0) colorWithAlphaComponent:1];
 
    
    return lbl;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 26;
}

- (IBAction)leftBtnClick:(UIButton *)sender {
    [self removeSelf];
}

- (IBAction)rightBtnClick:(UIButton *)sender {
    NSLog(@"%@", dataArr[[myPickerView selectedRowInComponent:0]]);
    self.ReturnString(dataArr[[myPickerView selectedRowInComponent:0]]);
    [self removeSelf];
}

- (void)removeSelf {
    
    [UIView animateWithDuration:0.01 animations:^{
        backView.alpha = 0;
    } completion:^(BOOL finished) {
        backView.hidden = YES;
        [UIView animateWithDuration:0.5 animations:^{
            CGRect frame = self.frame;
            frame.origin.y = HEIGHT_K;
            self.frame = frame;
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
