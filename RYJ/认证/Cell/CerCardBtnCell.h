//
//  CerCardBtnCell.h
//  RYJ
//
//  Created by wyp on 2017/7/26.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CerCardBtnCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *AgreeBtn;

@property (nonatomic, strong) void(^returnSelect)(BOOL isSelect);

@end
