//
//  CertificationListCell.m
//  RYJ
//
//  Created by 云鹏 on 2017/11/6.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "CertificationListCell.h"

@implementation CertificationListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc indexPath:(NSIndexPath *)indexPath {
    NSDictionary *authDic = dict[@"authDic"];
    
    if (indexPath.row == 0) {
        _img.image = [UIImage imageNamed:@"gerenrenzheng"];
        _label.text = @"个人信息";
        [self changeRightImg:@"baiscAuth" authDic:authDic];
    } else if (indexPath.row==1) {
        _img.image = [UIImage imageNamed:@"shengfenrenzheng"];
        _label.text = @"身份认证";
        [self changeRightImg:@"identityAuth" authDic:authDic];
    } else if (indexPath.row==2) {
        _img.image = [UIImage imageNamed:@"yinghangkarenheng"];
        _label.text = @"银行卡认证";
        [self changeRightImg:@"bankAuth" authDic:authDic];
    } else if (indexPath.row==3) {
        [self changeRightImg:@"phoneAuth" authDic:authDic];
    } else if (indexPath.row==4) {
        [self changeRightImg:@"zhimaAuth" authDic:authDic];
    }
}

- (void)reloadFourSectionCellForData:(NSDictionary *)dict vc:(UIViewController *)vc indexPath:(NSIndexPath *)indexPath{
    NSDictionary *authDic = dict[@"authDic"];
    
    if (indexPath.row == 0) {
        _img.image = [UIImage imageNamed:@"yunyingshangrenzheng"];
        _label.text = @"运营商认证";
        [self changeRightImg:@"phoneAuth" authDic:authDic];
    } else if (indexPath.row==1) {
        _img.image = [UIImage imageNamed:@"zhimashouxin"];
        _label.text = @"芝麻信用";
        [self changeRightImg:@"zhimaAuth" authDic:authDic];
    } 
}


- (void)changeRightImg:(NSString *)typeStr authDic:(NSDictionary *)authDic {
    if ([authDic[typeStr] integerValue] == 0) {
        _rightLabel.text = @"未认证";
    } else if ([authDic[typeStr] integerValue] == 1) {
        _rightLabel.text = @"已认证";
    } else if ([authDic[typeStr] integerValue] == 2) {
        _rightLabel.text = @"审核中";
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
