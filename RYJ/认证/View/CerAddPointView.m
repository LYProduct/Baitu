
//
//  CerAddPointView.m
//  RYJ
//
//  Created by wyp on 2017/7/21.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "CerAddPointView.h"

#import "CerAddPointCCell.h"

@interface CerAddPointView ()<UICollectionViewDelegate, UICollectionViewDataSource> {

    __weak IBOutlet UICollectionView *myCollectionView;
    
    UIViewController *theVC;
    NSArray *dataArr;
    
}

@end

@implementation CerAddPointView

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc {
    theVC = vc;
//    dataArr = @[@{@"img":@"shebaorenzheng",@"title":@"社保认证"}, @{@"img":@"gongjijinrenzheng",@"title":@"公积金认证"}, @{@"img":@"zhifubaocaise",@"title":@"支付宝认证"}, @{@"img":@"jindongcaise",@"title":@"京东认证"}, @{@"img":@"taobaocaise",@"title":@"淘宝认证"}];
    
    dataArr = @[@{@"img":@"shebao-moren",@"title":@"社保认证"}, @{@"img":@"gongjijin-moren",@"title":@"公积金认证"}, @{@"img":@"jingdong",@"title":@"京东认证"}, @{@"img":@"zhifubao",@"title":@"芝麻信用"}];
    
    [self setLayout];
    [self setCollectionView];
    
}

- (void)setLayout {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    //设置每一个item大小
    
    layout.itemSize = CGSizeMake(WIDTH_K/3, WIDTH_K/3);
    //设置行间距
    layout.minimumLineSpacing = 0;
    //设置最小行间距
    layout.minimumInteritemSpacing = 0;
    //设置分区的缩进量
    //    layout.sectionInset = UIEdgeInsetsMake(0, 8, 0, 8);
    
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    myCollectionView.collectionViewLayout = layout;
}

- (void)setCollectionView {
    
    
    //设置collectionView的属性
    myCollectionView.delegate = self;
    myCollectionView.dataSource = self;
    [myCollectionView registerNib:[UINib nibWithNibName:@"CerAddPointCCell" bundle:nil] forCellWithReuseIdentifier:@"cellID"];
    myCollectionView.showsVerticalScrollIndicator = NO;
    myCollectionView.showsHorizontalScrollIndicator = NO;
    myCollectionView.backgroundColor = [UIColor clearColor];
}

//有几个区
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

//每个区有几个item
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return dataArr.count;
}
//重用cell
#pragma mark CollectionSelfCell
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CerAddPointCCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellID" forIndexPath:indexPath];
    [cell.btn setTitle:dataArr[indexPath.row][@"title"] forState:(UIControlStateNormal)];
    [cell.btn setImage:[UIImage imageNamed:dataArr[indexPath.row][@"img"]] forState:(UIControlStateNormal)];
    //    [Methods dataInText:dataArr[indexPath.row][@"bankName"] InLabel:cell.title defaultStr:@"-"];
    //    [Methods dataInText:dataArr[indexPath.row][@"secondTitle"] InLabel:cell.detail defaultStr:@"-"];
    //    [Methods dataInImg:dataArr[indexPath.row][@"imgUrl"] InImg:cell.img placeholderImage:@"jiaotongyinghang"];
    return cell;
}

//定义每个Section 的 margin
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);//分别为上、左、下、右
}


//选择了某个cell
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%ld---", indexPath.row);
    self.clickIndex(indexPath.row);
}

//每个item之间的间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
