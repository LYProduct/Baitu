//
//  LYCertificationCell.m
//  RYJ
//
//  Created by liuyong on 2017/11/27.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "LYCertificationCell.h"

@implementation LYCertificationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc indexPath:(NSIndexPath *)indexPath {
    NSDictionary *authDic = dict[@"userAuth"];

    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            _img.image = [UIImage imageNamed:@"geren"];
            _name.text = @"个人信息";
            _content.text = @"请完善个人信息";
            if ([authDic[@"baiscAuth"] integerValue] == 3) {
                _status.text = @"未认证";
                _status.textColor = [UIColor blackColor];
            }
            if ([authDic[@"baiscAuth"] integerValue] == 0) {
                _status.text = @"正在审核";
                _status.textColor = [UIColor blackColor];
            }
            if ([authDic[@"baiscAuth"] integerValue] == 1) {
                _status.text = @"已认证";
                _status.textColor = [UIColor blackColor];
            }
            if ([authDic[@"baiscAuth"] integerValue] == 2) {
                _status.text = @"审核失败";
                _status.textColor = [UIColor blackColor];
            }
           
        }else if (indexPath.row == 2) {
            _img.image = [UIImage imageNamed:@"shouji-"];
            _name.text = @"手机认证";
            _content.text = @"请进行手机认证";
            if ([authDic[@"phoneAuth"] integerValue] == 3) {
                _status.text = @"未认证";
                _status.textColor = [UIColor blackColor];
            }
            if ([authDic[@"phoneAuth"] integerValue] == 0) {
                _status.text = @"正在审核";
                _status.textColor = [UIColor blackColor];
            }
            if ([authDic[@"phoneAuth"] integerValue] == 1) {
                _status.text = @"已认证";
                _status.textColor = [UIColor blackColor];
            }
            if ([authDic[@"phoneAuth"] integerValue] == 2) {
                _status.text = @"审核失败";
                _status.textColor = [UIColor blackColor];
            }
            
        } else if (indexPath.row==1) {
            _img.image = [UIImage imageNamed:@"yinghang"];
            _name.text = @"银行卡认证";
            _content.text = @"请绑定您的银行卡";
            if ([authDic[@"bankAuth"] integerValue] == 3) {
                _status.text = @"未认证";
                _status.textColor = [UIColor blackColor];
            }
            if ([authDic[@"bankAuth"] integerValue] == 0) {
                _status.text = @"正在审核";
                _status.textColor = [UIColor blackColor];
            }
            if ([authDic[@"bankAuth"] integerValue] == 1) {
                _status.text = @"已认证";
                _status.textColor = [UIColor blackColor];
            }
            if ([authDic[@"bankAuth"] integerValue] == 2) {
                _status.text = @"审核失败";
                _status.textColor = [UIColor blackColor];
            }
            
        } else if (indexPath.row==3) {
            _img.image = [UIImage imageNamed:@"zhima"];
            _name.text = @"芝麻授信";
            _content.text = @"借款需要通过芝麻信用的授权哦";
           
            if ([authDic[@"zhimaAuth"] integerValue] == 3) {
                _status.text = @"未认证";
                _status.textColor = [UIColor blackColor];
            }
            if ([authDic[@"zhimaAuth"] integerValue] == 0) {
                _status.text = @"正在审核";
                _status.textColor = [UIColor blackColor];
            }
            if ([authDic[@"zhimaAuth"] integerValue] == 1) {
                _status.text = @"已认证";
                _status.textColor = [UIColor blackColor];
            }
            if ([authDic[@"zhimaAuth"] integerValue] == 2) {
                _status.text = @"审核失败";
                _status.textColor = [UIColor blackColor];
            }
        }
    }
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            _img.image = [UIImage imageNamed:@"taobao"];
            _name.text = @"淘宝认证";
            _content.text = @"认证您的淘宝信息";
            
            if ([authDic[@"taobaoAuth"] integerValue] == 3) {
                _status.text = @"未认证";
                _status.textColor = [UIColor blackColor];
            }
            if ([authDic[@"taobaoAuth"] integerValue] == 0) {
                _status.text = @"正在审核";
                _status.textColor = [UIColor blackColor];
            }
            if ([authDic[@"taobaoAuth"] integerValue] == 1) {
                _status.text = @"已认证";
                _status.textColor = [UIColor blackColor];
            }
            if ([authDic[@"taobaoAuth"] integerValue] == 2) {
                _status.text = @"审核失败";
                _status.textColor = [UIColor blackColor];
            }
        }
        if (indexPath.row == 1) {
            _img.image = [UIImage imageNamed:@"xuexin"];
            _name.text = @"学信认证";
            _content.text = @"进行学信认证提高额度";
            
            if ([authDic[@"schoolInfo"] integerValue] == 3) {
                _status.text = @"未认证";
                _status.textColor = [UIColor blackColor];
            }
            if ([authDic[@"schoolInfo"] integerValue] == 0) {
                _status.text = @"正在审核";
                _status.textColor = [UIColor blackColor];
            }
            if ([authDic[@"schoolInfo"] integerValue] == 1) {
                _status.text = @"已认证";
                _status.textColor = [UIColor blackColor];
            }
            if ([authDic[@"schoolInfo"] integerValue] == 2) {
                _status.text = @"审核失败";
                _status.textColor = [UIColor blackColor];
            }
        }
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
