//
//  CerAddPointView.h
//  RYJ
//
//  Created by wyp on 2017/7/21.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CerAddPointView : UITableViewCell

@property (nonatomic, strong) void(^clickIndex)(NSInteger index);

@end
