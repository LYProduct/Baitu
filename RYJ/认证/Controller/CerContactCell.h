//
//  CerContactCell.h
//  RYJ
//
//  Created by wyp on 2017/7/26.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CerContactCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *topTF;
@property (weak, nonatomic) IBOutlet UITextField *centerTF;
@property (weak, nonatomic) IBOutlet UITextField *bottomTF;



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomView_L;
@property (weak, nonatomic) IBOutlet UILabel *topLeftLabel;
@end
