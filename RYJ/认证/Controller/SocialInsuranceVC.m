//
//  SocialInsuranceVC.m
//  RYJ
//
//  Created by wyp on 2017/7/26.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "SocialInsuranceVC.h"

#import "LabelTFLineCell.h"
#import "SocLabelCell.h"
#import "AbtnAndCardCell.h"

@interface SocialInsuranceVC ()<UITableViewDelegate, UITableViewDataSource> {
    
    __weak IBOutlet RootTableView *myTableView;
}

@end

@implementation SocialInsuranceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    
    myTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, 10)];
    myTableView.tableHeaderView.backgroundColor = [UIColor clearColor];
    // Do any additional setup after loading the view.
}

#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 6;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        LabelTFLineCell loadingTableViewCell("LabelTFLineCell", )
        [cell reloadCellForSocial:@{} vc:self indexPath:indexPath];
        return cell;
    } else if (indexPath.section == 1) {
        SocLabelCell loadingTableViewCell("SocLabelCell", )
        if (_isSoc == NO) {
            NSMutableString *string = [NSMutableString stringWithString:cell.label.text];
            cell.label.text = [string stringByReplacingOccurrencesOfString:@"社保" withString:@"公积金"];
        }
        return cell;
    }
    AbtnAndCardCell loadingTableViewCell("AbtnAndCardCell", )
    [cell.btn addTarget:self action:@selector(up) forControlEvents:(UIControlEventTouchUpInside)];
    return cell;
}

- (void)up {
    NSLog(@"提交");
    kNavPop
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        return 70;
    } else if (indexPath.section == 2) {
        return 86;
    }
    return 44;
}

- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
