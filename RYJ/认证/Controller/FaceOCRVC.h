//
//  FaceOCRVC.h
//  RYJ
//
//  Created by 云鹏 on 2017/11/7.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FaceOCRVC : UIViewController

@property (nonatomic, strong) UIImage *frontImg;
@property (nonatomic, strong) UIImage *backImg;
@property (nonatomic, strong) NSString *theUserName;
@property (nonatomic, strong) NSString *userIDCard;

@end
