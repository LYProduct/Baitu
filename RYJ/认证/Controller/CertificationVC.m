//
//  CertificationVC.m
//  RYJ
//
//  Created by wyp on 2017/7/21.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "CertificationVC.h"

#import "CerTopCell.h"
#import "CerLabelCell.h"
#import "BasicInformationCell.h"
#import "CerAddPointView.h"

#import "CertificationListCell.h"

#import "CerPersonMsgVC.h"
#import "CerPeopleIDVC.h"
#import "CerPhoneVC.h"
#import "ZhiMaVC.h"
#import "CerCardVC.h"

#import "SocialInsuranceVC.h"

#import <CoreLocation/CoreLocation.h>
#import <ifaddrs.h>
#import <arpa/inet.h>



@interface CertificationVC ()<UITableViewDelegate, UITableViewDataSource, AXWebViewControllerDelegate, CLLocationManagerDelegate> {
    
    __weak IBOutlet RootTableView *myTableView;
    
    NSDictionary *dataDic;
    NSDictionary *authDic;
    
    CerPersonMsgVC *cerPersonMsgVC;
}

@property (nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation CertificationVC

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    // Do any additional setup after loading the view.
    
    cerPersonMsgVC = [TheGlobalMethod setstoryboard:@"CerPersonMsgVC" controller:self];
    
    [self setMJ];
    
    myTableView.hidden = YES;
    [self getNetWorking];
    
}

- (void)setMJ {
    myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self getNetWorking];
    }];
    //    下拉刷新
//    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
//        [self getNetWorking];
//    }];
//    header.lastUpdatedTimeLabel.hidden = YES;
//    header.stateLabel.hidden = YES;
//    [header setImages:@[[UIImage imageNamed:@"字母_r"],[UIImage imageNamed:@"字母_y"],[UIImage imageNamed:@"字母_j"]] forState:MJRefreshStateIdle];
//    [header setImages:@[[UIImage imageNamed:@"字母_r"],[UIImage imageNamed:@"字母_y"],[UIImage imageNamed:@"字母_j"]] forState:MJRefreshStatePulling];
//    [header setImages:@[[UIImage imageNamed:@"字母_r"],[UIImage imageNamed:@"字母_y"],[UIImage imageNamed:@"字母_j"]] forState:MJRefreshStateRefreshing];
//    myTableView.mj_header = header;
//    [myTableView bringSubviewToFront:myTableView.mj_header];
}

- (void)reloadNowVC {
    [myTableView.mj_header beginRefreshing];
}

- (void)getNetWorking {
    
    [HttpUrl NetErrorGET:@"userAuth/selectAll" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        NSLog(@"返回数据:%@", data);
        if ([data[@"success"] integerValue] == 1) {
            dataDic = data[@"data"];
            authDic = dataDic[@"auth"];
            [myTableView reloadData];
            myTableView.hidden = NO;
        } else {
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
        
        [myTableView.mj_header endRefreshing];
    } WithFailBlock:^(id data) {
        [TheGlobalMethod xianShiAlertView:@"连接网络失败" controller:self];
        [myTableView.mj_header endRefreshing];
    }];
    
    
}

#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (!dataDic) {
        return 0;
    }
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else if (section == 1) {
        return 1;
    } else if (section == 2) {
        return 3;
    } else if (section == 3) {
        return 1;
    } else if (section == 4) {
        return 2;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        CerTopCell loadingTableViewCell("CerTopCell", )
    
        return cell;
    } else if (indexPath.section == 1) {
        CerLabelCell loadingTableViewCell("CerLabelCell", )
        cell.leftLabel.text = @"基本认证";
        cell.rightLabel.text = @"";
        return cell;
    } else if (indexPath.section == 2) {
        CertificationListCell loadingTableViewCell("CertificationListCell",)
        [cell reloadCellForData:@{@"authDic":authDic} vc:self indexPath:indexPath];
        return cell;
    } else if (indexPath.section == 3) {
        CerLabelCell loadingTableViewCell("CerLabelCell", )
        cell.leftLabel.text = @"提额认证";
        cell.rightLabel.text = @"";
        return cell;
    } else if (indexPath.section == 4) {
        CertificationListCell loadingTableViewCell("CertificationListCell",)
        [cell reloadFourSectionCellForData:@{@"authDic":authDic} vc:self indexPath:indexPath];
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                __block CertificationVC *vc = self;
                if ([authDic[@"baiscAuth"] integerValue] == 1) {
                    ShowAlert(@"您已认证成功", self);
                } else {
                    cerPersonMsgVC.reloadTheVC = ^{
                        [vc getNetWorking];
                    };
                    cerPersonMsgVC.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:cerPersonMsgVC animated:YES];
                }
            });
            
            
        } else if (indexPath.row == 1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([authDic[@"baiscAuth"] integerValue] == 0) {
                    ShowAlert(@"请先认证个人信息", self);
                    return;
                }
                if ([authDic[@"identityAuth"] integerValue] == 1) {
                    ShowAlert(@"您已认证成功", self);
                }
                CerPeopleIDVC *vc = [TheGlobalMethod setstoryboard:@"CerPeopleIDVC" controller:self];
                vc.reloadVC = ^{
                    [self getNetWorking];
                };
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            });
            
        } else if (indexPath.row == 2) {
            dispatch_async(dispatch_get_main_queue(), ^{
            if ([authDic[@"identityAuth"] integerValue] == 0) {
                ShowAlert(@"请先进行身份认证", self);
                return;
            }
                if ([authDic[@"bankAuth"] integerValue] == 1) {
                    ShowAlert(@"您已认证成功", self);
                }
                
                CerCardVC *vc = [TheGlobalMethod setstoryboard:@"CerCardVC" controller:self];
                vc.reloadTheVC = ^{
                    [self getNetWorking];
                };
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            
            });
        } else if (indexPath.row == 4) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([authDic[@"phoneAuth"] integerValue] == 0) {
                    ShowAlert(@"请先进行手机认证", self);
                    return;
                }
                if ([authDic[@"bankAuth"] integerValue] == 1) {
                    ShowAlert(@"您已认证成功", self);
                }
                CerCardVC *vc = [TheGlobalMethod setstoryboard:@"CerCardVC" controller:self];
                vc.reloadTheVC = ^{
                    [self getNetWorking];
                };
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            });
            
        }
    } else if (indexPath.section == 4) {
        if (indexPath.row == 0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([authDic[@"bankAuth"] integerValue] == 0) {
                    ShowAlert(@"请先进行银行卡认证", self);
                    return;
                }
                
                if ([authDic[@"phoneAuth"] integerValue] == 1) {
                    ShowAlert(@"您已认证成功", self);
                }
                CerPhoneVC *vc = [TheGlobalMethod setstoryboard:@"CerPhoneVC" controller:self];
                vc.reloadTheVC = ^{
                    [self getNetWorking];
                };
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                
            });
            
        } else if (indexPath.row == 1) {
            if ([authDic[@"phoneAuth"] integerValue] == 0) {
                ShowAlert(@"请先进行运营商认证", self);
            }
            if ([authDic[@"zhimaAuth"] integerValue] == 1) {
                ShowAlert(@"您已认证成功", self);
            }
            [HttpUrl GET:@"userIdentity/zhimaxinyong" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
                NSLog(@"%@", data);
//                [Methods pushUrl:data[@"msg"] seleVC:self];
                AXWebViewController *webVC = [[AXWebViewController alloc] initWithAddress:data[@"msg"]];
                webVC.returnUrlString = ^(NSString * _Nonnull urlString) {
                    NSLog(@"回调回调回调回调%@", urlString);
                    [self getNetWorking];
                    [self dingWei];//定位
                };
                webVC.showsToolBar = NO;
                
                webVC.navigationController.navigationBar.translucent = NO;
                self.navigationController.navigationBar.tintColor = appText333333Color;
                //                        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.996f green:0.867f blue:0.522f alpha:1.00f];
                webVC.navigationType = AXWebViewControllerNavigationToolItem;
                webVC.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:webVC animated:YES];
//                returnUrlString
            }];
        }
    }
}


- (void)clickIndex:(NSInteger)index {
    if (index == 0) {
        SocialInsuranceVC *vc = [TheGlobalMethod setstoryboard:@"SocialInsuranceVC" controller:self];
        vc.isSoc = YES;
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    } else if (index == 1) {
        SocialInsuranceVC *vc = [TheGlobalMethod setstoryboard:@"SocialInsuranceVC" controller:self];
        vc.isSoc = NO;
        vc.hidesBottomBarWhenPushed = YES;
        vc.title = @"公积金认证";
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 180;
    } else if (indexPath.section == 2) {
        return 44;
    }
    return 44;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dingWei {
    // 初始化定位管理器
    self.locationManager = [[CLLocationManager alloc] init];
    //2.获取授权
    [_locationManager requestWhenInUseAuthorization];
    //    [_locationManager requestAlwaysAuthorization];
    //3.设置精确度
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    //4.设置米数
    _locationManager.distanceFilter = 5.0f;
    //5.设置代理
    _locationManager.delegate = self;
    //6.开始定位
    [_locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    //CLLocation中存放的是一些经纬度, 速度等信息. 要获取地理位置需要转换做地理位置编码.
    // 1.取出位置对象
    CLLocation *loc = [locations firstObject];
    
    NSLog(@"CLLocation----%@",loc);
    
    // 2.取出经纬度
    CLLocationCoordinate2D coordinate = loc.coordinate;
    // 3.打印经纬度
    NSLog(@"didUpdateLocations------  %f %f", coordinate.latitude, coordinate.longitude);
    [TheGlobalMethod insertUserDefault:[NSString stringWithFormat:@"%f", coordinate.longitude] Key:@"lng"];
    [TheGlobalMethod insertUserDefault:[NSString stringWithFormat:@"%f", coordinate.latitude] Key:@"lat"];
    CLGeocoder *clGeoCoder = [[CLGeocoder alloc] init];
    [clGeoCoder reverseGeocodeLocation:loc completionHandler: ^(NSArray *placemarks,NSError *error) {
        
        for (CLPlacemark *placemark in placemarks) {
            NSDictionary *addressDic = placemark.addressDictionary;
            NSLog(@"地址信息:%@",addressDic);
            NSString *city=[addressDic objectForKey:@"City"];
            NSString *subLocality=[addressDic objectForKey:@"SubLocality"];
            NSString *street=[addressDic objectForKey:@"Street"];
            NSString *State = [addressDic objectForKey:@"State"];
            
            //
            [TheGlobalMethod insertUserDefault:State Key:@"State"];
            [TheGlobalMethod insertUserDefault:city Key:@"city"];
            [TheGlobalMethod insertUserDefault:subLocality Key:@"SubLocality"];
        }
        NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
        CFShow((__bridge CFTypeRef)(infoDictionary));
        // app版本
        NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
        // app build版本
        //        NSString *app_build = [infoDictionary objectForKey:@"CFBundleVersion"];
        
        NSDate *date = [NSDate date]; // 获得时间对象
        NSDateFormatter *forMatter = [[NSDateFormatter alloc] init];
        [forMatter setDateFormat:@"yyyy-MM-dd HH-mm-ss"];
        NSString *dateStr = [forMatter stringFromDate:date];
        if ([[TheGlobalMethod getUserDefault:@"lng"] length] > 0) {//如果用户允许访问了地址
            
            [HttpUrl POST:@"userLoginLog/add" dict:@{@"loginTime":dateStr, @"lng":[TheGlobalMethod getUserDefault:@"lng"], @"lat":[TheGlobalMethod getUserDefault:@"lat"], @"addressDetails":[[TheGlobalMethod getUserDefault:@"city"] stringByAppendingString:[TheGlobalMethod getUserDefault:@"SubLocality"]], @"ip":[self getIPAddress], @"app_version":app_Version} hud:self.view isShow:NO WithSuccessBlock:^(id data) {
                NSLog(@"上传位置信息:%@", data);
            }];
        } else {
            [HttpUrl POST:@"userLoginLog/add" dict:@{@"loginTime":dateStr, @"lng":@"0", @"lat":@"0", @"addressDetails":@"地址未授权", @"ip":[self getIPAddress], @"app_version":app_Version} hud:self.view isShow:NO WithSuccessBlock:^(id data) {
                NSLog(@"上传位置信息:%@", data);
            }];
        }
        
    }];
    // 停止定位(省电措施：只要不想用定位服务，就马上停止定位服务)
    [_locationManager stopUpdatingLocation];
    
    
}

- (NSString *)getIPAddress {
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
