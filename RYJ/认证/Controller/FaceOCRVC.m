//
//  FaceOCRVC.m
//  RYJ
//
//  Created by 云鹏 on 2017/11/7.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "FaceOCRVC.h"

#import "LFMultipleLivenessController.h"
#import "LFLivenessEnumType.h"
#import "LFAlertView.h"

#import "ImageTopTextBottomBtn.h"

#import "LFImage.h"

#import "CertificationVC.h"

@interface FaceOCRVC ()<LFMultipleLivenessDelegate, LFAlertViewDelegate> {
    __weak IBOutlet ImageTopTextBottomBtn *backBtn;
    
    UIImage *faceImg;
    
    NSString *frontUrl;
    NSString *backUrl;
    NSString *faceUrl;
}

@property (nonatomic, strong) LFMultipleLivenessController *multipleLiveVC;

@end

@implementation FaceOCRVC

- (void)viewDidLoad {
    [super viewDidLoad];
    setLeftPopButtonItemAndTitle(@"人脸识别")
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)UP:(UIButton *)sender {
    if (!faceImg) {
        ShowAlert(@"请进行人脸扫描", self)
    }
    
    [HttpUrl upLoadImage:_frontImg hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([data[@"success"] integerValue] == 1) {
            frontUrl = data[@"data"][0];
            
            [HttpUrl upLoadImage:_backImg hud:self.view isShow:YES WithSuccessBlock:^(id data) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if ([data[@"success"] integerValue] == 1) {
                    backUrl = data[@"data"][0];
                    
                    [HttpUrl upLoadImage:faceImg hud:self.view isShow:YES WithSuccessBlock:^(id data) {
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        if ([data[@"success"] integerValue] == 1) {
                            faceUrl = data[@"data"][0];
                            
                            [HttpUrl POST:@"userIdentity/personScan" dict:@{@"identityFront":frontUrl, @"identityBack":backUrl, @"faceUrl":faceUrl, @"identityNum":_userIDCard, @"userName":_theUserName} hud:self.view isShow:YES WithSuccessBlock:^(id data) {
                                if ([data[@"success"] integerValue] == 1) {
                                    [TheGlobalMethod selectAlertView:@"提交成功!" controller:self select:^{
                                        UINavigationController *navigationVC = self.navigationController;
                                        //    遍历导航控制器中的控制器
                                        
                                        for (UIViewController *vc in navigationVC.viewControllers) {
                                            //        CourseTableController就是你需要返回到指定的控制器名称，这里我需要跳转到CourseTableController这个控制器
                                            
                                            if ([vc isKindOfClass:[CertificationVC class]]) {
                                                CertificationVC *certificationVC = (CertificationVC *)vc;
                                                [certificationVC reloadNowVC];
                                                break;
                                            }
                                            
                                        }
                                        [navigationVC popToRootViewControllerAnimated:YES];
                                    }];
                                } else {
                                    [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
                                }
                            }];
                            
                            
                        } else {
                            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
                        }
                    }];
                } else {
                    [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
                }
            }];
            
        } else {
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
    
}


- (IBAction)startFace:(UIButton *)sender {
    [self setupMultiController];
}

- (void)setupMultiController
{
    //设置输出模式 singleImg , multiImg , video , YAW
    NSString *outType = @"video";
    //设置难易程度
    LivefaceComplexity complexity = LIVE_COMPLEXITY_NORMAL;
    //设置动作序列 BLINK  ,  MOUTH  ,  NOD  , YAW
    NSArray *sequence = @[@"BLINK", @"MOUTH", @"NOD", @"YAW"];
    NSDictionary *dictJson = @{@"sequence":sequence,
                               @"outType":outType,
                               @"Complexity":@(complexity)};
    //转化为json字符串
    NSString *strJson = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dictJson options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding];
    LFMultipleLivenessController * multipleLiveVC = [[LFMultipleLivenessController alloc] init];
    [multipleLiveVC setJsonCommand:strJson];
    
    self.multipleLiveVC = multipleLiveVC;
    
    multipleLiveVC.delegate = self;
    [multipleLiveVC setVoicePromptOn:YES];
    BOOL bJsonOK = [multipleLiveVC setJsonCommand:strJson];
    
    if (!bJsonOK) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"服务器错误 bad json" delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil, nil];
        [alert show];
    }
    [self presentViewController:self.multipleLiveVC animated:YES completion:^{
        [self restart];
    }];
}

- (void)restart {
    [self.multipleLiveVC restart];
}

- (void)multiLivenessDidSuccessfulGetData:(NSData *)encryTarData lfImages:(NSArray *)arrLFImage lfVideoData:(NSData *)lfVideoData
{
    NSLog(@"encryTarData:%@ and imgArr:%@", [NSJSONSerialization JSONObjectWithData:encryTarData options:0 error:nil], arrLFImage);
    [backBtn setImage:nil forState:(UIControlStateNormal)];
    [backBtn setTitle:@"" forState:(UIControlStateNormal)];
    
    LFImage *img = arrLFImage[0];
    
    faceImg = img.image;
    [backBtn setBackgroundImage:img.image forState:(UIControlStateNormal)];
    [self multiLivenessDidCancel];
//    [self handleResultWithData:encryTarData lfImages:arrLFImage lfVideoData:lfVideoData SDKVersion:self.version];
}

- (void)multiLivenessDidFailWithType:(LFMultipleLivenessError)iErrorType DetectionType:(LFDetectionType)iDetectionType DetectionIndex:(NSInteger)iIndex Data:(NSData *)encryTarData lfImages:(NSArray *)arrLFImage lfVideoData:(NSData *)lfVideoData
{
    //第一个动作跟丢不弹框
    if (iErrorType == LFMultipleLivenessFaceChanged && iIndex == 0) {
        [self restart];
        return;
    }
    switch (iErrorType) {
            
        case LFMultipleLivenessInitFaild: {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"初始化失败" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
            [alert show];
        }
            break;
            
        case LFMultipleLivenessCameraError: {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"相机权限获取失败" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
            [alert show];
        }
            break;
            
        case LFMultipleLivenessFaceChanged: {
            LFAlertView *alert = [[LFAlertView alloc] initWithTitle:@"采集失败，再试一次吧" delegate:self];
            [alert showOnView:[UIApplication sharedApplication].keyWindow];
        }
            break;
            
        case LFMultipleLivenessTimeOut: {
            LFAlertView *alert = [[LFAlertView alloc] initWithTitle:@"动作超时" delegate:self];
            [alert showOnView:[UIApplication sharedApplication].keyWindow];
            
        }
            break;
            
        case LFMultipleLivenessWillResignActive: {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"肖像提示" message:@"活体验证失败, 请保持前台运行,点击确定重试" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            [alert show];
        }
            break;
            
        case LFMultipleLivenessInternalError: {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"内部错误" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
            [alert show];
        }
            break;
            
        case LFMultipleLivenessBadJson: {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"bad json"] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
            break;
            
        case LFMultipleLivenessBundleIDError: {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"包名错误"] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
            break;
        case LFMultipleLivenessAuthExpire: {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"证书过期"] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
        default:
            break;
    }
}

- (void)multiLivenessDidCancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma - mark AlertViewDelegate

- (void)alertView:(LFAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0: {
            [self.multipleLiveVC cancel];
        }
            break;
        case 1: {
            [self restart];
        }
            break;
            
        default: {
            [self.multipleLiveVC cancel];
        }
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0: {
            [self.multipleLiveVC cancel];
        }
            break;
        case 1: {
            [self restart];
        }
            break;
            
        default: {
            [self.multipleLiveVC cancel];
        }
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
