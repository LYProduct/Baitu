//
//  CerPhoneVC.m
//  RYJ
//
//  Created by wyp on 2017/7/26.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "CerPhoneVC.h"

#import "LabelTFLineCell.h"
#import "AbtnAndCardCell.h"
#import "PhonePromptMsgCell.h"


@interface CerPhoneVC ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate> {

    __weak IBOutlet RootTableView *myTableView;
    NSTimer *timer;
    
    NSString *isDX;
    
    NSString *theVerType;
    NSDictionary *getOneDic;
    
    NSString *isOne;
}

@end

@implementation CerPhoneVC

- (void)viewDidLoad {
    [super viewDidLoad];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    
    theVerType = @"";
    getOneDic = @{};
    
    isOne = @"0";
    // Do any additional setup after loading the view.
}


#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        if (theVerType.length > 0) {
            return 4;
        }
        return 3;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        LabelTFLineCell loadingTableViewCell("LabelTFLineCell", )
        NSArray *leftArr = @[@"您的手机号", @"所属运营商", @"服务密码", @"验证码", @"姓名", @"身份证号"];
        cell.leftLabel.text = leftArr[indexPath.row];
        if (indexPath.row == 0) {
            cell.TF.keyboardType = UIKeyboardTypeNumberPad;
            cell.TF.placeholder = @"请输入手机号";
            cell.TF.delegate = self;
//            cell.TF.text = @"18993770022";
        } else if (indexPath.row == 1) {
            [cell.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
            cell.TF.userInteractionEnabled = NO;
            cell.TF.placeholder = @"请先输入11位手机号";
        } else if (indexPath.row == 2) {
            cell.TF.placeholder = @"运营商的服务密码";
//            cell.TF.text = @"900629";
        } else if (indexPath.row == 3) {
            cell.TF.placeholder = @"请输入验证码";
            if (![isDX isEqualToString:@"1"]) {
                cell.lineView_L.constant = 0;
            }
            cell.getBtn.hidden = NO;
            cell.TF_R.constant = 105;
            if ([getOneDic[@"code"] integerValue] == 101) {
                cell.getBtn.hidden = NO;
                cell.getBtn.layer.cornerRadius = 0;
                NSData *imageData = [[NSData alloc] initWithBase64EncodedString:getOneDic[@"authCode"] options:NSDataBase64DecodingIgnoreUnknownCharacters];
//                imageView.image = [UIImage imageWithData:imageData];
                [cell.getBtn setBackgroundImage:[UIImage imageWithData:imageData] forState:(UIControlStateNormal)];
                [cell.getBtn setTitle:@"" forState:(UIControlStateNormal)];
            } else {
                cell.getBtn.hidden = YES;
            }
            
        } else if (indexPath.row == 4) {
            cell.TF.placeholder = @"请输入姓名";
        } else if (indexPath.row == 5) {
            cell.TF.placeholder = @"请输入身份证号";
        }
        return cell;
    } else if (indexPath.section == 1) {
        AbtnAndCardCell loadingTableViewCell("AbtnAndCardCell", )
        [cell.btn addTarget:self action:@selector(next) forControlEvents:(UIControlEventTouchUpInside)];
        [cell.btn setTitle:@"提交" forState:(UIControlStateNormal)];
        return cell;
    }
    PhonePromptMsgCell loadingTableViewCell("PhonePromptMsgCell", )
    return cell;
}

- (void)next {
    LabelTFLineCell *cell1 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if (cell1.TF.text.length != 11) {
        ShowAlert(@"手机号需为11位", self)
        return;
    }
    LabelTFLineCell *cell2 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    if (cell2.TF.text.length == 0) {
        ShowAlert(@"请输入服务密码", self)
        return;
    }
    
//    if (![[TheGlobalMethod getUserDefault:@"phone"] isEqualToString:cell1.TF.text]) {
//        ShowAlert(@"验证手机号与您注册号码手机不符", self);
//    }
    
    [self.view endEditing:YES];
    if (getOneDic.allKeys.count > 0) {
        [self requTwoAuth];
    } else {
    [HttpUrl GET:@"userPhoneRecord/mobileAuth1" dict:@{@"phone":cell1.TF.text, @"password":cell2.TF.text} hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"得到的信息%@", data);
        if (BB_isSuccess) {
            getOneDic = data[@"data"];

            if ([data[@"code"] integerValue] == 137 || [data[@"code"] integerValue] == 2007) {
                self.reloadTheVC();
                [TheGlobalMethod popAlertView:@"认证成功" controller:self];
            } else if ([getOneDic[@"code"] integerValue] == 101) {
                theVerType = @"imgVer";
                [myTableView reloadData];
            } else if ([getOneDic[@"code"] integerValue] == 105) {
                theVerType = @"textVer";
                [myTableView reloadData];
            } else if ([getOneDic[@"code"] integerValue] == 100) {
                theVerType = @"";
                [self requTwoAuth];
            } else {
                //            ShowAlert(getOneDic[@"message"], self)
                [TheGlobalMethod popAlertView:getOneDic[@"message"] controller:self];
            }
        }else{
            [TheGlobalMethod xianShiAlertView:@"发送验证码失败" controller:self];
        }
        
    }];
    }
}


- (void)requTwoAuth {
    [self.view endEditing:YES];
    LabelTFLineCell *cell1 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    LabelTFLineCell *cell2 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    if (![[TheGlobalMethod getUserDefault:@"phone"] isEqualToString:cell1.TF.text]) {
        ShowAlert(@"验证手机号与您注册号码手机不符", self);
    }
    NSString *txt;
    if (theVerType.length > 0) {
        LabelTFLineCell *cell2 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
        if (cell2.TF.text.length == 0) {
//            ShowAlert(@"请输入验证码", self)
        }
        txt = cell2.TF.text;
    } else {
        txt = @"";
    }
    
    NSString *nextStage;
    if (getOneDic[@"nextStage"]) {
        nextStage = getOneDic[@"nextStage"];
    } else {
        nextStage = @"";
    }
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.detailsLabel.text = @"请勿关闭本界面 验证时间大约需要3~5分钟";
    [HttpUrl GET:@"userPhoneRecord/mobileAuth2" dict:@{@"taskId":getOneDic[@"taskId"], @"passwd":cell2.TF.text, @"nextStage":nextStage, @"code":getOneDic[@"code"], @"txt":txt, @"phone":cell1.TF.text} hud:self.view isShow:NO WithSuccessBlock:^(id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        MBProgressHUD *hud1 = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud1.detailsLabel.text = @"请勿关闭本界面 验证时间大约需要3~5分钟";
        NSLog(@"得到的信息%@", data);
        if (BB_isSuccess) {
            getOneDic = data[@"data"];
            if ([getOneDic[@"code"] integerValue] == 137 || [getOneDic[@"code"] integerValue] == 2007) {
                self.reloadTheVC();
                [TheGlobalMethod popAlertView:@"认证成功" controller:self];
            } else if ([data[@"data"][@"code"] integerValue] == 101) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                LabelTFLineCell *cellOne = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
                cellOne.TF.text = @"";
                theVerType = @"imgVer";
                [myTableView reloadData];
            } else if ([data[@"data"][@"code"] integerValue] == 105) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                LabelTFLineCell *cellOne = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
                cellOne.TF.text = @"";
                //            [MBProgressHUD hideHUDForView:self.view animated:YES];
                theVerType = @"textVer";
                [myTableView reloadData];
            } else if ([data[@"data"][@"code"] integerValue] == 100) {
                [self performSelector:@selector(requTwoAuth) withObject:self afterDelay:10];
                theVerType = @"";
            } else {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                //            ShowAlert(data[@"data"][@"message"], self)
                [TheGlobalMethod popAlertView:data[@"data"][@"message"] controller:self];
            }
        }else{
            [TheGlobalMethod xianShiAlertView:@"认证失败" controller:self];
        }
        
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 1) {
            return 0;
        }
        return 44;
    } else if (indexPath.section == 1) {
        return 86;
    }
    return 150;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 10;
    }
    return 20;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, 20)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

- (NSString *)isMobileNumber:(NSString *)mobileNum
{
    if (mobileNum.length != 11)
    {
        return @"";
    }
    /**
     * 手机号码:
     * 13[0-9], 14[5,7], 15[0, 1, 2, 3, 5, 6, 7, 8, 9], 17[6, 7, 8], 18[0-9], 170[0-9]
     * 移动号段: 134,135,136,137,138,139,150,151,152,157,158,159,182,183,184,187,188,147,178,1705
     * 联通号段: 130,131,132,155,156,185,186,145,176,1709
     * 电信号段: 133,153,180,181,189,177,1700
     */
    NSString *MOBILE = @"^1(3[0-9]|4[57]|5[0-35-9]|8[0-9]|7[0678])\\d{8}$";
    /**
     * 中国移动：China Mobile
     * 134,135,136,137,138,139,150,151,152,157,158,159,182,183,184,187,188,147,178,1705
     */
    NSString *CM = @"(^1(3[4-9]|4[7]|5[0-27-9]|7[8]|8[2-478])\\d{8}$)|(^1705\\d{7}$)";
    /**
     * 中国联通：China Unicom
     * 130,131,132,155,156,185,186,145,176,1709
     */
    NSString *CU = @"(^1(3[0-2]|4[5]|5[56]|7[6]|8[56])\\d{8}$)|(^1709\\d{7}$)";
    /**
     * 中国电信：China Telecom
     * 133,153,180,181,189,177,1700
     */
    NSString *CT = @"(^1(33|53|77|8[019])\\d{8}$)|(^1700\\d{7}$)";
    
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestcm evaluateWithObject:mobileNum] == YES)) {
        NSLog(@"中国移动");
        return @"中国移动";
    } else if ([regextestct evaluateWithObject:mobileNum] == YES) {
        NSLog(@"中国电信");
        return @"中国电信";
    } else if ([regextestcu evaluateWithObject:mobileNum] == YES) {
        NSLog(@"中国联通");
        return @"中国联通";
    }
    
    return @"未识别,但您可以提交";
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
