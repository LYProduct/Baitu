//
//  CerCardVC.h
//  RYJ
//
//  Created by wyp on 2017/7/26.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "RootViewController.h"



@interface CerCardVC : RootViewController

@property (nonatomic, strong) void(^reloadTheVC)();

@property (nonatomic, strong) NSString *isCer;


@property (nonatomic, strong) NSString *isMine;

@end
