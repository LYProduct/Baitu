//
//  ChsiLoginVC.m
//  RYJ
//
//  Created by liuyong on 2017/12/13.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "ChsiLoginVC.h"

@interface ChsiLoginVC ()
{
    NSString * _taskId;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeight;
@property (weak, nonatomic) IBOutlet UITextField *passWTF;

@property (weak, nonatomic) IBOutlet UITextField *accountTF;
@property (weak, nonatomic) IBOutlet UITextField *codeTF;
@property (weak, nonatomic) IBOutlet UIButton *LoginBtn;

@property (weak, nonatomic) IBOutlet UIButton *getCodeBtn;
@property (weak, nonatomic) IBOutlet UIImageView *codeImg;

@end

@implementation ChsiLoginVC
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(pop)];
    [item setImage:[UIImage imageNamed:@"fanhui"]];
    
    self.navigationItem.leftBarButtonItem = item;
    [self.navigationItem.leftBarButtonItem setTintColor:[UIColor blackColor]];
}
-(void)pop {
    SetPop;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"学信网登录";
    
    _getCodeBtn.layer.borderWidth = 1;
    _getCodeBtn.layer.borderColor = appDefaultColor.CGColor;
    
//    _accountTF.text = @"330302199608104016";
//    _passWTF.text = @"88352160";
    
    
}
- (IBAction)getCodeClick:(id)sender {
    NSMutableDictionary * dict = [[NSMutableDictionary alloc]initWithCapacity:0];
    
    if (!isStringEmpty(_accountTF.text)) {
        [dict setObject:_accountTF.text forKey:@"userName"];
    }
    if (!isStringEmpty(_passWTF.text)) {
        [dict setObject:_passWTF.text forKey:@"userPass"];
    }
    [HttpUrl GET:@"userSchoolInfo/getPicCode" dict:dict hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if (BB_isSuccess) {
            if ([data[@"data"][@"captcha"] boolValue] == YES) {
                _viewHeight.constant = 350;
                _codeImg.hidden = NO;
                
                NSData *decodeData = [[NSData alloc]initWithBase64EncodedString:data[@"data"][@"picode"] options:(NSDataBase64DecodingIgnoreUnknownCharacters)];
                // 将NSData转为UIImage
                _codeImg.image = [UIImage imageWithData: decodeData];
                
                _taskId = data[@"data"][@"taskId"];
                
            }else{
                _codeImg.hidden = YES;
                _viewHeight.constant = 220;
            }
        }else{
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
}
- (IBAction)loginClick:(id)sender {
    NSMutableDictionary * dict = [[NSMutableDictionary alloc]initWithCapacity:0];
    
    if (!isStringEmpty(_accountTF.text)) {
        [dict setObject:_accountTF.text forKey:@"userName"];
    }else{
        [TheGlobalMethod xianShiAlertView:@"请填写学信网账号" controller:self];
        return;
    }
    if (!isStringEmpty(_passWTF.text)) {
        [dict setObject:_passWTF.text forKey:@"userPass"];
    }else{
        [TheGlobalMethod xianShiAlertView:@"请填写登录密码" controller:self];
        return;
    }
    if (!isStringEmpty(_codeTF.text)) {
        [dict setObject:_codeTF.text forKey:@"authCode"];
    }else{
        [TheGlobalMethod xianShiAlertView:@"请填写验证码" controller:self];
        return;
    }
    [dict setObject:_taskId forKey:@"taskId"];
    [HttpUrl GET:@"userSchoolInfo/loginAndAcquire" dict:dict hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if (BB_isSuccess) {
            SetPop;
        }else{
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
