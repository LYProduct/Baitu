//
//  CerPeopleIDVC.m
//  RYJ
//
//  Created by wyp on 2017/7/26.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "CerPeopleIDVC.h"

#import "LFIDCardScannerController.h"
#import "LFIDCard.h"

#import "FaceOCRVC.h"

@interface CerPeopleIDVC () <LFIDCardScannerControllerDelegate>{
    
    __weak IBOutlet UIView *idView;
    
    __weak IBOutlet UIView *faceView;
    __weak IBOutlet UIButton *bottomBtn;
    
    
    __weak IBOutlet UIImageView *topImg;
    __weak IBOutlet UIImageView *bottomImg;
    
    NSString *userName;
    NSString *userNumber;
    
    LFIDCardScannerController *_scanFrontAndBackVC;
    
    NSString *frontUrl;
    NSString *backUrl;
    NSString *theUserName;
    NSString *userIDCard;
}

@end

@implementation CerPeopleIDVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.disableDragBack = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scanning)];
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scanning)];
    [topImg addGestureRecognizer:tap];
    [bottomImg addGestureRecognizer:tap1];
    
    // Do any additional setup after loading the view.
}

- (void)scanning {
    NSLog(@"扫描");
    LFIDCardScannerController *scannerVC = [[LFIDCardScannerController alloc] initWithOrientation:AVCaptureVideoOrientationPortrait licenseName:@"SenseID_OCR" shouldFullCard:YES];
//    [[LFBankCardScannerController alloc] initWithOrientation:AVCaptureVideoOrientationPortrait licenseName:@"SenseID_OCR" isVertical:NO shouldFullCard:YES];
    scannerVC.delegate = self;
    scannerVC.showAnimation = YES;
    scannerVC.cardMode = kIDCardFrontal;
    _scanFrontAndBackVC = scannerVC;
//    [self.navigationController pushViewController:scannerVC animated:YES];
    [self presentViewController:scannerVC animated:YES completion:nil];
}

//获取身份证扫描结果
-(void) getCardResult: (LFIDCard *)idcard {
    if (idcard.encryptedData) {
        [self uploadData:idcard.encryptedData compeltion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            if (data) {
                NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
                NSLog(@"上传数据%@", resultDict);
                
                if (_scanFrontAndBackVC.cardMode == kIDCardFrontal) {
                    frontUrl = resultDict[@"image_crop"];
                    theUserName = resultDict[@"info"][@"name"][@"text"];
                    userIDCard = resultDict[@"info"][@"idNum"][@"text"];
                } else {
                backUrl = resultDict[@"image_crop"];
                }
                
                if (!resultDict.allKeys.count || ![resultDict[@"status"] isEqual:@"OK"]) {
                    [self didCancel];
                    return;
                }
                
                LFIDCard *resultCard = [self handleData:resultDict originCard:idcard];
                [self handleResult:resultCard];
            } else {
                [self didCancel];
            }
        }];
    } else {
        [self didCancel];
    }
}

#pragma mark - Private Methods

- (void)uploadData:(NSData *)data compeltion:(void(^)(NSURLResponse *response, NSData *data, NSError *connectionError)) block
{
    //#error 请删除此行, 在线版填充api_id、api_secret、urlString值
    NSString *api_id = @"ddfed7276a714c25bfe8e296a76365b9";
    NSString *api_secret = @"1174d6fe0eb0492c84a2c7a6c3a4aa78";
    NSString *urlString = @"http://cloudapi.linkface.cn/ocr/parse_idcard_ocr_result";
    
    NSString *MPboundary = [[NSString alloc]initWithFormat:@"--%@", @"Linkface"];
    NSString *endMPboundary = [[NSString alloc]initWithFormat:@"%@--",MPboundary];
    NSString *end = [[NSString alloc]initWithFormat:@"\r\n%@",endMPboundary];
    
    NSMutableData *fileData = [NSMutableData data];
    NSMutableString *bodyString = [[NSMutableString alloc]init];
    
    // 非文件参数
    NSDictionary *dict = @{@"api_id": api_id, @"api_secret": api_secret, @"return_image": @"1"};
    NSArray *keys= [dict allKeys];
    //遍历keys
    for (int i = 0;i < [keys count];i++) {
        NSString *key = [keys objectAtIndex:i];
        if ([key isKindOfClass:[NSString class]]) {
            [bodyString appendFormat:@"%@\r\n", MPboundary];
            [bodyString appendFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",key];
            [bodyString appendFormat:@"%@\r\n",[dict objectForKey:key]];
        }
    }
    
    [bodyString appendFormat:@"%@\r\n",MPboundary];
    [bodyString appendFormat:@"Content-Disposition: form-data; name=\"file\"; filename=\"file\"\r\n"];
    [bodyString appendFormat:@"Content-Type: image/png\r\n\r\n"];
    
    [fileData appendData:[bodyString dataUsingEncoding:NSUTF8StringEncoding]];
    [fileData appendData:data];
    [fileData appendData:[end dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:1 timeoutInterval:2.0f];
    request.HTTPMethod = @"POST";
    request.timeoutInterval = 15.0;
    NSString *content = [[NSString alloc]initWithFormat:@"multipart/form-data; boundary=%@", @"Linkface"];
    [request setValue:content forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[fileData length]] forHTTPHeaderField:@"Content-Length"];
    request.HTTPBody = fileData;
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        block(response, data, connectionError);
    }];
}

- (void)handleResult:(LFIDCard *)idcard
{
    
            [_scanFrontAndBackVC doRecognitionProcess:NO];
            
            if (_scanFrontAndBackVC.cardMode == kIDCardFrontal) {
//                [self praseIDCardFront:idcard];
//                self.resultVC.imgCardFront = idcard.imgCardDetected;
//                self.resultVC.imgCropedFront = idcard.imgOriginCroped;
                
                UILabel *label = [[UILabel alloc] init];
                label.text = @"扫描成功";
                label.textColor = [UIColor colorWithRed:83/255.0 green:239/255.0 blue:160/255.0 alpha:1];
                [_scanFrontAndBackVC setHintLabel:label];
                [_scanFrontAndBackVC resetAutoCancelTimer];
                
                dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.2 * NSEC_PER_SEC));
                dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                    
                topImg.image = idcard.imgCardDetected;
                    
                _scanFrontAndBackVC.cardMode = kIDCardBack;
                [_scanFrontAndBackVC doRecognitionProcess:YES];
                    
                });
                
                return;
            }else{
//                [self praseIDCardBack:idcard];
                bottomImg.image = idcard.imgCardDetected;
                [_scanFrontAndBackVC doRecognitionProcess:NO];
            }
            
    
    
    /**
     *  The following line would return the captured image as the result.
     */
    //    resultVC.imgCard = [idcard imgOriginCaptured];
    /**
     *  The following line would return the card image as the result.
     */
//    self.resultVC.imgCard = idcard.imgCardDetected;
//    self.resultVC.imgCroped = idcard.imgOriginCroped;
    
//    [self.navigationController pushViewController:self.resultVC animated:NO];
    [self dismissController];
    [_scanFrontAndBackVC doRecognitionProcess:YES];
}
- (LFIDCard *)handleData:(NSDictionary *)dict originCard:(LFIDCard *)card
{
    card.strName = dict[@"info"][@"name"][@"text"];
    card.strSex = dict[@"info"][@"sex"][@"text"];
    card.strNation = dict[@"info"][@"nation"][@"text"];
    card.strYear = dict[@"info"][@"year"][@"text"];
    card.strMonth = dict[@"info"][@"month"][@"text"];
    card.strDay = dict[@"info"][@"day"][@"text"];
    card.strAddress = dict[@"info"][@"address"][@"text"];
    card.strID = dict[@"info"][@"idNum"][@"text"];
    card.strAuthority = dict[@"info"][@"authority"][@"text"];
    card.strValidity = dict[@"info"][@"validity"][@"text"];
    card.rectName = [self getPersonInfoRect:dict[@"info"][@"name"][@"text_region"]];
    card.rectID = [self getPersonInfoRect:dict[@"info"][@"idNum"][@"text_region"]];
    card.side = [dict[@"side"] integerValue];
    card.type = [dict[@"type"] integerValue];
    card.strDate = [NSString stringWithFormat:@"%@%@%@", card.strYear, card.strMonth, card.strDay];
    return card;
}

- (CGRect)getPersonInfoRect:(NSDictionary *)rect{
    CGRect rectInfo = CGRectMake([[rect[@"left"] stringValue] floatValue],
                                 [[rect[@"top"] stringValue] floatValue],
                                 [[rect[@"right"] stringValue] floatValue] - [[rect[@"left"] stringValue] floatValue],
                                 [[rect[@"bottom"] stringValue] floatValue] - [[rect[@"top"] stringValue] floatValue]);
    return rectInfo;
}

- (void)didCancel {
    [self dismissController];
}

- (void)autoCancel {
    [self dismissController];
}

- (void)dismissController{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)next:(UIButton *)sender {
    if (theUserName.length == 0 || userIDCard.length == 0 || frontUrl.length == 0 || backUrl.length == 0) {
        ShowAlert(@"请先进行扫描", self)
    }
    FaceOCRVC *vc = [[FaceOCRVC alloc] initWithNibName:nil bundle:nil];
    vc.theUserName = theUserName;
    vc.frontImg = topImg.image;
    vc.backImg = bottomImg.image;
    vc.userIDCard = userIDCard;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIImage *)dataURL2Image:(NSString *)imgSrc
{
    NSURL *url = [NSURL URLWithString: imgSrc];
    NSData *data = [NSData dataWithContentsOfURL: url];
    UIImage *image = [UIImage imageWithData: data];
    
    return image;
}

- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
