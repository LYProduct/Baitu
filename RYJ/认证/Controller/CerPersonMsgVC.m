//
//  CerPersonMsgVC.m
//  RYJ
//
//  Created by wyp on 2017/7/26.
//  Copyright © 2017年 RongKe. All rights reserved.
//
#import <CoreLocation/CoreLocation.h>

#import "CerPersonMsgVC.h"

#import "TopAttCell.h"
#import "LabelTFLineCell.h"
#import "TextViewCell.h"

#import "CerLLCell.h"
#import "CerContactCell.h"

#import "AbtnAndCardCell.h"

#import "PickerView.h"
#import "STPickerArea.h"

#import <PPGetAddressBook.h>

@interface CerPersonMsgVC ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,CLLocationManagerDelegate> {
    
    __weak IBOutlet RootTableView *myTableView;
    STPickerArea *stPickerArea;
    
    NSMutableDictionary *dataDic;
    __weak IBOutlet UIView *coverView;
    NSDictionary * _userInfo;
}
@property (nonatomic,strong) CLLocationManager *manager;

@end

@implementation CerPersonMsgVC
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];

    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // 实例化对象
    _manager = [[CLLocationManager alloc] init];
    
    _manager.delegate = self;
    
    // 请求授权，记得修改的infoplist，NSLocationAlwaysUsageDescription（描述）
    [_manager requestAlwaysAuthorization];
    
    if ([_isCer isEqualToString:@"1"]) {
        _userInfo = self.authDict;
        if (_userInfo.allKeys.count > 0) {
            if (![_userInfo[@"userBasicMsg"] isEqual:[NSNull null]]) {
                [self setDict:_userInfo];
            }
        }
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
//    if ([_isCer isEqualToString:@"1"]) {
//        _userInfo = self.authDict;
//        [self setDict:_userInfo];
//    }
    
    dataDic = [NSMutableDictionary dictionary];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        myTableView.delegate = self;
        myTableView.dataSource = self;
        [PPGetAddressBook requestAddressBookAuthorization];
    });
    
    
    // Do any additional setup after loading the view.
}
-(void)setDict:(NSDictionary *)authDict{
    LabelTFLineCell *cell1 = [myTableView cellForRowAtIndexPath:[NSIndexPath  indexPathForRow:1 inSection:1]];

    TextViewCell *cell4 = [myTableView cellForRowAtIndexPath:[NSIndexPath  indexPathForRow:2 inSection:1]];
    cell1.TF.text = [NSString stringWithFormat:@"%@ %@ %@",authDict[@"userBasicMsg"][@"address"][@"province"],authDict[@"userBasicMsg"][@"address"][@"city"],authDict[@"userBasicMsg"][@"address"][@"area"]];
//    cell2.TF.text = authDict[@"userBasicMsg"][@"address"][@"city"];
//    cell3.TF.text = authDict[@"userBasicMsg"][@"address"][@"area"];
    cell4.myTextView.text = isStringEmpty(authDict[@"userBasicMsg"][@"address"][@"addressDetails"]) == YES ? [NSString stringWithFormat:@"%@",cell1.TF.text]:authDict[@"userBasicMsg"][@"address"][@"addressDetails"];
    if (cell4.myTextView.text.length == 0) {
        cell4.myTextView.text = [NSString stringWithFormat:@"%@",cell1.TF.text];
    }
//    [dataDic setObject:cell4.myTextView.text forKey:@"addressDetail"];
    [dataDic setObject:authDict[@"userBasicMsg"][@"address"][@"province"] forKey:@"3"];
    [dataDic setObject:authDict[@"userBasicMsg"][@"address"][@"city"] forKey:@"4"];
    [dataDic setObject:authDict[@"userBasicMsg"][@"address"][@"area"] forKey:@"5"];
    
    CerContactCell *cell5 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:4]];
    cell5.topTF.text = authDict[@"userBasicMsg"][@"firstContactName"];
    cell5.centerTF.text = authDict[@"userBasicMsg"][@"firstContactPhone"];
    cell5.bottomTF.text = authDict[@"userBasicMsg"][@"firstContactRelation"];
    
    CerContactCell *cell6 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:5]];
    cell6.topTF.text = authDict[@"userBasicMsg"][@"secondContactName"];
    cell6.centerTF.text = authDict[@"userBasicMsg"][@"secondContactPhone"];
    cell6.bottomTF.text = authDict[@"userBasicMsg"][@"secondContactRelation"];
    
    cell4.myTextView.text = @"11";
}
#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 7;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else if (section == 1) {
        return 7;
    } else if (section == 2) {
        return 0;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        TopAttCell loadingTableViewCell("TopAttCell", )
        return cell;
    } else if (indexPath.section == 1) {
        
        if (indexPath.row == 6) {
            TextViewCell loadingTableViewCell("TextViewCell", [cell reloadCellForData:@{} vc:self])
            cell.myTextView.placeholder = @"请填写详细地址";
            cell.TV_L.constant = 94;
            cell.lineView.hidden = NO;
            cell.myTextView.textColor = UIColorFromRGB(0x333333);
            cell.returnTextViewText = ^(NSString *string) {
                [dataDic setObject:string forKey:@"addressDetail"];
            };
            return cell;
        }
        
        LabelTFLineCell loadingTableViewCell("LabelTFLineCell", [cell reloadCellForCer:@{} vc:self indexPath:indexPath])
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.TF.userInteractionEnabled = NO;
        
        return cell;
    } else if (indexPath.section == 2) {
        LabelTFLineCell loadingTableViewCell("LabelTFLineCell", [cell reloadCellForCer:@{} vc:self indexPath:indexPath])
        cell.TF.tag = 6000 + indexPath.row;
        cell.TF.delegate = self;
        if (indexPath.row == 0) {
            cell.leftLabel.text = @"工作单位";
            cell.TF.placeholder = @"请输入工作单位";
        } else if (indexPath.row == 1) {
            cell.leftLabel.text = @"工作地点";
            cell.TF.placeholder = @"请输入工作地点";
        } else if (indexPath.row == 2) {
            cell.leftLabel.text = @"月收入";
            cell.TF.placeholder = @"请输入月收入(元)";
            cell.TF.keyboardType = UIKeyboardTypeNumberPad;
        } else if (indexPath.row == 3) {
            cell.leftLabel.text = @"单位电话";
            cell.TF.placeholder = @"请输入电话";
            cell.TF.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
            cell.lineView_L.constant = 0;
        }
        return cell;
    } else if (indexPath.section == 3) {
        CerLLCell loadingTableViewCell("CerLLCell", )
        return cell;
    } else if (indexPath.section == 6) {
        AbtnAndCardCell loadingTableViewCell("AbtnAndCardCell", )
        [cell.btn addTarget:self action:@selector(up) forControlEvents:(UIControlEventTouchUpInside)];
        return cell;
    }
    CerContactCell loadingTableViewCell("CerContactCell", )
    [cell reloadCellForData:@{} vc:self indexPath:indexPath];
    return cell;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [dataDic setObject:textField.text forKey:@(textField.tag)];
}

//* 1:婚姻状况;2:学历;3:省;4:市;5:区; */
- (void)up {
    if ([dataDic[@"1"] length] == 0) {
        ShowAlert(@"请选择婚姻状况", self);
        return;
    }
    if ([dataDic[@"2"] length] == 0) {
        ShowAlert(@"请选择学历", self);
        return;
    }
    if ([dataDic[@"3"] length] == 0) {
        ShowAlert(@"请选择地址", self);
        return;
    }

    if ([dataDic[@"addressDetail"] length] == 0) {
        ShowAlert(@"请填写详细地址", self);
        return;
    }
    
//    if ([dataDic[@(6000)] length] == 0) {
//        ShowAlert(@"请输入工作单位", self)
//        return;
//    }
//
//    if ([dataDic[@(6001)] length] == 0) {
//        ShowAlert(@"请输入工作地点", self)
//        return;
//    }
//
//    if ([dataDic[@(6002)] length] == 0) {
//        ShowAlert(@"请输入月收入", self)
//        return;
//    }
//
//    if ([dataDic[@(6003)] length] == 0) {
//        ShowAlert(@"请输入单位电话", self)
//        return;
//    }
    
    CerContactCell *cell5 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:4]];
    if (cell5.topTF.text.length == 0) {
        ShowAlert(@"联系人一的名字不可为空", self);
        return;
    }
    if (cell5.centerTF.text.length == 0) {
        ShowAlert(@"联系人一的联系方式不可为空", self);
        return;
    }
    
//    if (![self isPhone:cell5.centerTF.text]) {
//        ShowAlert(@"联系人一的手机号不正确", self);
//        return;
//    }
    
    if (cell5.bottomTF.text.length == 0) {
        ShowAlert(@"请选择您与联系人一的关系", self);
        return;
    }
    
    CerContactCell *cell6 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:5]];
    if (cell6.topTF.text.length == 0) {
        ShowAlert(@"联系人二的名字不可为空", self);
        return;
    }
    if (cell6.centerTF.text.length == 0) {
        ShowAlert(@"联系人二的联系方式不可为空", self);
        return;
    }
//    if (![self isPhone:cell6.centerTF.text]) {
//        ShowAlert(@"联系人二的手机号不正确", self);
//        return;
//    }
    if (cell6.bottomTF.text.length == 0) {
        ShowAlert(@"请选择您与联系人二的关系", self);
        return;
    }
    
    if ([cell6.centerTF.text isEqualToString:cell5.centerTF.text]) {
        ShowAlert(@"联系人一和联系人二的手机号码相同,请选择两个不同的号码", self);
        return;
    }
    
    NSMutableDictionary * paramDict = [[NSMutableDictionary alloc]initWithCapacity:0];
    NSMutableDictionary * phoneAddressBook1 = [[NSMutableDictionary alloc]initWithCapacity:0];
    NSMutableDictionary * phoneAddressBook2 = [[NSMutableDictionary alloc]initWithCapacity:0];
    NSMutableDictionary * addressDict = [[NSMutableDictionary alloc]initWithCapacity:0];
    [addressDict setObject:dataDic[@"3"] forKey:@"province"];
    [addressDict setObject:dataDic[@"4"] forKey:@"city"];
    [addressDict setObject:dataDic[@"5"] forKey:@"area"];
    [addressDict setObject:dataDic[@"addressDetail"] forKey:@"addressDetail"];
    [paramDict setObject:addressDict forKey:@"address"];
    
    [phoneAddressBook1 setObject:cell5.topTF.text forKey:@"contact"];
    [phoneAddressBook1 setObject:[cell5.centerTF.text stringByReplacingOccurrencesOfString:@"-" withString:@""] forKey:@"phone"];
    [phoneAddressBook1 setObject:cell5.bottomTF.text forKey:@"relationship"];
    [paramDict setObject:phoneAddressBook1 forKey:@"phoneAddressBook1"];
    
    [phoneAddressBook2 setObject:cell6.topTF.text forKey:@"contact"];
    [phoneAddressBook2 setObject:[cell6.centerTF.text stringByReplacingOccurrencesOfString:@"-" withString:@""] forKey:@"phone"];
    [phoneAddressBook2 setObject:cell6.bottomTF.text forKey:@"relationship"];
    [paramDict setObject:phoneAddressBook2 forKey:@"phoneAddressBook2"];
    
    [paramDict setObject:dataDic[@"1"] forKey:@"maritalStatus"];
    [paramDict setObject:dataDic[@"2"] forKey:@"education"];
    
//    [paramDict setObject:cell5.topTF.text forKey:@"firstContactName"];
//    [paramDict setObject:[cell5.centerTF.text stringByReplacingOccurrencesOfString:@"-" withString:@""] forKey:@"firstContactPhone"];
//    [paramDict setObject:cell5.bottomTF.text forKey:@"firstContactRelation"];
//    [paramDict setObject:cell6.topTF.text forKey:@"secondContactName"];
//    [paramDict setObject:[cell6.centerTF.text stringByReplacingOccurrencesOfString:@"-" withString:@""] forKey:@"secondContactPhone"];
//    [paramDict setObject:cell6.bottomTF.text forKey:@"secondContactRelation"];
    
    
    
    [HttpUrl POST:@"userBaseMsg/add" dict:paramDict hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        NSLog(@"提交基本信息返回数据:%@", data);
        if ([data[@"success"] integerValue] == 1) {
            self.reloadTheVC();
            [TheGlobalMethod popAlertView:@"提交成功" controller:self];
        } else {
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
}

- (BOOL)isPhone:(NSString *)mobileNum{
    NSString *MOBILE = @"^1(3[0-9]|4[57]|5[0-35-9]|8[0-9]|7[0678])\\d{8}$";
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    return [regextestmobile evaluateWithObject:mobileNum];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        if (indexPath.row == 1 || indexPath.row == 2) {
            PickerView *pickerView = [TheGlobalMethod setCell:@"PickerView"];
            pickerView.frame = CGRectMake(0, HEIGHT_K, WIDTH_K, HEIGHT_K);
            [[UIApplication sharedApplication].keyWindow addSubview:pickerView];
            if (indexPath.row == 1) {
                [pickerView reloadCellForData:@{@"arr":@[@"未婚",@"已婚",@"离异"]} vc:self];
            } else {
                [pickerView reloadCellForData:@{@"arr":@[@"研究生",@"大学",@"高中", @"初中", @"小学", @"其它"]} vc:self];
            }
            pickerView.ReturnString = ^(NSString *string) {
                LabelTFLineCell *cell = [myTableView cellForRowAtIndexPath:indexPath];
                cell.TF.text = string;
                if (indexPath.row == 1) {
                    [dataDic setObject:string forKey:@"1"];
                } else {
                    [dataDic setObject:string forKey:@"2"];
                }
            };
        } else if (indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5) {
            [self selectAddress:indexPath];
        }
    } else if (indexPath.section == 2) {
        if (indexPath.row == 1) {
            [self selectAddress:indexPath];
        }
    }
}

- (void)selectAddress:(NSIndexPath *)indexPath {
    if (!stPickerArea) {
        stPickerArea = [[STPickerArea alloc]initWithDelegate:self];
    }
    [stPickerArea show];
}

//选择地址代理方法
- (void)pickerArea:(STPickerArea *)pickerArea province:(NSString *)province city:(NSString *)city area:(NSString *)area {
    
    NSLog(@"1:%@; 2:%@; 3:%@", province, city, area);
    LabelTFLineCell *cell1 = [myTableView cellForRowAtIndexPath:[NSIndexPath  indexPathForRow:3 inSection:1]];
    LabelTFLineCell *cell2 = [myTableView cellForRowAtIndexPath:[NSIndexPath  indexPathForRow:4 inSection:1]];
    LabelTFLineCell *cell3 = [myTableView cellForRowAtIndexPath:[NSIndexPath  indexPathForRow:5 inSection:1]];
    if (area.length == 0) {
        [dataDic setObject:province forKey:@"3"];
        [dataDic setObject:province forKey:@"4"];
        [dataDic setObject:city forKey:@"5"];
        cell1.TF.text = [NSString stringWithFormat:@"%@ %@ %@",province,province,city];
 
    } else {
        [dataDic setObject:province forKey:@"3"];
        [dataDic setObject:city forKey:@"4"];
        [dataDic setObject:area forKey:@"5"];
        cell1.TF.text = province;
        cell2.TF.text = city;
        cell3.TF.text = area;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 44;
    } else if (indexPath.section == 1) {
        if (indexPath.row == 6) {
            return 88;
        }
    } else if (indexPath.section == 4 || indexPath.section == 5) {
        return 132;
    } else if (indexPath.section == 6) {
        return 86;
    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 2 || section == 3) {
        return 10;
    } else if (section == 6) {
        return 20;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, 20)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - 代理方法，当授权改变时调用
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    
    // 获取授权后，通过
    if (status == kCLAuthorizationStatusAuthorizedAlways) {
        
        //开始定位(具体位置要通过代理获得)
        [_manager startUpdatingLocation];
        
        //设置精确度
        _manager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        
        //设置过滤距离
        _manager.distanceFilter = 1000;
        
        //开始定位方向
        [_manager startUpdatingHeading];
    }
}
#pragma mark - 方向
- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    
    //输出方向
    
    //地理方向
    //    NSLog(@"true = %f ",newHeading.trueHeading);
    
    // 磁极方向
    //    NSLog(@"mag = %f",newHeading.magneticHeading);
    
    
    
}

#pragma mark - 定位代理
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    //    NSLog(@"%@",locations);
    
    //获取当前位置
    CLLocation *location = manager.location;
    //获取坐标
    CLLocationCoordinate2D corrdinate = location.coordinate;
    
    //打印地址
    NSLog(@"latitude = %f longtude = %f",corrdinate.latitude,corrdinate.longitude);
    
    // 地址的编码通过经纬度得到具体的地址
    CLGeocoder *gecoder = [[CLGeocoder alloc] init];
    
    [gecoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        
        CLPlacemark *placemark = [placemarks firstObject];
//        _localAddress = placemark.addressDictionary[@"City"];
        //打印地址
        //        NSLog(@"%@",placemark.name);
        NSLog(@"Name= %@ City = %@ Street = %@   SubLocality= %@  FormattedAddressLines = %@   SubThoroughfare = %@  Thoroughfare = %@  dict = %@   ",placemark.name,placemark.addressDictionary[@"City"],placemark.addressDictionary[@"Street"],placemark.addressDictionary[@"SubLocality"],placemark.addressDictionary[@"SubThoroughfare"],placemark.addressDictionary[@"Thoroughfare"],placemark.addressDictionary[@"FormattedAddressLines"][0],placemark.addressDictionary);
//        NSString *city = placemark.locality;

        NSLog(@"省 = = %@",placemark.administrativeArea);
        if ([_authDict[@"userAuth"][@"baiscAuth"] integerValue] == 3 ||[_authDict[@"userAuth"][@"baiscAuth"] integerValue] == 2) {
            LabelTFLineCell *cell1 = [myTableView cellForRowAtIndexPath:[NSIndexPath  indexPathForRow:1 inSection:1]];
 
            TextViewCell *cell4 = [myTableView cellForRowAtIndexPath:[NSIndexPath  indexPathForRow:2 inSection:1]];
            cell1.TF.text = [NSString stringWithFormat:@"%@ %@ %@",placemark.administrativeArea,isStringEmpty(placemark.locality) == YES ? placemark.administrativeArea:placemark.locality,placemark.addressDictionary[@"SubLocality"]];
            
            cell4.myTextView.text = placemark.addressDictionary[@"Street"];
            
            [dataDic setObject:placemark.administrativeArea forKey:@"3"];
            [dataDic setObject:isStringEmpty(placemark.locality) == YES ? placemark.administrativeArea:placemark.locality forKey:@"4"];
            [dataDic setObject:placemark.addressDictionary[@"Street"] forKey:@"5"];
        }
        
        
    }];
    
    // 通过具体地址去获得经纬度
    //    CLGeocoder *coder = [[CLGeocoder alloc] init];
    //
    //    [coder geocodeAddressString:@"天河城" completionHandler:^(NSArray *placemarks, NSError *error) {
    //
    //
    //
    //        NSLog(@"_________________________反编码");
    //
    //        CLPlacemark *placeMark = [placemarks firstObject];
    //
    //
    //
    //        NSLog(@"%@ lati = %f long = %f",placeMark.name,placeMark.location.coordinate.latitude,placeMark.location.coordinate.longitude);
    //
    //
    //    }];
    //
    
    //停止定位
    [_manager stopUpdatingLocation];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
