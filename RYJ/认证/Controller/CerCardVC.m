//
//  CerCardVC.m
//  RYJ
//
//  Created by wyp on 2017/7/26.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "CerCardVC.h"

//#import "XLBankScanViewController.h"

#import "LabelTFLineCell.h"
#import "RightBtnCell.h"
#import "CerCardBtnCell.h"
#import "AbtnAndCardCell.h"

#import "BankCardDataManager.h"

@interface CerCardVC ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate> {

    __weak IBOutlet RootTableView *myTableView;
    NSTimer *timer;
    
    NSString *bankName;
    
    NSDictionary *dataDic;
}

@end

@implementation CerCardVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    
    bankName = @"";
    
    if ([_isMine isEqualToString:@"1"]) {
        self.title = @"银行卡管理";
    }
    
    if ([_isCer isEqualToString:@"1"]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            [HttpUrl GET:@"userBank/findOneByUser" dict:nil hud:self.view isShow:NO WithSuccessBlock:^(id data) {
                NSLog(@"银行卡信息:%@", data);
                if ([data[@"success"] integerValue] == 1) {
                    dataDic = data[@"data"];
                    //直接遍历
                    if (![dataDic isEqual:[NSNull null]]) {
                        for (NSString * key in [BankCardDataManager shareManager].cardDataDict) {
                            if ([dataDic[@"bankcardno"] hasPrefix:key]) {
                                bankName = [BankCardDataManager shareManager].cardDataDict[key];
                                break;
                            }else{
                                bankName = @"";
                            }
                        }
                       
                    }else{
                        _isCer = @"0";
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        [myTableView reloadData];
                    });
                    
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        
                    });
                    [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
                }
                
                
            }];
        });
        
    }
    
    myTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, 10)];
    myTableView.tableHeaderView.backgroundColor = [UIColor clearColor];
    // Do any additional setup after loading the view.
}

#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        if (bankName.length > 0) {
            return 4;
        }
        return 3;
    } else if (section == 2) {
        return 1;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        LabelTFLineCell loadingTableViewCell("LabelTFLineCell", if(indexPath.row==2){cell.rightBtn.hidden = NO;})
        if ([_isCer isEqualToString:@"1"]) {
            cell.TF.userInteractionEnabled = NO;
        } else {
            cell.TF.userInteractionEnabled = YES;
        }
        if (indexPath.row == 0) {
            cell.leftLabel.text = @"姓名";
            cell.TF.placeholder = @"请输入姓名";
            if ([_isCer isEqualToString:@"1"]) {
                [Methods dataInTFText:dataDic[@"name"] InTF:cell.TF defaultStr:@"-"];
            }
        } else if (indexPath.row == 1) {
            cell.leftLabel.text = @"身份证";
            cell.TF.placeholder = @"请输入18位身份证号";

            if ([_isCer isEqualToString:@"1"]) {
                [Methods dataInTFText:dataDic[@"idcardno"] InTF:cell.TF defaultStr:@"-"];
            }
        } else if (indexPath.row == 2) {
            cell.leftLabel.text = @"储蓄卡";
            cell.TF.placeholder = @"请输入银行卡号";
            cell.TF.delegate = self;

            cell.TF.keyboardType = UIKeyboardTypeNumberPad;
            [cell.rightBtn addTarget:self action:@selector(saomiao) forControlEvents:(UIControlEventTouchUpInside)];
            
            if ([_isCer isEqualToString:@"1"]) {
                [Methods dataInTFText:dataDic[@"bankcardno"] InTF:cell.TF defaultStr:@"-"];
                
                cell.rightBtn.userInteractionEnabled = NO;
            } else {
                cell.rightBtn.userInteractionEnabled = YES;
            }
        } else if (indexPath.row == 3) {
            cell.leftLabel.text = @"银行名称";
            cell.TF.text = bankName;

            cell.TF.userInteractionEnabled = NO;
            if ([_isCer isEqualToString:@"1"]) {
                [Methods dataInTFText:dataDic[@"bankName"] InTF:cell.TF defaultStr:@"-"];
                
            }
        }
        if ([cell.TF.text isEqualToString:@"-"]) {
            cell.TF.text = @"";
        }
        return cell;
    } else if (indexPath.section == 1) {
        RightBtnCell loadingTableViewCell("RightBtnCell", )
        return cell;
    } else if (indexPath.section == 2) {
        LabelTFLineCell loadingTableViewCell("LabelTFLineCell", )
        if ([_isCer isEqualToString:@"1"]) {
            cell.TF.userInteractionEnabled = NO;
        } else {
            cell.TF.userInteractionEnabled = YES;
        }

        if (indexPath.row == 0) {
            cell.leftLabel.text = @"预留手机";
            cell.TF.placeholder = @"请输入手机号码";
            cell.lineView_L.constant = 0;
            cell.TF.keyboardType = UIKeyboardTypeNumberPad;
            if ([_isCer isEqualToString:@"1"]) {
                [Methods dataInTFText:dataDic[@"bankPhone"] InTF:cell.TF defaultStr:@"-"];
            }
        } else if (indexPath.row == 1) {
            cell.leftLabel.text = @"验证码";
            cell.TF.placeholder = @"请输入验证码";
            cell.lineView_L.constant = 0;
            cell.getBtn.hidden = NO;
            cell.TF_R.constant = 105;
            [cell.getBtn addTarget:self action:@selector(getVerification) forControlEvents:(UIControlEventTouchUpInside)];

        }
        if ([cell.TF.text isEqualToString:@"-"]) {
            cell.TF.text = @"";
        }
        return cell;
    }
    AbtnAndCardCell loadingTableViewCell("AbtnAndCardCell", )
    [cell.btn addTarget:self action:@selector(up) forControlEvents:(UIControlEventTouchUpInside)];
    if ([_isCer isEqualToString:@"1"]) {
        [cell.btn setTitle:@"修改银行卡信息" forState:(UIControlStateNormal)];
    }
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, 12)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 2) {
        return 12;
    }
    return 0;
}

- (void)up {

    if ([_isCer isEqualToString:@"1"]) {
        _isCer = @"0";
        [myTableView reloadData];
        LabelTFLineCell *cell1 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        cell1.TF.userInteractionEnabled  = YES;
        [cell1.TF becomeFirstResponder];
        
        AbtnAndCardCell *cell2 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:4]];
        [cell2.btn setTitle:@"提交" forState:(UIControlStateNormal)];
        return;
    }
    
    [self.view endEditing:YES];
    LabelTFLineCell *cell1 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if (cell1.TF.text.length == 0) {
        ShowAlert(@"请输入姓名", self);
        return;
    }
    LabelTFLineCell *cell2 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    if (cell2.TF.text.length == 0) {
        ShowAlert(@"请输入身份证号", self);
        return;
    }
    LabelTFLineCell *cell3 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    if (cell3.TF.text.length == 0) {
        ShowAlert(@"请输入银行卡号或直接扫描银行卡", self);
        return;
    }
    LabelTFLineCell *cell4 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
    if (cell4.TF.text.length != 11) {
        ShowAlert(@"请输入11位手机号码", self);
        return;
    }
            [HttpUrl GET:@"bankCard/bankCardAuth" dict:@{@"bankcardno":cell3.TF.text, @"name":cell1.TF.text, @"idcardno":cell2.TF.text, @"phone":cell4.TF.text} hud:self.view isShow:YES WithSuccessBlock:^(id data) {
                NSLog(@"提交银行卡认证数据:%@", data);
                if ([data[@"success"] integerValue] == 1) {
                        self.reloadTheVC();
                    [TheGlobalMethod popAlertView:@"验证成功" controller:self];
                    
                } else {
                    [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
                }
            }];

}

static NSString *tempStr;
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (!tempStr) {
        tempStr = string;
    } else {
        tempStr = [NSString stringWithFormat:@"%@%@",tempStr,string];
    }
    
    if ([string isEqualToString:@""]) {
        if (tempStr.length > 0) {   //  删除最后一个字符串
            NSString *lastStr = [tempStr substringToIndex:[tempStr length] - 1];
            tempStr = lastStr;
        }
    }
    
    LabelTFLineCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    if (tempStr.length > 0) {
        cell.rightBtn.hidden = YES;
    } else {
        cell.rightBtn.hidden = NO;
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.text.length > 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            //直接遍历
            for (NSString * key in [BankCardDataManager shareManager].cardDataDict) {
                if ([textField.text hasPrefix:key]) {
                    bankName = [BankCardDataManager shareManager].cardDataDict[key];
                    [myTableView reloadData];
                    
                    break;
                }else{
                    bankName = @"";
                    [myTableView reloadData];
                }
            }
        });
        
    }
}

- (void)saomiao {
    WYIDScanViewController *VC = [[WYIDScanViewController alloc] initWithCarInfo:CardIDBank];
    self.navigationController.navigationBar.tintColor = appText333333Color;

    [self.navigationController pushViewController:VC animated:YES];


    [VC scanDidFinishCarInfo:^(CardType status, WYScanResultModel *scanModel) {
        [self.navigationController popViewControllerAnimated:YES];
        [JXTAlertView showAlertViewWithTitle:@"请核对您的银行卡信息" message:[NSString stringWithFormat:@"银行名称:%@\n银行卡号:%@", scanModel.bankName, scanModel.bankNumber] cancelButtonTitle:nil buttonIndexBlock:^(NSInteger buttonIndex) {
            if (buttonIndex == 1) {
                [self.navigationController pushViewController:VC animated:YES];
            }
        } otherButtonTitles:@[@"信息正确", @"重新扫描"]];
        NSLog(@"%@--%@", scanModel.bankName, scanModel.bankNumber);
        bankName = scanModel.bankName;
        LabelTFLineCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
        cell.TF.text = [scanModel.bankNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
        [myTableView reloadData];
    }];
}

- (void)getVerification {
    NSLog(@"获取验证码");
    [self.view endEditing:YES];
    LabelTFLineCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
    if (cell.TF.text.length != 11) {
        [TheGlobalMethod xianShiAlertView:@"请输入11位手机号" controller:self];
        return;
    }
    
    //    [HttpUrl GET:@"user/getPhoneCode" dict:@{@"phone":cell.TF.text} hud:self.view isShow:YES WithSuccessBlock:^(id data) {
    //
    //    }];
    
    LabelTFLineCell *cell1 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:2]];
    cell1.getBtn.userInteractionEnabled = NO;
    timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(timer) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}
//定时器验证码计时
static NSInteger timerNumber = 59;
- (void)timer {
    LabelTFLineCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:2]];
    
    [cell.getBtn setTitle:[NSString stringWithFormat:@"%lds重新获取", timerNumber] forState:(UIControlStateNormal)];
    [cell.getBtn setTitleColor:NAVCOLOR_C(153.0, 153.0, 153.0) forState:(UIControlStateNormal)];
    cell.getBtn.layer.borderColor = NAVCOLOR_C(153.0, 153.0, 153.0).CGColor;
    timerNumber--;
    if (timerNumber == 0) {
        [timer invalidate];
        timer = nil;
        [cell.getBtn setTitle:[NSString stringWithFormat:@"获取验证码"] forState:(UIControlStateNormal)];
        [cell.getBtn setTitleColor:appDefaultColor forState:(UIControlStateNormal)];
        cell.getBtn.layer.borderUIColor = appDefaultColor;
        cell.getBtn.backgroundColor = [UIColor clearColor];
        timerNumber = 59;
        cell.getBtn.userInteractionEnabled = YES;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 4) {
        return 86;
    } else if (indexPath.section == 1) {
        return 44;
    }
    return 44;
}

- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
