//
//  TheCertificationVC.m
//  RYJ
//
//  Created by 云鹏 on 2017/11/22.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>

#import <PPGetAddressBook.h>

#import "TheCertificationVC.h"
#import "CerPhoneVC.h"
#import "CerCardVC.h"

#import "MLBCertificationListCell.h"
#import "ListCell.h"
#import "ABtnCell.h"
#import "PickerView.h"
#import "STPickerArea.h"
#import "ChsiLoginVC.h"
#pragma mark 铜盾淘宝
#import "shujumohePB.h"

#import "LYCertificationCell.h"
#import "CerPersonMsgVC.h"

@interface TheCertificationVC ()<UITableViewDelegate,UITableViewDataSource,ABPeoplePickerNavigationControllerDelegate,AXWebViewControllerDelegate,shujumoheDelegate>
{
    
    __weak IBOutlet RootTableView *myTableView;
    NSArray *dataArr;
    
    STPickerArea *stPickerArea;
    
    NSIndexPath * _selectIndexPath;
    
    
    NSMutableDictionary * _paramDict;
    
    NSString * _provence;
    NSString * _city;
    NSString * _area;
    
    NSString * _bankAuth;
    NSString * _phoneAuth;
    NSString * _xuexinAuth;
    NSString * _zimaAuth;
    NSString * _taobaoAuth;
    NSString * _basicAuth;
    NSMutableDictionary * _authDict;
}
//@property (nonatomic,strong)NSDictionary * authDict;

@end

@implementation TheCertificationVC
- (void)viewDidLoad {
    [super viewDidLoad];
    _bankAuth = @"0";
    _phoneAuth = @"0";
    _xuexinAuth = @"0";
    _zimaAuth = @"0";
    _taobaoAuth = @"0";
    _xuexinAuth = @"0";
    _basicAuth = @"0";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tbBack:) name:@"淘宝认证回调" object:nil];
    myTableView.backgroundColor = [UIColor whiteColor];
    setLeftPopButtonItemAndTitle(@"认证")
    dataArr = @[@[@"最高学历", @"婚姻状态", @"紧急联系人",@"紧急联系人2", @"所在地区", @"银行卡认证",@"手机认证", @"芝麻授信",], @[@"淘宝认证",@"学信网认证"]];

    _paramDict = [[NSMutableDictionary alloc]initWithCapacity:0];
//    [PPGetAddressBook requestAddressBookAuthorization];
//    if (_type == 1) {
//        [self getCer];
//    }
//    [self getAllUserContact];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getCer];

}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
//    if (_type == 1) {
//        [self getCer];
//    }
}
-(void)getCer {
    [HttpUrl GET:@"userAuth/selectUserAuth" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if (BB_isSuccess) {
            _authDict = [[NSMutableDictionary alloc]initWithCapacity:0];

//            data =     {
//                userAuth =         {
//                    auditorId = "<null>";
//                    baiscAuth = 1;
//                    bankAuth = 1;
//                    gmtDatetime = "2017-12-26 19:50:59";
//                    id = 16;
//                    identityAuth = 1;
//                    phoneAuth = 1;
//                    schoolInfo = 0;
//                    taobaoAuth = 0;
//                    user = "<null>";
//                    userId = 21;
//                    zhimaAuth = 0;
//                };
//                userBasicMsg =         {
//                    address =             {
//                        addressCode = "<null>";
//                        addressDetails = "<null>";
//                        area = "\U6bd5\U8282\U5e02";
//                        areaCode = "<null>";
//                        city = "\U6bd5\U8282\U5730";
//                        gmtDatetime = "<null>";
//                        id = 25;
//                        province = "\U8d35\U5dde";
//                        status = "<null>";
//                        uptDatetime = "<null>";
//                        userId = 21;
//                    };
//                    addressId = 25;
//                    firstContactName = "\U522b\U6d6a";
//                    firstContactPhone = 13554151701;
//                    firstContactRelation = " ";
//                    gmtDatetime = "2017-12-26 19:59:46";
//                    id = 22;
//                    marry = "\U672a\U5a5a";
//                    secondContactName = "\U7238\U7238";
//                    secondContactPhone = 18849541277;
//                    secondContactRelation = " ";
//                    status = "\U4f7f\U7528";
//                    study = "\U535a\U58eb";
//                    uptDatetime = "<null>";
//                    user = "<null>";
//                    userId = 21;
//                };
//            };

            _authDict = data[@"data"];
            _bankAuth =[_authDict[@"userAuth"][@"bankAuth"] description];
            _phoneAuth = [_authDict[@"userAuth"][@"phoneAuth"] description];
            _zimaAuth = [_authDict[@"userAuth"][@"zhimaAuth"] description];
            _taobaoAuth = [_authDict[@"userAuth"][@"taobaoAuth"] description];
            _xuexinAuth = [_authDict[@"userAuth"][@"schoolInfo"] description];
            _basicAuth = [_authDict[@"userAuth"][@"baiscAuth"] description];
//            [self setRightLab:_authDict];
            [myTableView reloadData];
        }
    }];
}
- (void)tbBack:(NSNotification *)notification {
    [self.navigationController popViewControllerAnimated:YES];
    if ([notification.userInfo[@"url"] isEqualToString:@"weijie://www.baidu.com"]) {
        ShowAlert(@"认证成功", self)
    } else if ([notification.userInfo[@"url"] isEqualToString:@"weijie://www.sogou.com/"]) {
        ShowAlert(@"认证失败", self)
    }
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (_authDict.allKeys.count>0) {
        return 2;

    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 4;
    } else {
        return 2;
    }
}






- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LYCertificationCell loadingTableViewCell("LYCertificationCell",)
    [cell reloadCellForData:_authDict vc:self indexPath:indexPath];//
    return cell;
}

//- (void)done {
//    NSLog(@"完成");
//    NSMutableDictionary * dict = [[NSMutableDictionary alloc]initWithCapacity:0];
//
//    MLBCertificationListCell * cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
//    if (isStringEmpty(cell.contentLab.text)||cell.contentLab.text.length == 0) {
//        [TheGlobalMethod xianShiAlertView:@"请选择您的最高学历" controller:self];
//        return;
//    }else{
//        [dict setObject:cell.contentLab.text forKey:@"study"];
//    }
//    MLBCertificationListCell * cell1 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
//    if (isStringEmpty(cell1.contentLab.text)||cell1.contentLab.text.length == 0) {
//        [TheGlobalMethod xianShiAlertView:@"请选择您的婚姻状态" controller:self];
//        return;
//    }else{
//        [dict setObject:cell1.contentLab.text forKey:@"marry"];
//    }
//    MLBCertificationListCell * cell2 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
//    if (isStringEmpty(cell2.contentLab.text)||cell2.contentLab.text.length == 0) {
//        [TheGlobalMethod xianShiAlertView:@"请选择紧急联系人" controller:self];
//        return;
//    }else{
//        NSArray * arr = [cell2.contentLab.text componentsSeparatedByString:@" "];
//        NSString * phone = arr[1];
//
//        [dict setObject:arr[0] forKey:@"firstContactName"];
//        [dict setObject:[phone stringByReplacingOccurrencesOfString:@"-" withString:@""] forKey:@"firstContactPhone"];
//        [dict setObject:@" " forKey:@"firstContactRelation"];
//    }
//    MLBCertificationListCell * cell3 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
//    if (isStringEmpty(cell3.contentLab.text)||cell3.contentLab.text.length == 0) {
//        [TheGlobalMethod xianShiAlertView:@"请选择紧急联系人" controller:self];
//        return;
//    }else{
//        NSArray * arr = [cell3.contentLab.text componentsSeparatedByString:@" "];
//        NSString * phone = arr[1];
//        [dict setObject:arr[0] forKey:@"secondContactName"];
//        [dict setObject:[phone stringByReplacingOccurrencesOfString:@"-" withString:@""] forKey:@"secondContactPhone"];
//        [dict setObject:@" " forKey:@"secondContactRelation"];
//    }
//    MLBCertificationListCell * cell4 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
//    if (isStringEmpty(cell4.contentLab.text)||cell4.contentLab.text.length == 0) {
//        [TheGlobalMethod xianShiAlertView:@"请选择所在地区" controller:self];
//        return;
//    }else{
//        NSMutableDictionary * addressDict = [[NSMutableDictionary alloc]initWithCapacity:0];
//        [addressDict setObject:_provence forKey:@"province"];
//        [addressDict setObject:_city forKey:@"city"];
//        [addressDict setObject:_area forKey:@"area"];
//        [dict setObject:addressDict forKey:@"address"];
//    }
//
//    if ([_bankAuth isEqualToString:@"3"]) {
//        [TheGlobalMethod xianShiAlertView:@"请完成银行卡认证" controller:self];
//        return;
//    }
//    if ([_phoneAuth isEqualToString:@"3"]) {
//        [TheGlobalMethod xianShiAlertView:@"请完成手机认证" controller:self];
//        return;
//    }
//    if ([_zimaAuth isEqualToString:@"3"]) {
//        [TheGlobalMethod xianShiAlertView:@"请完成芝麻授信" controller:self];
//        return;
//    }
//    [HttpUrl POST:@"userBasicMsg/add" dict:dict hud:self.view isShow:YES WithSuccessBlock:^(id data) {
//        if (BB_isSuccess) {
//            [TheGlobalMethod selectAlertView:@"认证成功" controller:self select:^{
//                SetPop;
//            }];
//        }else{
//            [TheGlobalMethod xianShiAlertView:@"认证失败" controller:self];
//        }
//    }];
//
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
   
    ListCell *view = [TheGlobalMethod setCell:@"ListCell"];
    view.backgroundColor = UIColorFromRGB(0xf0f0f0);
    view.frame = CGRectMake(0, 0, WIDTH_K-24, 30);
    view.label.font = [UIFont systemFontOfSize:12];
    view.label.textColor = NAVCOLOR_C(153.0, 153.0, 153.0);
    view.rightImg.hidden = YES;
    
    if (section==0) {
        view.label.text = @"必填资料";
    } else if (section == 1){
        view.label.text = @"选填资料";
    }
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 1) {
        return 16;
    }
   
    return 0;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:{
                if ([_authDict[@"userAuth"][@"baiscAuth"] integerValue] == 2 || [_authDict[@"userAuth"][@"baiscAuth"] integerValue] == 3) {
                    UIStoryboard * story = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                    CerPersonMsgVC * vc = [story instantiateViewControllerWithIdentifier:@"CerPersonMsgVC"];
                    vc.reloadTheVC = ^{
                        _basicAuth = @"0";
                    };
                    vc.authDict = _authDict;
                    if ([_authDict[@"userAuth"][@"baiscAuth"] integerValue] == 1 || [_authDict[@"userAuth"][@"baiscAuth"] integerValue] == 0) {
                        vc.isCer = @"1";
                    }
                    vc.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:vc animated:YES];
                }
                
            }
            
            break;
//            case 0:
//                //学历
//            {
//                PickerView *pickerView = [TheGlobalMethod setCell:@"PickerView"];
//                pickerView.frame = CGRectMake(0, HEIGHT_K, WIDTH_K, HEIGHT_K);
//                [[UIApplication sharedApplication].keyWindow addSubview:pickerView];
//                [pickerView reloadCellForData:@{@"arr":@[@"博士",@"硕士",@"本科",@"专科",@"高中", @"初中", @"小学"]} vc:self];
//                pickerView.ReturnString = ^(NSString *string) {
//                    MLBCertificationListCell *cell = [myTableView cellForRowAtIndexPath:indexPath];
//                    cell.contentLab.text = string;
//                    [_paramDict setObject:string forKey:@"0"];
//
//                };
//            }
//                break;
//            case 1:
//                //婚姻状态
//            {
//                PickerView *pickerView = [TheGlobalMethod setCell:@"PickerView"];                pickerView.frame = CGRectMake(0, HEIGHT_K, WIDTH_K, HEIGHT_K);                [[UIApplication sharedApplication].keyWindow addSubview:pickerView];
//                [pickerView reloadCellForData:@{@"arr":@[@"未婚",@"已婚",@"未知"]} vc:self];
//                pickerView.ReturnString = ^(NSString *string) {
//                    MLBCertificationListCell *cell = [myTableView cellForRowAtIndexPath:indexPath];
//                    cell.contentLab.text = string;
//                    [_paramDict setObject:string forKey:@"1"];
//                };
//            }
//                break;
//            case 2:
//                //紧急联系人
//            {
//                _selectIndexPath = indexPath;
//                ABPeoplePickerNavigationController * vc = [[ABPeoplePickerNavigationController alloc] init];
//                vc.peoplePickerDelegate = self;
//                [self presentViewController:vc animated:YES completion:nil];
//            }
//                break;
//            case 3:
//                //紧急联系人2
//            {
//                _selectIndexPath = indexPath;
//
//                ABPeoplePickerNavigationController * vc = [[ABPeoplePickerNavigationController alloc] init];
//                vc.peoplePickerDelegate = self;
//                [self presentViewController:vc animated:YES completion:nil];
//            }
//                break;
//            case 4:
//                //所在地区
//                [self selectAddress:indexPath];
//                break;
            case 1:
                //银行卡认证
            {
                if ([_authDict[@"userAuth"][@"bankAuth"] integerValue] == 3 ||[_authDict[@"userAuth"][@"bankAuth"] integerValue] == 2) {
                    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                    CerCardVC * vc = [storyboard instantiateViewControllerWithIdentifier:@"CerCardVC"];
                    vc.reloadTheVC = ^{
                        //                    [self getNetWorking];
                        _bankAuth = @"0";
                        //                    MLBCertificationListCell *cell = [myTableView cellForRowAtIndexPath:indexPath];
                        //                    cell.contentLab.text = @"正在审核";
                    };
                    if ([_authDict[@"userAuth"][@"bankAuth"] integerValue] != 3) {
                        vc.isCer = @"1";
                    }
                    [self.navigationController pushViewController:vc animated:YES];
                }
                
                
            } 
                break;
            case 2:
                //手机认证
            {
                if ([_authDict[@"userAuth"][@"phoneAuth"] integerValue] == 3 ||[_authDict[@"userAuth"][@"phoneAuth"] integerValue] == 2) {
                    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                    
                    CerPhoneVC * vc = [storyboard instantiateViewControllerWithIdentifier:@"CerPhoneVC"];
                    vc.reloadTheVC = ^{
                        _phoneAuth = @"0";
                        //                    MLBCertificationListCell *cell = [myTableView cellForRowAtIndexPath:indexPath];
                        //                    cell.contentLab.text = @"正在审核";
                    };
                    [self.navigationController pushViewController:vc animated:YES];
                }
                
            }
                break;
            case 3:
                //芝麻授信
            {
                if ([_authDict[@"userAuth"][@"zhimaAuth"] integerValue] == 2 ||
                    [_authDict[@"userAuth"][@"zhimaAuth"] integerValue] == 3) {
                    [HttpUrl GET:@"userIdentity/zhimaxinyong" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
                        NSLog(@"%@", data);
                        //                [Methods pushUrl:data[@"msg"] seleVC:self];
                        AXWebViewController *webVC = [[AXWebViewController alloc] initWithAddress:data[@"msg"]];
                        webVC.returnUrlString = ^(NSString * _Nonnull urlString) {
                            NSLog(@"回调回调回调回调%@", urlString);
                            _zimaAuth = @"0";
                            //                        MLBCertificationListCell *cell = [myTableView cellForRowAtIndexPath:indexPath];
                            //                        cell.contentLab.text = @"正在审核";
                            //                        [self getNetWorking];
                            //                        [self dingWei];//定位
                        };
                        webVC.showsToolBar = NO;
                        
                        webVC.navigationController.navigationBar.translucent = NO;
                        self.navigationController.navigationBar.tintColor = appText333333Color;
                        //                        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.996f green:0.867f blue:0.522f alpha:1.00f];
                        webVC.navigationType = AXWebViewControllerNavigationToolItem;
                        webVC.hidesBottomBarWhenPushed = YES;
                        webVC.delegate = self;
                        [self.navigationController pushViewController:webVC animated:YES];
                        //                returnUrlString
                    }];
                }
                
            }
                break;
            
            default:
                break;
        }
    }else{
        switch (indexPath.row) {
            case 0:
            {
//                userTaobao/taobaoLoading
                if ([_authDict[@"userAuth"][@"taobaoAuth"] integerValue] == 3 ||[_authDict[@"userAuth"][@"taobaoAuth"] integerValue] == 2) {
                    [self btnClick];
                }

            }
                
                break;
            case 1:
            {
                if ([_authDict[@"userAuth"][@"schoolInfo"] integerValue] == 3 ||[_authDict[@"userAuth"][@"schoolInfo"] integerValue] == 2) {
                    ChsiLoginVC * vc = [[ChsiLoginVC alloc]initWithNibName:@"ChsiLoginVC" bundle:nil];
                    [self.navigationController pushViewController:vc animated:YES];
                    
                }
                
                
            }
                
                break;
                
            default:
                break;
        }
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, 16)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}
- (void)selectAddress:(NSIndexPath *)indexPath {
    if (!stPickerArea) {
        stPickerArea = [[STPickerArea alloc]initWithDelegate:self];
    }
    [stPickerArea show];
}

//选择地址代理方法
- (void)pickerArea:(STPickerArea *)pickerArea province:(NSString *)province city:(NSString *)city area:(NSString *)area {
    _provence = province;
    _city = city;
    _area = area;
    MLBCertificationListCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
    cell.contentLab.text = [NSString stringWithFormat:@"%@%@%@",province,city,area];
//    [_paramDict setObject:cell.contentLab.text forKey:@"4"];
    NSLog(@"1:%@; 2:%@; 3:%@", province, city, area);
    
}

#pragma mark 淘宝认证
- (void)btnClick
{
    PBBaseReq *br = [PBBaseReq new];
    br.partnerCode=@"mlb_mohe";//合作方code
    br.partnerKey = @"2f0c8a5b1d8e45eca20a150abe4c10d5";//合作方key
    br.channel_code = @"005003";//淘宝的channel_code
    /*
     基础设置
     如果不用自定义PBBaseSet，withBaseSet传nil
     如果只用某一个颜色、字体大小、图片，创建PBBaseSet对象，给相应颜色、字体大小、图片的对象即可.
     */
    PBBaseSet *set = [PBBaseSet new];
    //导航栏颜色
    set.navBGColor = appDefaultColor;
    //导航栏标题颜色
    set.navTitleColor = [UIColor whiteColor];
    //导航栏标题字体
    set.navTitleFont = [UIFont systemFontOfSize:19];
    //导航栏按钮图片
    set.backBtnImage = [UIImage imageNamed:@"tbfanhui"];
    
    [shujumohePB openPBPluginAtViewController:self withDelegate:self withReq:br withBaseSet:set];
}

#pragma mark - HTTPS证书信任,此方法必须放到conroller内。
- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler
{
    NSURLProtectionSpace *protectionSpace = challenge.protectionSpace;
    if ([protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        SecTrustRef serverTrust = protectionSpace.serverTrust;
        completionHandler(NSURLSessionAuthChallengeUseCredential, [NSURLCredential credentialForTrust:serverTrust]);
    } else {
        completionHandler(NSURLSessionAuthChallengePerformDefaultHandling, nil);
    }
}

- (void)thePBMissionWithCode:(NSString *)code withMessage:(NSString *)message
{
    /*
     code = 0:表示成功，其他状态请参考文档。
     */
    if ([code isEqualToString: @"0"]) {
        
        [HttpUrl NetErrorGET:@"user/loginPhone" dict:@{@"phone":[TheGlobalMethod getUserDefault:@"phone"], @"password":[TheGlobalMethod getUserDefault:@"password"]} hud:self.view isShow:NO WithSuccessBlock:^(id data) {
            
            if ([data[@"success"] integerValue] == 1) {
                
                [TheGlobalMethod insertUserDefault:[data[@"data"][@"token"] description] Key:@"token"];
                [TheGlobalMethod insertUserDefault:data[@"data"][@"uuid"] Key:@"uuid"];
                
                [HttpUrl GET:@"userTaobao/taobaoLoading" dict:@{@"task_id":message} hud:self.view isShow:YES WithSuccessBlock:^(id data) {
                    NSLog(@"传给后台淘宝回调数据:%@", data);
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    [self getCer];
                }];
            }
            
            NSLog(@"登录登录登录%@", data);
        }WithFailBlock:^(id data) {
            [JXTAlertView showAlertViewWithTitle:@"提示" message:@"连接网络失败" cancelButtonTitle:nil buttonIndexBlock:^(NSInteger buttonIndex) {
                
            } otherButtonTitles:@[@"确定"]];
        }];
        
        
    } else {
        [HttpUrl NetErrorGET:@"user/loginPhone" dict:@{@"phone":[TheGlobalMethod getUserDefault:@"phone"], @"password":[TheGlobalMethod getUserDefault:@"password"]} hud:self.view isShow:NO WithSuccessBlock:^(id data) {
            
            if ([data[@"success"] integerValue] == 1) {
                
               
                [TheGlobalMethod insertUserDefault:[data[@"data"][@"token"] description] Key:@"token"];
                [TheGlobalMethod insertUserDefault:data[@"data"][@"uuid"] Key:@"uuid"];
                
            }
            
            NSLog(@"登录登录登录%@", data);
        }WithFailBlock:^(id data) {
            [JXTAlertView showAlertViewWithTitle:@"提示" message:@"连接网络失败" cancelButtonTitle:nil buttonIndexBlock:^(NSInteger buttonIndex) {
                
            } otherButtonTitles:@[@"确定"]];
        }];
    }
    NSLog(@"状态码：%@    对应信息：%@",code,message);
}
#pragma mark - 通讯录

#pragma mark -- ABPeoplePickerNavigationControllerDelegate
- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController*)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier {
    ABMultiValueRef valuesRef = ABRecordCopyValue(person, kABPersonPhoneProperty);
    CFIndex index = ABMultiValueGetIndexForIdentifier(valuesRef,identifier);
    //电话号码
    CFStringRef telValue = ABMultiValueCopyValueAtIndex(valuesRef,index);
    //读取firstname
    //获取个人名字（可以通过以下两个方法获取名字，第一种是姓、名；第二种是通过全名）。
    //第一中方法
    //    CFTypeRef firstName = ABRecordCopyValue(person, kABPersonFirstNameProperty);
    //    CFTypeRef lastName = ABRecordCopyValue(person, kABPersonLastNameProperty);
    //    //姓
    //    NSString * nameString = (__bridge NSString *)firstName;
    //    //名
    //    NSString * lastString = (__bridge NSString *)lastName;
    //第二种方法：全名
    CFStringRef anFullName = ABRecordCopyCompositeName(person);
    NSString * nameStr = (__bridge NSString *)(anFullName);
    [self dismissViewControllerAnimated:YES completion:^{
        NSString *phoneStr = (__bridge NSString *)telValue;
        NSLog(@"查看手机号格式:%@", phoneStr);
        
//        centerTF.text = [phoneStr stringByReplacingOccurrencesOfString:@"-" withString:@""];
//        centerTF.text = [phoneStr stringByReplacingOccurrencesOfString:@" " withString:@""];
//        //        self.nameLabel.text = [NSString stringWithFormat:@"%@%@",nameString,lastString];
//        topTF.text = [NSString stringWithFormat:@"%@",anFullName];
        MLBCertificationListCell *cell = [myTableView cellForRowAtIndexPath:_selectIndexPath];
        cell.contentLab.text = [NSString stringWithFormat:@"%@ %@",nameStr,phoneStr];
        [_paramDict setObject:cell.contentLab.text forKey:@"3"];
        [self getAllUserContact];
    }];
}

- (void)getAllUserContact {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        //获取没有经过排序的联系人模型
        [PPGetAddressBook getOriginalAddressBook:^(NSArray<PPPersonModel *> *addressBookArray) {
            NSMutableArray *dataArr = [NSMutableArray array];
            for (int i = 0; i < addressBookArray.count; i++) {
                PPPersonModel *model = addressBookArray[i];
                if (model.mobileArray.count == 0) {
                    
                } else {
                    if ([[model.mobileArray[0] stringByReplacingOccurrencesOfString:@"-" withString:@""] length] >=12) {
                        
                    }else{
                        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:@{@"name":model.name, @"phone":[model.mobileArray[0] stringByReplacingOccurrencesOfString:@"-" withString:@""], @"link":@""}];
                        [dataArr addObject:dict];
                    }
                    NSLog(@"%@:%@", model.name,model.mobileArray[0]);
                }
            }
            NSString *jsonStr = [NSString stringWithFormat:@"[%@]",[dataArr componentsJoinedByString:@","]];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [self upUserMsg:dataArr];
            });
            
            
        } authorizationFailure:^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                            message:@"请在iPhone的“设置-隐私-通讯录”选项中，允许百途访问您的通讯录"
                                                           delegate:nil
                                                  cancelButtonTitle:@"知道了"
                                                  otherButtonTitles:nil];
            [alert show];
        }];
    });
    
    
}


- (void)upUserMsg:(NSArray *)array {
    NSString *time = [NSString stringWithFormat:@"%lld000",(long long int)[[NSDate date] timeIntervalSince1970]];
    
    AFHTTPSessionManager *manage = [AFHTTPSessionManager manager];
    manage.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manage.responseSerializer = [AFHTTPResponseSerializer serializer];//响应
    manage.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/plain",@"text/html",nil];
    [manage.requestSerializer setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    //    manage.securityPolicy.allowInvalidCertificates = YES;
    if ([[TheGlobalMethod getUserDefault:@"token"] length] > 0) {
        [manage.requestSerializer setValue:[TheGlobalMethod getUserDefault:@"token"] forHTTPHeaderField:@"token"];
    }
    
    NSString *sign = [NSString stringWithFormat:@"E10ADC3949BA59ABBE56E057F20F883EE10ADC3949BA59ABBE56E057F20F883E%@APP",time];
    
    NSString *stringUrl = [Http_IP stringByAppendingString:@"userPhoneList/saveUserPhoneList"];
    
    stringUrl = [stringUrl stringByAppendingString:[NSString stringWithFormat:@"?sign=%@&source=APP&timestamp=%@&appKey=E10ADC3949BA59ABBE56E057F20F883E", [CJMD5 md5HexDigest:sign], time]];
    
    
    [manage POST:stringUrl parameters:array progress:^(NSProgress * _Nonnull downloadProgress){
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
        NSLog(@"上传用户通讯录数据:%@", [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil]);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"----------------------------->%@",error);
        
    }];
}

- (NSString *)jsonStringWithObject:(id)jsonObject{
    // 将字典或者数组转化为JSON串
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonObject
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                 encoding:NSUTF8StringEncoding];
    
    if ([jsonString length] > 0 && error == nil){
        return jsonString;
    }else{
        return @"";
    }
}
-(void)setRightLab:(NSDictionary *)dict {
    if (![dict[@"userBasicMsg"] isEqual:[NSNull null]]) {
        MLBCertificationListCell * cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        cell.contentLab.text = stringEmpty(dict[@"userBasicMsg"][@"study"]);
        
        
        MLBCertificationListCell * cell1 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        cell1.contentLab.text = stringEmpty(dict[@"userBasicMsg"][@"marry"]);
        
        MLBCertificationListCell * cell2 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
        cell2.contentLab.text = [stringEmpty(dict[@"userBasicMsg"][@"firstContactName"]) stringByAppendingString:[@" " stringByAppendingString:stringEmpty(dict[@"userBasicMsg"][@"firstContactPhone"])]];
        
        MLBCertificationListCell * cell3 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
        cell3.contentLab.text = [stringEmpty(dict[@"userBasicMsg"][@"secondContactName"]) stringByAppendingString:[@" " stringByAppendingString:stringEmpty(dict[@"userBasicMsg"][@"secondContactPhone"])]];
        
        MLBCertificationListCell * cell4 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
        cell4.contentLab.text = [NSString stringWithFormat:@"%@%@%@",stringEmpty(dict[@"userBasicMsg"][@"address"][@"province"]),stringEmpty(dict[@"userBasicMsg"][@"address"][@"city"]),stringEmpty(dict[@"userBasicMsg"][@"address"][@"area"])];
        _provence = stringEmpty(dict[@"userBasicMsg"][@"address"][@"province"]);
        _city = stringEmpty(dict[@"userBasicMsg"][@"address"][@"city"]);
        _area = stringEmpty(dict[@"userBasicMsg"][@"address"][@"area"]);
    }
   
    
    MLBCertificationListCell * cell5 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0]];
    if ([dict[@"userAuth"][@"bankAuth"] integerValue] == 1) {
        cell5.contentLab.text = @"已认证";
    }else if ([dict[@"userAuth"][@"bankAuth"] integerValue] == 3) {
        cell5.contentLab.text = @"未认证";
    }else if ([dict[@"userAuth"][@"bankAuth"] integerValue] == 0){
        cell5.contentLab.text = @"正在审核";
    }else if ([dict[@"userAuth"][@"bankAuth"] integerValue] == 2){
        cell5.contentLab.text = @"审核失败";
    }
    
    MLBCertificationListCell * cell6 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:6 inSection:0]];
    if ([dict[@"userAuth"][@"phoneAuth"] integerValue] == 1) {
        cell6.contentLab.text = @"已认证";
    }else if ([dict[@"userAuth"][@"phoneAuth"] integerValue] == 3) {
        cell6.contentLab.text = @"未认证";
    }else if ([dict[@"userAuth"][@"phoneAuth"] integerValue] == 0){
        cell6.contentLab.text = @"正在审核";
    }else if ([dict[@"userAuth"][@"phoneAuth"] integerValue] == 2){
        cell6.contentLab.text = @"审核失败";
    }
    MLBCertificationListCell * cell7 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:7 inSection:0]];
    if ([dict[@"userAuth"][@"zhimaAuth"] integerValue] == 1) {
        cell7.contentLab.text = @"已认证";
    }else if ([dict[@"userAuth"][@"zhimaAuth"] integerValue] == 3) {
        cell7.contentLab.text = @"未认证";
    }else if ([dict[@"userAuth"][@"zhimaAuth"] integerValue] == 0){
        cell7.contentLab.text = @"正在审核";
    }else if ([dict[@"userAuth"][@"zhimaAuth"] integerValue] == 2){
        cell7.contentLab.text = @"审核失败";
    }
    
    MLBCertificationListCell * cell8 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    if ([dict[@"userAuth"][@"taobaoAuth"] integerValue] == 1) {
        cell8.contentLab.text = @"已认证";
    }else if ([dict[@"userAuth"][@"taobaoAuth"] integerValue] == 3) {
        cell8.contentLab.text = @"未认证";
    }else if ([dict[@"userAuth"][@"taobaoAuth"] integerValue] == 0){
        cell8.contentLab.text = @"正在审核";
    }else if ([dict[@"userAuth"][@"taobaoAuth"] integerValue] == 2){
        cell8.contentLab.text = @"审核失败";
    }
    
    MLBCertificationListCell * cell9 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1]];
    if ([dict[@"userAuth"][@"schoolInfo"] integerValue] == 1) {
        cell9.contentLab.text = @"已认证";
    }else if ([dict[@"userAuth"][@"schoolInfo"] integerValue] == 3) {
        cell9.contentLab.text = @"未认证";
    }else if ([dict[@"userAuth"][@"schoolInfo"] integerValue] == 0){
        cell9.contentLab.text = @"正在审核";
    }else if ([dict[@"userAuth"][@"schoolInfo"] integerValue] == 2){
        cell9.contentLab.text = @"审核失败";
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
