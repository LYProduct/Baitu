//
//  AgreementCell.h
//  RYJ
//
//  Created by wyp on 2017/7/18.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgreementCell : UITableViewCell

@property (nonatomic, strong) void(^returnClick)(NSInteger index);

@end
