//
//  ABtnCell.h
//  MageLive
//
//  Created by wyp on 2017/5/4.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABtnCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btn;

@end
