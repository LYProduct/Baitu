//
//  TextFieldButtonCell.h
//  意通
//
//  Created by wyp on 16/8/24.
//  Copyright © 2016年 MIFAN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextFieldButtonCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *TF;
@property (weak, nonatomic) IBOutlet UIButton *btn;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TF_R;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomView_L;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomView_R;

@end
