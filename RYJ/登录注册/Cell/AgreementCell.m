//
//  AgreementCell.m
//  RYJ
//
//  Created by wyp on 2017/7/18.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "AgreementCell.h"

#import "MessageDetailVC.h"

@interface AgreementCell () {
    UIViewController *theVC;
}

@end

@implementation AgreementCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc {
    theVC = vc;
    
}

- (IBAction)selectType:(UIButton *)sender {
    sender.selected = !sender.isSelected;
    self.returnClick(sender.isSelected);
}

- (IBAction)ServiceAgreement:(UIButton *)sender {
    [HttpUrl GET:@"agreement/selectOneByType" dict:@{@"type":@"3"} hud:theVC.view isShow:YES WithSuccessBlock:^(id data) {
        NSLog(@"%@", data);
        if (BB_isSuccess) {
            if (![data[@"data"] isEqual:[NSNull null]]) {
                MessageDetailVC *vc = [TheGlobalMethod setstoryboard:@"MessageDetailVC" controller:theVC];
                //    vc.detailStr = listArr[indexPath.row][@"content"];
                vc.titleStr = @"百途服务协议";
                NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[data[@"data"][@"content"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
                vc.attStr = attrStr;
                vc.detailStr = @"";
                [theVC.navigationController pushViewController:vc animated:YES];
            }else{
                [TheGlobalMethod xianShiAlertView:@"暂无协议" controller:theVC];

            }
        }else{
            [TheGlobalMethod xianShiAlertView:@"暂无协议" controller:theVC];

        }
        
        
    }];
    

//    NSURL *url = [NSURL fileURLWithPath:path];
//    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, Width, Height)];
//    [webView loadRequest:[NSURLRequest requestWithURL:url]];
//    [webView sizeToFit];
//    webView.scalesPageToFit = YES;
//    webView.delegate = self;
//    [self.view addSubview:webView];
    
    
//    NSString *path;
//    if (time(NULL) >= 1506034800) {
//        path = [[NSBundle mainBundle] pathForResource:@"用户注册协议" ofType:@"docx"];
//    } else {
//        path = [[NSBundle mainBundle] pathForResource:@"注册协议" ofType:@"docx"];
//    }
    
//    AXWebViewController *webVC = [[AXWebViewController alloc] initWithURL:[NSURL fileURLWithPath:path]];
//
//    [theVC.navigationController pushViewController:webVC animated:YES];
    
    //
    
    
//
//    self.webView = webView;
//    [HttpUrl NetErrorGET:@"agreement/selectOneByType" dict:@{@"type":@"3"} hud:theVC.view isShow:YES WithSuccessBlock:^(id data) {
//        
//        
//        
//        MessageDetailVC *vc = [TheGlobalMethod setstoryboard:@"MessageDetailVC" controller:theVC];
//        NSDictionary *textDic = data[@"data"];
//        if (![textDic[@"content"] isEqual:[NSNull null]]) {
//            NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[textDic[@"content"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
//            vc.attStr = attrStr;
//        } else {
//            vc.detailStr = @"-";
//        }
//        vc.titleStr = @"注册协议";
//        [theVC.navigationController pushViewController:vc animated:YES];
//    } WithFailBlock:^(id data) {
//        ShowAlert(@"请求失败,请允许网络访问重新查看", theVC);
//    }];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
