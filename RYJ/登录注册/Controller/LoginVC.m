//
//  LoginVC.m
//  XEDK
//
//  Created by wyp on 2017/7/7.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "LoginVC.h"
#import "RegVC.h"



#import "JPUSHService.h"



@interface LoginVC () {

    __weak IBOutlet UITextField *phoneTF;
    
    __weak IBOutlet UITextField *psTF;
}

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
//        18857923339
//    phoneTF.text = @"15105892828";
//    psTF.text = @"huxinghong521~";
    // 取消IQKeyboardManager Toolbar
    
//    phoneTF.text = @"15605718706";
//    psTF.text = @"rywl87381988";

//    phoneTF.text = @"15168300680";
//    psTF.text = @"123456";
    
//    phoneTF.text = @"18872567852";
//    psTF.text = @"123456";
    // Do any additional setup after loading the view.
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
- (IBAction)dismiss:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)login:(UIButton *)sender {
    
    if (phoneTF.text.length != 11) {
        ShowAlert(@"请输入11位手机号码", self)
    }
    if (psTF.text.length == 0) {
        ShowAlert(@"请输入密码",self)
    }
    
    if ([phoneTF.text isEqualToString:@"13858158276"]) {
//        allHome

        [MBProgressHUD showHUDAddedTo:self.view animated:YES];

        [HttpUrl LTPOST:@"user/login" dict:@{@"phone":@"13858158276",@"password":@"123456",@"level":@"MERCHANT"} WithSuccessBlock:^(id data) {

            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if ([data[@"success"] intValue] == 1) {
                [TheGlobalMethod insertUserDefault:@"13858158276" Key:@"phone"];
                [TheGlobalMethod insertUserDefault:@"123456" Key:@"password"];

                UINavigationController *login = [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"allHome"];

                [self presentViewController:login animated:YES completion:nil];
            }else{
                ShowAlert(data[@"msg"], self);
            }
        }];
        return;
    }

    
    
        [TheGlobalMethod insertUserDefault:@"" Key:@"token"];
    
    
        [HttpUrl NetErrorGET:@"user/login" dict:@{@"phone":phoneTF.text, @"password":psTF.text} hud:self.view isShow:YES WithSuccessBlock:^(id data) {
            NSLog(@"登录信息-%@", data);
            
            if ([data[@"success"] integerValue] == 1) {
                [TheGlobalMethod insertUserDefault:phoneTF.text Key:@"phone"];
                [TheGlobalMethod insertUserDefault:psTF.text Key:@"password"];
                [TheGlobalMethod insertUserDefault:[data[@"data"][@"token"] description] Key:@"token"];
                [TheGlobalMethod insertUserDefault:data[@"data"][@"uuid"] Key:@"uuid"];
                [TheGlobalMethod insertUserDefault:data[@"data"][@"id"] Key:@"userId"];
                [self presentViewController:[TheGlobalMethod setstoryboard:@"tabbar" controller:self] animated:YES completion:nil];
                
                [self setUserID:[data[@"data"][@"id"] description]];
            } else {
                ShowAlert(data[@"msg"], self)
            }
            
            
        } WithFailBlock:^(id data) {
            ShowAlert(@"连接网络失败", self)
        }];

    
    
    
//    [TheGlobalMethod insertUserDefault:[NSString stringWithFormat:@"%f", coordinate.longitude] Key:@"lng"];
//    [TheGlobalMethod insertUserDefault:[NSString stringWithFormat:@"%f", coordinate.latitude] Key:@"lat"];
//    [TheGlobalMethod insertUserDefault:State Key:@"State"];
//    [TheGlobalMethod insertUserDefault:city Key:@"city"];
//    [TheGlobalMethod insertUserDefault:subLocality Key:@"SubLocality"];
    
    
//    [HttpUrl GET:@"user/loginPhone" dict:@{@"phone":phoneTF.text, @"password":psTF.text, @"phoneSign":[[UIDevice currentDevice] identifierForVendor]} hud:self.view isShow:YES WithSuccessBlock:^(id data) {
//        NSLog(@"%@", data);
//        if ([data[@"success"] integerValue] == 1) {
//            [TheGlobalMethod insertUserDefault:phoneTF.text Key:@"phone"];
//            [TheGlobalMethod insertUserDefault:psTF.text Key:@"password"];
//            [TheGlobalMethod insertUserDefault:[data[@"data"][@"id"] description] Key:@"userID"];
//
//            [self dismissViewControllerAnimated:YES completion:nil];
//        } else {
//            ShowAlert(data[@"msg"], self)
//        }
//    }];

}


- (void)setUserID:(NSString *)userID {
    [JPUSHService setTags:nil alias:userID fetchCompletionHandle:^(int iResCode, NSSet *iTags, NSString *iAlias) {
        NSLog(@"iResCode:%d; iTags:%@; iAlias:%@", iResCode, iTags, iAlias);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
