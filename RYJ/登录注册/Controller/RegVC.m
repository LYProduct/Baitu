//
//  RegVC.m
//  XEDK
//
//  Created by wyp on 2017/7/7.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "RegVC.h"
#import "TextFieldButtonCell.h"
#import "ABtnCell.h"

#import "AgreementCell.h"

@interface RegVC ()<UITableViewDelegate, UITableViewDataSource> {
    
    __weak IBOutlet RootTableView *myTableView;
    
    NSTimer *timer;
    NSInteger AgreeIndex;
}

@end

@implementation RegVC

- (void)viewDidLoad {
    [super viewDidLoad];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    AgreeIndex = 0;
    // Do any additional setup after loading the view.
//    [self getXY];
    
}
-(void)getXY {
    [HttpUrl GET:@"agreement/selectOneByType" dict:@{@"type":@"3"} hud:self.view isShow:NO WithSuccessBlock:^(id data) {
        if (BB_isSuccess) {
            
        }
    }];
}
#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 2;
    } else if (section == 1) {
        return 2;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        
        TextFieldButtonCell loadingTableViewCell("TextFieldButtonCell",);
        cell.TF.keyboardType = UIKeyboardTypeNumberPad;
        if (indexPath.row == 0) {
            cell.TF.keyboardType = UIKeyboardTypeNumberPad;
            cell.TF.placeholder = @"请输入登录手机号码验证";
            
        } else if (indexPath.row == 1) {
            cell.bottomView_L.constant = 0;
            cell.bottomView_R.constant = 0;
            cell.TF_R.constant = 130;
            cell.btn.hidden = NO;
            [cell.btn addTarget:self action:@selector(getVerification) forControlEvents:(UIControlEventTouchUpInside)];
            cell.TF.placeholder = @"请输入验证码";
        }
        return cell;
    } else if (indexPath.section == 1) {
        TextFieldButtonCell loadingTableViewCell("TextFieldButtonCell",);;
        cell.TF.secureTextEntry = YES;
        if (indexPath.row == 0) {
            cell.TF.placeholder = @"请设置6位以上密码";
        } else {
            cell.TF.placeholder = @"请设置6位以上密码";
            cell.bottomView_L.constant = 0;
            cell.bottomView_R.constant = 0;
        }
        
        return cell;
    } else if (indexPath.section == 2) {
        AgreementCell loadingTableViewCell("AgreementCell",);
        [cell reloadCellForData:@{} vc:self];
        cell.returnClick = ^(NSInteger index) {
            NSLog(@"%ld", index);
            AgreeIndex = index;
        };
        return cell;
    }
    
    ABtnCell *cell = [TheGlobalMethod setCell:@"ABtnCell"];
    [cell.btn addTarget:self action:@selector(reg) forControlEvents:(UIControlEventTouchUpInside)];
    return cell;
    
}

- (void)reg {
    NSLog(@"注册");
    
    TextFieldButtonCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if (cell.TF.text.length != 11) {
        ShowAlert(@"请输入11位手机号", self)
    }
    TextFieldButtonCell *cell1 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    if (cell1.TF.text.length == 0) {
        ShowAlert(@"请输入验证码", self)
    }
    TextFieldButtonCell *cell2 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    if (cell2.TF.text.length < 6 || cell2.TF.text.length > 12) {
        ShowAlert(@"请输入密码", self)
    }
    TextFieldButtonCell *cell3 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1]];
    if (![cell2.TF.text isEqualToString:cell3.TF.text]) {
        ShowAlert(@"两次密码输入不同", self)
    }
    if (AgreeIndex == 0) {
        ShowAlert(@"请勾选同意百途服务协议后继续", self)
        return;
    }
    
    [HttpUrl GET:@"user/register" dict:@{@"phone":cell.TF.text, @"password":cell2.TF.text, @"code":cell1.TF.text} hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        NSLog(@"注册信息-%@", data);
        if ([data[@"success"] integerValue] == 1) {
            
            [HttpUrl NetErrorGET:@"user/login" dict:@{@"phone":cell.TF.text, @"password":cell2.TF.text} hud:self.view isShow:YES WithSuccessBlock:^(id data) {
                NSLog(@"登录信息-%@", data);
                if ([data[@"success"] integerValue] == 1) {
                    [TheGlobalMethod insertUserDefault:cell.TF.text Key:@"phone"];
                    [TheGlobalMethod insertUserDefault:cell2.TF.text Key:@"password"];
                    [TheGlobalMethod insertUserDefault:[data[@"data"][@"token"] description] Key:@"token"];
                    [TheGlobalMethod insertUserDefault:data[@"data"][@"uuid"] Key:@"uuid"];
                    [self presentViewController:[TheGlobalMethod setstoryboard:@"tabbar" controller:self] animated:YES completion:nil];
                } else {
                    ShowAlert(data[@"msg"], self)
                }
            } WithFailBlock:^(id data) {
                ShowAlert(@"连接网络失败", self)
            }];
            
        } else {
            ShowAlert(data[@"msg"], self)
        }
        
    }];
    
}

- (void)getVerification {
    NSLog(@"获取验证码");
    [self.view endEditing:YES];
    TextFieldButtonCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if (cell.TF.text.length != 11) {
        [TheGlobalMethod xianShiAlertView:@"请输入11位手机号" controller:self];
        return;
    }
    
    [HttpUrl GET:@"user/smsCode" dict:@{@"phone":cell.TF.text} hud:self.view isShow:NO WithSuccessBlock:^(id data) {
        if (BB_isSuccess) {
            TextFieldButtonCell *cell1 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
            cell1.btn.userInteractionEnabled = NO;
            timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(timer) userInfo:nil repeats:YES];
            [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
        }else{
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
    
   
}
//定时器验证码计时
static NSInteger timerNumber = 59;
- (void)timer {
    TextFieldButtonCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    
    [cell.btn setTitle:[NSString stringWithFormat:@"%lds重新获取", timerNumber] forState:(UIControlStateNormal)];
    [cell.btn setTitleColor:NAVCOLOR_C(153.0, 153.0, 153.0) forState:(UIControlStateNormal)];
    cell.btn.layer.borderColor = NAVCOLOR_C(153.0, 153.0, 153.0).CGColor;
    timerNumber--;
    if (timerNumber == 0) {
        [timer invalidate];
        timer = nil;
        [cell.btn setTitle:[NSString stringWithFormat:@"获取验证码"] forState:(UIControlStateNormal)];
        [cell.btn setTitleColor:appDefaultColor forState:(UIControlStateNormal)];
        cell.btn.layer.borderUIColor = appDefaultColor;
        cell.btn.backgroundColor = [UIColor clearColor];
        timerNumber = 59;
        cell.btn.userInteractionEnabled = YES;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2) {
        return 24;
    }
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    CGFloat height;
    if (section == 3) {
        height = 24;
    } else {
        height = 12;
    }
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, height)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 3) {
        return 24;
    }
    return 12;
}

- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
