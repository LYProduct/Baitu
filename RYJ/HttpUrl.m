//
//  HttpUrl.m
//  BS
//
//  Created by ike on 16/7/15.
//  Copyright © 2016年 mc. All rights reserved.
//
#import "HttpUrl.h"

#import "AppDelegate.h"

@implementation HttpUrl

+ (void)POST:(NSString *)httpUrl dict:(NSDictionary *)dict hud:(UIView *)hud isShow:(BOOL)isShow WithSuccessBlock:(void(^)(id data))successBlock {
    
    if (isShow) {
        [MBProgressHUD showHUDAddedTo:hud animated:YES];
    }
    
    NSString *time = [NSString stringWithFormat:@"%lld000",(long long int)[[NSDate date] timeIntervalSince1970]];
    
    AFHTTPSessionManager *manage = [AFHTTPSessionManager manager];
    manage.requestSerializer = [AFJSONRequestSerializer new];
    manage.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manage.responseSerializer = [AFHTTPResponseSerializer serializer];//响应
    manage.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/plain",@"text/html",nil];
    [manage.requestSerializer setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
//    manage.securityPolicy.allowInvalidCertificates = YES;
    if ([[TheGlobalMethod getUserDefault:@"token"] length] > 0) {
        [manage.requestSerializer setValue:[TheGlobalMethod getUserDefault:@"token"] forHTTPHeaderField:@"x-client-token"];
    }
    
    NSMutableDictionary *dataDic = [NSMutableDictionary dictionaryWithDictionary:dict];
    
    NSString *sign = [NSString stringWithFormat:@"E10ADC3949BA59ABBE56E057F20F883EE10ADC3949BA59ABBE56E057F20F883E%@APP",time];
//    [dataDic setObject:[CJMD5 md5HexDigest:sign] forKey:@"sign"];
//    [dataDic setObject:@"APP" forKey:@"source"];
//    [dataDic setObject:time forKey:@"timestamp"];
//    [dataDic setObject:@"E10ADC3949BA59ABBE56E057F20F883E" forKey:@"appKey"];
    
    NSString *stringUrl = [Http_IP stringByAppendingString:httpUrl];
    
    stringUrl = [stringUrl stringByAppendingString:[NSString stringWithFormat:@"?sign=%@&source=APP&timestamp=%@&appKey=E10ADC3949BA59ABBE56E057F20F883E", [CJMD5 md5HexDigest:sign], time]];
    NSLog(@"---url---%@---%@", stringUrl, dataDic);

    
    [manage POST:stringUrl parameters:dataDic progress:^(NSProgress * _Nonnull downloadProgress){
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        [MBProgressHUD hideHUDForView:hud animated:YES];
        successBlock([NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil]);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"----------------------------->%@",error);
        [MBProgressHUD hideHUDForView:hud animated:YES];
        [JXTAlertView showAlertViewWithTitle:@"提示" message:@"连接网络失败" cancelButtonTitle:nil buttonIndexBlock:^(NSInteger buttonIndex) {
        } otherButtonTitles:@[@"确定"]];
    }];
}


+ (void)NetErrorPOST:(NSString *)httpUrl dict:(NSDictionary *)dict hud:(UIView *)hud isShow:(BOOL)isShow WithSuccessBlock:(void(^)(id data))successBlock WithFailBlock:(void(^)(id data))FailBlock{
    
    if (isShow) {
        [MBProgressHUD showHUDAddedTo:hud animated:YES];
        //        hudview.activityIndicatorColor =
    }
    
    NSString *time = [NSString stringWithFormat:@"%lld000",(long long int)[[NSDate date] timeIntervalSince1970]];
    
    AFHTTPSessionManager *manage = [AFHTTPSessionManager manager];
    manage.requestSerializer = [AFJSONRequestSerializer new];
    manage.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manage.responseSerializer = [AFHTTPResponseSerializer serializer];//响应
    manage.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/plain",@"text/html",nil];
    [manage.requestSerializer setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    if ([[TheGlobalMethod getUserDefault:@"token"] length] > 0) {
        [manage.requestSerializer setValue:[TheGlobalMethod getUserDefault:@"token"] forHTTPHeaderField:@"x-client-token"];
    }
    
    NSMutableDictionary *dataDic = [NSMutableDictionary dictionaryWithDictionary:dict];
    
    NSString *sign = [NSString stringWithFormat:@"E10ADC3949BA59ABBE56E057F20F883EE10ADC3949BA59ABBE56E057F20F883E%@APP",time];
    [dataDic setObject:[CJMD5 md5HexDigest:sign] forKey:@"sign"];
    [dataDic setObject:@"APP" forKey:@"source"];
    [dataDic setObject:time forKey:@"timestamp"];
    [dataDic setObject:@"E10ADC3949BA59ABBE56E057F20F883E" forKey:@"appKey"];
    
    NSLog(@"---url---%@%@---%@", Http_IP,httpUrl, dict);
    NSString *stringUrl = [Http_IP stringByAppendingString:httpUrl];
    
    
    [manage POST:stringUrl parameters:dataDic progress:^(NSProgress * _Nonnull downloadProgress){
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        [MBProgressHUD hideHUDForView:hud animated:YES];
        successBlock(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"----------------------------->%@",error);
        [MBProgressHUD hideHUDForView:hud animated:YES];
        FailBlock(error);
    }];
}

+ (void)GET:(NSString *)httpUrl dict:(NSDictionary *)dict hud:(UIView *)hud isShow:(BOOL)isShow WithSuccessBlock:(void(^)(id data))successBlock {
    
    if (isShow) {
        [MBProgressHUD showHUDAddedTo:hud animated:YES];
        //        hudview.activityIndicatorColor =
    }
    NSString *time = [NSString stringWithFormat:@"%lld000",(long long int)[[NSDate date] timeIntervalSince1970]];
    
    AFHTTPSessionManager *manage = [AFHTTPSessionManager manager];
    manage.requestSerializer = [AFHTTPRequestSerializer serializer];
    manage.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/plain",@"text/html",nil];
    [manage.requestSerializer setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    if ([[TheGlobalMethod getUserDefault:@"token"] length] > 0) {
        [manage.requestSerializer setValue:[TheGlobalMethod getUserDefault:@"token"] forHTTPHeaderField:@"x-client-token"];
    }
    
    NSLog(@"---url---%@%@---%@", Http_IP,httpUrl, dict);
    NSString *stringUrl = [[Http_IP stringByAppendingString:httpUrl] stringByAppendingString:@"?"];
    NSString *sign = [NSString stringWithFormat:@"E10ADC3949BA59ABBE56E057F20F883EE10ADC3949BA59ABBE56E057F20F883E%@APP",time];
    
    for (int i = 0; i < [[dict allKeys]count]; i++) {
        stringUrl = [NSString stringWithFormat:@"%@%@=%@&",stringUrl,[dict allKeys][i],dict[[dict allKeys][i]]];
    }
    
    if ([[dict allKeys]count] > 0) {
        stringUrl = [stringUrl stringByAppendingString:[NSString stringWithFormat:@"sign=%@&source=APP&timestamp=%@&appKey=E10ADC3949BA59ABBE56E057F20F883E", [CJMD5 md5HexDigest:sign], time]];
    } else {
        stringUrl = [stringUrl stringByAppendingString:[NSString stringWithFormat:@"sign=%@&source=APP&timestamp=%@&appKey=E10ADC3949BA59ABBE56E057F20F883E", [CJMD5 md5HexDigest:sign], time]];
    }
    
    NSLog(@"GET--stringUrl--%@---sign:%@", stringUrl,[CJMD5 md5HexDigest:sign]);
    
    stringUrl = [stringUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [manage GET:stringUrl parameters:nil progress:^(NSProgress * _Nonnull downloadProgress){
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        [MBProgressHUD hideHUDForView:hud animated:YES];
        successBlock(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"----------------------------->%@",error);
        
        [MBProgressHUD hideHUDForView:hud animated:YES];
        [JXTAlertView showAlertViewWithTitle:@"提示" message:@"连接网络失败" cancelButtonTitle:nil buttonIndexBlock:^(NSInteger buttonIndex) {
        } otherButtonTitles:@[@"确定"]];
        
    }];
}

+ (void)NetErrorGET:(NSString *)httpUrl dict:(NSDictionary *)dict hud:(UIView *)hud isShow:(BOOL)isShow WithSuccessBlock:(void(^)(id data))successBlock WithFailBlock:(void(^)(id data))FailBlock{
    
    if (isShow) {
        [MBProgressHUD showHUDAddedTo:hud animated:YES];
    }
    NSString *time = [NSString stringWithFormat:@"%lld000",(long long int)[[NSDate date] timeIntervalSince1970]];

    AFHTTPSessionManager *manage = [AFHTTPSessionManager manager];
    manage.requestSerializer = [AFHTTPRequestSerializer serializer];
    manage.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/plain",@"text/html",nil];
    [manage.requestSerializer setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    if ([[TheGlobalMethod getUserDefault:@"token"] length] > 0) {
        [manage.requestSerializer setValue:[TheGlobalMethod getUserDefault:@"token"] forHTTPHeaderField:@"x-client-token"];
    }
    
    NSLog(@"---url---%@%@---%@", Http_IP,httpUrl, dict);
    NSString *stringUrl = [[Http_IP stringByAppendingString:httpUrl] stringByAppendingString:@"?"];
    NSString *sign = [NSString stringWithFormat:@"E10ADC3949BA59ABBE56E057F20F883EE10ADC3949BA59ABBE56E057F20F883E%@APP",time];
    
    for (int i = 0; i < [[dict allKeys]count]; i++) {
        stringUrl = [NSString stringWithFormat:@"%@%@=%@&",stringUrl,[dict allKeys][i],dict[[dict allKeys][i]]];
    }
    
    if ([[dict allKeys]count] > 0) {
        stringUrl = [stringUrl stringByAppendingString:[NSString stringWithFormat:@"sign=%@&source=APP&timestamp=%@&appKey=E10ADC3949BA59ABBE56E057F20F883E", [CJMD5 md5HexDigest:sign], time]];
    } else {
        stringUrl = [stringUrl stringByAppendingString:[NSString stringWithFormat:@"sign=%@&source=APP&timestamp=%@&appKey=E10ADC3949BA59ABBE56E057F20F883E", [CJMD5 md5HexDigest:sign], time]];
    }
    NSLog(@"GET--stringUrl--%@---sign:%@", stringUrl,sign);
    stringUrl = [stringUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    [manage GET:stringUrl parameters:nil progress:^(NSProgress * _Nonnull downloadProgress){
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        [MBProgressHUD hideHUDForView:hud animated:YES];
        successBlock(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"----------------------------->%@",error);
        
        [MBProgressHUD hideHUDForView:hud animated:YES];
        FailBlock(error);
        
    }];
}

#pragma mark 上传图片
+ (void)upLoadImage:(UIImage *)image hud:(UIView *)hud isShow:(BOOL)isShow WithSuccessBlock:(void(^)(id data))successBlock{
    if (isShow) {
        [MBProgressHUD showHUDAddedTo:hud animated:YES];
    }
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    //接收类型不一致请替换一致text/html或别的
    manager.requestSerializer = [AFJSONRequestSerializer new];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];//请求
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];//响应
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/plain",@"text/html",nil];
    [manager.requestSerializer setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    if ([[TheGlobalMethod getUserDefault:@"token"] length] > 0) {
        [manager.requestSerializer setValue:[TheGlobalMethod getUserDefault:@"token"] forHTTPHeaderField:@"token"];
    }
    
    NSString *httpStr = [Http_IP stringByAppendingString:@"attachment/upload"];
    [manager POST:httpStr parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> _Nonnull formData) {
        //对图片大小进行压缩--
        NSData *data=UIImageJPEGRepresentation(image, 1.0);
        if (data.length>100*1024) {
            if (data.length>1024*1024) {//1M以及以上
                data=UIImageJPEGRepresentation(image, 0.1);
            }else if (data.length>512*1024) {//0.5M-1M
                data=UIImageJPEGRepresentation(image, 0.5);
            }else if (data.length>200*1024) {//0.25M-0.5M
                data=UIImageJPEGRepresentation(image, 0.9);
            }
        }
        
//        NSLog(@"图片data:%@", data);
        //上传的参数(上传图片，以文件流的格式)
        [formData appendPartWithFileData:data
                                    name:@"files"
                                fileName:@"123.jpg"
                                mimeType:@"image/jpeg"];
        
    } progress:^(NSProgress *_Nonnull uploadProgress) {
        //打印下上传进度
        NSLog(@"%@", uploadProgress);
    } success:^(NSURLSessionDataTask *_Nonnull task, id _Nullable responseObject) {
        
        NSLog(@"上传结果:%@", responseObject);
        //上传成功
        [MBProgressHUD hideHUDForView:hud animated:YES];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"打印上传结果:%@", dict);
        successBlock(dict);
    } failure:^(NSURLSessionDataTask *_Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
        //上传失败
        [JXTAlertView showAlertViewWithTitle:@"提示" message:@"当前无网络" cancelButtonTitle:nil buttonIndexBlock:^(NSInteger buttonIndex) {
        } otherButtonTitles:@[@"确定"]];
        [MBProgressHUD hideHUDForView:hud animated:YES];
    }];
}

#pragma mark 临洮
//传值
+(void)POST:(NSString *)httpUrl dict:(NSDictionary *)dict sign:(NSString *)sign WithSuccessBlock:(void(^)(id data))successBlock{// WithFailBlock:(void (^)(NSError *))failBlock
    NSString *time = [NSString stringWithFormat:@"%lld000",(long long int)[[NSDate date] timeIntervalSince1970]];
    NSString *token = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"data"][@"data"]];
    
    /**
     sign appkey +secretKey + 时间戳
     */
    
    NSMutableDictionary *dataDic = [NSMutableDictionary dictionaryWithDictionary:dict];
    //    NSArray *array = [NSArray arrayWithArray:[dataDic allKeys]];
    //    for (int i =0; i < [dataDic  allKeys].count; i ++) {
    //        sign = [NSString stringWithFormat:@"%@%@",sign,dict[array[i]]];
    //    }
    
    if (dict != nil){
        
        
    }else{
        [dataDic setObject:@"97cffd6ecb062bbae93beddf67a32b4d" forKey:@"appkey"];
        [dataDic setObject:time forKey:@"timestamp"];
        [dataDic setObject:token forKey:@"token"];
        NSString *signStr = [NSString stringWithFormat:@"97cffd6ecb062bbae93beddf67a32b4d3f690ca4307652c3396430a548e1fdb4%@APP%@%@",time,token,sign];
        [dataDic setObject:[CJMD5 md5HexDigest:signStr] forKey:@"sign"];
        
    }
    
    
    
    AFHTTPSessionManager *manage = [AFHTTPSessionManager manager];
    manage.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/plain",@"text/html",nil];
    NSString *stringUrl = [NSString stringWithFormat:@"%@%@",HTTP_IP,httpUrl];
    NSLog(@"httpUrl========>%@-----<<<<%@>>>>",stringUrl, dataDic);
    
    [manage POST:stringUrl parameters:dataDic progress:^(NSProgress * _Nonnull downloadProgress){
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        NSString *string = [NSString stringWithFormat:@"%@",responseObject[@"msg"]];
        if ([responseObject[@"code"] rangeOfString:@"FAIL"].location != NSNotFound) {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"FAIL(201, '失败'),%@",string]}];
        }else if ([responseObject[@"code"] rangeOfString:@"PARAMETER_INVALID"].location != NSNotFound){
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"(204, '参数不合法'),%@",string]}];
        }else if ([responseObject[@"code"] rangeOfString:@"EXISTING"].location != NSNotFound){
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"(205, '参数不合法'),%@",string]}];
        }else if ([responseObject[@"code"] rangeOfString:@"UNREGISTERED"].location != NSNotFound){
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"(206, '未注册'),%@",string]}];
        }else if ([responseObject[@"code"] rangeOfString:@"TOKEN_NOT_FIND"].location != NSNotFound){
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"(207, '未找到'),%@",string]}];
        }else if ([responseObject[@"code"] rangeOfString:@"OVERTIME"].location != NSNotFound){
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"(208, '已超时或过期'),%@",string]}];
        }else if ([responseObject[@"code"] rangeOfString:@"EMPTY"].location != NSNotFound){
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"(209, '空数据'),%@",string]}];
        }else if ([responseObject[@"code"] rangeOfString:@"PARAMETER_ERROR"].location != NSNotFound){
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"(210, '参数填写错误'),%@",string]}];
        }else if ([responseObject[@"code"] rangeOfString:@"CONFLICT"].location != NSNotFound){
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"(211, '冲突'),%@",string]}];
        }else if ([responseObject[@"code"] rangeOfString:@"BALANCE_NOT_ENOUGH"].location != NSNotFound){
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"(212, '余额不足'),%@",string]}];
        }else if ([responseObject[@"code"] rangeOfString:@"NOT_AUTHORIZATION"].location != NSNotFound){
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"(213, '没有权限'),%@",string]}];
        }else{
            successBlock(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"----------------------------->%@",error);
        if ([[NetworkStatus networkingStatesFromStatebar] rangeOfString:@"没有"].location == NSNotFound){
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"服务器错误!"]}];
        }
    }];
}

//临洮
+(void)LTPOST:(NSString *)httpUrl dict:(NSDictionary *)dict WithSuccessBlock:(void(^)(id data))successBlock{
    
    [self dengLu];
    
    NSString *time = [NSString stringWithFormat:@"%lld000",(long long int)[[NSDate date] timeIntervalSince1970]];
    NSString *token = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"data"][@"data"]];
    NSMutableDictionary *dataDic = [NSMutableDictionary dictionaryWithDictionary:dict];
    [dataDic setObject:@"97cffd6ecb062bbae93beddf67a32b4d" forKey:@"appkey"];
    [dataDic setObject:time forKey:@"timestamp"];
    if ([token rangeOfString:@"null"].location == NSNotFound) {
        [dataDic setObject:token forKey:@"token"];
    }
    
    NSString *sign = @"";
    for (int i = 0; i < [[dataDic allKeys]count]; i++) {
        sign = [NSString stringWithFormat:@"%@%@",sign,dataDic[[dataDic allKeys][i]]];
    }
    [dataDic setObject:[CJMD5 md5HexDigest:sign] forKey:@"sign"];
    AFHTTPSessionManager *manage = [AFHTTPSessionManager manager];
    manage.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/plain",@"text/html",nil];
    NSString *stringUrl = [NSString stringWithFormat:@"%@%@",HTTP_IP,httpUrl];
    NSLog(@"httpUrl========>%@-----<<<<%@>>>>",stringUrl, dataDic);
    [manage POST:stringUrl parameters:dataDic progress:^(NSProgress * _Nonnull downloadProgress){
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        NSString *string = [NSString stringWithFormat:@"%@",responseObject[@"msg"]];
        if ([responseObject[@"code"] rangeOfString:@"FAIL"].location != NSNotFound) {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"FAIL(201, '失败'),%@",string]}];
        }else if ([responseObject[@"code"] rangeOfString:@"PARAMETER_INVALID"].location != NSNotFound){
            
            successBlock(responseObject);
            //            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"(204, '参数不合法'),%@",string]}];
        }else if ([responseObject[@"code"] rangeOfString:@"EXISTING"].location != NSNotFound){
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"(205, '参数不合法'),%@",string]}];
        }else if ([responseObject[@"code"] rangeOfString:@"UNREGISTERED"].location != NSNotFound){
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"(206, '未注册'),%@",string]}];
        }else if ([responseObject[@"code"] rangeOfString:@"TOKEN_NOT_FIND"].location != NSNotFound){
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"(207, '未找到'),%@",string]}];
        }else if ([responseObject[@"code"] rangeOfString:@"OVERTIME"].location != NSNotFound){
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"(208, '已超时或过期'),%@",string]}];
        }else if ([responseObject[@"code"] rangeOfString:@"EMPTY"].location != NSNotFound){
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"(209, '空数据'),%@",string]}];
        }else if ([responseObject[@"code"] rangeOfString:@"PARAMETER_ERROR"].location != NSNotFound){
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"(210, '参数填写错误'),%@",string]}];
        }else if ([responseObject[@"code"] rangeOfString:@"CONFLICT"].location != NSNotFound){
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"(211, '冲突'),%@",string]}];
        }else if ([responseObject[@"code"] rangeOfString:@"BALANCE_NOT_ENOUGH"].location != NSNotFound){
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"(212, '余额不足'),%@",string]}];
        }else if ([responseObject[@"code"] rangeOfString:@"NOT_AUTHORIZATION"].location != NSNotFound){
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"(213, '没有权限'),%@",string]}];
        }else{
            NSLog(@"%@---------->%@",stringUrl,responseObject);
            successBlock(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"----------------------------->%@",error);
        if ([[NetworkStatus networkingStatesFromStatebar] rangeOfString:@"没有"].location == NSNotFound){
            [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"服务器错误!"]}];
        }
    }];
}

+ (void)dengLu {
    if ([[TheGlobalMethod getUserDefault:@"phone"] length] > 0 && [[TheGlobalMethod getUserDefault:@"password"] length] >0) {
        NSString *time = [NSString stringWithFormat:@"%lld000",(long long int)[[NSDate date] timeIntervalSince1970]];
        NSString *token = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"data"][@"data"]];
        NSMutableDictionary *dataDic = [NSMutableDictionary dictionaryWithDictionary:@{@"phone":[TheGlobalMethod getUserDefault:@"phone"], @"password":[TheGlobalMethod getUserDefault:@"password"], @"level":@"SERVICE_CENTER"}];
        
        [dataDic setObject:@"97cffd6ecb062bbae93beddf67a32b4d" forKey:@"appkey"];
        [dataDic setObject:time forKey:@"timestamp"];
        if ([token rangeOfString:@"null"].location == NSNotFound) {
            [dataDic setObject:token forKey:@"token"];
        }
        
        NSString *sign = @"";
        for (int i = 0; i < [[dataDic allKeys]count]; i++) {
            sign = [NSString stringWithFormat:@"%@%@",sign,dataDic[[dataDic allKeys][i]]];
        }
        [dataDic setObject:[CJMD5 md5HexDigest:sign] forKey:@"sign"];
        AFHTTPSessionManager *manage = [AFHTTPSessionManager manager];
        manage.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/plain",@"text/html",nil];
        NSString *stringUrl = [NSString stringWithFormat:@"%@user/login",HTTP_IP];
        
        
        NSLog(@"httpUrl========>%@-----<<<<%@>>>>",stringUrl, dataDic);
        [manage POST:stringUrl parameters:dataDic progress:^(NSProgress * _Nonnull downloadProgress){
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
            
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"----------------------------->%@",error);
            if ([[NetworkStatus networkingStatesFromStatebar] rangeOfString:@"没有"].location == NSNotFound){
                [[NSNotificationCenter defaultCenter]postNotificationName:@"AlerView" object:nil userInfo:@{@"text":[NSString stringWithFormat:@"服务器错误!"]}];
            }
        }];
    }
}


@end
