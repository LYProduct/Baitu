//
//  Methods.m
//  MageLive
//
//  Created by wyp on 2017/5/17.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "Methods.h"

#import "ComprehensiveView.h"

@implementation Methods


#pragma mark label赋值
+ (void)dataInText:(id)string InLabel:(UILabel *)label defaultStr:(NSString *)defaultStr{
    if (![string isEqual:[NSNull null]]) {
        label.text = [string description];
        if ([[string description] length] == 0) {
            label.text = defaultStr;
        }
    } else {
        label.text = defaultStr;
    }
}

#pragma mark label赋值
+ (void)dataInTFText:(id)string InTF:(UITextField *)textField defaultStr:(NSString *)defaultStr{
    if (![string isEqual:[NSNull null]]) {
        textField.text = [string description];
        if ([[string description] length] == 0) {
            textField.text = defaultStr;
        }
    } else {
        textField.text = defaultStr;
    }
}

#pragma mark imageView赋值
+ (void)dataInImg:(id)string InImg:(UIImageView *)img placeholderImage:(NSString *)imgName{
    if (![string isEqual:[NSNull null]]) {
        [img sd_setImageWithURL:[NSURL URLWithString:[string description]] placeholderImage:[UIImage imageNamed:imgName]];
    } else {
        img.image = [UIImage imageNamed:imgName];
    }
}

#pragma mark AttributedString赋值(颜色)
+ (void)dataInAttributedString:(NSString *)beforeStr afterStr:(NSString *)afterStr range:(NSRange)range label:(UILabel *)label{
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:[beforeStr  stringByAppendingString:afterStr]];
    [attStr addAttribute:NSForegroundColorAttributeName value:appDefaultColor range:range];
    label.attributedText = attStr;
}

#pragma mark 跳web
+ (void)pushUrl:(NSString *)urlString seleVC:(UIViewController *)vc {
    AXWebViewController *webVC = [[AXWebViewController alloc] initWithAddress:urlString];
    webVC.showsToolBar = NO;
    
    webVC.navigationController.navigationBar.translucent = NO;
    vc.navigationController.navigationBar.tintColor = appText333333Color;
    //                        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.996f green:0.867f blue:0.522f alpha:1.00f];
    webVC.navigationType = AXWebViewControllerNavigationToolItem;
    webVC.hidesBottomBarWhenPushed = YES;
    [vc.navigationController pushViewController:webVC animated:YES];
}

#pragma mark 展示费用框
+ (void)showComprehensiveViewLeftArr:(NSArray *)leftArr rightArr:(NSArray *)rightArr bottomStatus:(id)bottomStatus selfVC:(UIViewController *)theVC{
    ComprehensiveView *comprehensiveView = [TheGlobalMethod setCell:@"ComprehensiveView"];
    comprehensiveView.frame = CGRectMake(0, 0, WIDTH_K, HEIGHT_K);
    [comprehensiveView reloadCellForData:@{@"L":leftArr, @"R":rightArr} vc:theVC];
    comprehensiveView.titleLabel.text = @"支持银行";
    if (bottomStatus != nil) {
        comprehensiveView.bottomStatus = bottomStatus;
    }
    [[UIApplication sharedApplication].keyWindow addSubview:comprehensiveView];
}

+ (void)share:(UIViewController *)vc descr:(NSString*)descr url:(NSString *)url platformType:(UMSocialPlatformType)platformType{
    //显示分享面板
//    [UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
//        // 根据获取的platformType确定所选平台进行下一步操作
//        [self shareWebPageToPlatformType:platformType vc:vc descr:descr url:url];
//    }];
    [self shareWebPageToPlatformType:platformType vc:vc descr:descr url:url];
}

+ (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType vc:(UIViewController *)vc descr:(NSString *)descr url:(NSString *)url{
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    
    //创建网页内容对象
    //    NSString* thumbURL =  @"https://mobile.umeng.com/images/pic/home/social/img-1.png";
    UMShareWebpageObject *shareObject;
    
    if (platformType != 0) {
        shareObject = [UMShareWebpageObject shareObjectWithTitle:@"百途" descr:descr thumImage:[UIImage imageNamed:@"logo"]];
    } else {
        shareObject = [UMShareWebpageObject shareObjectWithTitle:[NSString stringWithFormat:@"邀请您注册百途 http://app.hzrywl.com/share/?type=iOS&uuid=%@", [TheGlobalMethod getUserDefault:@"uuid"]] descr:[NSString stringWithFormat:@"邀请您注册百途 http://app.hzrywl.com/share/?type=iOS&uuid=%@", [TheGlobalMethod getUserDefault:@"uuid"]] thumImage:[UIImage imageNamed:@"logo"]];
    }
    //设置网页地址
    NSString *new_url=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    shareObject.webpageUrl = new_url;
    
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:vc completion:^(id data, NSError *error) {
        if (error) {
            UMSocialLogInfo(@"************Share fail with error %@*********",error);
        }else{
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                
            }else{
                UMSocialLogInfo(@"response data is %@",data);
            }
        }
        NSLog(@"fenxiang:%@", error);
        //        [self alertWithError:error];
    }];
}



/**
 首页弹出支付方式

 @param leftArr 左边文字
 @param rightArr 右边内容
 @param bottomStatus 底部状态
 @param theVC vc
 @param titleText 标题
 @param titleColor 标题颜色
 */
+ (void)showAlertForPayLeftArr:(NSArray *)leftArr rightArr:(NSArray *)rightArr bottomStatus:(id)bottomStatus selfVC:(UIViewController *)theVC titleLabelText:(NSString *)titleText titleLabelTextColor:(UIColor *)titleColor alicode:(NSString * )alicode wechat:(NSString *)wechat{
    ComprehensiveView *comprehensiveView = [TheGlobalMethod setCell:@"ComprehensiveView"];
    comprehensiveView.frame = CGRectMake(0, 0, WIDTH_K, HEIGHT_K);
    [comprehensiveView reloadCellForData:@{@"L":leftArr, @"R":rightArr} vc:theVC];
    comprehensiveView.alipayCode = alicode;
    comprehensiveView.wechatCode = wechat;
    comprehensiveView.titleLabel.text = titleText;
    comprehensiveView.titleLabel.textColor = titleColor;
    if (bottomStatus != nil) {
        comprehensiveView.bottomStatus = bottomStatus;
    }
    [[UIApplication sharedApplication].keyWindow addSubview:comprehensiveView];
}

@end
