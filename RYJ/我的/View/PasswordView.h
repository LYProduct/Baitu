//
//  PasswordView.h
//  RYJ
//
//  Created by wyp on 2017/7/25.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PasswordView : UITableViewCell

@property (nonatomic, strong) void(^returnString)(NSString *string);

@end
