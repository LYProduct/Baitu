//
//  PasswordView.m
//  RYJ
//
//  Created by wyp on 2017/7/25.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "PasswordView.h"

#import "NoPasteTF.h"

@interface PasswordView ()<UITextFieldDelegate> {
    
    __weak IBOutlet NoPasteTF *TF;
    NSString *tempStr;
}

@end

@implementation PasswordView



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    if (!tempStr) {
        tempStr = string;
    } else {
        if (tempStr.length < 6) {
            tempStr = [NSString stringWithFormat:@"%@%@",tempStr,string];
        }
    }
    
    if ([string isEqualToString:@""]) {
        if (tempStr.length > 0) {   //  删除最后一个字符串
            NSString *lastStr = [tempStr substringToIndex:[tempStr length] - 1];
            tempStr = lastStr;
        }
        if (self.returnString) {
            self.returnString(tempStr);
        }
        [self setLabelText:tempStr.length];
    } else {
        if (tempStr.length == 6) {
            if (self.returnString) {
                self.returnString(tempStr);
            }
            
        }
        [self setLabelText:tempStr.length];
    }
    NSLog(@"text:%@, string:%@", textField.text, string);
    return YES;
}

- (void)setLabelText:(NSInteger)num {
    for (int i = 0; i < num; i++) {
        [(UILabel *)[self viewWithTag:101+i] setText:@"●"];
    }
    for (int i = num; i < 6; i++) {
        [(UILabel *)[self viewWithTag:101+i] setText:@""];
    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    TF.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
