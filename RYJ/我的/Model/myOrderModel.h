//
//  myOrderModel.h
//  RYJ
//
//  Created by wyp on 2018/1/15.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface myOrderModel : NSObject
/** 评价(字典 */
@property(nonatomic,strong) NSDictionary *evaluation;
/** 订单编号 */
@property(nonatomic,strong) NSString *orderNumber;
/** 订单编号 */
@property(nonatomic,strong) NSString *ID;
/** 状态 */
@property(nonatomic,strong) NSString *status;

@end
