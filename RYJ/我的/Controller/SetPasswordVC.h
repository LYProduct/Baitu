//
//  SetPasswordVC.h
//  RYJ
//
//  Created by wyp on 2017/7/25.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetPasswordVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *topLabel;

@property (weak, nonatomic) IBOutlet UIButton *forgetBtn;

@property (nonatomic, strong) NSString *password;


@property (nonatomic, strong) NSString *isPS;//验证旧密码

@end
