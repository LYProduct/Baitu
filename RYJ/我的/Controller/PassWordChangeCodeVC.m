//
//  PassWordChangeCodeVC.m
//  RYJ
//
//  Created by wyp on 2017/8/16.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "PassWordChangeCodeVC.h"

#import "JQUnitField.h"

#import "SetPasswordVC.h"

@interface PassWordChangeCodeVC () {
    
    __weak IBOutlet UITextField *phone;
    __weak IBOutlet UIButton *getBtn;
    __weak IBOutlet JQUnitField *TF;
    
    NSTimer *timer;
    
    NSString *verificationStr;
}

@end

@implementation PassWordChangeCodeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    phone.text = [[TheGlobalMethod getUserDefault:@"phone"] stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
    

    // Do any additional setup after loading the view.
}

- (IBAction)up:(UIButton *)sender {
    NSLog(@"%@", TF.text);
    if ([TF.text isEqualToString:verificationStr]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                SetPasswordVC *vc = [TheGlobalMethod setstoryboard:@"SetPasswordVC" controller:self];
                [self.navigationController pushViewController:vc animated:YES];
            });
    } else {
        ShowAlert(@"验证码有误，请重新输入", self)
    }
}

- (IBAction)getVerification:(UIButton *)sender {
    
    [self getVerification];
}

- (void)getVerification {
    NSLog(@"获取验证码");
    [self.view endEditing:YES];
    
    
    [HttpUrl GET:@"user/getPhoneCode" dict:@{@"phone":[TheGlobalMethod getUserDefault:@"phone"]} hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        NSLog(@"获取验证码数据:%@",  data);
        verificationStr = [data[@"data"] description];
    }];
    
    getBtn.userInteractionEnabled = NO;
    timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(timer) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}
//定时器验证码计时
static NSInteger timerNumber = 59;
- (void)timer {
    
    [getBtn setTitle:[NSString stringWithFormat:@"%lds重新获取", timerNumber] forState:(UIControlStateNormal)];
    [getBtn setTitleColor:NAVCOLOR_C(153.0, 153.0, 153.0) forState:(UIControlStateNormal)];
    getBtn.layer.borderColor = NAVCOLOR_C(153.0, 153.0, 153.0).CGColor;
    timerNumber--;
    if (timerNumber == 0) {
        [timer invalidate];
        timer = nil;
        [getBtn setTitle:[NSString stringWithFormat:@"获取验证码"] forState:(UIControlStateNormal)];
        [getBtn setTitleColor:appDefaultColor forState:(UIControlStateNormal)];
        getBtn.layer.borderUIColor = appDefaultColor;
        getBtn.backgroundColor = [UIColor clearColor];
        timerNumber = 59;
        getBtn.userInteractionEnabled = YES;
    }
    
}

- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
