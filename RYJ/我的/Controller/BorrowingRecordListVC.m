//
//  BorrowingRecordListVC.m
//  RYJ
//
//  Created by wyp on 2017/7/24.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "BorrowingRecordListVC.h"

#import "BorrowListCell.h"

#import "BorrowDetailVC.h"

@interface BorrowingRecordListVC ()<UITableViewDelegate, UITableViewDataSource> {

    __weak IBOutlet RootTableView *myTableView;
    NSInteger page;
    NSMutableArray *dataArr;
}

@end

@implementation BorrowingRecordListVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    page = 1;
    dataArr = [NSMutableArray array];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self getNetWorking];
    });
    // Do any additional setup after loading the view.
    myTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, 12)];
    myTableView.tableHeaderView.backgroundColor = [UIColor clearColor];
    
    myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        page = 1;
        [dataArr removeAllObjects];
        [self getNetWorking];
    }];
    myTableView.mj_footer = [MJRefreshBackStateFooter footerWithRefreshingBlock:^{
        page++;
        [self getNetWorking];
    }];
}

- (void)getNetWorking {
    [HttpUrl GET:@"loanOrder/selectLoanList" dict:@{@"current":@(page)} hud:self.view isShow:NO WithSuccessBlock:^(id data) {
        NSLog(@"借款列表%@", data);
        if ([data[@"success"] integerValue] == 1) {
            [myTableView.mj_header endRefreshing];
            [myTableView.mj_footer endRefreshing];
            NSArray *arr = data[@"data"][@"records"];
            if ([arr count] == 0) {
                [myTableView.mj_footer endRefreshingWithNoMoreData];
            }
            [dataArr addObjectsFromArray:data[@"data"][@"records"]];
            [myTableView reloadData];
            
        } else {
            [TheGlobalMethod popAlertView:@"加载数据失败!" controller:self];
        }
    }];
}

#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return dataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BorrowListCell loadingTableViewCell("BorrowListCell", )
    [cell reloadCellForData:dataArr[indexPath.section] vc:self];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([dataArr[indexPath.section][@"id"] isEqual:[NSNull null]]) {
        ShowAlert(@"此订单查询异常,请刷新后重试", self);
    } else {
    BorrowDetailVC *vc = [TheGlobalMethod setstoryboard:@"BorrowDetailVC" controller:self];
//    vc.yuqi = [NSString stringWithFormat:@"%ld", (long)indexPath.section];
    vc.orderId = [dataArr[indexPath.section][@"id"] description];
    [self.navigationController pushViewController:vc animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 125;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, 12)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 12;
}

- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
