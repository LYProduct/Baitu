//
//  UserFeedbackVC.m
//  RYJ
//
//  Created by wyp on 2017/7/26.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "UserFeedbackVC.h"

#import "FeedBackSelectTypeCell.h"

#import "TextViewCell.h"
#import "SelectImgCell.h"

#import "TextFieldButtonCell.h"

#import "ABtnCell.h"

@interface UserFeedbackVC ()<UITableViewDelegate, UITableViewDataSource> {

    __weak IBOutlet RootTableView *myTableView;
    
    NSArray *selectImgArr;
    NSString *selectType;
}

@end

@implementation UserFeedbackVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    myTableView.delegate = self;
    myTableView.dataSource = self;
    
    selectType = @"1";
    // Do any additional setup after loading the view.
}

#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else if (section == 1) {
        return 2;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        FeedBackSelectTypeCell loadingTableViewCell("FeedBackSelectTypeCell", [cell reloadCellForData:@{} vc:self]);
        cell.clickIndex = ^(NSInteger index) {
            NSArray *arr = @[@"1", @"2", @"3", @"4", @"6"];
            NSLog(@"%ld", index);
            selectType = arr[index];
        };
        return cell;
    } else if (indexPath.section == 1) {
        
        if (indexPath.row == 0) {
            TextViewCell loadingTableViewCell("TextViewCell", [cell reloadCellForData:@{} vc:self])
            cell.returnTextViewText = ^(NSString *string) {
                
            };
            cell.myTextView.placeholder = @"我们接受赞美,也接受批评,我们为你设计,也为自己设计。";
            return cell;
        } else if (indexPath.row == 1) {
            SelectImgCell loadingTableViewCell("SelectImgCell", [cell reloadCellForData:@{} vc:self])
            cell.returnImgArr = ^(NSArray *imgArr) {
                selectImgArr = imgArr;
                [myTableView reloadData];
            };
            return cell;
        }
    } else if (indexPath.section == 2) {
        TextFieldButtonCell loadingTableViewCell("TextFieldButtonCell", )
        cell.bottomView_L.constant = 0;
        cell.bottomView_R.constant = 0;
        cell.TF.placeholder = @"手机号(选填,方便我们给您答复)";
        cell.TF.keyboardType = UIKeyboardTypeNumberPad;
        return cell;
    }
    ABtnCell *cell = [TheGlobalMethod setCell:@"ABtnCell"];
    [cell.btn addTarget:self action:@selector(up) forControlEvents:(UIControlEventTouchUpInside)];
    [cell.btn setTitle:@"提交" forState:(UIControlStateNormal)];
    return cell;
}

- (void)up {
    TextViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    if (cell.myTextView.text.length == 0) {
        ShowAlert(@"请输入反馈内容", self);
        return;
    }
    
    if (selectImgArr.count > 0) {
        [self upImage];
    } else {
        TextFieldButtonCell *cell1 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
        if (cell1.TF.text.length > 0 && cell1.TF.text.length != 11) {
            ShowAlert(@"请输入11位手机号码", self);
            return;
        }
        
        NSMutableDictionary *upDic = [NSMutableDictionary dictionaryWithDictionary:@{@"type":selectType, @"content":cell.myTextView.text, @"phone":cell1.TF.text, @"imgUrl":@""}];
        if (cell1.TF.text.length > 0) {
            [upDic setObject:cell1.TF.text forKey:@"phone"];
        }
        [HttpUrl POST:@"appFeedback/add" dict:upDic hud:self.view isShow:YES WithSuccessBlock:^(id data) {
            if ([data[@"success"] integerValue] == 1) {
                [TheGlobalMethod popAlertView:@"提交成功!" controller:self];
            } else {
                [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
            }
        }];
    }
}


- (void)upImage {
    NSMutableArray *urlArr = [NSMutableArray array];
    NSLog(@"selectImgArr Count: %ld", (unsigned long)selectImgArr.count);
    for (int i = 0; i < selectImgArr.count; i++) {
        [HttpUrl upLoadImage:selectImgArr[i] hud:self.view isShow:YES WithSuccessBlock:^(id data) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            NSLog(@"上传图片数据:%@", data);
            if ([data[@"success"] integerValue] == 1) {
                [urlArr addObject:data[@"data"][0]];
                if (urlArr.count == selectImgArr.count) {
                    [self upFeedBack:[NSArray arrayWithArray:urlArr]];
                }
            } else {
                [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
            }
        }];
    }
}

- (void)upFeedBack:(NSArray *)array {
    TextViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    TextFieldButtonCell *cell1 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
    if (cell1.TF.text.length > 0 && cell1.TF.text.length != 11) {
        ShowAlert(@"请输入11位手机号码", self);
        return;
    }
    
    NSMutableDictionary *upDic = [NSMutableDictionary dictionaryWithDictionary:@{@"type":selectType, @"content":cell.myTextView.text, @"phone":cell1.TF.text, @"imgUrl":@""}];
    if (cell1.TF.text.length > 0) {
        [upDic setObject:cell1.TF.text forKey:@"phone"];
    }
    [upDic setObject:[array componentsJoinedByString:@"***"] forKey:@"imgUrl"];
    [HttpUrl POST:@"appFeedback/add" dict:upDic hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if ([data[@"success"] integerValue] == 1) {
            [TheGlobalMethod popAlertView:@"提交成功!" controller:self];
        } else {
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 44;
    } else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            return 200;
        }
        if (selectImgArr.count == 0) {
            return (WIDTH_K-40)/4 + 8;
        }
        if (selectImgArr.count % 4 == 0) {
            return ((WIDTH_K-40)/4 + 8) * selectImgArr.count/4;
        }
        return ((WIDTH_K-40)/4 + 8) * (selectImgArr.count/4 + 1);

    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 3) {
        return 20;
    }
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, 20)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
