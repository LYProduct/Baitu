//
//  RenewalPayVC.h
//  RYJ
//
//  Created by wyp on 2017/7/24.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RenewalPayVC : UIViewController

@property (nonatomic, assign) BOOL isBorrow;
@property (nonatomic, strong) NSString *theOrderId;
@property (nonatomic, strong) NSString *drawMoney;
@property (nonatomic, strong) NSString *dayForDraw;

@property (nonatomic, strong) NSString *isMsg;

@end
