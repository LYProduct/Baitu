//
//  RenewalVC.m
//  RYJ
//
//  Created by wyp on 2017/7/24.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "RenewalVC.h"

#import "RenewalTimeCell.h"
#import "leftAndRightLabel.h"
#import "LLICell.h"

#import "RenewalPayVC.h"
#import "CouponsListVC.h"

@interface RenewalVC ()<UITableViewDelegate, UITableViewDataSource> {
    
    __weak IBOutlet RootTableView *myTableView;
 
    NSDictionary *dataDic;
    NSInteger selectDay;
    
    NSDictionary * _couponDict;
    
    NSMutableArray * leftArr;
    NSMutableArray * rightArr;
    NSString * aliPayCode;
    NSString * wechatCode;
}

@end

@implementation RenewalVC

- (void)viewDidLoad {
    [super viewDidLoad];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    selectDay = 7;
    selectDay = _limitDays.integerValue;
    [self getNetWorking];
    [self getPayWay];
    // Do any additional setup after loading the view.
}
-(void)getPayWay{
    [HttpUrl GET:@"sysConfig/selectSysConfig" dict:nil hud:self.view isShow:NO WithSuccessBlock:^(id data) {
        NSLog(@"支付方式--%@",data);
        leftArr = [[NSMutableArray alloc]initWithCapacity:0];
        rightArr = [[NSMutableArray alloc]initWithCapacity:0];
        if (BB_isSuccess) {
            NSArray * arr = data[@"data"];
            for (int i = 0 ; i < arr.count ; i++) {
                NSDictionary * dict = arr[i];
                
                if ([dict[@"configKey"] isEqualToString:@"alipay"]) {
                    [leftArr addObject:@"支付宝"];
                    [rightArr addObject:dict[@"configValue"]];
                    
                }
                else if ([dict[@"configKey"] isEqualToString:@"wechatAccount"]) {
                    [leftArr addObject:@"微信"];
                    [rightArr addObject:dict[@"configValue"]];
                    
                }
                else if ([dict[@"configKey"] isEqualToString:@"debitCardNo"]) {
                    [leftArr addObject:@"储蓄卡号"];
                    [rightArr addObject:dict[@"configValue"]];
                    
                }
                else if ([dict[@"configKey"] isEqualToString:@"debitCardName"]) {
                    [leftArr addObject:@"储蓄卡名称"];
                    [rightArr addObject:dict[@"configValue"]];
                    
                }
                else if ([dict[@"configKey"] isEqualToString:@"customServiceQq"]) {
                    [leftArr addObject:@"客服QQ"];
                    [rightArr addObject:dict[@"configValue"]];
                    
                }
                else if ([dict[@"configKey"] isEqualToString:@"alipayCode"]) {
                    [leftArr addObject:@"支付宝付款码"];
                    [rightArr addObject:@"微信收款码"];
                    aliPayCode = dict[@"configValue"];
                    
                }
                if ([dict[@"configKey"] isEqualToString:@"wechatCode"]) {
                    [leftArr addObject:@"*必须标注姓名和手机号码"];
                    [rightArr addObject:@""];
                    wechatCode = dict[@"configValue"];
                }
            }
        }
    }];
}
- (void)getNetWorking {
    
    [HttpUrl GET:@"orderExtend/getOrderExtendData" dict:@{@"id":_orderID, @"days":[NSString stringWithFormat:@"%ld",selectDay]} hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if ([data[@"success"] integerValue] == 1) {
            dataDic = data[@"data"];
            [myTableView reloadData];
            if (_couponDict.allKeys.count > 0) {
                [self setCoupon];

            }
        } else {
            [TheGlobalMethod popAlertView:data[@"msg"] controller:self];
        }
    }];
}

#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (!dataDic) {
        return 0;
    }
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 3;
    } else if (section == 1) {
        return 5;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            RenewalTimeCell loadingTableViewCell("RenewalTimeCell",)
            cell.returnClick = ^(NSInteger index) {
                selectDay = index;
                [self getNetWorking];
            };
            return cell;
        }
    } else if (indexPath.section == 2) {
        LLICell loadingTableViewCell("LLICell", )
        return cell;
    }
    leftAndRightLabel loadingTableViewCell("leftAndRightLabel", )
    [cell reloadDataForxuqi:dataDic vc:self indexPath:indexPath];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2 && indexPath.row == 0) {
        CouponsListVC *vc = [TheGlobalMethod setstoryboard:@"CouponsListVC" controller:self];
        vc.selectCoupon = @"1";
        vc.returnSelectCoupon = ^(NSDictionary *dic) {
            _couponDict = dic;
            [self setCoupon];
            
        };
        [self.navigationController pushViewController:vc animated:YES];
    }
}
-(void)setCoupon {
    LLICell * cell0 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
    cell0.label.text = [[_couponDict[@"saveMoney"] description] stringByAppendingString:@"元"];
    
    
    leftAndRightLabel * cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:1]];
    cell.rightLabel.text = [_couponDict[@"saveMoney"] description];
    cell.rightLabel.text = [cell.rightLabel.text stringByAppendingString:@"元"];
    cell.rightLabel.text = [@"-" stringByAppendingString:cell.rightLabel.text];
    
    leftAndRightLabel * cell1 = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:1]];
    float money = [dataDic[@"interestPercent"] doubleValue]+[dataDic[@"omnibusFee"] doubleValue] - [_couponDict[@"saveMoney"] doubleValue];
    if (money >= 0) {
        [Methods dataInText:[NSString stringWithFormat:@"%.2f",money] InLabel:cell1.rightLabel defaultStr:@"-"];
    }else{
        [Methods dataInText:[NSString stringWithFormat:@"%.2f",0.0] InLabel:cell1.rightLabel defaultStr:@"-"];
    }
    cell1.rightLabel.text = [cell1.rightLabel.text stringByAppendingString:@"元"];
}
- (void)useCoupon:(NSString *)couponId {
    [HttpUrl GET:@"loanOrder/useCoupon" dict:@{@"userCouponId":couponId, @"orderId":[dataDic[@"id"] description]} hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if ([data[@"success"] integerValue] == 1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                dataDic = data[@"data"];
                [myTableView reloadData];
            });
            
        } else {
            [TheGlobalMethod popAlertView:data[@"msg"] controller:self];
        }
        NSLog(@"借款使用优惠券数据:%@", data);
    }];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return 44;
        } else if (indexPath.row == 1) {
            return 24;
        } else {
            return 36;
        }
    } else if (indexPath.section == 1) {
        if (indexPath.row == 0 || indexPath.row == 4) {
            return 44;
        } else if (indexPath.row == 3) {
            return 36;
        }
        return 26;
    }
    return 44;
}

- (IBAction)upMsg:(UIButton *)sender {
    
    NSDictionary * dict ;
    if (_couponDict.allKeys.count > 0) {
        dict = @{@"days":[NSString stringWithFormat:@"%ld",selectDay],@"money":[NSString stringWithFormat:@"%@",dataDic[@"needPayMoney"]],@"paramSettingId":[NSString stringWithFormat:@"%@",dataDic[@"paramSettingId"]],@"userCouponId":[_couponDict[@"id"] description],@"orderId":_orderID};
    }else{
        dict = @{@"days":[NSString stringWithFormat:@"%ld",selectDay],@"money":[NSString stringWithFormat:@"%@",dataDic[@"needPayMoney"]],@"paramSettingId":[NSString stringWithFormat:@"%@",dataDic[@"paramSettingId"]],@"orderId":_orderID};
    }
    
//    orderExtend/applyOrderExtend
    [HttpUrl GET:@"orderExtend/submitRenewal" dict:dict hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if (BB_isSuccess) {
            [self.navigationController popToRootViewControllerAnimated:YES];
            [Methods showAlertForPayLeftArr:leftArr rightArr:rightArr bottomStatus:@"3" selfVC:self titleLabelText:@"付款方式(可长按复制)" titleLabelTextColor:appDefaultColor alicode:aliPayCode wechat:wechatCode];
        }else{
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];//,@"userCouponId":@""
    
//    RenewalPayVC *vc = [TheGlobalMethod setstoryboard:@"RenewalPayVC" controller:self];
//    vc.isBorrow = NO;
//    vc.theOrderId = _orderID;
//
//    vc.drawMoney = [NSString stringWithFormat:@"%.2f", [dataDic[@"interestPercent"] floatValue]+[dataDic[@"omnibusFee"] floatValue]];
//    vc.dayForDraw = [NSString stringWithFormat:@"%ld",selectDay];
//    [self.navigationController pushViewController:vc animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, 10)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
