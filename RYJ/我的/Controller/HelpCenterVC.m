//
//  HelpCenterVC.m
//  RYJ
//
//  Created by wyp on 2017/7/25.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "HelpCenterVC.h"

#import "LabelImgCell.h"
#import "MessageDetailVC.h"


@interface HelpCenterVC ()<UITableViewDelegate, UITableViewDataSource> {

    __weak IBOutlet RootTableView *myTableView;
    NSArray *dataArr;
    
}

@end

@implementation HelpCenterVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    // Do any additional setup after loading the view.
    myTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, 10)];
    myTableView.tableHeaderView.backgroundColor = [UIColor clearColor];
    
    [self requestNetWorking];
}

- (void)requestNetWorking {
    if ([_isZiXun isEqualToString:@"1"]) {
        [HttpUrl GET:@"helpCenter/selectPage2" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
            if ([data[@"success"] integerValue] == 1) {
                dataArr = data[@"data"][@"records"];
                [myTableView reloadData];
            } else {
                [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
            }
        }];
    } else {
    [HttpUrl GET:@"helpCenter/selectPage1" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if ([data[@"success"] integerValue] == 1) {
            dataArr = data[@"data"][@"records"];
            [myTableView reloadData];
        } else {
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
    }
}

#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LabelImgCell loadingTableViewCell("LabelImgCell", )
    if (indexPath.row == dataArr.count-1) {
        cell.lineView_L.constant = 0;
    }
    cell.backgroundColor = [UIColor whiteColor];
    [Methods dataInText:dataArr[indexPath.row][@"title"] InLabel:cell.label defaultStr:@"-"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MessageDetailVC *vc = [TheGlobalMethod setstoryboard:@"MessageDetailVC" controller:self];
    if (![dataArr[indexPath.row][@"content"] isEqual:[NSNull null]]) {
        NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[dataArr[indexPath.row][@"content"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
        vc.attStr = attrStr;
    } else {
        vc.detailStr = @"-";
    }
    
    vc.titleStr = @"详情";
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
