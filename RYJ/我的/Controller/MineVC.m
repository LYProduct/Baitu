//
//  MineVC.m
//  RYJ
//
//  Created by wyp on 2017/7/21.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "MineVC.h"

#import "MineTopCell.h"
#import "MineListCell.h"

#import "BorrowingRecordListVC.h"
#import "CerCardVC.h"
#import "CouponsListVC.h"

#import "InvitationFriendsVC.h"

#import "SetPasswordVC.h"
#import "HelpCenterVC.h"
#import "AboutUSVC.h"

#import "ExitLoginCell.h"

#import "JPUSHService.h"
#import "TheCertificationVC.h"


@interface MineVC ()<UITableViewDelegate, UITableViewDataSource> {

    __weak IBOutlet RootTableView *myTableView;
    NSArray *listBottomArr;
    NSDictionary *authDic;
    NSArray *listHeaderSectionArr;
    NSDictionary * _loginUserDict;
    
    
}

@end

@implementation MineVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
   
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}
-(void)getLoginUser {
    dispatch_async(dispatch_get_main_queue(), ^{
//        [HttpUrl GET:@"user/findLoginUser" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
//            NSLog(@"当前用户信息 --  %@",data);
//            if (BB_isSuccess) {
//                _loginUserDict = data[@"data"];
//                listBottomArr = @[@[],@[[_loginUserDict[@"needPayMoney"] description]], @[@"优惠券"], @[@"银行卡", @"借款记录", @"提升额度"]];
//            }
//            [myTableView reloadData];
//        }];
    });
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (BB_isLogin) {
        [self getLoginUser];
        [self getCer];
    }
}
-(void)getCer {
//    [HttpUrl GET:@"userAuth/selectUserAuth" dict:nil hud:self.view isShow:NO WithSuccessBlock:^(id data) {
//        if (BB_isSuccess) {
            //            data =     {
            //                userAuth =         {
            //                    auditorId = 0;
            //                    baiscAuth = 1;
            //                    bankAuth = 1;
            //                    gmtDatetime = "<null>";
            //                    id = 13;
            //                    identityAuth = 1;
            //                    phoneAuth = 1;
            //                    schoolInfo = 3;
            //                    taobaoAuth = 3;
            //                    user = "<null>";
            //                    userId = 8;
            //                    zhimaAuth = 1;
            //                };
            //                userBasicMsg =         {
            //                    address =             {
            //                        addressCode = "<null>";
            //                        addressDetails = "<null>";
            //                        area = "\U677e\U6eaa\U53bf";
            //                        areaCode = "<null>";
            //                        city = "\U5357\U5e73";
            //                        gmtDatetime = "<null>";
            //                        id = 19;
            //                        province = "\U798f\U5efa";
            //                        status = "<null>";
            //                        uptDatetime = "<null>";
            //                        userId = 8;
            //                    };
            //                    addressId = 20;
            //                    firstContactName = "\U522b\U6d6a";
            //                    firstContactPhone = 13554151701;
            //                    firstContactRelation = " ";
            //                    gmtDatetime = "2017-12-25 13:26:06";
            //                    id = 21;
            //                    marry = "\U672a\U5a5a";
            //                    secondContactName = "\U7238\U7238";
            //                    secondContactPhone = 18849541277;
            //                    secondContactRelation = " ";
            //                    status = "<null>";
            //                    study = "\U535a\U58eb";
            //                    uptDatetime = "<null>";
            //                    user = "<null>";
            //                    userId = 8;
            //                };
//            authDic = data[@"data"];
//
//
//        }
//    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [myTableView registerNib:[UINib nibWithNibName:@"MineTopCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"MineTopCell"];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    
    listBottomArr = @[@[],@[@"0.00元"], @[@"优惠券"], @[@"银行卡", @"借款记录", @"提升额度"]];
    listHeaderSectionArr = @[@"", @"待还", @"福利", @"其它"];
    
//    [self requestNetWorking];
    
//    [self setMJ];
    // Do any additional setup after loading the view.
}

- (void)setMJ {
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        [self requestNetWorking];
    }];
    header.lastUpdatedTimeLabel.textColor = [UIColor whiteColor];
    header.stateLabel.textColor = [UIColor whiteColor];
    myTableView.mj_header = header;
    [myTableView bringSubviewToFront:myTableView.mj_header];
}

//- (void)requestNetWorking {
//    [HttpUrl GET:@"userAuth/selectAll" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
//        if ([data[@"success"] integerValue] == 1) {
//            authDic = data[@"data"][@"auth"];
//            [myTableView reloadData];
//            [myTableView.mj_header endRefreshing];
//        } else {
//            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
//        }
//    }];
//}

#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
        return 4;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        MineTopCell *cell = [MineTopCell tableViewCellWithTableView:tableView];
        if (![_loginUserDict isEqual:[NSNull null]] && _loginUserDict) {
            [cell reloadCellForData:_loginUserDict vc:self];
        }
        cell.block = ^{
            AboutUSVC *vc = GETStoryboard(@"AboutUSVC");
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        };
        return cell;
    }
//    else if (indexPath.section == 4) {
//        ExitLoginCell loadingTableViewCell("ExitLoginCell", )
//        cell.backgroundColor = [UIColor clearColor];
//        return cell;
//    }
    MineListCell loadingTableViewCell("MineListCell", )
    cell.backgroundColor = [UIColor clearColor];
    [cell reloadCellForData:@{@"array":listBottomArr[indexPath.section], @"section":listHeaderSectionArr[indexPath.section]} vc:self indexPath:indexPath];
    cell.returnSelectRow = ^(NSInteger row) {
        [self clickRow:[NSIndexPath indexPathForRow:row inSection:indexPath.section]];
    };
    return cell;
    
}

- (void)clickRow:(NSIndexPath *)indexPath {
    NSLog(@"%@", indexPath);
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            [self.tabBarController setSelectedIndex:0];
        }
    } else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            CouponsListVC *vc = [TheGlobalMethod setstoryboard:@"CouponsListVC" controller:self];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
            
        } else if (indexPath.row == 1) {
            
        } else if (indexPath.row == 2) {
            InvitationFriendsVC *vc = [TheGlobalMethod setstoryboard:@"InvitationFriendsVC" controller:self];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
    } else if (indexPath.section == 3) {
        if (indexPath.row == 0) {
            CerCardVC *vc = [TheGlobalMethod setstoryboard:@"CerCardVC" controller:self];
            if ([authDic[@"userAuth"][@"bankAuth"] integerValue] == 3) {
                
                ShowAlert(@"请先到认证界面进行银行卡认证", self)

            } else {
                vc.isCer = @"1";
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
            vc.reloadTheVC = ^{
            };
        } else if (indexPath.row == 1) {
            BorrowingRecordListVC *vc = [TheGlobalMethod setstoryboard:@"BorrowingRecordListVC" controller:self];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        } else if (indexPath.row == 2) {
            TheCertificationVC * vc = [[TheCertificationVC alloc]initWithNibName:@"TheCertificationVC" bundle:nil];
            vc.type = 1;
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        } else if (indexPath.row == 3) {
            dispatch_async(dispatch_get_main_queue(), ^{
                SetPasswordVC *vc = [TheGlobalMethod setstoryboard:@"SetPasswordVC" controller:self];
                vc.hidesBottomBarWhenPushed = YES;
//                [HttpUrl GET:@"user/payPwdIsExist" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
//                    NSLog(@"是否设置过交易密码%@", [data[@"msg"] description]);
//                    if ([[data[@"msg"] description] isEqualToString:@"true"]) {
//                        vc.isPS = @"1";
//                    }
//                    [self.navigationController pushViewController:vc animated:YES];
//                }];
                
            });
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            BorrowingRecordListVC *vc = [TheGlobalMethod setstoryboard:@"BorrowingRecordListVC" controller:self];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        } else if (indexPath.row == 1) {
            CerCardVC *vc = [TheGlobalMethod setstoryboard:@"CerCardVC" controller:self];
            if ([authDic[@"userAuth"][@"bankAuth"] integerValue] == 1) {
                vc.isCer = @"1";
            }
            vc.reloadTheVC = ^{
//                [self requestNetWorking];
            };
            
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        } else if (indexPath.row == 2) {
            CouponsListVC *vc = [TheGlobalMethod setstoryboard:@"CouponsListVC" controller:self];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
    } else if (indexPath.section == 2) {
        InvitationFriendsVC *vc = [TheGlobalMethod setstoryboard:@"InvitationFriendsVC" controller:self];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    } else if (indexPath.section == 3) {
        if (indexPath.row == 0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                SetPasswordVC *vc = [TheGlobalMethod setstoryboard:@"SetPasswordVC" controller:self];
                vc.hidesBottomBarWhenPushed = YES;
//                [HttpUrl GET:@"user/payPwdIsExist" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
//
//                    NSLog(@"是否设置过交易密码%@", [data[@"msg"] description]);
//                    if ([[data[@"msg"] description] isEqualToString:@"true"]) {
//                        vc.isPS = @"1";
//
//                    }
//                    [self.navigationController pushViewController:vc animated:YES];
//                }];
                
            });
        } else if (indexPath.row == 1) {
            HelpCenterVC *vc = [TheGlobalMethod setstoryboard:@"HelpCenterVC" controller:self];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        } else if (indexPath.row == 2) {
            HelpCenterVC *vc = [TheGlobalMethod setstoryboard:@"HelpCenterVC" controller:self];
            vc.title = @"资讯中心";
            vc.isZiXun = @"1";
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        } else if (indexPath.row == 3) {
            AboutUSVC *vc = [TheGlobalMethod setstoryboard:@"AboutUSVC" controller:self];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
    } else if (indexPath.section == 4) {
        ;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 230;
    } else if (indexPath.section == 4) {
        return 44;
    }
    NSArray *arr = listBottomArr[indexPath.section];
    NSInteger count = [arr count];
    return 40*count + 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, 10)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 12;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
