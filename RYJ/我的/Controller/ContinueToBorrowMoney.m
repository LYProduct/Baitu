//
//  RenewalPayVC.m
//  RYJ
//
//  Created by wyp on 2017/7/24.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#define kRequestTime 3.0f
#define kDelay 1.0f

#import "ContinueToBorrowMoney.h"

#import "CardCell.h"
#import "leftAndRightLabel.h"
#import "ABtnCell.h"

#import "CYPasswordView.h"

#import "BorrowResultsVC.h"

//连连支付
#import "LLPayUtil.h"
#import "LLOrder.h"
#import "LLPaySdk.h"
/*! TODO: 修改两个参数成商户自己的配置 */
static NSString *kLLOidPartner = LLMerchantsNumber;//@"201408071000001546";                 // 商户号
static NSString *kLLPartnerKey = LLPayPrivateKey;//@"201408071000001546_test_20140815";   // 密钥
static NSString *signType = @"RSA";//签名方式


/*! 接入什么支付产品就改成那个支付产品的LLPayType，如快捷支付就是LLPayTypeQuick */

static LLPayType payType = LLPayTypeInstalments;




@interface ContinueToBorrowMoney ()<UITableViewDelegate, UITableViewDataSource, LLPaySdkDelegate> {
    
    __weak IBOutlet RootTableView *myTableView;
    
    NSDictionary *userOrderDic;
    
    /**连连支付配置信息*/
    NSString *userId;
    /*！ 若是签约、认证、实名快捷、分期付等请填入以下内容 */
    NSString *cardNumber;  //卡号
    NSString *acctName;    //姓名
    NSString *idNumber;    //身份证号
    NSString *money;    //钱
    NSString *orderId;  //订单id
    NSString *no_order;
    NSString *risk_item;
    
}

@property (nonatomic, strong) CYPasswordView *passwordView;

@property (nonatomic, retain) NSMutableDictionary *orderDic;

@property (nonatomic, strong) NSString *resultTitle;

@property (nonatomic, strong) LLOrder *order;

@end

@implementation ContinueToBorrowMoney

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.label.text = @"正在查询您的续期信息";
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [HttpUrl GET:@"user/getPayInterfaceMsg" dict:nil hud:self.view isShow:NO WithSuccessBlock:^(id data) {
            if ([data[@"success"] integerValue] == 1) {
                NSLog(@"订单信息%@", data);
                userOrderDic = data[@"data"];
                [myTableView reloadData];
            } else {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
            }
            
        }];
    });
    // Do any additional setup after loading the view.
}

#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (!userOrderDic) {
        return 0;
    }
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        CardCell loadingTableViewCell("CardCell", )
        [Methods dataInText:userOrderDic[@"bankName"] InLabel:cell.bankName defaultStr:@"-"];
        if (![userOrderDic[@"bankNum"] isEqual:[NSNull null]]) {
            if ([[userOrderDic[@"bankNum"] description] length] >= 4) {
                cell.bankNum.text = [cell.bankNum.text stringByAppendingString:[NSString stringWithFormat:@"%@", [[userOrderDic[@"bankNum"] description] substringFromIndex:[[userOrderDic[@"bankNum"] description] length]-4]]];
            }
        }
        return cell;
    } else if (indexPath.section == 1) {
        leftAndRightLabel loadingTableViewCell("leftAndRightLabel", )
        cell.leftLabel.text = @"应扣金额";
        
        [Methods dataInText:userOrderDic[@"money"] InLabel:cell.rightLabel defaultStr:@"-"];
        cell.rightLabel.text = [cell.rightLabel.text stringByAppendingString:@"元"];
        cell.rightLabel.hidden = NO;
        return cell;
    }
    ABtnCell *cell = [TheGlobalMethod setCell:@"ABtnCell"];
    [cell.btn addTarget:self action:@selector(pay) forControlEvents:(UIControlEventTouchUpInside)];
    [cell.btn setTitle:@"确认续期" forState:(UIControlStateNormal)];
    
    return cell;
}

- (void)pay {
    __weak ContinueToBorrowMoney *weakSelf = self;
    self.passwordView = [[CYPasswordView alloc] init];
    self.passwordView.title = @"请输入支付密码";
    self.passwordView.loadingText = @"提交中...";
    [self.passwordView showInView:self.view.window];
    
    __block ContinueToBorrowMoney *vc = self;
    
    self.passwordView.finish = ^(NSString *password) {
        [weakSelf.passwordView hideKeyboard];
        [weakSelf.passwordView startLoading];
        CYLog(@"cy ========= 发送网络请求  pwd=%@", password);
        
        [HttpUrl GET:@"user/getPayMsg" dict:@{@"payPwd":[CJMD5 md5HexDigest:password]} hud:vc.view isShow:YES WithSuccessBlock:^(id data) {
            NSLog(@"付款信息%@", data);
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kRequestTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                BOOL flag = [data[@"success"] integerValue];
                if (flag) {
                    CYLog(@"申购成功，跳转到成功页");
                    [weakSelf.passwordView requestComplete:YES message:@"支付成功"];
                    [weakSelf.passwordView stopLoading];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kDelay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [weakSelf.passwordView hide];
                        //                        BorrowResultsVC *vc = [TheGlobalMethod setstoryboard:@"BorrowResultsVC" controller:weakSelf];
                        //                        if (weakSelf.isBorrow) {
                        //                            vc.idMsg = @"还款成功";
                        //                        } else {
                        //                            vc.idMsg = @"续期成功";
                        //                        }
                        //
                        //                        [weakSelf.navigationController pushViewController:vc animated:YES];
                        
                        NSDictionary *dict = data[@"data"];
                        userId = [dict[@"userId"] description];
                        cardNumber = [dict[@"bankNum"] description];
                        acctName = [dict[@"name"] description];
                        idNumber = [dict[@"idCard"] description];
                        money = [dict[@"money"] description];
                        orderId = [dict[@"orderId"] description];
                        no_order = [dict[@"no_order"] description];
                        risk_item = [dict[@"risk_item"] description];
                        [weakSelf llorder];
                    });
                    
                    
                    
                } else {
                    CYLog(@"申购失败，跳转到失败页");
                    [weakSelf.passwordView requestComplete:NO message:data[@"msg"]];
                    [weakSelf.passwordView stopLoading];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kDelay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [weakSelf.passwordView showKeyboard];
                        
                    });
                    
                    
                }
                
            });
        }];
        
        
    };
}



- (void)llorder {
    _order = [[LLOrder alloc] initWithLLPayType:payType];
    NSString *timeStamp = [LLOrder timeStamp];
    _order.oid_partner = kLLOidPartner;
    _order.sign_type = signType;
    _order.busi_partner = @"101001";
    _order.no_order = no_order;
    _order.dt_order = timeStamp;
    _order.money_order = money;
    _order.notify_url = [NSString stringWithFormat:@"http://app.hzrywl.com/api/api/lianpay/fenqipayapi/receiveNotify.htm?money=%@&orderId=%@&no_order=%@",money,orderId,no_order];
    _order.acct_name = acctName;
    _order.card_no = cardNumber;
    _order.id_no = idNumber;
    _order.risk_item = risk_item;
    _order.user_id = userId;
    _order.name_goods = @"百途续期";
    //    _order.sign = @"ivaTAz8c/YSWYXf37G37y0uSyDbTx92iK+HcCi5sWJT2o9ne4hREVaDYirMGqHd6R4BMpk6qhfcC4qbYyv7FuisYU/8OfszwFmj5rXtcevMAZcPPRft4mBFvdT13QvgVIknPSreJ2DoBn/cAnCt5GKwdUJ4wMDnKs40qzu+F0fw=";
    
    //    if (payType == LLPayTypeInstalments) {
    //        _order.repayment_plan = [LLOrder llJsonStringOfObj:@{@"repaymentPlan":@[@{@"date" : @"2017-09-01", @"amount" : @"0.01"}]}];
    //        _order.repayment_no = [NSString stringWithFormat:@"%@%@", @"FenQi", timeStamp];
    //        _order.sms_param = [LLOrder llJsonStringOfObj:@{@"contract_type" : @"短信显示的商户名",
    //                                                        @"contact_way" : @"400-018-8888"}];
    //    }
    [self payOrder];
}

#pragma mark - 订单支付

- (void)payOrder {
    self.resultTitle = @"支付结果";
    self.orderDic = [[_order tradeInfoForPayment] mutableCopy];
    
    NSLog(@"%@", self.orderDic);
    
    LLPayUtil *payUtil = [[LLPayUtil alloc] init];
    
    // 进行签名
    NSDictionary *signedOrder = [payUtil signedOrderDic:self.orderDic andSignKey:kLLPartnerKey];
    NSLog(@"%@",    signedOrder);
    [LLPaySdk sharedSdk].sdkDelegate = self;
    
    [[UINavigationBar appearance] setBarStyle:UIBarStyleBlack];
    //接入什么产品就传什么LLPayType
    [[LLPaySdk sharedSdk] presentLLPaySDKInViewController:self
                                              withPayType:payType
                                            andTraderInfo:signedOrder];
}


#pragma - mark 支付结果 LLPaySdkDelegate
// 订单支付结果返回，主要是异常和成功的不同状态
// TODO: 开发人员需要根据实际业务调整逻辑
- (void)paymentEnd:(LLPayResult)resultCode withResultDic:(NSDictionary *)dic {
    
    NSString *msg = @"异常";
    switch (resultCode) {
        case kLLPayResultSuccess: {
            msg = @"支付成功";
        } break;
        case kLLPayResultFail: {
            msg = @"支付失败";
        } break;
        case kLLPayResultCancel: {
            msg = @"支付取消";
        } break;
        case kLLPayResultInitError: {
            msg = @"连连支付初始化异常";
        } break;
        case kLLPayResultInitParamError: {
            msg = dic[@"ret_msg"];
        } break;
        default:
            break;
    }
    
    //    NSString *showMsg =
    //    [msg stringByAppendingString:[LLPayUtil jsonStringOfObj:dic]];
    
    [JXTAlertView showAlertViewWithTitle:self.resultTitle message:msg cancelButtonTitle:nil buttonIndexBlock:^(NSInteger buttonIndex) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    } otherButtonTitles:@[@"确定"]];
    
    //    UIAlertController *alert = [UIAlertController alertControllerWithTitle:self.resultTitle
    //                                                                   message:msg
    //                                                            preferredStyle:UIAlertControllerStyleAlert];
    //
    //    [alert addAction:[UIAlertAction actionWithTitle:@"确认"
    //                                              style:UIAlertActionStyleDefault
    //                                            handler:nil]];
    //    [self presentViewController:alert animated:YES completion:nil];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 60;
    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 2) {
        return 20;
    }
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    CGFloat height;
    if (section == 2) {
        height = 20;
    } else {
        height = 10;
    }
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, height)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
