//
//  SetPasswordVC.m
//  RYJ
//
//  Created by wyp on 2017/7/25.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "SetPasswordVC.h"

#import "JQUnitField.h"
#import "NoPasteTF.h"
#import "PassWordChangeCodeVC.h"


@interface SetPasswordVC ()<UITextFieldDelegate, JQUnitFieldDelegate> {
    __weak IBOutlet UIButton *btn;
    __weak IBOutlet JQUnitField *unitField;
}

@end

@implementation SetPasswordVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    unitField.cursorColor = nil;
    unitField.delegate = self;
    
    if ([_isPS isEqualToString:@"1"]) {
        self.topLabel.text = @"输入旧交易密码";
        self.title = @"支付密码修改";
        self.forgetBtn.hidden = NO;
    }
}

- (BOOL)unitField:(JQUnitField *)uniField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return YES;
}

- (IBAction)unitFieldEditingChanged:(JQUnitField *)sender {
    NSLog(@"%@", sender.text);
}
- (IBAction)btnClick:(UIButton *)sender {
    
    
    if (unitField.text.length != 6) {
        [TheGlobalMethod xianShiAlertView:@"请输入6位密码" controller:self];
        return;
    }
    
    if (![_forgetBtn isHidden]) {
        [HttpUrl GET:@"user/oldPayPwdConfirm" dict:@{@"oldPayPwd":[CJMD5 md5HexDigest:unitField.text]} hud:self.view isShow:YES WithSuccessBlock:^(id data) {
            NSLog(@"旧密码验证是否通过:%@", data);
            if ([data[@"success"] integerValue] == 1) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    SetPasswordVC *vc = [TheGlobalMethod setstoryboard:@"SetPasswordVC" controller:self];
                    [self.navigationController pushViewController:vc animated:YES];
                });
            } else {
                [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
            }
            

        }];
    } else {
    
    
    if (_password.length > 0) {
        if (![_password isEqualToString:unitField.text]) {
            ShowAlert(@"两次密码输入不一致", self);
        } else {
            [HttpUrl GET:@"user/payPwdSetting" dict:@{@"payPwd":[CJMD5 md5HexDigest:_password], @"payPwdConfirm":[CJMD5 md5HexDigest:unitField.text]} hud:self.view isShow:YES WithSuccessBlock:^(id data) {
                if ([data[@"success"] integerValue] == 1) {
                    [JXTAlertView showAlertViewWithTitle:@"提示" message:@"设置成功" cancelButtonTitle:nil buttonIndexBlock:^(NSInteger buttonIndex) {
                        [self.navigationController popToRootViewControllerAnimated:YES];
                    } otherButtonTitles:@[@"确定"]];
                } else {
                    [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
                }
            }];
        }
    
    }else {
        dispatch_async(dispatch_get_main_queue(), ^{
            SetPasswordVC *vc = [TheGlobalMethod setstoryboard:@"SetPasswordVC" controller:self];
            vc.password = unitField.text;
            [self.navigationController pushViewController:vc animated:YES];
        });
    }
        
    }
}

- (IBAction)Forget:(UIButton *)sender {
    PassWordChangeCodeVC *vc = [TheGlobalMethod setstoryboard:@"PassWordChangeCodeVC" controller:self];
    [self.navigationController pushViewController:vc animated:YES];
}


- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
