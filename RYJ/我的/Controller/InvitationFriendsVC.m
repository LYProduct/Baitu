//
//  InvitationFriendsVC.m
//  RYJ
//
//  Created by wyp on 2017/7/25.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "InvitationFriendsVC.h"

#import "TopImageCell.h"
#import "InvitationMoneyCell.h"
#import "InvitationTitleCell.h"
#import "CouponsCell.h"

#import "PopThreeView.h"

@interface InvitationFriendsVC ()<UITableViewDelegate, UITableViewDataSource> {
    
    __weak IBOutlet RootTableView *myTableView;

    NSDictionary *dataDic;
}

@end

@implementation InvitationFriendsVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    
    [self getNetWorking];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            [self getNetWorking];
    });

}


- (void)getNetWorking {

    [HttpUrl GET:@"userCoupon/inviteFriend" dict:nil hud:self.view isShow:NO WithSuccessBlock:^(id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"邀请好友数据%@", data);
        if ([data[@"success"] integerValue] == 1) {
            dataDic = data[@"data"];
        } else {
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [myTableView reloadData];
        });
    }];
}

#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (!dataDic) {
        return 0;
    }
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 3;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            TopImageCell loadingTableViewCell("TopImageCell", )
            return cell;
        } else if (indexPath.row == 1) {
            InvitationMoneyCell loadingTableViewCell("InvitationMoneyCell", [cell reloadCellForData:dataDic vc:self])
            return cell;
        } else if (indexPath.row == 2) {
            InvitationTitleCell loadingTableViewCell("InvitationTitleCell", )
            return cell;
        }
    }
    CouponsCell loadingTableViewCell("CouponsCell", )
    return cell;
}

- (IBAction)yaoqing:(UIButton *)sender {
//    PopThreeView
    PopThreeView *popThreeView = [TheGlobalMethod setCell:@"PopThreeView"];
    popThreeView.frame = CGRectMake(0, 0, WIDTH_K, HEIGHT_K);
    [popThreeView reloadCellForData:@{} vc:self];
    [[UIApplication sharedApplication].keyWindow addSubview:popThreeView];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return HEIGHT_K/3;
        } else if (indexPath.row == 1) {
            return 44;
        } else if (indexPath.row == 2) {
            return 44;
        }
    }
    return 110;
}



- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
