//
//  BorrowFeedbackVC.m
//  RYJ
//
//  Created by wyp on 2017/7/24.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "BorrowFeedbackVC.h"

#import "TextViewCell.h"
#import "SelectImgCell.h"
#import "ABtnCell.h"

@interface BorrowFeedbackVC ()<UITableViewDelegate, UITableViewDataSource> {
    
    __weak IBOutlet RootTableView *myTableView;
    NSArray *selectImgArr;

}

@end

@implementation BorrowFeedbackVC

- (void)viewDidLoad {
    [super viewDidLoad];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    // Do any additional setup after loading the view.
}

#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 2;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {

    if (indexPath.row == 0) {
        TextViewCell loadingTableViewCell("TextViewCell", [cell reloadCellForData:@{} vc:self])
        cell.returnTextViewText = ^(NSString *string) {
            
        };
        return cell;
    } else if (indexPath.row == 1) {
    SelectImgCell loadingTableViewCell("SelectImgCell", [cell reloadCellForData:@{} vc:self])
    cell.returnImgArr = ^(NSArray *imgArr) {
        selectImgArr = imgArr;
        [myTableView reloadData];
    };
    return cell;
    }
    }
    ABtnCell *cell = [TheGlobalMethod setCell:@"ABtnCell"];
    [cell.btn addTarget:self action:@selector(up) forControlEvents:(UIControlEventTouchUpInside)];
    [cell.btn setTitle:@"提交" forState:(UIControlStateNormal)];
    return cell;
}

- (void)up {
    TextViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if (cell.myTextView.text.length == 0) {
        ShowAlert(@"请输入还款反馈原因", self);
        return;
    }
    
    if (selectImgArr.count > 0) {
        [self upImage];
    } else {
        
        NSMutableDictionary *upDic = [NSMutableDictionary dictionaryWithDictionary:@{@"reason":cell.myTextView.text, @"imgUrl":@""}];
        
        [HttpUrl POST:@"payFeedback/add" dict:upDic hud:self.view isShow:YES WithSuccessBlock:^(id data) {
            if ([data[@"success"] integerValue] == 1) {
                [TheGlobalMethod popAlertView:@"提交成功!" controller:self];
            } else {
                [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
            }
        }];
    }
}

- (void)upImage {
    NSMutableArray *urlArr = [NSMutableArray array];
    NSLog(@"selectImgArr Count: %ld", (unsigned long)selectImgArr.count);
    for (int i = 0; i < selectImgArr.count; i++) {
        [HttpUrl upLoadImage:selectImgArr[i] hud:self.view isShow:YES WithSuccessBlock:^(id data) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            NSLog(@"上传图片数据:%@", data);
            if ([data[@"success"] integerValue] == 1) {
                [urlArr addObject:data[@"data"][0]];
                if (urlArr.count == selectImgArr.count) {
                    [self upFeedBack:[NSArray arrayWithArray:urlArr]];
                }
            } else {
                [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
            }
        }];
    }
}

- (void)upFeedBack:(NSArray *)array {
    TextViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];

    NSMutableDictionary *upDic = [NSMutableDictionary dictionaryWithDictionary:@{@"reason":cell.myTextView.text, @"imgUrl":@""}];

    [upDic setObject:[array componentsJoinedByString:@"***"] forKey:@"imgUrl"];
    [HttpUrl POST:@"payFeedback/add" dict:upDic hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if ([data[@"success"] integerValue] == 1) {
            [TheGlobalMethod popAlertView:@"提交成功!" controller:self];
        } else {
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        return 44;
    }
    if (indexPath.row == 0) {
        return 200;
    }
    if (selectImgArr.count == 0) {
        return (WIDTH_K-40)/4 + 8;
    }
    if (selectImgArr.count % 4 == 0) {
        return ((WIDTH_K-40)/4 + 8) * selectImgArr.count/4;
    }
    return ((WIDTH_K-40)/4 + 8) * (selectImgArr.count/4 + 1);
}

- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, 10)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
