//
//  myOrderViewController.m
//  RYJ
//
//  Created by wyp on 2018/1/15.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import "myOrderViewController.h"
#import "MKPmyOrderCell.h"
#import "myOrderModel.h"

@interface myOrderViewController ()<UITableViewDelegate,UITableViewDataSource>
/** 自定义视图 */
@property(nonatomic,strong) UITableView *TableView;
/** 数据  */
@property(nonatomic,strong) NSArray *dataArray;
/** 上一次的请求参数 */
@property (nonatomic, strong) NSDictionary *params;
/** 当前页码 */
@property (nonatomic, assign) NSInteger pageNowO;
@property(nonatomic,strong) NSMutableArray *dataSourArray;
@end

@implementation myOrderViewController

static NSString *const MKPCell = @"MKPmyCell";

-(NSMutableArray *)dataSourArray
{
    if(!_dataSourArray)
    {
        _dataSourArray = [NSMutableArray array];
    }
    return _dataSourArray;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = NAVCOLOR_C(242, 242, 242);
    self.title = @"回租订单";
    // 视图
    [self createView];
    
    [self setupRefresh];
}

#pragma mark -- 视图
-(void)createView
{
    self.TableView = [[UITableView alloc]initWithFrame:CGRectMake(0, H(10), self.view.width, self.view.height)];
    [self.TableView registerNib:[UINib nibWithNibName:@"MKPmyOrderCell" bundle:nil] forCellReuseIdentifier:MKPCell];
    self.TableView.backgroundColor = [UIColor clearColor];
    self.TableView.separatorStyle = 0;
    self.TableView.delegate = self;
    self.TableView.dataSource = self;
    [self.view addSubview:self.TableView];
}

#pragma mark - 刷新
-(void)setupRefresh
{
    self.TableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewx)];
    self.TableView.mj_header.automaticallyChangeAlpha = YES;
    [self.TableView.mj_header beginRefreshing];
      self.TableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
}
/**
 * 加载新的数据
 */
-(void)loadNewx
{
    // 结束上拉
    [self.TableView.mj_footer endRefreshing];
    self.pageNowO = 1 ;
    NSString *stringInt = [NSString stringWithFormat:@"%ld",self.pageNowO];
    // 参数
    NSMutableDictionary *parmsO = [NSMutableDictionary dictionary];
    parmsO[@"current"] = stringInt;
    parmsO[@"size"] = @"10";
    self.params = parmsO;
    
    [HttpUrl GET:@"order/selectMyOrderList" dict:parmsO hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if (BB_isSuccess) {
            self.dataSourArray = [myOrderModel  mj_objectArrayWithKeyValuesArray:data[@"data"]];
            [self.TableView reloadData];
        }else{
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
    
    [self.TableView.mj_header endRefreshing];
    
}

-(void)loadMore
{
    // 结束下拉
    [self.TableView.mj_header endRefreshing];
    NSInteger page = self.pageNowO + 1;
    NSString *stringInt = [NSString stringWithFormat:@"%ld",page];
    // 参数
    NSMutableDictionary *parmsO = [NSMutableDictionary dictionary];
    parmsO[@"current"] = stringInt;
    parmsO[@"size"] = @"10";
    self.params = parmsO;
    
    [HttpUrl GET:@"order/selectMyOrderList" dict:parmsO hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if (BB_isSuccess) {
            NSArray *newInventory = [myOrderModel  mj_objectArrayWithKeyValuesArray:data[@"data"]];
            [self.dataSourArray addObjectsFromArray:newInventory];
            [self.TableView reloadData];
        }else{
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
    
    self.pageNowO = page;
    [self.TableView reloadData];
    [self.TableView.mj_footer endRefreshing];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSourArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MKPmyOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:MKPCell];
    
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    
    cell.model = self.dataSourArray[indexPath.row];

    cell.tintColor = [UIColor orangeColor];

    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    MKPModelPersonalHomePageViewController *vc = [[MKPModelPersonalHomePageViewController alloc]init];
//    MKPClub *model = self.dataArray[indexPath.row];
//    vc.personalImageViewStr = model.user[@"picUrl"];
//    vc.ID  = model.ID;
//    vc.userId = model.user[@"id"];
//    [self.navigationController pushViewController:vc animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return H(128);
}
- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
