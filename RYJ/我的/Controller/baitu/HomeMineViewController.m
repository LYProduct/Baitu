//
//  HomeMineViewController.m
//  RYJ
//
//  Created by wyp on 2018/1/11.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import "HomeMineViewController.h"
#import "HomeMineTableViewCell.h"
#import "JPUSHService.h"
#import "HomeTopTableViewCell.h"
#import "MsgListVC.h"
#import "CouponsListVC.h"
#import "InvitationFriendsVC.h"
#import "myOrderViewController.h"
#import "MKPSetViewController.h"

@interface HomeMineViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    __weak IBOutlet RootTableView *myTableView;
    
     NSArray *listBottomArr;
}
@end

@implementation HomeMineViewController

static NSString *const MKPCell = @"HomeMineViewCell";

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的";
    myTableView.delegate = self;
    myTableView.dataSource = self;
    myTableView.scrollEnabled = NO;
    listBottomArr = @[@[@{}], @[@{@"img":@"wodezhangdan", @"title":@"我的订单", @"type":@""},@{@"img":@"wodepingu", @"title":@"我的评估", @"type":@""}, @{@"img":@"yinghangka", @"title":@"银行卡", @"type":@""}, @{@"img":@"xiaoxizhongxin", @"title":@"消息中心", @"type":@""}, @{@"img":@"yaoqinhaoou", @"title":@"邀请好友", @"type":@""}, @{@"img":@"youhuiquan", @"title":@"优惠劵", @"type":@""}, @{@"img":@"wenti", @"title":@"帮助中心", @"type":@""}, @{@"img":@"lianxikefu", @"title":@"联系客服", @"type":@""}]];
    
    // 设置
    UIButton * confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    confirmButton.frame = CGRectMake(0, 0, 40, 40);
    confirmButton.titleLabel.font = [UIFont systemFontOfSize:18 weight:30];
    [confirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [confirmButton setTitle:@"确定" forState:UIControlStateNormal];
    [confirmButton setImage:[UIImage imageNamed:@"shezhi"] forState:UIControlStateNormal];
    confirmButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [confirmButton addTarget:self action:@selector(confirmBtClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * right = [[UIBarButtonItem alloc]initWithCustomView:confirmButton];;
    self.navigationItem.rightBarButtonItem = right;
    
}
#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return listBottomArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *array =listBottomArr[section];
    return array.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 200;
    }
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 0)
    {
        HomeTopTableViewCell loadingTableViewCell("HomeTopTableViewCell", )
   
        return cell;
    }
    
    HomeMineTableViewCell loadingTableViewCell("HomeMineTableViewCell", )
    [cell reloadCellForData:listBottomArr[indexPath.section][indexPath.row]  VC:self indexPath:indexPath];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 1)
    {
        if(indexPath.row == 0)
        {
            myOrderViewController *vc = [TheGlobalMethod setstoryboard:@"myOrderViewController" controller:self];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
            
        }
        if(indexPath.row == 3)
        {
            MsgListVC *vc = [TheGlobalMethod setstoryboard:@"MsgListVC" controller:self];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
        if(indexPath.row == 4)
        {
            InvitationFriendsVC *vc = [TheGlobalMethod setstoryboard:@"InvitationFriendsVC" controller:self];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
        if(indexPath.row == 5)
        {
            CouponsListVC *vc = [TheGlobalMethod setstoryboard:@"CouponsListVC" controller:self];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }

}

-(void)confirmBtClick
{
    MKPSetViewController *vc = [TheGlobalMethod setstoryboard:@"MKPSetViewController" controller:self];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];

}

@end
