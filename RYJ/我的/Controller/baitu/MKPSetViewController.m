//
//  MKPSetViewController.m
//  RYJ
//
//  Created by wyp on 2018/1/16.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import "MKPSetViewController.h"
#import "JPUSHService.h"
#import "MKPSetTableViewCell.h"

@interface MKPSetViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    
}

@property(nonatomic,strong) UITableView *myTableView;
@property (nonatomic, strong) NSArray *defaultArray;

@end

@implementation MKPSetViewController

static NSString *const MKPCell = @"setCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"设置";
    self.view.backgroundColor = NAVCOLOR_C(242, 242, 242);
    [self createView];
}
//-(NSMutableArray *)defaultArray
//{
//    if(!self.defaultArray)
//    {
//        self.defaultArray = [NSMutableArray array];
//    }
//    return self.defaultArray;
//}

-(void)createView
{
   self.defaultArray =  [NSMutableArray array];
    self.defaultArray = @[@[@{@"leftText":@"修改密码",@"placeholder":@""},
                         @{@"leftText":@"意见反馈",@"placeholder":@""},
                         @{@"leftText":@"关于我们",@"placeholder":@""},
                         @{@"leftText":@"版本升级",@"placeholder":@""},
                         @{@"leftText":@"清除缓存",@"placeholder":@"0.0M"}]].mutableCopy;
    self.myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, H(10), self.view.width, self.view.height)];
    [self.myTableView registerNib:[UINib nibWithNibName:@"MKPSetTableViewCell" bundle:nil] forCellReuseIdentifier:MKPCell];
    self.myTableView.backgroundColor = [UIColor clearColor];
    self.myTableView.separatorStyle = 0;
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    [self.view addSubview:self.myTableView];
    
    // 退出
    UIView *tuichuView = [[UIView alloc]initWithFrame:CGRectMake(0, H(285), WIDTH_K, H(45))];
    tuichuView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:tuichuView];
    UITapGestureRecognizer *taptuichu = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tuichuTap)];
    [tuichuView addGestureRecognizer:taptuichu];

    UILabel *exitlabel = [[UILabel alloc]initWithFrame:CGRectMake(WIDTH_K /2 - W(20), H(10) , W(50), H(20))];
    exitlabel.text = @"退 出";
    exitlabel.textColor = [UIColor blackColor];
    [tuichuView addSubview:exitlabel];
    
}    

#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.defaultArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *rowArray =self.defaultArray[section];
    return rowArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MKPSetTableViewCell loadingTableViewCell("MKPSetTableViewCell",)
    [cell reloadCellForData:self.defaultArray[indexPath.section][indexPath.row] vc:self indexPath:indexPath];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return H(50);
}

-(void)tuichuTap
{
    [JXTAlertView showAlertViewWithTitle:@"提示" message:@"确定退出登录" cancelButtonTitle:nil buttonIndexBlock:^(NSInteger buttonIndex) {
        if (buttonIndex == 1) {
            [JPUSHService setTags:nil alias:@"" fetchCompletionHandle:^(int iResCode, NSSet *iTags, NSString *iAlias) {
                NSLog(@"iResCode:%d; iTags:%@; iAlias:%@", iResCode, iTags, iAlias);
            }];
            [TheGlobalMethod insertUserDefault:@"" Key:@"password"];
            [TheGlobalMethod insertUserDefault:@"" Key:@"token"];
            [self presentViewController:[TheGlobalMethod setstoryboard:@"loginNA" controller:self] animated:YES completion:nil];
        }
    } otherButtonTitles:@[@"取消", @"确定"]];
}
- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}



@end
