//
//  BorrowDetailVC.m
//  RYJ
//
//  Created by wyp on 2017/7/24.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "BorrowDetailVC.h"

//cells
#import "BorrowProgressCell.h"
#import "CenterBtnCell.h"
#import "leftAndRightLabel.h"
#import "DetailAgreeCell.h"
#import "AttLabelCell.h"
#import "DetailBottomBtnCell.h"

@interface BorrowDetailVC ()<UITableViewDelegate, UITableViewDataSource> {

    __weak IBOutlet RootTableView *myTableView;
    __weak IBOutlet NSLayoutConstraint *MY_B;
    
    NSDictionary *dataDic;
    NSMutableArray * leftArr;
    NSMutableArray * rightArr;
    NSString * aliPayCode;
    NSString * wechatCode;
}


@end

@implementation BorrowDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    myTableView.delegate = self;
    myTableView.dataSource = self;
        
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self getNetWorking];
        [self getPayWay];

    });
    // Do any additional setup after loading the view.
}

-(void)getPayWay{
    [HttpUrl GET:@"sysConfig/selectSysConfig" dict:nil hud:self.view isShow:NO WithSuccessBlock:^(id data) {
        NSLog(@"支付方式--%@",data);
        leftArr = [[NSMutableArray alloc]initWithCapacity:0];
        rightArr = [[NSMutableArray alloc]initWithCapacity:0];
        if (BB_isSuccess) {
            NSArray * arr = data[@"data"];
            for (int i = 0 ; i < arr.count ; i++) {
                NSDictionary * dict = arr[i];
                
                if ([dict[@"configKey"] isEqualToString:@"alipay"]) {
                    [leftArr addObject:@"支付宝"];
                    [rightArr addObject:dict[@"configValue"]];
                    
                }
                else if ([dict[@"configKey"] isEqualToString:@"wechatAccount"]) {
                    [leftArr addObject:@"微信"];
                    [rightArr addObject:dict[@"configValue"]];
                    
                }
                else if ([dict[@"configKey"] isEqualToString:@"debitCardNo"]) {
                    [leftArr addObject:@"储蓄卡号"];
                    [rightArr addObject:dict[@"configValue"]];
                    
                }
                else if ([dict[@"configKey"] isEqualToString:@"debitCardName"]) {
                    [leftArr addObject:@"储蓄卡名称"];
                    [rightArr addObject:dict[@"configValue"]];
                    
                }
                else if ([dict[@"configKey"] isEqualToString:@"customServiceQq"]) {
                    [leftArr addObject:@"客服QQ"];
                    [rightArr addObject:dict[@"configValue"]];
                    
                }
                else if ([dict[@"configKey"] isEqualToString:@"alipayCode"]) {
                    [leftArr addObject:@"支付宝付款码"];
                    [rightArr addObject:@"微信收款码"];
                    aliPayCode = dict[@"configValue"];
                    
                }
                if ([dict[@"configKey"] isEqualToString:@"wechatCode"]) {
                    [leftArr addObject:@"*必须标注姓名和手机号码"];
                    [rightArr addObject:@""];
                    wechatCode = dict[@"configValue"];
                }
            }
        }
    }];
}
- (void)getNetWorking {
    if (_orderId) {
        [HttpUrl GET:@"loanOrder/selectLoanDetail" dict:@{@"loanOrderId":_orderId} hud:self.view isShow:NO WithSuccessBlock:^(id data) {
            NSLog(@"详情信息:%@", data);
            if ([data[@"success"] integerValue] == 1) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    dataDic = data[@"data"];
                    if ([dataDic[@"loanOrder"][@"orderStatus"]  isEqualToString:@"逾期"]) {
                        _yuqi = @"1";
                    } else {
                        _yuqi = @"";
                    }
                    [self setBottomBtn];
                    [myTableView reloadData];
                });
                
            } else {
                [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
            }
        }];
    }
}

#pragma mark 设置下方btn
- (void)setBottomBtn {
    
    if ([dataDic[@"loanOrder"][@"orderStatus"] isEqualToString:@"审核中"] || [dataDic[@"loanOrder"][@"orderStatus"] isEqualToString:@"待打款"] || [dataDic[@"loanOrder"][@"orderStatus"] isEqualToString:@"审核失败"] ||[dataDic[@"loanOrder"][@"orderStatus"] isEqualToString:@"已还款"] || [dataDic[@"loanOrder"][@"orderStatus"] isEqualToString:@"坏账"] ) {
        MY_B.constant = 0;
    } else {
    
        DetailBottomBtnCell *view = [TheGlobalMethod setCell:@"DetailBottomBtnCell"];
//    [view reloadCellForData:dataDic vc:self];
        view.orderId = _orderId;
        [view reloadCellForData:dataDic vc:self leftArray:leftArr rightArray:rightArr aliCode:aliPayCode wechat:wechatCode];
//    view.btn.hidden = NO;
        view.frame = CGRectMake(0, HEIGHT_K-64-60, WIDTH_K, 60);
        [self.view addSubview:view];
    }
}

#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (!dataDic) {
        return 0;
    }
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 0;
    } else if (section == 1) {
        return 0;
    } else if (section == 2) {
        return 6;
    } else if (section == 3) {
        return 1;
    } else if (section == 4) {
        return 7;
    }
    if ([_yuqi isEqualToString:@"1"]) {
        return 1;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        BorrowProgressCell loadingTableViewCell("BorrowProgressCell", )
        [cell reloadCellForData:dataDic[@"loanOrder"] vc:self indexPath:indexPath];
        return cell;
    } else if (indexPath.section == 1) {
        CenterBtnCell loadingTableViewCell("CenterBtnCell", [cell reloadCellForData:@{} vc:self])
        return cell;
    } else if (indexPath.section == 5) {
//        if (indexPath.row == 0) {
//            leftAndRightLabel loadingTableViewCell("leftAndRightLabel", )
//            cell.bottomView.hidden = NO;
//            cell.leftLabel.text = @"交易协议";
//            return cell;
//        }
//        else if (indexPath.row == 1) {
//            DetailAgreeCell loadingTableViewCell("DetailAgreeCell", )
//            [cell reloadCellForData:dataDic[@"loanOrder"] vc:self];
//            return cell;
//        }
//        else
        if (indexPath.row == 0) {
            AttLabelCell loadingTableViewCell("AttLabelCell", )
            [cell reloadCellWithData:dataDic[@"loanOrder"] vc:self];
            return cell;
        }
    }
    leftAndRightLabel loadingTableViewCell("leftAndRightLabel", )
    cell.returnCenterBtnClick = ^{
        if (indexPath.section == 3) {
            [Methods showComprehensiveViewLeftArr:@[@"平台服务费", @"信息认证费",@"风控服务费", @"风险准备金", @"利息", @"总计"] rightArr:@[[self stringByAppendingString:[dataDic[@"loanOrder"][@"paramSetting"][@"placeServePercent"] description]], [self stringByAppendingString:[dataDic[@"loanOrder"][@"paramSetting"][@"msgAuthPercent"] description]],[self stringByAppendingString:[dataDic[@"loanOrder"][@"paramSetting"][@"riskServePercent"] description]], [self stringByAppendingString:[dataDic[@"loanOrder"][@"paramSetting"][@"riskPlanPercent"] description]], [self stringByAppendingString:[dataDic[@"loanOrder"][@"paramSetting"][@"interestPercent"] description]], [self stringByAppendingString:[dataDic[@"wateMoney"] description]]] bottomStatus:nil selfVC:self];
        }
    };
    NSMutableDictionary *ddict = [NSMutableDictionary dictionaryWithDictionary:dataDic];
    [ddict setObject:_yuqi forKey:@"yuqi"];
    [cell reloadDataForDetail:ddict vc:self indexPath:indexPath];
    return cell;
}

- (NSString *)stringByAppendingString:(NSString *)string {
    return [string stringByAppendingString:@"元"];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 60;
    } else if (indexPath.section == 1) {
        return 44;
    }
    if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            return 44;
        } else if (indexPath.row == 5) {
            return 36;
        }
        return 24;
    }  else if (indexPath.section == 4) {
        if (indexPath.row == 0 || indexPath.row == 6) {
            return 44;
        } else if (indexPath.row == 5) {
            if (![_yuqi isEqualToString:@"1"]) {
                return 0;
            }
            return 36;
        } else if (indexPath.row == 4) {
            if (![_yuqi isEqualToString:@"1"]) {
                return 36;
            }
        } else if (indexPath.row == 2) {
            return 0;
        }
        return 24;
    }
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, 10)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 1) {
        return 0.0;
    }
    return 10;
}

- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
