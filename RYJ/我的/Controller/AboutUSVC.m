//
//  AboutUSVC.m
//  RYJ
//
//  Created by wyp on 2017/7/26.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "AboutUSVC.h"

#import "AboutUSLogoCell.h"
#import "LabelImgCell.h"

#import "UserFeedbackVC.h"

#import "MessageDetailVC.h"
#import "ABtnCell.h"
#import "JPUSHService.h"

@interface AboutUSVC ()<UITableViewDelegate, UITableViewDataSource> {

    __weak IBOutlet RootTableView *myTableView;
    NSArray *dataArr;
    
    NSDictionary *dataDic;
    NSDictionary *textDic;
}

@end

@implementation AboutUSVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"设置";
    myTableView.delegate = self;
    myTableView.dataSource = self;
    myTableView.backgroundColor = [UIColor whiteColor];
    dataArr = @[@"关于我们", @"用户反馈", @"客服电话", @"官方微信"];
    [self requestNetWorking];
    [self requestAboutUS];
    // Do any additional setup after loading the view.
}

- (void)requestNetWorking {
    [HttpUrl GET:@"aboutXed/selectAboutUsRetails" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([data[@"success"] integerValue] == 1) {
            dataDic = data[@"data"];
            [myTableView reloadData];
        } else {
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
}

- (void)requestAboutUS {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [HttpUrl GET:@"agreement/selectOneByType" dict:@{@"type":@"4"} hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if ([data[@"success"] integerValue] == 1) {
            textDic = data[@"data"];
        } else {
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
}

#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 1) {
        return 1;
    }
    return dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        ABtnCell *cell = [TheGlobalMethod setCell:@"ABtnCell"];
        [cell.btn setTitle:@"退出登录" forState:UIControlStateNormal];
        [cell.btn addTarget:self action:@selector(exitLogin) forControlEvents:(UIControlEventTouchUpInside)];
        return cell;

    }
    LabelImgCell loadingTableViewCell("LabelImgCell", )
    cell.lineView_R.constant = 12;
    cell.label.text = dataArr[indexPath.row];
    if (indexPath.row == 2) {
        cell.rightLabel.hidden = NO;
        if (![dataDic isEqual:[NSNull null]]) {
            [Methods dataInText:dataDic[@"servicePhone"] InLabel:cell.rightLabel defaultStr:@""];

        }
    } else if (indexPath.row == 3) {
        cell.rightLabel.hidden = NO;
        if (![dataDic isEqual:[NSNull null]]) {
            [Methods dataInText:dataDic[@"wechatOfficial"] InLabel:cell.rightLabel defaultStr:@""];
        }
    }
    return cell;
}

- (void)exitLogin {
    [JXTAlertView showAlertViewWithTitle:@"提示" message:@"确定退出登录" cancelButtonTitle:nil buttonIndexBlock:^(NSInteger buttonIndex) {
        if (buttonIndex == 1) {
            [JPUSHService setTags:nil alias:@"" fetchCompletionHandle:^(int iResCode, NSSet *iTags, NSString *iAlias) {
                NSLog(@"iResCode:%d; iTags:%@; iAlias:%@", iResCode, iTags, iAlias);
            }];
            [TheGlobalMethod insertUserDefault:@"" Key:@"password"];
            [TheGlobalMethod insertUserDefault:@"" Key:@"token"];
            [self presentViewController:[TheGlobalMethod setstoryboard:@"loginNA" controller:self] animated:YES completion:nil];
        }
    } otherButtonTitles:@[@"取消", @"确定"]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
         
            MessageDetailVC *vc = [TheGlobalMethod setstoryboard:@"MessageDetailVC" controller:self];
                if (![textDic isEqual:[NSNull null]]) {
                    if (![textDic[@"content"] isEqual:[NSNull null]]) {
                        NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[textDic[@"content"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
                        vc.attStr = attrStr;
                    }else{
                        vc.detailStr = @"-";
                    }
                    
                } else {
                    vc.detailStr = @"-";
                }

            vc.titleStr = @"关于我们";
            [self.navigationController pushViewController:vc animated:YES];
        } else if (indexPath.row == 1) {
            UserFeedbackVC *vc = [TheGlobalMethod setstoryboard:@"UserFeedbackVC" controller:self];
            [self.navigationController pushViewController:vc animated:YES];
        }else if (indexPath.row == 2){
            NSMutableString *str =[[NSMutableString alloc] initWithFormat:@"telprompt://%@", dataDic[@"servicePhone"]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, 10)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
