//
//  NoPasteTF.m
//  MDD
//
//  Created by wyp on 2017/7/31.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "NoPasteTF.h"

@implementation NoPasteTF

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    if (menuController) {
        [UIMenuController sharedMenuController].menuVisible = NO;
    }
    return NO;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
