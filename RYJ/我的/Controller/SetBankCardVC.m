//
//  SetBankCardVC.m
//  RYJ
//
//  Created by wyp on 2017/7/25.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "SetBankCardVC.h"

#import "LabelTFLineCell.h"
#import "RightBtnCell.h"
#import "ABtnCell.h"

@interface SetBankCardVC ()<UITableViewDelegate, UITableViewDataSource> {

    __weak IBOutlet RootTableView *myTableView;
    
    NSMutableDictionary *dataDic;
    
}

@end

@implementation SetBankCardVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    
    dataDic = [NSMutableDictionary dictionary];
    // Do any additional setup after loading the view.
}

#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 4;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        LabelTFLineCell loadingTableViewCell("LabelTFLineCell", )
        [cell reloadCellForData:@{} vc:self indexPath:indexPath];
        return cell;
    } else if (indexPath.section == 1) {
        RightBtnCell loadingTableViewCell("RightBtnCell", )
        [cell reloadCellForData:@{} vc:self indexPath:indexPath];
        return cell;
    } else if (indexPath.section == 2) {
        LabelTFLineCell loadingTableViewCell("LabelTFLineCell", )
        [cell reloadCellForData:dataDic vc:self indexPath:indexPath];
        return cell;
    }
    ABtnCell *cell = [TheGlobalMethod setCell:@"ABtnCell"];
    [cell.btn addTarget:self action:@selector(up) forControlEvents:(UIControlEventTouchUpInside)];
    [cell.btn setTitle:@"提交" forState:(UIControlStateNormal)];
    return cell;
}

- (void)up {
    kNavPop
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 10;
    } else if (section == 3) {
        return 20;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, 20)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
