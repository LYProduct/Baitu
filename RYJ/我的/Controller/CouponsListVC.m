//
//  CouponsListVC.m
//  RYJ
//
//  Created by wyp on 2017/7/25.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "CouponsListVC.h"

#import "CouponsCell.h"

@interface CouponsListVC ()<UITableViewDelegate, UITableViewDataSource> {

    __weak IBOutlet RootTableView *myTableView;
    NSArray *dataArr;
}

@end

@implementation CouponsListVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self getNetWorking];
    });
    // Do any additional setup after loading the view.
}

- (void)getNetWorking {
    [HttpUrl GET:@"userCoupon/selectByUser" dict:nil hud:self.view isShow:NO WithSuccessBlock:^(id data) {
        NSLog(@"我的优惠券:%@", data);
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([data[@"success"] integerValue] == 1) {
                dataArr = data[@"data"];
                [myTableView reloadData];
            } else {
                [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
            }
        });
        
    }];
}

#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return dataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CouponsCell loadingTableViewCell("CouponsCell", [cell reloadCellForData:dataArr[indexPath.section] vc:self])
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([_selectCoupon isEqualToString:@"1"]) {
        self.returnSelectCoupon(dataArr[indexPath.section]);
        kNavPop
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 12;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, 10)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
