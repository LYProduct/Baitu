//
//  CouponsListVC.h
//  RYJ
//
//  Created by wyp on 2017/7/25.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CouponsListVC : UIViewController

@property (nonatomic, strong) NSString *selectCoupon;
@property (nonatomic, strong) void(^returnSelectCoupon)(NSDictionary *dic);

@end
