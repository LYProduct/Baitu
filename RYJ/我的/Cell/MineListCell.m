//
//  MineListCell.m
//  AnimalsLive
//
//  Created by wyp on 17/3/23.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "MineListCell.h"

#import "ListCell.h"

@interface MineListCell ()<UITableViewDelegate, UITableViewDataSource> {

    __weak IBOutlet RootTableView *myTableView;
    __weak IBOutlet NSLayoutConstraint *bottomView_L;
    NSArray *dataArr;
    __weak IBOutlet NSLayoutConstraint *bottomView_R;
    NSString *sectionTitle;
}

@end

@implementation MineListCell

@synthesize img;
@synthesize label;
@synthesize bottomView;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc indexPath:(NSIndexPath *)indexPath {
    dataArr = dict[@"array"];
    if (dict[@"section"]) {
        sectionTitle = dict[@"section"];
    }
    
    myTableView.delegate = self;
    myTableView.dataSource = self;
    [myTableView reloadData];
//    img.image = [UIImage imageNamed:dict[@"img"]];
//    label.text = dict[@"title"];
//    if (indexPath.section == 1 && indexPath.row == 2) {
//        bottomView_L.constant = 0;
//        bottomView_R.constant = 0;
//    } else if (indexPath.section == 2 && indexPath.row == 0) {
//        bottomView_L.constant = 0;
//        bottomView_R.constant = 0;
//    } else if (indexPath.section == 3 && indexPath.row == 3) {
//        bottomView_L.constant = 0;
//        bottomView_R.constant = 0;
//    }

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ListCell loadingTableViewCell("ListCell", )
    cell.label.text = dataArr[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.returnSelectRow(indexPath.row);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    ListCell *view = [TheGlobalMethod setCell:@"ListCell"];
    view.backgroundColor = [UIColor whiteColor];
    view.frame = CGRectMake(0, 0, WIDTH_K-24, 30);
    view.label.font = [UIFont systemFontOfSize:12];
    view.label.textColor = NAVCOLOR_C(153.0, 153.0, 153.0);
    view.rightImg.hidden = YES;
    view.label.text = sectionTitle;
    return view;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
