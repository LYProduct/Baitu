//
//  MKPSetTableViewCell.h
//  RYJ
//
//  Created by wyp on 2018/1/16.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MKPSetTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;
- (void)reloadCellForData:(NSDictionary *)dict VC:(UIViewController *)VC indexPath:(NSIndexPath *)indexPath;
@end
