//
//  HomeMineTableViewCell.m
//  RYJ
//
//  Created by wyp on 2018/1/11.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import "HomeMineTableViewCell.h"

@implementation HomeMineTableViewCell

@synthesize image;
@synthesize lefLabel;
@synthesize jiantouImage;
@synthesize lineBottom;
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)reloadCellForData:(NSDictionary *)dict VC:(UIViewController *)VC indexPath:(NSIndexPath *)indexPath {
if(indexPath.section == 0)
{
    image.image = nil;
    lefLabel.text = nil;
    jiantouImage.image = nil;
    lineBottom.alpha = 0;
}
    if(indexPath.section == 1)
    {
    image.image = [UIImage imageNamed:dict[@"img"]];
    lefLabel.text = dict[@"title"];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
