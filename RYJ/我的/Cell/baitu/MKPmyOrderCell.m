//
//  myOrderCell.m
//  RYJ
//
//  Created by wyp on 2018/1/15.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import "MKPmyOrderCell.h"
#import "myOrderModel.h"

@interface MKPmyOrderCell()

@end

@implementation MKPmyOrderCell

@synthesize piceLabel;
@synthesize statusLabel;
@synthesize phoneLabel;
@synthesize SerialNumberLabel;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setModel:(myOrderModel *)model
{
    _model = model;
    SerialNumberLabel.text = [NSString stringWithFormat:@"回租编号:%@",model.orderNumber];
    if(isStringEmpty(model.evaluation[@"name"]))
    {
        phoneLabel.text = @"";
    }
    else
    {
        phoneLabel.text = model.evaluation[@"name"];
    }
    if(isStringEmpty([NSString stringWithFormat:@"%@",model.evaluation[@"evaluationPrice"]]))
    {
        piceLabel.text = @"";
    }
    else
    {
        piceLabel.text = [NSString stringWithFormat:@"%@",model.evaluation[@"evaluationPrice"]];
    }
    if([model.evaluation[@"status"] isEqualToString:@"待审核"] || [model.evaluation[@"status"] isEqualToString:@"已完结"])
    {
        if([model.evaluation[@"status"] isEqualToString:@"待审核"])
        {
            statusLabel.text = @"待审核";
            statusLabel.textColor = [[UIColor blueColor] colorWithAlphaComponent:0.5];
        }
        else
        {
            statusLabel.textColor = NAVCOLOR_C(242, 242, 242);
            statusLabel.text = @"已完结";
        }
    }
    else
    {
        statusLabel.text = model.status;
        statusLabel.textColor = [[UIColor blueColor]colorWithAlphaComponent:0.5];
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
