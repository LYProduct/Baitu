//
//  HomeMineTableViewCell.h
//  RYJ
//
//  Created by wyp on 2018/1/11.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeMineTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *lefLabel;
@property (weak, nonatomic) IBOutlet UIImageView *jiantouImage;
@property (weak, nonatomic) IBOutlet UILabel *lineBottom;

- (void)reloadCellForData:(NSDictionary *)dict VC:(UIViewController *)VC indexPath:(NSIndexPath *)indexPath;

@end
