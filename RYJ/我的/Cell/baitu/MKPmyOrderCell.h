//
//  myOrderCell.h
//  RYJ
//
//  Created by wyp on 2018/1/15.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "myOrderModel.h"

@interface MKPmyOrderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *SerialNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *piceLabel;
@property (nonatomic,strong) myOrderModel *model;

@end
