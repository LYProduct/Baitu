//
//  HomeTopTableViewCell.m
//  RYJ
//
//  Created by wyp on 2018/1/12.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import "HomeTopTableViewCell.h"

@implementation HomeTopTableViewCell

@synthesize PhoneLabel;

- (void)awakeFromNib {
    [super awakeFromNib];
    
    PhoneLabel.text = [TheGlobalMethod getUserDefault:@"phone"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
