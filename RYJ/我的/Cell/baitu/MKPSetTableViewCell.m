//
//  MKPSetTableViewCell.m
//  RYJ
//
//  Created by wyp on 2018/1/16.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import "MKPSetTableViewCell.h"

@interface MKPSetTableViewCell()


@end

@implementation MKPSetTableViewCell

@synthesize leftLabel;
@synthesize rightLabel;

- (void)reloadCellForData:(NSDictionary *)dict VC:(UIViewController *)VC indexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0||indexPath.row == 1||indexPath.row == 2||indexPath.row == 3)
    {
        rightLabel.alpha = 0;
    }
    else if(indexPath.row == 4)
    {
        rightLabel.alpha = 1;
        rightLabel.text = dict[@"placeholder"];
    }
    leftLabel.text = dict[@"leftText"];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];


}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
