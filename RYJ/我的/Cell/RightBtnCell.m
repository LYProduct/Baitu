//
//  RightBtnCell.m
//  RYJ
//
//  Created by wyp on 2017/7/25.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "RightBtnCell.h"

#import "ComprehensiveView.h"

@interface RightBtnCell () {
    UIViewController *theVC;
}

@end

@implementation RightBtnCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc indexPath:(NSIndexPath *)indexPath {
    theVC = vc;
}

- (IBAction)btnClick:(UIButton *)sender {
    ComprehensiveView *comprehensiveView = [TheGlobalMethod setCell:@"ComprehensiveView"];
    comprehensiveView.frame = CGRectMake(0, 0, WIDTH_K, HEIGHT_K);
    [comprehensiveView reloadCellForData:@{@"L":@[@"农业银行", @"工商银行",@"交通银行",@"邮储银行", @"浦发银行",@"广发银行",@"平安银行", @"招商银行",@"中国银行",@"建设银行", @"光大银行",@"兴业银行",@"中信银行", @"华夏银行",@"杭州银行",@"北京银行", @"浙商银行",@"上海银行"]} vc:theVC];
    comprehensiveView.titleLabel.text = @"支持银行";
    comprehensiveView.bottomStatus = @"1";
    [[UIApplication sharedApplication].keyWindow addSubview:comprehensiveView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
