//
//  PopThreeView.m
//  RYJ
//
//  Created by wyp on 2017/7/25.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "PopThreeView.h"

@interface PopThreeView () {
    UIViewController *theVC;
}

@end

@implementation PopThreeView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc {
    theVC = vc;
}

- (IBAction)btnClick:(UIButton *)sender {
    NSString *shareUrl = [NSString stringWithFormat:@"http://app.hzrywl.com/share/?type=iOS&uuid=%@", [TheGlobalMethod getUserDefault:@"uuid"]];
    if (sender.tag == 101) {
        [Methods share:theVC descr:@"百途邀请您注册" url:shareUrl platformType:1];
    } else if (sender.tag == 102) {
        [Methods share:theVC descr:@"百途邀请您注册" url:shareUrl platformType:2];
    } else if (sender.tag == 103) {
        [Methods share:theVC descr:@"百途邀请您注册" url:shareUrl platformType:0];
    } else if (sender.tag == 104) {
        [Methods share:theVC descr:@"百途邀请您注册" url:shareUrl platformType:4];
    } else if (sender.tag == 105) {
        [Methods share:theVC descr:@"百途邀请您注册" url:shareUrl platformType:5];
    }
}

- (IBAction)exit:(UIButton *)sender {
    [self removeFromSuperview];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
