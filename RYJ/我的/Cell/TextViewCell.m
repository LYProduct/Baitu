//
//  TextViewCell.m
//  RYJ
//
//  Created by wyp on 2017/7/24.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "TextViewCell.h"

@interface TextViewCell () {
    
}

@end

@implementation TextViewCell

@synthesize myTextView;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    myTextView.delegate = self;
}

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc {
    // 添加输入改变Block回调.
    [myTextView addTextDidChangeHandler:^(FSTextView *textView) {
        NSLog(@"text:%@", textView.text);
        self.returnTextViewText(textView.text);
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
