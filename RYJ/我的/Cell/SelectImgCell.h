//
//  SelectImgCell.h
//  RYJ
//
//  Created by wyp on 2017/7/24.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectImgCell : UITableViewCell

@property (nonatomic, strong) void(^returnImgArr)(NSArray *imgArr);

@end
