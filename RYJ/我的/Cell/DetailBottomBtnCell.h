//
//  DetailBottomBtnCell.h
//  RYJ
//
//  Created by wyp on 2017/7/24.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailBottomBtnCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btn;
- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc leftArray:(NSMutableArray * )leftArr rightArray:(NSMutableArray *)rightArr aliCode:(NSString *)aliCode wechat:(NSString *)wechatColde;
@property(nonatomic,strong)NSString * orderId;
@end
