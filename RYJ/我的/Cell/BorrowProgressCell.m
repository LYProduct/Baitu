//
//  BorrowProgressCell.m
//  RYJ
//
//  Created by wyp on 2017/7/24.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "BorrowProgressCell.h"

@interface BorrowProgressCell () {
    
    __weak IBOutlet UIView *lineView;
}

@end

@implementation BorrowProgressCell

@synthesize topView;
@synthesize topLabel;
@synthesize bottomView;
@synthesize bottomLabel;
@synthesize img;
@synthesize rightLabel;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc indexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        [self dealWithOneRow:dict];
    } else if (indexPath.row == 1) {
        [self dealWithTwoRow:dict];
    } else if (indexPath.row == 2) {
        [self dealWithThreeRow:dict];
    } else if (indexPath.row == 3) {
        [self dealWithFourRow:dict];
    }
    
}

#pragma mark 借款状态界面上分为四行

#pragma mark 第一行处理
- (void)dealWithOneRow:(NSDictionary *)dict {
    topView.hidden = YES;
//    if ([dict[@"orderStatus"] isEqualToString:@"审核中"]) {
        bottomView.backgroundColor = appDefaultColor;
        img.image = [UIImage imageNamed:@"dangqian"];
        [Methods dataInText:dict[@"gmtDatetime"] InLabel:rightLabel defaultStr:@""];
//    }
}

#pragma mark 第二行处理
- (void)dealWithTwoRow:(NSDictionary *)dict {
    topLabel.text = @"审核通过";
    bottomLabel.text = @"恭喜您通过风控审核";
    [Methods dataInText:dict[@"passTime"] InLabel:rightLabel defaultStr:@""];
    
    NSString * status = dict[@"orderStatus"];
    topView.backgroundColor = appDefaultColor;
    if ([status isEqualToString:@"待打款"]) {
        topLabel.text = @"审核中";
        bottomLabel.text = @"我们会尽快处理您的借款申请";
    } else if ([status isEqualToString:@"审核失败"]) {
        topLabel.text = @"审核失败";
        bottomLabel.text = @"很遗憾,您申请的此次借款未通过审核";
    } else {
        img.image = [UIImage imageNamed:@"dangqian"];
        bottomView.backgroundColor = appDefaultColor;
    }
}

#pragma mark 第三行处理
- (void)dealWithThreeRow:(NSDictionary *)dict {
    topLabel.text = @"打款成功";
    if (![dict[@"bankCardNum"] isEqual:[NSNull null]]) {
        if ([[dict[@"bankCardNum"] description] length] >= 4) {
            bottomLabel.text = [NSString stringWithFormat:@"打款至%@(%@)", dict[@"bankName"], [[dict[@"bankCardNum"] description] substringFromIndex:[[dict[@"bankCardNum"] description] length]-4]];
        }
    }else{
        bottomLabel.text = @"打款成功";
    }
    [Methods dataInText:dict[@"giveTime"] InLabel:rightLabel defaultStr:@""];

    
    NSString * status = dict[@"orderStatus"];
    if (![status isEqualToString:@"审核失败"]) {
        if ([status isEqualToString:@"待还款"]) {
            topView.backgroundColor = appDefaultColor;
            img.image = [UIImage imageNamed:@"dangqian"];
        }
        if ([status isEqualToString:@"已还款"]) {
            topView.backgroundColor = appDefaultColor;
            img.image = [UIImage imageNamed:@"dangqian"];
        }
    }
//    if (status >= 2 && status != 7) {
//        topView.backgroundColor = appDefaultColor;
//    }
//    if (status >= 3 && status != 7) {
//        img.image = [UIImage imageNamed:@"dangqian"];
//    }
//    if (status == 6) {
//        bottomView.backgroundColor = appDefaultColor;
//    }
}

#pragma mark 第四行处理
- (void)dealWithFourRow:(NSDictionary *)dict {
    topLabel.text = @"已还款";
    bottomLabel.text = @"还款成功";
    [Methods dataInText:dict[@"realPayTime"] InLabel:rightLabel defaultStr:@""];
    bottomView.hidden = YES;
    lineView.hidden = NO;
    
    NSString * status = dict[@"orderStatus"];
    if ([status isEqualToString:@"已还款"]) {
        topView.backgroundColor = appDefaultColor;
        img.image = [UIImage imageNamed:@"dangqian"];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
