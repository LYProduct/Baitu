//
//  FeedBackSelectTypeCell.m
//  RYJ
//
//  Created by wyp on 2017/7/26.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "FeedBackSelectTypeCell.h"

#import "FeedBackCCell.h"

@interface FeedBackSelectTypeCell ()<UICollectionViewDelegate, UICollectionViewDataSource> {

    __weak IBOutlet UICollectionView *myCollectionView;
    
    UIViewController *theVC;
    NSArray *dataArr;
    
    NSInteger selectIndex;
}

@end

@implementation FeedBackSelectTypeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc {
    theVC = vc;
    selectIndex = 0;
    
    dataArr = @[@"认证", @"借款", @"还款", @"体验与界面", @"其它"];
    
    [self setLayout];
    [self setCollectionView];
    
}

- (void)setLayout {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    //设置每一个item大小
    
    layout.itemSize = CGSizeMake(WIDTH_K/3, WIDTH_K/3);
    //设置行间距
    layout.minimumLineSpacing = 8;
    //设置最小行间距
    layout.minimumInteritemSpacing = 8;
    //设置分区的缩进量
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 8);
    
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    myCollectionView.collectionViewLayout = layout;
}

- (void)setCollectionView {
    
    
    //设置collectionView的属性
    myCollectionView.delegate = self;
    myCollectionView.dataSource = self;
    [myCollectionView registerNib:[UINib nibWithNibName:@"FeedBackCCell" bundle:nil] forCellWithReuseIdentifier:@"cellID"];
    myCollectionView.showsVerticalScrollIndicator = NO;
    myCollectionView.showsHorizontalScrollIndicator = NO;
    myCollectionView.backgroundColor = [UIColor clearColor];
}

//有几个区
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

//每个区有几个item
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return dataArr.count;
}
//重用cell
#pragma mark CollectionSelfCell
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    FeedBackCCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellID" forIndexPath:indexPath];
    cell.Label.text = dataArr[indexPath.row];
    if (selectIndex == indexPath.row) {
        cell.Label.backgroundColor = appDefaultColor;
        cell.Label.textColor = [UIColor whiteColor];
        cell.Label.layer.borderUIColor = [UIColor clearColor];
    } else {
        cell.Label.textColor = NAVCOLOR_C(102.0, 102.0, 102.0);
        cell.Label.backgroundColor = [UIColor whiteColor];
        cell.Label.layer.borderUIColor = NAVCOLOR_C(229.0, 229.0, 229.0);
    }
    return cell;
}


//选择了某个cell
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    selectIndex = indexPath.row;
    [myCollectionView reloadData];
    NSLog(@"%ld---", indexPath.row);
    self.clickIndex(indexPath.row);
}

- (CGSize) collectionView:(UICollectionView *)collectionView
 layout:(UICollectionViewLayout *)collectionViewLayout
 sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize sizef = [[dataArr[indexPath.row] stringByAppendingString:@"  "] boundingRectWithSize:CGSizeMake(WIDTH_K, 30) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingTruncatesLastVisibleLine attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil].size;
     return CGSizeMake(sizef.width, 30);
}

//每个item之间的间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 8;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
