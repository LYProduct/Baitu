//
//  LabelTFLineCell.h
//  RYJ
//
//  Created by wyp on 2017/7/25.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LabelTFLineCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineView_R;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineView_L;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UITextField *TF;
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TF_R;
@property (weak, nonatomic) IBOutlet UIButton *getBtn;

/** 个人信息认证 */
- (void)reloadCellForCer:(NSDictionary *)dict vc:(UIViewController *)vc indexPath:(NSIndexPath *)indexPath;


/** 社保认证 */
- (void)reloadCellForSocial:(NSDictionary *)dict vc:(UIViewController *)vc indexPath:(NSIndexPath *)indexPath;

@end
