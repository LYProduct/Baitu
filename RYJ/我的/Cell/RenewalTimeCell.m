//
//  RenewalTimeCell.m
//  RYJ
//
//  Created by wyp on 2017/7/24.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "RenewalTimeCell.h"

@interface RenewalTimeCell () {
    NSInteger index;
}

@end

@implementation RenewalTimeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    NSLog(@"打印");
    index = 7;
}

- (IBAction)btnClick:(UIButton *)sender {
    if (sender.tag == index) {
        return;
    }
    sender.selected = !sender.isSelected;
    sender.layer.borderUIColor = appDefaultColor;
    
    [(UIButton *)[self viewWithTag:index] setSelected:NO];
    [[(UIButton *)[self viewWithTag:index] layer] setBorderUIColor:NAVCOLOR_C(229.0, 229.0, 229.0)];
    index = sender.tag;
    self.returnClick(index);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
