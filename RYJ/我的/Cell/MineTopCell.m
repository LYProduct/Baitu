//
//  MineTopCell.m
//  RYJ
//
//  Created by wyp on 2017/7/21.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "MineTopCell.h"

#import "YLLView.h"

#import "KTWaveView.h"
#import "AboutUSVC.h"

@interface MineTopCell () {
    UIViewController *theVC;
}

@property (nonatomic, strong) YLLView *headerView;

@end

@implementation MineTopCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
}
+ (instancetype)tableViewCellWithTableView:(UITableView *)tableView{
    static NSString *ID = @"MineTopCell";
    MineTopCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[MineTopCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    return cell;
}
- (IBAction)setting:(UIButton *)sender {
    if (_block) {
        _block();
    }
//    AboutUSVC *vc = GETStoryboard(@"AboutUSVC");
//    vc.hidesBottomBarWhenPushed = YES;
//    [theVC.navigationController pushViewController:vc animated:YES];
}

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc {
    theVC = vc;
//    [self.contentView addSubview:self.headerView];
    NSString * phone = [NSString stringWithFormat:@"%@",dict[@"user"][@"phone"]];
    _phoneLabel.text = phone;
    if ([phone length] > 7) {
        _phoneLabel.text = [phone stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
    }
    _nameLab.text = isStringEmpty(dict[@"user"][@"userName"]) == YES ? _phoneLabel.text:dict[@"user"][@"userName"];
    _canBorrowMoney.text = [NSString stringWithFormat:@"%ld",[dict[@"money"] integerValue] - [dict[@"needPayMoney"] integerValue]];
    _allMoney.text = [NSString stringWithFormat:@"总额度:%@",[dict[@"money"] description]];
//            NSDictionary *authDic = dict;
//            if ([authDic[@"baiscAuth"] integerValue] == 0 || [authDic[@"bankAuth"] integerValue] == 0 || [authDic[@"phoneAuth"] integerValue] == 0 || [authDic[@"identityAuth"] integerValue] == 0 || [authDic[@"taobaoAuth"] integerValue] == 0) {
//                [_status setTitle:@"未认证" forState:(UIControlStateNormal)];
//            } else {
//                [_status setTitle:@"已认证" forState:(UIControlStateNormal)];
//            }
      
}


- (YLLView *)headerView{
    if (!_headerView) {
        _headerView = [[YLLView alloc] initWithFrame:CGRectMake(0, 200, WIDTH_K, 40)];
        _headerView.waveHeight = 10;
//        _headerView.backgroundColor = appGrayColor;
        _headerView.yllBlock = ^(CGFloat currentY){
            
        };
        [_headerView startWaveAnimation];
    }
    return _headerView;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
