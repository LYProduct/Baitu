//
//  LabelTFLineCell.m
//  RYJ
//
//  Created by wyp on 2017/7/25.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "LabelTFLineCell.h"

@implementation LabelTFLineCell

@synthesize lineView;
@synthesize lineView_L;
@synthesize lineView_R;
@synthesize leftLabel;
@synthesize rightBtn;
@synthesize TF;

#pragma mark 银行卡管理
- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc indexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        NSArray *leftArr = @[@"姓名", @"身份证", @"储蓄卡", @""];
        leftLabel.text = leftArr[indexPath.row];
        if (indexPath.row == 0) {
            TF.text = @"*芳";
        } else if (indexPath.row == 1) {
            TF.text = @"**************6652";
        } else if (indexPath.row == 2) {
            TF.text = @"***********5569";
            if ([dict[@"bankname"] length] == 0) {
                lineView_L.constant = 0;
            }
            rightBtn.hidden = NO;
        } else if (indexPath.row == 3) {
            leftLabel.hidden = YES;
            TF.text = @"农业银行";
            TF.userInteractionEnabled = NO;
            lineView_L.constant = 0;
        }
    } else if (indexPath.section == 2) {
        leftLabel.text = @"预留手机";
        TF.text = @"151****0680";
        lineView_L.constant = 0;
    }
}

#pragma mark 认证
- (void)reloadCellForCer:(NSDictionary *)dict vc:(UIViewController *)vc indexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        NSArray *leftArr = @[@"基本信息", @"婚姻状况", @"学历", @"现居住地",@"",@""];//, @"婚姻状况", @"学历",
        TF.userInteractionEnabled = NO;
        leftLabel.textColor = NAVCOLOR_C(102.0, 102.0, 102.0);
        rightBtn.hidden = NO;
        [rightBtn setImage:[UIImage imageNamed:@"huise-youjiantou"] forState:(UIControlStateNormal)];
        leftLabel.text = leftArr[indexPath.row];
        leftLabel.font = FONT(14);
        if (indexPath.row == 0) {
            leftLabel.textColor = NAVCOLOR_C(51.0, 51.0, 51.0);
            TF.hidden = YES;
            rightBtn.hidden = YES;
            lineView_L.constant = 0;
            leftLabel.font = FONT(16);
        }
        else if (indexPath.row == 1) {
            TF.placeholder = @"请选择婚姻状况";
        } else if (indexPath.row == 2) {
            TF.placeholder = @"请选择学历";
        }
        else if (indexPath.row == 3) {
            TF.placeholder = @"请选择";
        } else if (indexPath.row == 4) {
            TF.placeholder = @"请选择";
        } else if (indexPath.row == 5) {
            TF.placeholder = @"请选择";
        }
    } 
}


#pragma mark 社保认证
- (void)reloadCellForSocial:(NSDictionary *)dict vc:(UIViewController *)vc indexPath:(NSIndexPath *)indexPath {
    NSArray *leftArr = @[@"姓名", @"身份证号", @"所在城市", @"查询方式", @"用户名", @"密码"];
    NSArray *rightPlArr = @[@"请输入姓名", @"请输入身份证号", @"请选择所在城市", @"请选择查询方式", @"请输入您的用户名", @"请输入密码"];
    leftLabel.text = leftArr[indexPath.row];
    TF.placeholder = rightPlArr[indexPath.row];
    if (indexPath.row == 2) {
        TF.userInteractionEnabled = NO;
        rightBtn.hidden = NO;
        [rightBtn setImage:[UIImage imageNamed:@"huise-youjiantou"] forState:(UIControlStateNormal)];
    } else if (indexPath.row == 3) {
        TF.userInteractionEnabled = NO;
        rightBtn.hidden = NO;
        [rightBtn setImage:[UIImage imageNamed:@"huise-youjiantou"] forState:(UIControlStateNormal)];
    } else if (indexPath.row == 5) {
        lineView_L.constant = 0;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
