//
//  MineTopCell.h
//  RYJ
//
//  Created by wyp on 2017/7/21.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^SettingBlock)();

@interface MineTopCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UILabel *canBorrowMoney;
@property (weak, nonatomic) IBOutlet UILabel *allMoney;

@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UIButton *status;

@property(nonatomic,copy)SettingBlock block;
+ (instancetype)tableViewCellWithTableView:(UITableView *)tableView;
@end
