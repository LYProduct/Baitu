//
//  TextViewCell.h
//  RYJ
//
//  Created by wyp on 2017/7/24.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSTextView.h"

@interface TextViewCell : UITableViewCell<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TV_L;

@property (weak, nonatomic) IBOutlet FSTextView *myTextView;
@property (weak, nonatomic) IBOutlet UIView *lineView;

@property (nonatomic, strong) void(^returnTextViewText)(NSString *string);

@end
