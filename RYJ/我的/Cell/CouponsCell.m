//
//  CouponsCell.m
//  RYJ
//
//  Created by wyp on 2017/7/25.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "CouponsCell.h"

@interface CouponsCell () {
    
    __weak IBOutlet UILabel *money;
    __weak IBOutlet UILabel *title;
    __weak IBOutlet UILabel *time;
    
}

@end

@implementation CouponsCell

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc {
    [Methods dataInText:dict[@"saveMoney"] InLabel:money defaultStr:@"-"];
    [Methods dataInText:dict[@"coupon"][@"coupouName"] InLabel:title defaultStr:@"-"];
    [Methods dataInText:[dict[@"pastDatetime"] substringToIndex:10] InLabel:time defaultStr:@"-"];
    NSLog(@"优惠券：：%@", dict);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
