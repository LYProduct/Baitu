//
//  CenterBtnCell.m
//  RYJ
//
//  Created by wyp on 2017/7/24.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "CenterBtnCell.h"

#import "BorrowFeedbackVC.h"

@interface CenterBtnCell () {
    UIViewController *theVC;
    
}

@end

@implementation CenterBtnCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc {
    theVC = vc;
}
- (IBAction)click:(UIButton *)sender {
    BorrowFeedbackVC *vc = [TheGlobalMethod setstoryboard:@"BorrowFeedbackVC" controller:theVC];
    [theVC.navigationController pushViewController:vc animated:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
