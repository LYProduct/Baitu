//
//  DetailAgreeCell.m
//  RYJ
//
//  Created by wyp on 2017/7/24.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "DetailAgreeCell.h"
#import "MessageDetailVC.h"

@interface DetailAgreeCell (){
    NSDictionary *dataDic;
    UIViewController *theVC;
}

@end

@implementation DetailAgreeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc {
    dataDic = dict;
    theVC = vc;
}
- (IBAction)leftBtn:(UIButton *)sender {
    [HttpUrl GET:@"agreement/selectOneByType" dict:@{@"type":@"1"} hud:theVC.view isShow:YES WithSuccessBlock:^(id data) {
        NSLog(@"%@", data);
        if (BB_isSuccess) {
            if (![data[@"data"] isEqual:[NSNull null]]) {
                MessageDetailVC *vc = [TheGlobalMethod setstoryboard:@"MessageDetailVC" controller:theVC];
                //    vc.detailStr = listArr[indexPath.row][@"content"];
                vc.titleStr = @"借款协议";
                NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[data[@"data"][@"content"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
                vc.attStr = attrStr;
                vc.detailStr = @"";
                [theVC.navigationController pushViewController:vc animated:YES];
            }
        }else{
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:theVC];
        }
    }];
    
//    if (![dataDic[@"agreementUrl"] isEqual:[NSNull null]]) {
//        [Methods pushUrl:dataDic[@"agreementUrl"] seleVC:theVC];
//    };
}
- (IBAction)rightBtn:(UIButton *)sender {
//    if (![dataDic[@"agreementTwoUrl"] isEqual:[NSNull null]]) {
//        [Methods pushUrl:dataDic[@"agreementTwoUrl"] seleVC:theVC];
//    }
    [HttpUrl GET:@"agreement/selectOneByType" dict:@{@"type":@"2"} hud:theVC.view isShow:YES WithSuccessBlock:^(id data) {
        NSLog(@"%@", data);
        if (BB_isSuccess) {
            if (![data[@"data"] isEqual:[NSNull null]]) {
                MessageDetailVC *vc = [TheGlobalMethod setstoryboard:@"MessageDetailVC" controller:theVC];
                //    vc.detailStr = listArr[indexPath.row][@"content"];
                vc.titleStr = @"借款服务协议";
                NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[data[@"data"][@"content"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
                vc.attStr = attrStr;
                vc.detailStr = @"";
                [theVC.navigationController pushViewController:vc animated:YES];
            }
        }else{
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:theVC];
        }
        
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
