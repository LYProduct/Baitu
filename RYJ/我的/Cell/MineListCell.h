//
//  MineListCell.h
//  AnimalsLive
//
//  Created by wyp on 17/3/23.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MineListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *img;

@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIView *bottomView;



@property (nonatomic, strong) void(^returnSelectRow)(NSInteger row);

@end
