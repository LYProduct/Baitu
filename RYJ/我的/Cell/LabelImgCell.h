//
//  LabelImgCell.h
//  RYJ
//
//  Created by wyp on 2017/7/25.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LabelImgCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIImageView *rightImg;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineView_L;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineView_R;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;

@end
