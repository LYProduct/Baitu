//
//  DetailBottomBtnCell.m
//  RYJ
//
//  Created by wyp on 2017/7/24.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "DetailBottomBtnCell.h"

#import "RenewalVC.h"
#import "RenewalPayVC.h"

@interface DetailBottomBtnCell () {
    UIViewController *theVC;
    NSDictionary *dataDic;
    NSMutableArray * _leftArr;
    NSMutableArray * _rightArr;
    NSString * _alipayCode;
    NSString * _wechatCode;
    
    NSString * _oderId;
}

@end

@implementation DetailBottomBtnCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc leftArray:(NSMutableArray * )leftArr rightArray:(NSMutableArray *)rightArr aliCode:(NSString *)aliCode wechat:(NSString *)wechatColde{
    theVC = vc;
    dataDic = dict;
    _leftArr = leftArr;
    _rightArr = rightArr;
    _alipayCode = aliCode;
    _wechatCode = wechatColde;
}
- (IBAction)xuqi:(UIButton *)sender {
    RenewalVC *vc = [TheGlobalMethod setstoryboard:@"RenewalVC" controller:theVC];
    vc.orderID = [dataDic[@"loanOrder"][@"id"] description];
    vc.limitDays = [dataDic[@"loanOrder"][@"limitDays"] description];
    [theVC.navigationController pushViewController:vc animated:YES];
}
-(void)setOrderId:(NSString *)orderId{
    _oderId = orderId;
}
- (IBAction)huankuan:(UIButton *)sender {
    [HttpUrl GET:@"loanOrder/moneyBackStatus" dict:@{@"orderId":_oderId} hud:theVC.view isShow:NO WithSuccessBlock:^(id data) {
        
    }];
    [Methods showAlertForPayLeftArr:_leftArr rightArr:_rightArr bottomStatus:@"3" selfVC:theVC titleLabelText:@"付款方式(可长按复制)" titleLabelTextColor:appDefaultColor alicode:_alipayCode wechat:_wechatCode];
//    RenewalPayVC *vc = [TheGlobalMethod setstoryboard:@"RenewalPayVC" controller:theVC];
//    vc.isBorrow = YES;
//    [theVC.navigationController pushViewController:vc animated:YES];
}

- (IBAction)yuqihuankuan:(UIButton *)sender {
    [HttpUrl GET:@"loanOrder/moneyBackStatus" dict:@{@"orderId":_oderId} hud:theVC.view isShow:NO WithSuccessBlock:^(id data) {
        
    }];
    [Methods showAlertForPayLeftArr:_leftArr rightArr:_rightArr bottomStatus:@"3" selfVC:theVC titleLabelText:@"付款方式(可长按复制)" titleLabelTextColor:appDefaultColor alicode:_alipayCode wechat:_wechatCode];
//    RenewalPayVC *vc = [TheGlobalMethod setstoryboard:@"RenewalPayVC" controller:theVC];
//    vc.isBorrow = YES;
//    [theVC.navigationController pushViewController:vc animated:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
