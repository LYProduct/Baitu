//
//  BorrowListCell.m
//  RYJ
//
//  Created by wyp on 2017/7/24.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "BorrowListCell.h"

@interface BorrowListCell () {
    
    __weak IBOutlet UILabel *number;
    __weak IBOutlet UILabel *state;
    __weak IBOutlet UILabel *money;
    __weak IBOutlet UILabel *payMoney;
    __weak IBOutlet UILabel *day;
    
    
}

@end

@implementation BorrowListCell

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc {
    NSLog(@"单个借款详情:%@", dict);
    [Methods dataInText:dict[@"orderNumber"] InLabel:number defaultStr:@"-"];
    number.text = [@"借款编号:" stringByAppendingString:number.text];
    if (![dict[@"orderStatus"] isEqual:[NSNull null]]) {
        state.text = [dict[@"orderStatus"] description];
    }
    
    [Methods dataInText:dict[@"borrowMoney"] InLabel:money defaultStr:@"-"];
    money.text = [money.text stringByAppendingString:@"元"];
    
    [Methods dataInText:dict[@"needPayMoney"] InLabel:payMoney defaultStr:@"-"];
    payMoney.text = [payMoney.text stringByAppendingString:@"元"];
    
    [Methods dataInText:dict[@"limitDays"] InLabel:day defaultStr:@"-"];
    
}

//0未申请   1审核中2待打款3待还款4容限期中5已逾期6已还款7审核失败8坏账
- (NSString *)stateStr:(NSInteger)index {
    if (index == 0) {
        return @"未申请";
    } else if (index == 1) {
        return @"审核中";
    } else if (index == 2) {
        return @"待打款";
    } else if (index == 3) {
        return @"待还款";
    } else if (index == 4) {
        return @"容限期中";
    } else if (index == 5) {
        return @"已逾期";
    } else if (index == 6) {
        return @"已还款";
    } else if (index == 7) {
        return @"审核失败";
    } else if (index == 8) {
        return @"坏账";
    }
    return @"借款状态异常";
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
