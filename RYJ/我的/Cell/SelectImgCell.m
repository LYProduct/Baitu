//
//  SelectImgCell.m
//  RYJ
//
//  Created by wyp on 2017/7/24.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "SelectImgCell.h"

#import "ImgCCell.h"

#import "ZLPhotoActionSheet.h"
#import "KSPhotoBrowser.h"

@interface SelectImgCell ()<UICollectionViewDelegate, UICollectionViewDataSource> {
    
    __weak IBOutlet UICollectionView *myCollectionView;
    NSMutableArray *dataArr;
    NSArray<PHAsset *> *arrSelected;
    UIViewController *theVC;
}

@end

@implementation SelectImgCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc {
    theVC = vc;
    dataArr = [NSMutableArray array];
    [dataArr addObject:[UIImage imageNamed:@"selectimg"]];
    [self setLayout];
    [self setCollectionView];
    
}

- (void)setLayout {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    //设置每一个item大小
    
    layout.itemSize = CGSizeMake((WIDTH_K-40)/4, (WIDTH_K-40)/4);
    //设置行间距
    layout.minimumLineSpacing = 8;
    //设置最小行间距
    layout.minimumInteritemSpacing = 5;
    //设置分区的缩进量
    layout.sectionInset = UIEdgeInsetsMake(0, 8, 0, 8);
    
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    myCollectionView.collectionViewLayout = layout;
}

- (void)setCollectionView {
    
    
    //设置collectionView的属性
    myCollectionView.delegate = self;
    myCollectionView.dataSource = self;
    [myCollectionView registerNib:[UINib nibWithNibName:@"ImgCCell" bundle:nil] forCellWithReuseIdentifier:@"cellID"];
    myCollectionView.showsVerticalScrollIndicator = NO;
    myCollectionView.showsHorizontalScrollIndicator = NO;
    myCollectionView.backgroundColor = [UIColor clearColor];
}

//有几个区
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

//每个区有几个item
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return dataArr.count;
}
//重用cell
#pragma mark CollectionSelfCell
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ImgCCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellID" forIndexPath:indexPath];
    cell.img.image = dataArr[indexPath.row];
    return cell;
}

////定义每个Section 的 margin
//-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    return UIEdgeInsetsMake(0, 6, 0, 0);//分别为上、左、下、右
//}

//选择了某个cell
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    NSLog(@"%ld---", indexPath.row);
    if (indexPath.row == dataArr.count-1) {
        ZLPhotoActionSheet *actionSheet = [[ZLPhotoActionSheet alloc] init];
        //设置照片最大预览数
        actionSheet.maxPreviewCount = 20;
        //设置照片最大选择数
        actionSheet.maxSelectCount = 10;
        actionSheet.sender = theVC;
        
        if (arrSelected) {
            actionSheet.arrSelectedAssets = [NSMutableArray arrayWithArray:arrSelected];
        }
        
        [actionSheet setSelectImageBlock:^(NSArray<UIImage *> * _Nonnull images, NSArray<PHAsset *> * _Nonnull assets, BOOL isOriginal) {
            [dataArr removeAllObjects];
            [dataArr addObject:[UIImage imageNamed:@"selectimg"]];
            //your codes
            for (int i = 0; i < images.count; i++) {
                [dataArr insertObject:images[i] atIndex:0];
            }
            arrSelected = assets;
            [myCollectionView reloadData];
            self.returnImgArr([NSArray arrayWithArray:dataArr]);
        }];
        
        [actionSheet showPhotoLibraryWithSender:theVC];
    } else {
        if (dataArr.count > 0) {
            NSMutableArray *items = @[].mutableCopy;
            for (int i = 0; i < dataArr.count-1; i++) {
                UIImageView *imageView = [[UIImageView alloc] initWithImage:dataArr[i]];
                KSPhotoItem *item = [KSPhotoItem itemWithSourceView:imageView image:dataArr[i]];
                [items addObject:item];
            }
            KSPhotoBrowser *browser = [KSPhotoBrowser browserWithPhotoItems:items selectedIndex:indexPath.row];
            [browser showFromViewController:theVC];
        }
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
