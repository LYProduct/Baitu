//
//  InvitationMoneyCell.m
//  RYJ
//
//  Created by wyp on 2017/7/25.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "InvitationMoneyCell.h"

@interface InvitationMoneyCell () {
    NSDictionary *dataDic;
    UIViewController *theVC;
}

@end

@implementation InvitationMoneyCell

@synthesize leftLabel;
@synthesize rightLabel;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc {
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithAttributedString:leftLabel.attributedText];
    if (![dict[@"money"] isEqual:[NSNull null]]) {
        [attrStr replaceCharactersInRange:NSMakeRange(5, 1) withString:[dict[@"money"] description]];
    }
    leftLabel.attributedText = attrStr;
    
    NSMutableAttributedString *attrStr1 = [[NSMutableAttributedString alloc] initWithAttributedString:rightLabel.attributedText];
    if (![dict[@"count"] isEqual:[NSNull null]]) {
        [attrStr1 replaceCharactersInRange:NSMakeRange(attrStr.length-1, 1) withString:[dict[@"count"] description]];
    }
    rightLabel.attributedText = attrStr1;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
