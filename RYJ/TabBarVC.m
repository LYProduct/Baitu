//
//  TabBarVC.m
//  XEDK
//
//  Created by wyp on 2017/7/7.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "TabBarVC.h"

#import "MessageDetailVC.h"

@interface TabBarVC ()

@end

@implementation TabBarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 拿到 TabBar 在拿到想应的item
    UITabBar *tabBar = self.tabBar;
    UITabBarItem *item0 = [tabBar.items objectAtIndex:0];
    UITabBarItem *item1 = [tabBar.items objectAtIndex:1];
    UITabBarItem *item2 = [tabBar.items objectAtIndex:2];
    UITabBarItem *item3 = [tabBar.items objectAtIndex:3];
    // 对item设置相应地图片
    item0.selectedImage = [[UIImage imageNamed:@"zhuye-dianji"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];;
    item0.image = [[UIImage imageNamed:@"zhuyemoren"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    item1.selectedImage = [[UIImage imageNamed:@"fenlei-dianji"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];;
    item1.image = [[UIImage imageNamed:@"fenlei-moren"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    item2.selectedImage = [[UIImage imageNamed:@"pingu-dianji"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];;
    item2.image = [[UIImage imageNamed:@"pingu-moren"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    item3.selectedImage = [[UIImage imageNamed:@"wode-dianji"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];;
    item3.image = [[UIImage imageNamed:@"wode-morne"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushmsg:) name:@"跳转信息页面" object:nil];
    // Do any additional setup after loading the view.
}

- (void)pushmsg:(NSNotification *)Notification {
    NSLog(@"推送过来的消息%@", Notification.userInfo);
    //    [self push:Notification.userInfo[@"msg"]];
    NSLog(@"选中第几:%lu", (unsigned long)self.selectedIndex);
    [self push:Notification.userInfo[@"msg"]];
}

- (void)push:(NSDictionary *)msgDic {
    
    MessageDetailVC *vc = [TheGlobalMethod setstoryboard:@"MessageDetailVC" controller:self];
//    vc.detailStr = msgDic[@"content"];
    vc.titleStr = msgDic[@"aps"][@"alert"];
    vc.hidesBottomBarWhenPushed = YES;
    
    
    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[msgDic[@"content"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    vc.attStr = attrStr;
    UINavigationController *na = self.viewControllers[self.selectedIndex];
    
    [na pushViewController:vc animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
