//
//  Methods.h
//  MageLive
//
//  Created by wyp on 2017/5/17.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Methods : NSObject

#pragma mark textField赋值
+ (void)dataInTFText:(id)string InTF:(UITextField *)textField defaultStr:(NSString *)defaultStr;

#pragma mark label赋值 
+ (void)dataInText:(id)string InLabel:(UILabel *)label defaultStr:(NSString *)defaultStr;

/** imageView赋值 */
+ (void)dataInImg:(id)string InImg:(UIImageView *)img placeholderImage:(NSString *)imgName;

#pragma mark AttributedString赋值(颜色)
+ (void)dataInAttributedString:(NSString *)beforeStr afterStr:(NSString *)afterStr range:(NSRange)range label:(UILabel *)label;
#pragma mark 跳web
+ (void)pushUrl:(NSString *)urlString seleVC:(UIViewController *)vc;

+ (void)showComprehensiveViewLeftArr:(NSArray *)leftArr rightArr:(NSArray *)rightArr bottomStatus:(id)bottomStatus selfVC:(UIViewController *)theVC;

+ (void)share:(UIViewController *)vc descr:(NSString*)descr url:(NSString *)url platformType:(UMSocialPlatformType)platformType;


+ (void)showAlertForPayLeftArr:(NSArray *)leftArr rightArr:(NSArray *)rightArr bottomStatus:(id)bottomStatus selfVC:(UIViewController *)theVC titleLabelText:(NSString *)titleText titleLabelTextColor:(UIColor *)titleColor alicode:(NSString * )alicode wechat:(NSString *)wechat;
@end
