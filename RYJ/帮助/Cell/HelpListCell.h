//
//  HelpListCell.h
//  RYJ
//
//  Created by 云鹏 on 2017/11/21.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;

@property (weak, nonatomic) IBOutlet UILabel *rightLabel;

@end
