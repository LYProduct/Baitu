//
//  HelpHomeVC.m
//  RYJ
//
//  Created by 云鹏 on 2017/11/21.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "HelpHomeVC.h"

#import "HelpSonVC.h"

@interface HelpHomeVC ()<ZJScrollPageViewDelegate>

@property(strong, nonatomic)NSArray<NSString *> *titles;
@property (nonatomic, strong) ZJScrollPageView *scrollPageView;


@end

@implementation HelpHomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titles = @[@"借款问题", @"还款问题", @"其它问题"];
    [self setZJScrollPageView];
    // Do any additional setup after loading the view.
}

- (void)setZJScrollPageView {
    
    ZJSegmentStyle *style = [[ZJSegmentStyle alloc] init];
    style.normalTitleColor = NAVCOLOR_C(102.0, 102.0, 102.0);
    style.selectedTitleColor = appDefaultColor;
    style.scrollLineColor = appDefaultColor;
    style.contentViewBounces = NO;
        style.scrollTitle = NO;
        style.line_Width = 56;
//        style.line_Y = 5;
    //显示滚动条
    style.showLine = YES;
    // 颜色渐变
    style.gradualChangeTitleColor = YES;
    // 初始化
    _scrollPageView = [[ZJScrollPageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, HEIGHT_K-44) segmentStyle:style titles:self.titles parentViewController:self delegate:self];
    _scrollPageView.segmentView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_scrollPageView];
    
    NSLog(@"第几次打开应用%ld", [[TheGlobalMethod getUserDefault:@"HomeAlertIndex"] integerValue]);
    
}

- (NSInteger)numberOfChildViewControllers {
    return self.titles.count;
}

- (UIViewController<ZJScrollPageViewChildVcDelegate> *)childViewController:(UIViewController<ZJScrollPageViewChildVcDelegate> *)reuseViewController forIndex:(NSInteger)index {
    UIViewController<ZJScrollPageViewChildVcDelegate> *childVc = reuseViewController;
    
    if (!childVc) {
        HelpSonVC *vc = [[HelpSonVC alloc] initWithNibName:nil bundle:nil];
        vc.type = [NSString stringWithFormat:@"%ld",index+1];
        return vc;
    
    }
    
    //    NSLog(@"%ld-----%@",(long)index, childVc);
    
    return childVc;
}


- (BOOL)shouldAutomaticallyForwardAppearanceMethods {
    return NO;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
