//
//  HelpSonVC.m
//  RYJ
//
//  Created by 云鹏 on 2017/11/21.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "HelpSonVC.h"

#import "HelpListCell.h"
#import "ABtnCell.h"
#import "MessageDetailVC.h"

@interface HelpSonVC () {
    __weak IBOutlet RootTableView *myTableView;
    NSArray *titleArr;
}

@end

@implementation HelpSonVC

- (void)viewDidLoad {
    [super viewDidLoad];
//    titleArr = @[@"个人资料安全吗？", @"提现时绑定银行卡错误，怎么办？", @"征信不好，可以借款吗？"];
    myTableView.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view from its nib.
    [self getList];
}
-(void)getList {
    [HttpUrl GET:@"helpCenter/findListByType" dict:@{@"type":_type} hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        NSLog(@"帮助问题- - %@",data);
        if (BB_isSuccess) {
            titleArr = data[@"data"];
        }else{
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 1) {
        return 1;
    }
    return titleArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        ABtnCell *cell = [TheGlobalMethod setCell:@"ABtnCell"];
        [cell.btn setTitle:@"在线客服" forState:(UIControlStateNormal)];
        [cell.btn addTarget:self action:@selector(zaixiankefu) forControlEvents:(UIControlEventTouchUpInside)];
        return cell;
    }
    HelpListCell loadingTableViewCell("HelpListCell", )
    cell.leftLabel.text = [NSString stringWithFormat:@"%ld.", indexPath.row+1];
//    cell.rightLabel.text = titleArr[indexPath.row];
    cell.rightLabel.text = stringEmpty(titleArr[indexPath.row][@"title"]);
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    MessageDetailVC *vc = GETStoryboard(@"MessageDetailVC");
    
//        vc.detailStr = @"没完没了的垃圾邮件，防不胜防的银行卡盗刷，莫名其妙的推销电话，闻虎色变的病毒木马……一时间，我们突然意识到，现代生活中，伴随着快速和便捷，我们也面临着越来越多的危险。抽丝剥茧，我们发现，上述危险的源头正是个人信息的泄露，而减少或杜绝上述危险的方法就是避免个人信息的泄露。《你的个人信息安全吗》共分为三部分。第1部分主要介绍如何设置简单且安全的密码、如何通过加密拒绝“不速之客”、如何通过证据防御抵赖老手等通用内容。第2部分主要包括使用计算机上网时的数据保护、如何识别安全网址、QQ聊天安全、病毒和木马的防治等内容。第3部分是本书最独特的一部分，主要介绍家庭生活中座机电话、手机、银行卡、纸质证件等的使用安全——我们会惊讶地发现，原来数码相机、汽车电子钥匙、门卡、快递单、求职简历，甚至是垃圾，都有可能成为个人信息泄露的途径。为此，我们只有在接受新技术、享受新技术带来的便利的同时，关注自己的信息安全，主动保护自己的个人信息，才能做到事前防患于未然，事后把损失降到最低。";

    if ([titleArr[indexPath.row][@"content"] isEqual:[NSNull null]]) {
        vc.detailStr = @"";
    }else{
        NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[titleArr[indexPath.row][@"content"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
        vc.attStr = attrStr;
    }
    vc.titleStr = titleArr[indexPath.row][@"title"];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)zaixiankefu {
    NSLog(@"在线客服");
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, 10)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
