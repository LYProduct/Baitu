//
//  HomeViewModel.h
//  RYJ
//
//  Created by wyp on 2018/1/13.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomeViewModel : NSObject

@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *initialPrice;
@property(nonatomic,strong) NSString *picUrl;

@end
