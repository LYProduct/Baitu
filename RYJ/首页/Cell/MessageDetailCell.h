//
//  MessageDetailCell.h
//  MC
//
//  Created by ike on 16/6/28.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;

- (void)reloadCell:(NSString *)str;

- (void)reloadAttCell:(NSAttributedString *)str;

@end
