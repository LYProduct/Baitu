//
//  LLICell.h
//  RYJ
//
//  Created by wyp on 2017/7/20.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LLICell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
