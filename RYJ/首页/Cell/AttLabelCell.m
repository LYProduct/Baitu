//
//  AttLabelCell.m
//  RYJ
//
//  Created by wyp on 2017/7/20.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "AttLabelCell.h"
@interface AttLabelCell()
{
    NSDictionary * _couponDict;
}
@end

@implementation AttLabelCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setDict:(NSDictionary *)dict{
    _couponDict = dict;
}
- (void)reloadCellWithData:(NSDictionary *)dict vc:(UIViewController *)vc {
    NSMutableAttributedString *atStr = [[NSMutableAttributedString alloc] initWithString:@"您需要在-还款-元"];
    //添加文字颜色
    [atStr addAttribute:NSForegroundColorAttributeName value:appDefaultColor range:NSMakeRange(4, 1)];
    [atStr addAttribute:NSForegroundColorAttributeName value:appDefaultColor range:NSMakeRange(atStr.length-2, 1)];
    _label.attributedText = atStr;
    
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithAttributedString:_label.attributedText];
    
    
    
    
    
    if (![dict[@"limitPayTime"] isEqual:[NSNull null]]) {
        NSString * gmtStr = [self cTimestampFromString:[dict[@"limitPayTime"] description]];
        NSString * timeStr = [self timeWithTimeIntervalString:gmtStr borrowDay:[@"0" integerValue]];
        [attrStr replaceCharactersInRange:NSMakeRange(4, 1) withString:[timeStr substringToIndex:10]];
        
    }
    if (![dict[@"borrowMoney"] isEqual:[NSNull null]]) {
        float money = 0.0;
        if (_couponDict.allKeys.count > 0) {
            money = [dict[@"borrowMoney"] doubleValue]-[_couponDict[@"saveMoney"] doubleValue];
        }else{
            money = [dict[@"borrowMoney"] doubleValue];
        }
        [attrStr replaceCharactersInRange:NSMakeRange(attrStr.length-2, 1) withString:[NSString stringWithFormat:@"%.2f",money]];
    }
    _label.attributedText = attrStr;
}
- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc {
    NSMutableAttributedString *atStr = [[NSMutableAttributedString alloc] initWithString:@"您需要在-还款-元"];
    //添加文字颜色
    [atStr addAttribute:NSForegroundColorAttributeName value:appDefaultColor range:NSMakeRange(4, 1)];
    [atStr addAttribute:NSForegroundColorAttributeName value:appDefaultColor range:NSMakeRange(atStr.length-2, 1)];
    _label.attributedText = atStr;
    
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithAttributedString:_label.attributedText];
    
    
    

    
    if (![dict[@"currentDate"] isEqual:[NSNull null]]) {
        NSString * gmtStr = [self cTimestampFromString:[dict[@"currentDate"] description]];
        NSString * timeStr = [self timeWithTimeIntervalString:gmtStr borrowDay:[dict[@"limitDays"] integerValue]];
        [attrStr replaceCharactersInRange:NSMakeRange(4, 1) withString:[timeStr substringToIndex:10]];
        
    }
    if (![dict[@"borrowMoney"] isEqual:[NSNull null]]) {
        float money = 0.0;
        if (_couponDict.allKeys.count > 0) {
            money = [dict[@"borrowMoney"] doubleValue]-[_couponDict[@"saveMoney"] doubleValue];
        }else{
            money = [dict[@"borrowMoney"] doubleValue];
        }
        [attrStr replaceCharactersInRange:NSMakeRange(attrStr.length-2, 1) withString:[NSString stringWithFormat:@"%.2f",money]];;
    }
    _label.attributedText = attrStr;
}
- (NSString *)timeWithTimeIntervalString:(NSString *)timeString borrowDay:(NSInteger)day
{
    // 格式化时间
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    formatter.timeZone = [NSTimeZone timeZoneWithName:@"beijing"];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
     //毫秒值转化为秒
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:[timeString doubleValue]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    //    日期加
    
//    NSDate *Date0 = [datePicker date];
    
    NSTimeInterval interval = 60 * 60 * 24 * day;
    NSString *titleString = [dateFormatter stringFromDate:[date initWithTimeInterval:interval sinceDate:date]];
    
//    NSString* dateString = [formatter stringFromDate:date];
    return titleString;
}
-(NSString *)cTimestampFromString:(NSString *)theTime{
    //theTime __@"%04d-%02d-%02d %02d:%02d:00"
    NSDateFormatter *stampFormatter = [[NSDateFormatter alloc] init];
    [stampFormatter setDateFormat:@"yyyy-MM-dd"];
    //以 1970/01/01 GMT为基准，然后过了secs秒的时间
    NSDate *stampDate2 = [NSDate dateWithTimeIntervalSince1970:theTime.doubleValue];

    //装换为时间戳
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    formatter.timeZone = [NSTimeZone timeZoneWithName:@"beijing"];

    [formatter setDateFormat:@"yyyy-MM-dd"];
    //        NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];
    //        [formatter setTimeZone:timeZone];
    NSDate* dateTodo = [formatter dateFromString:theTime];
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[stampDate2 timeIntervalSince1970]/1000];
    
    return timeSp;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
