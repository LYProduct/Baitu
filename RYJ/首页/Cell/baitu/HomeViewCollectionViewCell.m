//
//  HomeViewCollectionViewCell.m
//  RYJ
//
//  Created by wyp on 2018/1/13.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import "HomeViewCollectionViewCell.h"
#import "HomeViewModel.h"

@interface HomeViewCollectionViewCell()
@property (weak, nonatomic) IBOutlet UIButton *assessmentBT;

@end

@implementation HomeViewCollectionViewCell

@synthesize phoneLabel;
@synthesize byPrice;
@synthesize phoneImageView;

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor whiteColor];
    [self.assessmentBT addTarget:self action:@selector(assessmentBTClick:) forControlEvents:UIControlEventTouchUpInside];
    self.assessmentBT.layer.cornerRadius = W(12.5);
    self.assessmentBT.layer.masksToBounds = YES;
    [self.assessmentBT setTitle:@"  去评估  " forState:UIControlStateNormal];
    self.assessmentBT.titleLabel.font = [UIFont systemFontOfSize:14];
    self.assessmentBT.layer.borderWidth = 1;
    [self.assessmentBT setTitleColor:[[UIColor blueColor] colorWithAlphaComponent:0.5] forState:UIControlStateNormal];
    self.assessmentBT.layer.borderColor = [[UIColor blueColor] colorWithAlphaComponent:0.5].CGColor;
    
}

-(void)assessmentBTClick:(UIButton *)sender
{
   if(self.assessmentBtBlock)
   {
       self.assessmentBtBlock();
   }
}

-(void)setHomeViewModel:(HomeViewModel *)HomeViewModel
{
    NSString *imageStr = Http_imgIP;
    _HomeViewModel = HomeViewModel;
    phoneLabel.text = HomeViewModel.name;
    byPrice.text = HomeViewModel.initialPrice;
    
    if(HomeViewModel.picUrl)
    {
    [phoneImageView sd_setImageWithURL:[NSURL URLWithString:[imageStr stringByAppendingString:HomeViewModel.picUrl]] placeholderImage:placeholderimage];
    }
    else
    {
        phoneImageView.image = placeholderimage;
    }
}

@end
