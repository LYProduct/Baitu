//
//  HomeViewCollectionViewCell.h
//  RYJ
//
//  Created by wyp on 2018/1/13.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewModel.h"

typedef void(^assessmentBtBlock)();

@interface HomeViewCollectionViewCell : UICollectionViewCell

@property(nonatomic,strong) HomeViewModel *HomeViewModel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property(strong,nonatomic) void(^assessmentBtBlock)();
@property (weak, nonatomic) IBOutlet UIImageView *phoneImageView;
@property (weak, nonatomic) IBOutlet UILabel *byPrice;


@end
