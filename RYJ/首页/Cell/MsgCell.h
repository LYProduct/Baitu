//
//  MsgCell.h
//  XEDK
//
//  Created by wyp on 2017/7/18.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MsgCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UILabel *time;

@end
