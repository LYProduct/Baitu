//
//  MessageDetailCell.m
//  MC
//
//  Created by ike on 16/6/28.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "MessageDetailCell.h"

@implementation MessageDetailCell
@synthesize label;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

static CGSize sizef;
- (void)reloadCell:(NSString *)str {
    label.text = str;
     
}

- (void)reloadAttCell:(NSAttributedString *)str {
    label.attributedText = str;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
