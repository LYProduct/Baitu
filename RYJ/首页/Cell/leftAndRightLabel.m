//
//  leftAndRightLabel.m
//  RYJ
//
//  Created by wyp on 2017/7/20.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "leftAndRightLabel.h"

#import "ComprehensiveView.h"

@interface leftAndRightLabel () {
    UIViewController *theVC;
    NSDictionary *dataDic;
    NSDictionary * couponDict;
}

@end

@implementation leftAndRightLabel

@synthesize leftLabel;
@synthesize btn;
@synthesize rightLabel;
@synthesize bottomView;
@synthesize bottomView_L;
@synthesize bottomView_R;

-(void)setDict:(NSDictionary *)dict{
    couponDict = dict;
}
#pragma mark 申请借款の
- (void)reloadData:(NSDictionary *)dict vc:(UIViewController *)vc indexPath:(NSIndexPath *)indexPath{
    theVC = vc;
    dataDic = dict;
    if (indexPath.section == 0) {
        [self reloadOneSection:indexPath];
    } else if (indexPath.section == 1) {
        btn.hidden = NO;
        rightLabel.hidden = NO;
        rightLabel.font = [UIFont systemFontOfSize:12];
        [Methods dataInText:dataDic[@"omnibusFee"] InLabel:rightLabel defaultStr:@"-"];
        rightLabel.text = [rightLabel.text stringByAppendingString:@"元"];
        leftLabel.text = @"综合费用";
        bottomView.hidden = NO;
        bottomView_R.constant = 0;
        bottomView_L.constant = 0;
    } else if (indexPath.section == 3) {
        [self reloadThreeSection:indexPath];
    }
}

#pragma mark 借款详情の
- (void)reloadDataForDetail:(NSDictionary *)dict vc:(UIViewController *)vc indexPath:(NSIndexPath *)indexPath {
    theVC = vc;
    dataDic = dict;
    if (indexPath.section == 2) {
        [self reloadBorrowDetailThreeSection:indexPath];
    } else if (indexPath.section == 3) {
        btn.hidden = NO;
        rightLabel.hidden = NO;
        rightLabel.font = [UIFont systemFontOfSize:12];
        [Methods dataInText:dataDic[@"wateMoney"] InLabel:rightLabel defaultStr:@""];
        rightLabel.text = [rightLabel.text stringByAppendingString:@"元"];
        leftLabel.text = @"综合费用";
        bottomView.hidden = NO;
        bottomView_R.constant = 0;
        bottomView_L.constant = 0;
    } else if (indexPath.section == 4) {
        [self reloadBorrowDetailFiveSection:indexPath dic:dict];
    }
}

#pragma mark 续期の
- (void)reloadDataForxuqi:(NSDictionary *)dict vc:(UIViewController *)vc indexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        leftLabel.font = [UIFont systemFontOfSize:14];
        leftLabel.textColor = NAVCOLOR_C(102.0, 102.0, 102.0);
        rightLabel.hidden = NO;
        rightLabel.font = [UIFont systemFontOfSize:14];
        NSArray *arr = @[@"",@"续期还款日",@"到期应还"];
        leftLabel.text = arr[indexPath.row];
        if (indexPath.row == 1) {
//            rightLabel.text = [TheGlobalMethod dateWithStr:[[dict[@"limitPayTime"] description] substringToIndex:10] Format:@"yyyy-MM-dd"];
            rightLabel.text = [dict[@"limitPayTime"] description];
        } else if (indexPath.row == 2) {
            rightLabel.text = @"1000.00元";
            [Methods dataInText:dict[@"needPayMoney"] InLabel:rightLabel defaultStr:@"-"];
            rightLabel.text = [rightLabel.text stringByAppendingString:@"元"];
            bottomView.hidden = NO;
            bottomView_R.constant = 0;
            bottomView_L.constant = 0;
        } 
    } else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            bottomView.hidden = NO;
            leftLabel.text = @"续期费用";
        } else {
            leftLabel.font = [UIFont systemFontOfSize:14];
            leftLabel.textColor = NAVCOLOR_C(102.0, 102.0, 102.0);
            rightLabel.hidden = NO;
            rightLabel.font = [UIFont systemFontOfSize:14];
            rightLabel.textColor = NAVCOLOR_C(102.0, 102.0, 102.0);
            NSArray *arr = @[@"",@"本期利息",@"综合费用",@"优惠券抵扣",@"合计应扣金额"];
            leftLabel.text = arr[indexPath.row];
            if (indexPath.row == 1) {
                rightLabel.font = [UIFont systemFontOfSize:16];
//                [Methods dataInText:dict[@"interestMoney"] InLabel:rightLabel defaultStr:@"-"];
                [Methods dataInText:[dict[@"interestPercent"] description] InLabel:rightLabel defaultStr:@"-"];
                rightLabel.text = [rightLabel.text stringByAppendingString:@"元"];
            } else if (indexPath.row == 2) {
                [Methods dataInText:[dict[@"omnibusFee"] description] InLabel:rightLabel defaultStr:@"-"];
                rightLabel.text = [rightLabel.text stringByAppendingString:@"元"];
            }else if (indexPath.row == 3){
                if (couponDict.allKeys.count > 0) {
                    rightLabel.text = [[@"-" stringByAppendingString:couponDict[@"saveMoney"]] stringByAppendingString:@"元"];
                }else{
                    rightLabel.text = [@"-0.0" stringByAppendingString:@"元"];
                }
            }
            else if (indexPath.row == 4) {
                rightLabel.font = [UIFont systemFontOfSize:16];
                leftLabel.font = [UIFont systemFontOfSize:16];
                rightLabel.textColor = appDefaultColor;
                
                float money = 0.0;
                if (couponDict.allKeys.count > 0) {
                    money = [dict[@"interestPercent"] doubleValue]+[dict[@"omnibusFee"] doubleValue] - [couponDict[@"saveMoney"] doubleValue];
                }else{
                    money = [dict[@"interestPercent"] doubleValue]+[dict[@"omnibusFee"] doubleValue];
                }
                
                [Methods dataInText:[NSString stringWithFormat:@"%.2f",money] InLabel:rightLabel defaultStr:@"-"];
                rightLabel.text = [rightLabel.text stringByAppendingString:@"元"];
                bottomView.hidden = NO;
                bottomView_R.constant = 0;
                bottomView_L.constant = 0;
            }
        }
    }
}

#pragma mark 借款详情の
- (void)reloadBorrowDetailThreeSection:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        bottomView.hidden = NO;
        [Methods dataInText:dataDic[@"loanOrder"][@"orderNumber"] InLabel:leftLabel defaultStr:@""];
        leftLabel.text = [@"借款编号:" stringByAppendingString:leftLabel.text];
    } else {
        leftLabel.font = [UIFont systemFontOfSize:12];
        leftLabel.textColor = NAVCOLOR_C(102.0, 102.0, 102.0);
        rightLabel.hidden = NO;
        rightLabel.font = [UIFont systemFontOfSize:12];
        rightLabel.textColor = NAVCOLOR_C(102.0, 102.0, 102.0);
        NSArray *arr = @[@"",@"申请时间",@"借款金额",@"借款期限",@"到账金额",@"到账银行卡",@"利率"];
        leftLabel.text = arr[indexPath.row];
        if (indexPath.row == 1) {
            [Methods dataInText:dataDic[@"loanOrder"][@"gmtDatetime"] InLabel:rightLabel defaultStr:@""];
        } else if (indexPath.row == 2) {
            [Methods dataInText:dataDic[@"loanOrder"][@"borrowMoney"] InLabel:rightLabel defaultStr:@""];
            rightLabel.text = [rightLabel.text stringByAppendingString:@"元"];
        } else if (indexPath.row == 3) {
            [Methods dataInText:dataDic[@"loanOrder"][@"limitDays"] InLabel:rightLabel defaultStr:@""];
            rightLabel.text = [rightLabel.text stringByAppendingString:@"天"];
        } else if (indexPath.row == 4) {
            [Methods dataInText:dataDic[@"loanOrder"][@"realMoney"] InLabel:rightLabel defaultStr:@""];
            rightLabel.text = [rightLabel.text stringByAppendingString:@"元"];
        } else if (indexPath.row == 5) {
            [Methods dataInText:dataDic[@"loanOrder"][@"bankName"] InLabel:rightLabel defaultStr:@"-"];
            if (![dataDic[@"loanOrder"][@"bankCardNum"] isEqual:[NSNull null]]) {
                if ([[dataDic[@"loanOrder"][@"bankCardNum"] description] length] >= 4) {
                    rightLabel.text = [rightLabel.text stringByAppendingString:[NSString stringWithFormat:@"(****%@)", [[dataDic[@"loanOrder"][@"bankCardNum"] description] substringFromIndex:[[dataDic[@"loanOrder"][@"bankCardNum"] description] length]-4]]];
                }
            }
        } else if (indexPath.row == 6) {
            [Methods dataInText:dataDic[@"loanOrder"][@"interestPrecent"] InLabel:rightLabel defaultStr:@"-"];
            rightLabel.text = [rightLabel.text stringByAppendingString:@"‰"];
            bottomView.hidden = NO;
            bottomView_R.constant = 0;
            bottomView_L.constant = 0;
        }
    }
}

- (void)reloadBorrowDetailFiveSection:(NSIndexPath *)indexPath dic:(NSDictionary *)dict {
    if (indexPath.row == 0) {
        bottomView.hidden = NO;
        leftLabel.text = @"还款信息";
    } else if (indexPath.row == 6) {
        leftLabel.text = @"合计应还金额";
        rightLabel.hidden = NO;
        [Methods dataInText:dataDic[@"loanOrder"][@"needPayMoney"] InLabel:rightLabel defaultStr:@""];
        rightLabel.text = [rightLabel.text stringByAppendingString:@"元"];
        bottomView.hidden = NO;
        bottomView_R.constant = 0;
        bottomView_L.constant = 0;
    } else {
        leftLabel.font = [UIFont systemFontOfSize:12];
        leftLabel.textColor = NAVCOLOR_C(102.0, 102.0, 102.0);
        rightLabel.hidden = NO;
        rightLabel.font = [UIFont systemFontOfSize:12];
        rightLabel.textColor = NAVCOLOR_C(102.0, 102.0, 102.0);
        NSArray *arr = @[@"",@"应还本金",@"应还利息",@"优惠券抵扣",@"实际还款日",@"逾期费用"];
        leftLabel.text = arr[indexPath.row];
        if (indexPath.row == 1) {
            [Methods dataInText:dataDic[@"loanOrder"][@"borrowMoney"] InLabel:rightLabel defaultStr:@""];
            rightLabel.text = [rightLabel.text stringByAppendingString:@"元"];
        } else if (indexPath.row == 2) {
            leftLabel.hidden = YES;
            rightLabel.hidden = YES;
            [Methods dataInText:dataDic[@"loanOrder"][@"interestMoney"] InLabel:rightLabel defaultStr:@""];
            rightLabel.text = [rightLabel.text stringByAppendingString:@"元"];
        } else if (indexPath.row == 3) {
            [Methods dataInText:dataDic[@"loanOrder"][@"saveMoney"] InLabel:rightLabel defaultStr:@"0.00"];
            rightLabel.text = [rightLabel.text stringByAppendingString:@"元"];
            rightLabel.text = [@"-" stringByAppendingString:rightLabel.text];
        } else if (indexPath.row == 4) {
            [Methods dataInText:dataDic[@"loanOrder"][@"realPayTime"] InLabel:rightLabel defaultStr:@"-"];
            if (![dict[@"yuqi"] isEqualToString:@"1"]) {
                bottomView.hidden = NO;
            }
        } else if (indexPath.row == 5) {
            if (![dict[@"yuqi"] isEqualToString:@"1"]) {
                self.hidden = YES;
            } else {
                [Methods dataInText:dataDic[@"overdueMoney"] InLabel:rightLabel defaultStr:@""];
                rightLabel.text = [rightLabel.text stringByAppendingString:@"元"];
            bottomView.hidden = NO;
            }
        }
    }
}

#pragma mark 查看综合弹出框
- (IBAction)lookMoney:(UIButton *)sender {
    self.returnCenterBtnClick();
//    ComprehensiveView *comprehensiveView = [TheGlobalMethod setCell:@"ComprehensiveView"];
//    comprehensiveView.frame = CGRectMake(0, 0, WIDTH_K, HEIGHT_K);
//    [comprehensiveView reloadCellForData:@{@"L":@[@"平台服务费", @"信息认证费",@"风控服务费", @"风险准备金", @"总计"], @"R":@[@"120.0元", @"10.00元",@"20.0元", @"152.00元", @"100.00元"]} vc:theVC];
//    [[UIApplication sharedApplication].keyWindow addSubview:comprehensiveView];
}


#pragma mark 申请借款の
- (void)reloadOneSection:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        bottomView.hidden = NO;
        leftLabel.text = @"借款信息";
    } else {
        leftLabel.font = [UIFont systemFontOfSize:12];
        leftLabel.textColor = NAVCOLOR_C(102.0, 102.0, 102.0);
        rightLabel.hidden = NO;
        rightLabel.font = [UIFont systemFontOfSize:12];
        rightLabel.textColor = NAVCOLOR_C(102.0, 102.0, 102.0);
        NSArray *arr = @[@"",@"借款金额",@"借款期限",@"到账金额",@"到账银行卡",@"利率"];
        leftLabel.text = arr[indexPath.row];
        if (indexPath.row == 1) {
            [Methods dataInText:dataDic[@"borrowMoney"] InLabel:rightLabel defaultStr:@"0.00"];
            rightLabel.text = [rightLabel.text stringByAppendingString:@"元"];
        } else if (indexPath.row == 2) {
            [Methods dataInText:dataDic[@"limitDays"] InLabel:rightLabel defaultStr:@"-"];
            rightLabel.text = [rightLabel.text stringByAppendingString:@"天"];
        } else if (indexPath.row == 3) {
            NSString * moneyStr = [NSString stringWithFormat:@"%.2f",[dataDic[@"realMoney"] doubleValue]-[dataDic[@"interest"] doubleValue]];
            [Methods dataInText:moneyStr InLabel:rightLabel defaultStr:@"-"];
            rightLabel.text = [rightLabel.text stringByAppendingString:@"元"];
        } else if (indexPath.row == 4) {
            [Methods dataInText:dataDic[@"cardname"] InLabel:rightLabel defaultStr:@"-"];
            if (![dataDic[@"bankcardno"] isEqual:[NSNull null]]) {
                if ([[dataDic[@"bankcardno"] description] length] >= 4) {
                    rightLabel.text = [rightLabel.text stringByAppendingString:[NSString stringWithFormat:@"(****%@)", [[dataDic[@"bankcardno"] description] substringFromIndex:[[dataDic[@"bankcardno"] description] length]-4]]];
                }
            }
        } else if (indexPath.row == 5) {
            [Methods dataInText:dataDic[@"interestPrecent"] InLabel:rightLabel defaultStr:@"-"];
            rightLabel.text = [rightLabel.text stringByAppendingString:@"‰"];
            bottomView.hidden = NO;
            bottomView_R.constant = 0;
            bottomView_L.constant = 0;
        }
    }
}

- (void)reloadThreeSection:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        bottomView.hidden = NO;
        leftLabel.text = @"还款信息";
    } else if (indexPath.row == 4) {
        leftLabel.text = @"合计应还金额";
        rightLabel.hidden = NO;
        float money = 0.0;
        if (couponDict.allKeys.count > 0) {
            money = [dataDic[@"borrowMoney"] doubleValue] - [couponDict[@"saveMoney"] doubleValue];
            if (money >= 0) {
            
            }else{
                money = 0.0;
            }
        }else{
            money = [dataDic[@"borrowMoney"] doubleValue];
        }
        [Methods dataInText:[NSString stringWithFormat:@"%.2f",money] InLabel:rightLabel defaultStr:@"0.00"];
        rightLabel.text = [rightLabel.text stringByAppendingString:@"元"];
        bottomView.hidden = NO;
        bottomView_R.constant = 0;
        bottomView_L.constant = 0;
    } else {
        leftLabel.font = [UIFont systemFontOfSize:12];
        leftLabel.textColor = NAVCOLOR_C(102.0, 102.0, 102.0);
        rightLabel.hidden = NO;
        rightLabel.font = [UIFont systemFontOfSize:12];
        rightLabel.textColor = NAVCOLOR_C(102.0, 102.0, 102.0);
        NSArray *arr = @[@"",@"应还本金",@"利息",@"优惠券抵扣"];
        leftLabel.text = arr[indexPath.row];
        if (indexPath.row == 1) {
            [Methods dataInText:dataDic[@"borrowMoney"] InLabel:rightLabel defaultStr:@"0.00"];
            rightLabel.text = [rightLabel.text stringByAppendingString:@"元"];
        } else if (indexPath.row == 2) {
//            rightLabel.hidden = YES;
//            leftLabel.hidden = YES;
            [Methods dataInText:dataDic[@"interest"] InLabel:rightLabel defaultStr:@"0.00"];
            rightLabel.text = [rightLabel.text stringByAppendingString:@"元"];
        } else if (indexPath.row == 3) {
            if ([couponDict[@"saveMoney"] isEqual:[NSNull null]]) {
                rightLabel.text = @"-0.00元";;
            } else {
            [Methods dataInText:[couponDict[@"saveMoney"] description] InLabel:rightLabel defaultStr:@"0.00"];
            rightLabel.text = [rightLabel.text stringByAppendingString:@"元"];
            rightLabel.text = [@"-" stringByAppendingString:rightLabel.text];
            }
            bottomView.hidden = NO;
        }
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
