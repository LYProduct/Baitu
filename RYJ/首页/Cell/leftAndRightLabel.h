//
//  leftAndRightLabel.h
//  RYJ
//
//  Created by wyp on 2017/7/20.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface leftAndRightLabel : UITableViewCell

@property (nonatomic, strong) void(^returnCenterBtnClick)();

- (void)reloadData:(NSDictionary *)dict vc:(UIViewController *)vc indexPath:(NSIndexPath *)indexPath;

- (void)reloadDataForDetail:(NSDictionary *)dict vc:(UIViewController *)vc indexPath:(NSIndexPath *)indexPath;

- (void)reloadDataForxuqi:(NSDictionary *)dict vc:(UIViewController *)vc indexPath:(NSIndexPath *)indexPath;

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;
@property (weak, nonatomic) IBOutlet UIButton *btn;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomView_L;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomView_R;

@property(nonatomic,strong)NSDictionary * dict;

@end
