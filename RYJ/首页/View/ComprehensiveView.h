//
//  ComprehensiveView.h
//  RYJ
//
//  Created by wyp on 2017/7/20.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ComprehensiveView : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UIButton *bottomBtn;


@property (nonatomic,strong)NSString * alipayCode;

@property(nonatomic,strong)NSString * wechatCode;

@property (nonatomic, strong) NSString *bottomStatus;

@end
