//
//  HHRuleView.m
//  RuleViewDemo
//
//  Created by ZHH on 16/11/10.
//  Copyright © 2016年 zhh. All rights reserved.
//

#import "HHRuleView.h"
#import "UIColor+HexString.h"

#define HBackGroundColor [UIColor whiteColor]
#define HHeight [UIScreen mainScreen].bounds.size.height
#define HWidth self.frame.size.width
#define Hone [UIScreen mainScreen].bounds.size.width / 320.0

#define kFontOfSize(font) ([UIScreen mainScreen].bounds.size.width < 375 ? 0.8 : 1.0)*font
#define kFontSize(font) [UIFont systemFontOfSize:kFontOfSize(font)]
#define KHexColor(color) [UIColor colorWithHexString:color]


@interface HHRuleView()<UIScrollViewDelegate>



@property (nonatomic, assign)NSInteger maxValue;//!< 最大值
@property (nonatomic, assign)NSInteger minValue;//!< 最小值
@property (nonatomic, assign)NSInteger scale;//!< 刻度比例 一大格十小格的
@property (nonatomic, assign)NSInteger pointValue;//!< 指针指向的值
@property (nonatomic, strong)UIView *pointView;//!< 指针

@end

NSInteger ruleImageViewHeight = 18;//刻度尺图片高度
CGFloat scrollViewScaleForRuleView = 4.0; // 整个刻度尺高度占整个控件的比例
CGFloat ruleLineHeight = 1.4                                                                                                    ; //刻度尺两边线条的宽度
CGFloat topDistance = 12.0; //控件之间 垂直距离

@implementation HHRuleView

+(HHRuleView *)ruleViewWithMaxValue:(NSInteger)maxValue minValue:(NSInteger)minValue scale:(NSInteger)scale frame:(CGRect)frame{
    HHRuleView *ruleView = [HHRuleView new];
    ruleView.backgroundColor = HBackGroundColor;
    ruleView.maxValue = maxValue;
    ruleView.minValue = minValue;
    ruleView.scale = scale;
    ruleView.isRound = YES;
    ruleView.frame = frame;
    
    
    [ruleView setScrollView];
    [ruleView setPointView];
    
    return ruleView;
}

-(void)setScrollView {
    self.scrollView = [[UIScrollView alloc] init];
    //刻度尺的位置
    self.scrollView.frame = CGRectMake(0, 0, self.frame.size.width, 40);
    UIImage *ruleImage =[UIImage imageNamed:@"rule"];
    //放几张刻度尺图
    NSInteger multiple = (self.maxValue-self.minValue)%self.scale == 0 ? (self.maxValue-self.minValue)/self.scale : (self.maxValue-self.minValue)/self.scale + 1;
    CGFloat contSizeWith = ruleImage.size.width*multiple+HWidth;
    self.scrollView.contentSize=CGSizeMake(contSizeWith, 40);
    self.scrollView.delegate = self;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.decelerationRate = 0;
    self.scrollView.bounces = NO;
    
    [self addSubview:self.scrollView];
    [self setImages];

}
-(void)setImages {
    NSInteger multiple = (self.maxValue-self.minValue)%self.scale == 0 ? (self.maxValue-self.minValue)/self.scale : (self.maxValue-self.minValue)/self.scale + 1;
    UIImage *ruleImage = [UIImage imageNamed:@"rule"];
    for (int a = 0; a < multiple; a++) {
        UIImageView *rule = [[UIImageView alloc] initWithImage:ruleImage];
        rule.frame = CGRectMake(HWidth/2+a*ruleImage.size.width, CGRectGetMaxY(self.scrollView.bounds)-ruleImageViewHeight, ruleImage.size.width, ruleImageViewHeight);
        rule.tag = 100+a;
        if (a == multiple-1) {
            [rule setImage:[UIImage imageNamed:@"ruleEnd"]];
        }
        [self.scrollView addSubview:rule];
        
        //放刻度尺标度
        [self setRuleImageValueWithIdx:a];
        
    }
    [self setRuleImageValueWithIdx:multiple];
    
    //刻度尺两边的线条
//    UIImageView *lineImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.scrollView.frame.size.height-ruleLineHeight, HWidth/2, ruleLineHeight)];
//    lineImage.backgroundColor = KHexColor(@"cccccc");
//    [self.scrollView addSubview:lineImage];
    UIImageView *lineImage2 = [[UIImageView alloc] initWithFrame:CGRectMake(self.scrollView.contentSize.width-HWidth/2, self.scrollView.frame.size.height-ruleLineHeight, HWidth/2, ruleLineHeight)];
//    lineImage2.backgroundColor = lineImage.backgroundColor;
    [self.scrollView addSubview:lineImage2];
}
- (void)setRuleImageValueWithIdx:(NSInteger)idx {
    UIImage *image = [UIImage imageNamed:@"rule"];
    UILabel *label = [UILabel new];
    int labelX = HWidth/2 + image.size.width * idx - image.size.width/2;
    label.frame = CGRectMake(labelX, CGRectGetMaxY(self.scrollView.bounds)-ruleImageViewHeight-13, image.size.width, 10);
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont systemFontOfSize:12];
    label.adjustsFontSizeToFitWidth = YES;
    label.text = [NSString stringWithFormat:@"%ld",  self.minValue + idx*self.scale];
    [self.scrollView addSubview:label];
}

- (void)setPointView {
    UIImageView  *pointView = [[UIImageView alloc] initWithFrame:CGRectMake(HWidth/2.0-5, 17, 11, 25)];
    pointView.image = [UIImage imageNamed:@"xuanzhongxian-jine"];
//    UIView
//    pointView.backgroundColor = [UIColor redColor];
    [self addSubview:pointView];
}

- (void)setDefaultValue:(NSInteger)defaultValue {
    if (defaultValue > _maxValue) {
        _defaultValue = _maxValue;
    } else if (defaultValue < _minValue) {
        _defaultValue = _minValue;
    } else {
        _defaultValue = defaultValue;
    }
    UIImage *ruleImage = [UIImage imageNamed:@"rule"];
    //刻度尺比例实际值
    CGFloat ruleLength = (CGFloat)self.scale/ruleImage.size.width;
    CGFloat transformX = (CGFloat)(_defaultValue-_minValue)/ruleLength;
    self.scrollView.contentOffset = CGPointMake(transformX, 0);
    [self scrollViewDidEndDecelerating:_scrollView];
    
}

#pragma mark - UIScrollViewDelegate 
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([self.delegate respondsToSelector:@selector(ruleViewDidScroll:pointValue:)]) {
        UIImage *ruleImage = [UIImage imageNamed:@"rule"];
        //刻度尺比例实际值
        CGFloat ruleLength = (CGFloat)self.scale/ruleImage.size.width;
        //指针指向的刻度
        CGFloat value=0;
        //滑动的刻度值
        CGFloat scrollValue=0;

        CGFloat contentOffSetX =0;
        contentOffSetX  = scrollView.contentOffset.x;
        
        scrollValue= ruleLength*contentOffSetX;
        if (self.isRound) {
            value=[self changeHundredValueWithValue:scrollValue];
        }else{
            value=self.minValue+scrollValue;
        }
        value = value < self.minValue ? self.minValue : value;
        value = value > self.maxValue ? self.maxValue : value;
        self.pointValue = value;
        [self.delegate ruleViewDidScroll:self pointValue:self.pointValue];
    }
    
}


//调控偏移的距离
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if(!decelerate)
    {   //OK,真正停止了，do something}
        [self.delegate ruleViewDidEndScroll:self pointValue:self.pointValue];
    }
    [self controlScrollViewContentOffSet:scrollView];
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self.delegate ruleViewDidEndScroll:self pointValue:self.pointValue];
    [self controlScrollViewContentOffSet:scrollView];
}


- (void)controlScrollViewContentOffSet:(UIScrollView *)scrollView {
    if (self.isRound) {
        UIImage *ruleImage = [UIImage imageNamed:@"rule"];
        //刻度尺比例实际值
        CGFloat ruleLength = self.scale/ruleImage.size.width;
        CGFloat resultContentX = (self.pointValue-self.minValue)/ruleLength;
        scrollView.contentOffset = CGPointMake(resultContentX, 0);
    }
}


- (NSInteger)changeHundredValueWithValue:(CGFloat)value {
    NSInteger result = 0;
    NSInteger oneTenth = self.scale/5;

    if ((int)value % oneTenth > oneTenth/2) {
        result = ((int)value/oneTenth+1)*oneTenth;
    } else {
        result = ((int)value/oneTenth)*oneTenth;
    }
    
    if (result > self.maxValue) {
        return self.maxValue;
    }
    
    return result + self.minValue;
}




@end
