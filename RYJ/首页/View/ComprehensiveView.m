//
//  ComprehensiveView.m
//  RYJ
//
//  Created by wyp on 2017/7/20.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "ComprehensiveView.h"

#import "leftAndRightLabel.h"

#import "KSPhotoBrowser.h"

#import "UILabel+copy.h"

@interface ComprehensiveView ()<UITableViewDelegate, UITableViewDataSource, UIWebViewDelegate> {
    
    __weak IBOutlet RootTableView *myTableView;
    UIViewController *theVC;
    
    
    
    NSArray *leftArr;
    NSArray *RightArr;
    __weak IBOutlet NSLayoutConstraint *view_H;
    
    
    NSString *copyText;
}

@end

@implementation ComprehensiveView

@synthesize bottomBtn;
@synthesize titleLabel;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (IBAction)removeSelf:(UIButton *)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self endEditing:YES];
        [self removeFromSuperview];
        [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    });
}

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc {
    myTableView.delegate = self;
    myTableView.dataSource = self;
    theVC = vc;
    
    leftArr = dict[@"L"];
    RightArr = dict[@"R"];
    
    if ((89 + 44*leftArr.count) > HEIGHT_K-64-130) {
        view_H.constant = HEIGHT_K-64-130;
    } else {
        view_H.constant = 89 + 44*leftArr.count;
    }
    [myTableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return leftArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    leftAndRightLabel loadingTableViewCell("leftAndRightLabel", )
    cell.leftLabel.text = leftArr[indexPath.row];
    cell.rightLabel.text = RightArr[indexPath.row];
    cell.rightLabel.hidden = NO;
    cell.rightLabel.textColor = NAVCOLOR_C(102.0, 102.0, 102.0);
    cell.bottomView.hidden = NO;
    
    cell.rightLabel.isCopyable = YES;
    
    if (![_bottomStatus isEqualToString:@"1"]) {
        if (indexPath.row == leftArr.count - 1) {
            cell.rightLabel.textColor = appDefaultColor;
            cell.leftLabel.textColor = appDefaultColor;
            cell.bottomView.hidden = YES;
            
            if ([_bottomStatus isEqualToString:@"2"] || [_bottomStatus isEqualToString:@"3"]) {
                cell.leftLabel.textColor = [UIColor redColor];
            }
        }
        
        if ([_bottomStatus isEqualToString:@"3"]) {
            if (indexPath.row == leftArr.count-2) {
                cell.rightLabel.textColor = appDefaultColor;
                cell.leftLabel.textColor = appDefaultColor;
                UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lookZFB)];
                cell.leftLabel.userInteractionEnabled = YES;
                [cell.leftLabel addGestureRecognizer:tap1];
                
                UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lookWX)];
                cell.rightLabel.userInteractionEnabled = YES;
                [cell.rightLabel addGestureRecognizer:tap2];
            } else if (indexPath.row == leftArr.count-3) {
                UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openQQ)];
                cell.rightLabel.userInteractionEnabled = YES;
                [cell.rightLabel addGestureRecognizer:tap1];
            }
        }
        
    }
    if (indexPath.row == leftArr.count - 1) {
        cell.bottomView.hidden = YES;
    }
    return cell;
}


- (void)openQQ {
    [HttpUrl GET:@"paramSetting/findMyConfig" dict:@{} hud:theVC.view isShow:YES WithSuccessBlock:^(id data) {
        NSLog(@"还款信息:%@", data);
        NSArray *array = data[@"data"];
        
        NSString  *qqNumber=[array[2][@"configValue"] description];
        
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"mqq://"]]) {
            UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectZero];
            NSURL * url=[NSURL URLWithString:[NSString stringWithFormat:@"mqq://im/chat?chat_type=wpa&uin=%@&version=1&src_type=web",qqNumber]];
            
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            webView.delegate = self;
            [webView loadRequest:request];
            [theVC.view addSubview:webView];
        }
        
    }];
}

- (void)lookZFB {
    NSLog(@"lookZFB");
//    [HttpUrl GET:@"paramSetting/findMyConfig" dict:@{} hud:theVC.view isShow:YES WithSuccessBlock:^(id data) {
//        NSLog(@"还款信息:%@", data);
//        NSArray *array = data[@"data"];
//
    if ([_alipayCode description].length > 0) {
        NSData *data1 = [NSData dataWithContentsOfURL:[NSURL URLWithString:[Http_imgIP stringByAppendingString:[_alipayCode description]]]];
        
        UIImage *image =  [UIImage imageWithData:data1];
        NSMutableArray *items = @[].mutableCopy;
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        KSPhotoItem *item = [KSPhotoItem itemWithSourceView:imageView image:image];
        [items addObject:item];
        [self removeFromSuperview];
        KSPhotoBrowser *browser = [KSPhotoBrowser browserWithPhotoItems:items selectedIndex:0];
        [browser showFromViewController:theVC];
    }else{
        [TheGlobalMethod xianShiAlertView:@"暂无收款码" controller:theVC];

        [self removeFromSuperview];

    }
    
//
//    }];
    
    
}

- (void)lookWX {
    NSLog(@"lookWX");
//    [HttpUrl GET:@"paramSetting/findMyConfig" dict:@{} hud:theVC.view isShow:YES WithSuccessBlock:^(id data) {
//        NSLog(@"还款信息:%@", data);
//        NSArray *array = data[@"data"];
    if ([_wechatCode description].length > 0) {
        NSData *data1 = [NSData dataWithContentsOfURL:[NSURL URLWithString:[Http_imgIP stringByAppendingString:[_wechatCode description]]]];
        
        UIImage *image =  [UIImage imageWithData:data1];
        NSMutableArray *items = @[].mutableCopy;
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        KSPhotoItem *item = [KSPhotoItem itemWithSourceView:imageView image:image];
        [items addObject:item];
        KSPhotoBrowser *browser = [KSPhotoBrowser browserWithPhotoItems:items selectedIndex:0];
        [self removeFromSuperview];
        [browser showFromViewController:theVC];
    }else{
        [TheGlobalMethod xianShiAlertView:@"暂无收款码" controller:theVC];
        [self removeFromSuperview];
    }
    
//    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (IBAction)Iknow:(UIButton *)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self endEditing:YES];
        [self removeFromSuperview];
        [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    });
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end

