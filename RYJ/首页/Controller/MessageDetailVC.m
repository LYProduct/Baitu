//
//  MessageDetailVC.m
//  MC
//
//  Created by ike on 16/6/28.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "MessageDetailVC.h"
#import "MessageDetailCell.h"

@interface MessageDetailVC ()<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet RootTableView *myTableView;

@end

@implementation MessageDetailVC
@synthesize myTableView;

- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [self.tabBarController.tabBar setHidden:YES];
    self.view.backgroundColor = [UIColor redColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    
    
    if (self.titleStr.length > 0) {
        self.title = self.titleStr;
    }
    
    if ([_longStr isEqualToString:@"1"]) {
        NSError *error;
        NSString *textFileContents = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"xy" ofType:@"txt"] encoding:NSUTF8StringEncoding error: & error];
        if (textFileContents == nil) {
            // an error occurred
            NSLog(@"Error reading text file. %@", [error localizedFailureReason]);
        } else {
            
            //        vc.detailStr = textFileContents;
            //        vc.textFont = 14;
            //        vc.theTextColor = NAVCOLOR_C(102.0, 102.0, 102.0);
            
            self.attStr = [[NSAttributedString alloc] initWithString:textFileContents];
            [myTableView reloadData];
            
        }
    }
    
    // Do any additional setup after loading the view.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MessageDetailCell *cell = [TheGlobalMethod setCell:@"MessageDetailCell"];
    if (self.attStr.length > 0) {
        [cell reloadAttCell:_attStr];
        
    } else {
    cell.label.text = self.detailStr;
        if (_textFont>0) {
            cell.label.font = [UIFont systemFontOfSize:_textFont];
        }
        if (_theTextColor) {
            cell.label.textColor = _theTextColor;
        }
    [cell reloadCell:self.detailStr];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.attStr.length > 0) {
        CGSize size = [_attStr boundingRectWithSize:CGSizeMake(WIDTH_K-24, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin context:nil].size;
        return size.height + 20;
    }
    NSInteger size;
    if (_textFont > 0) {
        size = _textFont;
    } else {
        size = 15;
    }
    
    CGSize sizef = [_detailStr boundingRectWithSize:CGSizeMake(WIDTH_K - 24, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingTruncatesLastVisibleLine attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:size]} context:nil].size;
    return sizef.height + 25;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
