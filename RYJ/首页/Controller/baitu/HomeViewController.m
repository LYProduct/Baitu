//
//  HomeViewController.m
//  RYJ
//
//  Created by wyp on 2018/1/10.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import "HomeViewController.h"
#import "HWRollingView.h"
#import "HomeViewCollectionViewCell.h"
#import "HomeViewModel.h"

@interface HomeViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
/** 上一次的请求参数 */
@property (nonatomic, strong) NSDictionary *params;
/** 当前页码 */
@property (nonatomic, assign) NSInteger pageNowO;

@property(nonatomic,strong) NSArray *dataSourArray;
@property(nonatomic,strong) NSMutableArray *dataPhoneArray;
/** 图片数据 */
@property(nonatomic,strong) NSMutableArray *pictureArray;
 /** collectionView */
@property(nonatomic,strong) UICollectionView *collectionView;
@end

@implementation HomeViewController

static NSString *const MKPCell = @"HomeViewCell";

-(NSArray *)dataSourArray
{
    if(!_dataSourArray)
    {
        _dataSourArray = [NSArray array];
    }
    return _dataSourArray;
}

-(NSMutableArray *)dataPhoneArray
{
    if(!_dataPhoneArray)
    {
        _dataPhoneArray = [NSMutableArray array];
    }
    return _dataPhoneArray;
}

-(NSMutableArray *)pictureArray
{
    if(!_pictureArray)
    {
        _pictureArray =  [NSMutableArray array];
    }
    return _pictureArray;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // 获取轮播图
    [self getPictureURL];
    [self.pictureArray removeAllObjects];
}

  // 获取轮播图
-(void)getPictureURL
{
    [HttpUrl GET:@"eachPicture/selectList" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if (BB_isSuccess) {
            if (![data[@"data"] isEqual:[NSNull null]]) {
                self.dataSourArray = data[@"data"];
                NSString *imageIP = Http_imgIP;
                for (NSDictionary *dic in self.dataSourArray) {
                    NSString *picStr =[imageIP stringByAppendingString:dic[@"imgUrl"]];
                    [self.pictureArray addObject:picStr];
                }
                [self createView];
                [self setupRefresh];
            }
        }else{
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"首页";
    
}
#pragma mark --- 创建视图
-(void)createView
{
    // 轮播图
    __weak typeof(self)weakSelf = self;
    HWRollingView *RollingView = [HWRollingView rollingViewWithFrame:CGRectMake(0, navY, WIDTH_K, H(190)) RollingStyle:RollingStyleNetwork Data:self.pictureArray action:^(NSInteger index) {
        //            NSLog(@"点击了第%ld页", index);
    }];
     [weakSelf.view addSubview:RollingView];
    // 布局
    UICollectionViewFlowLayout *whcLayout = [[UICollectionViewFlowLayout alloc]init];
    whcLayout.minimumLineSpacing = W(1);
    whcLayout.minimumInteritemSpacing = W(0);
    whcLayout.itemSize = CGSizeMake((WIDTH_K - W(1))/2, H(235));
    
    self.collectionView  = [[UICollectionView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(RollingView.frame) + H(10), WIDTH_K, HEIGHT_K - navY - CGRectGetMaxY(RollingView.frame) + H(4)) collectionViewLayout:whcLayout];
    self.collectionView.backgroundColor = NAVCOLOR_C(242, 242, 242);
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.view addSubview:self.collectionView];
    [self.collectionView registerNib:[UINib nibWithNibName:@"HomeViewCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:MKPCell];
    self.collectionView.showsVerticalScrollIndicator = NO;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView.alwaysBounceVertical = YES;
}
#pragma mark - 刷新
-(void)setupRefresh
{
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewx)];
    self.collectionView.mj_header.automaticallyChangeAlpha  = YES;
    [self.collectionView.mj_header beginRefreshing];
//    self.collectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
}
/**
 * 加载新的数据
 */
-(void)loadNewx
{
    // 结束上拉
    [self.collectionView.mj_footer endRefreshing];
     self.pageNowO = 1 ;
    NSString *stringInt = [NSString stringWithFormat:@"%ld",self.pageNowO];
    // 参数
    NSMutableDictionary *parmsO = [NSMutableDictionary dictionary];
    parmsO[@"current"] = stringInt;
    parmsO[@"size"] = @"6";
    self.params = parmsO;

    [HttpUrl GET:@"goods/selectPage" dict:parmsO hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if (BB_isSuccess) {
            NSDictionary *dic = [NSDictionary dictionary];
            dic = data[@"data"];
            self.dataPhoneArray = [HomeViewModel  mj_objectArrayWithKeyValuesArray:dic[@"records"]];
            [self.collectionView reloadData];
        }else{
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];

    [self.collectionView.mj_header endRefreshing];
    
}

-(void)loadMore
{
    // 结束下拉
    [self.collectionView.mj_header endRefreshing];
    NSInteger page = self.pageNowO + 1;
    NSString *stringInt = [NSString stringWithFormat:@"%ld",page];

    // 参数
    NSMutableDictionary *parmsO = [NSMutableDictionary dictionary];
    parmsO[@"current"] = stringInt;
    parmsO[@"size"] = @"4";
    self.params = parmsO;

    [HttpUrl GET:@"goods/selectPage" dict:parmsO hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if (BB_isSuccess) {
            NSDictionary *dic = [NSDictionary dictionary];
            dic = data[@"data"];
            
             NSArray *newInventory = [HomeViewModel  mj_objectArrayWithKeyValuesArray:dic[@"records"]];
            [self.dataPhoneArray addObjectsFromArray:newInventory];
            [self.collectionView reloadData];
        }else{
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];

    self.pageNowO = page;
    [self.collectionView reloadData];
    [self.collectionView.mj_footer endRefreshing];
}

#pragma mark collectionViewDatasouce
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    //    return self.viewModel.dataArray.count;
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataPhoneArray.count;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"当前点击的是第%ld个cell",(long)indexPath.item);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HomeViewCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MKPCell forIndexPath:indexPath];
    
    cell.HomeViewModel = self.dataPhoneArray[indexPath.row];
    cell.assessmentBtBlock = ^{
        self.tabBarController.selectedIndex = 2;
    };
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
