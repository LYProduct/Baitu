
//
//  ApplyForBorrowVC.m
//  RYJ
//
//  Created by wyp on 2017/7/20.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "ApplyForBorrowVC.h"

#import "leftAndRightLabel.h"
#import "LLICell.h"
#import "LoanAgreementCell.h"
#import "AttLabelCell.h"

#import "BorrowResultsVC.h"
#import "CouponsListVC.h"//优惠券列表

#import "SignContractVC.h"

//连连支付
#import "LLPayUtil.h"
#import "LLOrder.h"
#import "LLPaySdk.h"
/*! TODO: 修改两个参数成商户自己的配置 */
static NSString *kLLOidPartner = @"201710200001044004";//@"201408071000001546";                 // 商户号
static NSString *kLLPartnerKey = @"MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAKh6JJ/fHk4h33R6SUjH+qV3AVwS0Db2+sy7+QLjp2zNxGI3yTnz7lXvbEi7ua+jzWdXbOJ7Jb5RZLaCNtL3XJ7ztLVBI0eDSqFtonEiTS34j0rRUjA5esbH8WgUV/qBuVcCnuuaR+/xHMm518GUj9rurMMlo4umRyrIKfkvdMTRAgMBAAECgYAzOV/lU9tM+jOMoJmjjVoB8Rryua2g6Jr8oLJnVHxVMoLhMgD2o9n1OuR7gMhGRNFw+D3c5oBlSy4J57Fvl1ILa7j/mX8PGc7hbf/T9S8m1GIk2dU/L4fLhzYak3CGtvQLdJTecB4qca8U9xs26PW5ecjCRQ7xUa5RsjpPX5LHoQJBANj6xjEBgDOhiAp8WLfZ2lWIjQ8JFKINSTvQl6XKokl52HUorBl5VwplcrBgU8PMUEa7Wjpn8AELs4TnCGioep0CQQDGxm4rkNnGWKhee1ooENzuevG28+pgHoqjkRq437IBRslXx95CpJQftsQv7gLyi3GVfa20+ZeuLcdFKa/5nvLFAkBJeEiJqXduhCCbZFVqE3MxiSYyVCHPaW5FubDPq5heBsr7iMUVbxfA5m76N/PFFbaM7L11j2IeCIdF1jDrtzilAkEAjvVbet28olhVD06r8rDeFG47tZcLc4HDuAu+KkRH438jg4xtn4R6O6zYwJfvar07PYacDdKMcmreLDHlLCRS+QJAeVYoWWb2MCnyj/7PzyAt4xMP0oJEJOn67pccUxTYxAVSt4t5OZkdL4WImcjBq98Ie7FLOl7DsNcLJSPiWMDh/g==";//@"201408071000001546_test_20140815";   // 密钥
static NSString *signType = @"RSA";//签名方式


/*! 接入什么支付产品就改成那个支付产品的LLPayType，如快捷支付就是LLPayTypeQuick */

static LLPayType payType = LLPayTypeInstalments;

@interface ApplyForBorrowVC ()<UITableViewDelegate, UITableViewDataSource, LLPaySdkDelegate> {
    
    __weak IBOutlet RootTableView *myTableView;
    NSDictionary *dataDic;
    __weak IBOutlet UIView *BView;
    NSString *isAgree;
    
    /**连连支付配置信息*/
    NSString *userId;
    /*！ 若是签约、认证、实名快捷、分期付等请填入以下内容 */
    NSString *cardNumber;  //卡号
    NSString *acctName;    //姓名
    NSString *idNumber;    //身份证号
    NSString *money;    //钱
    NSString *orderId;  //订单id
    NSString *no_order;
    
    NSDictionary * _couponDict;
}

@property (nonatomic, retain) NSMutableDictionary *orderDic;

@property (nonatomic, strong) NSString *resultTitle;

@property (nonatomic, strong) LLOrder *order;

@end

@implementation ApplyForBorrowVC

- (void)viewDidLoad {
    [super viewDidLoad];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    
    myTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, 10)];
    myTableView.tableFooterView.backgroundColor = [UIColor clearColor];
    isAgree = @"0";
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self getNetWorking];
    });
    // Do any additional setup after loading the view.
}

- (void)getNetWorking {
    [HttpUrl GET:@"loanOrder/getLoanNeedData" dict:@{@"money":_borrowMoney, @"days":_limitDays} hud:self.view isShow:NO WithSuccessBlock:^(id data) {
        if ([data[@"success"] integerValue] == 1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                BView.hidden = NO;
                dataDic = data[@"data"];
                [myTableView reloadData];
            });
            
        } else {
            [TheGlobalMethod popAlertView:data[@"msg"] controller:self];
        }
       NSLog(@"借款使用优惠券数据:%@", data);
    }];
}

#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (!dataDic) {
        return 0;
    }
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 5;
    } else if (section == 1) {
        return 1;
    } else if (section == 2) {
        return 1;
    } else if (section == 3) {
        return 5;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2) {
        LLICell loadingTableViewCell("LLICell", )
        if ([dataDic[@"saveMoney"] isEqual:[NSNull null]]) {
            cell.label.text = @"未使用";;
        } else {
            [Methods dataInText:dataDic[@"saveMoney"] InLabel:cell.label defaultStr:@"-"];
            cell.label.text = [cell.label.text stringByAppendingString:@"元"];
            cell.label.text = [@"-" stringByAppendingString:cell.label.text];
        }
        return cell;
    } else if (indexPath.section == 4) {
//        if (indexPath.row == 0) {
//            leftAndRightLabel loadingTableViewCell("leftAndRightLabel", )
//            cell.bottomView.hidden = NO;
//            cell.leftLabel.text = @"交易协议";
//            return cell;
//        } else if (indexPath.row == 1) {
//            LoanAgreementCell loadingTableViewCell("LoanAgreementCell",)
//            cell.returnClick = ^(NSString *str) {
//                isAgree = str;
//            };
//            [cell reloadCellForData:dataDic vc:self];
//            return cell;
//        } else
            if (indexPath.row == 0) {
            AttLabelCell loadingTableViewCell("AttLabelCell", )
            if (_couponDict.allKeys.count > 0) {
                cell.dict = _couponDict;
            }
            [cell reloadCellForData:dataDic vc:self];
            return cell;
        }
    }
    leftAndRightLabel loadingTableViewCell("leftAndRightLabel", )
    if (_couponDict.allKeys.count > 0) {
        cell.dict = _couponDict;
    }
    cell.returnCenterBtnClick = ^{
        NSLog(@"点击:%@", indexPath);
        [Methods showComprehensiveViewLeftArr:@[@"平台服务费", @"信息认证费",@"风控服务费", @"风险准备金", @"利息", @"总计"] rightArr:@[[self stringByAppendingString:[dataDic[@"placeServePercent"] description]], [self stringByAppendingString:[dataDic[@"msgAuthPercent"] description]],[self stringByAppendingString:[dataDic[@"riskServePercent"] description]], [self stringByAppendingString:[dataDic[@"riskPlanPercent"] description]], [self stringByAppendingString:[dataDic[@"interest"] description]], [self stringByAppendingString:[dataDic[@"omnibusFee"] description]]] bottomStatus:nil selfVC:self];
    };
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:dataDic];
    [dict setObject:_borrowMoney forKey:@"borrowMoney"];
    [dict setObject:_limitDays forKey:@"limitDays"];
    [cell reloadData:dict vc:self indexPath:indexPath];
    return cell;
}

- (NSString *)stringByAppendingString:(NSString *)string {
    return [string stringByAppendingString:@"元"];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2) {
        CouponsListVC *vc = [TheGlobalMethod setstoryboard:@"CouponsListVC" controller:self];
        vc.selectCoupon = @"1";
        vc.returnSelectCoupon = ^(NSDictionary *dic) {
            _couponDict = dic;
            [myTableView reloadData];
        };
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)useCoupon:(NSString *)couponId {
    [HttpUrl GET:@"loanOrder/useCoupon" dict:@{@"userCouponId":couponId, @"orderId":[dataDic[@"id"] description]} hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if ([data[@"success"] integerValue] == 1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                dataDic = data[@"data"];
                [myTableView reloadData];
            });
            
        } else {
            [TheGlobalMethod popAlertView:data[@"msg"] controller:self];
        }
        NSLog(@"借款使用优惠券数据:%@", data);
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return 44;
        } else if (indexPath.row == 4) {
            return 36;
        }
        return 24;
    }  else if (indexPath.section == 3) {
        if (indexPath.row == 0 || indexPath.row == 4) {
            return 44;
        } else if (indexPath.row == 3) {
            return 36;
        }
        
        return 24;
    }
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, 10)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (IBAction)ApplyBtn:(UIButton *)sender {
    
//    if (![isAgree isEqualToString:@"1"]) {
//        ShowAlert(@"请阅读服务协议并勾选同意按钮后申请", self);
//        return;
//    }
    NSDictionary * dict;
    if (_couponDict.allKeys.count > 0) {
        dict = @{@"borrowMoney":_borrowMoney,@"realMoney":dataDic[@"realMoney"],@"limitDays":_limitDays,@"userCouponId":[_couponDict[@"id"] description],@"paramSettingId":dataDic[@"paramSettingId"]};
    }else{
        dict = @{@"borrowMoney":_borrowMoney,@"realMoney":dataDic[@"realMoney"],@"limitDays":_limitDays,@"paramSettingId":dataDic[@"paramSettingId"]};
    }
    [HttpUrl POST:@"loanOrder/add" dict:dict hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if (BB_isSuccess) {
            SignContractVC * vc = [[SignContractVC alloc]initWithNibName:@"SignContractVC" bundle:nil];
            vc.returnStr = [NSString stringWithFormat:@"%@",data[@"data"]];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
    
    
//    [self getLLMsg];
    
}

- (void)getLLMsg {
    
    __block ApplyForBorrowVC *weakSelf = self;
    
    [HttpUrl GET:@"user/getSignMsg" dict:@{@"orderId":[dataDic[@"id"] description]} hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        NSLog(@"获取签约身份信息%@", data);
        if ([data[@"success"] integerValue] == 1) {
            NSDictionary *dict = data[@"data"];
            userId = [dict[@"userId"] description];
            cardNumber = [dict[@"bankNum"] description];
            acctName = [dict[@"name"] description];
            idNumber = [dict[@"idCard"] description];
            money = [dict[@"money"] description];
            orderId = [dict[@"orderId"] description];
            no_order = [dict[@"no_order"] description];
            [weakSelf llorder];
        } else {
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
        
    }];
}

- (void)llorder {
    _order = [[LLOrder alloc] initWithLLPayType:payType];
    NSString *timeStamp = [LLOrder timeStamp];
    _order.oid_partner = kLLOidPartner;
    _order.sign_type = signType;
    _order.busi_partner = @"101001";
    _order.no_order = orderId;
    _order.dt_order = timeStamp;
    _order.money_order = money;
    _order.notify_url = [NSString stringWithFormat:@"http://www.baidu.com"];
    _order.acct_name = acctName;
    _order.card_no = cardNumber;
    _order.id_no = idNumber;
    _order.risk_item = [LLOrder llJsonStringOfObj:@{@"user_info_dt_register" : orderId}];
    _order.user_id = userId;
    _order.name_goods = @"百途还款";
    //    _order.sign = @"ivaTAz8c/YSWYXf37G37y0uSyDbTx92iK+HcCi5sWJT2o9ne4hREVaDYirMGqHd6R4BMpk6qhfcC4qbYyv7FuisYU/8OfszwFmj5rXtcevMAZcPPRft4mBFvdT13QvgVIknPSreJ2DoBn/cAnCt5GKwdUJ4wMDnKs40qzu+F0fw=";
    
    //    if (payType == LLPayTypeInstalments) {
    //        _order.repayment_plan = [LLOrder llJsonStringOfObj:@{@"repaymentPlan":@[@{@"date" : @"2017-09-01", @"amount" : @"0.01"}]}];
    //        _order.repayment_no = [NSString stringWithFormat:@"%@%@", @"FenQi", timeStamp];
    //        _order.sms_param = [LLOrder llJsonStringOfObj:@{@"contract_type" : @"短信显示的商户名",
    //                                                        @"contact_way" : @"400-018-8888"}];
    //    }
    [self startSign];
}

- (void)startSign {
    self.resultTitle = @"签约结果";
    self.orderDic = [[_order tradeInfoForSign] mutableCopy];
    
    LLPayUtil *payUtil = [[LLPayUtil alloc] init];
    
    payUtil.signKeyArray = @[
                             @"acct_name",
                             @"card_no",
                             @"id_no",
                             @"oid_partner",
                             @"risk_item",
                             @"sign_type",
                             @"user_id"
                             ];
    
    BOOL isInstalmentsPay = payType == LLPayTypeInstalments;
    
    if (isInstalmentsPay) { //如果是分期付，那么repayment_plan/repayment_no/sms_param需要参与签名
        payUtil.signKeyArray = @[
                                 @"acct_name",
                                 @"card_no",
                                 @"id_no",
                                 @"oid_partner",
                                 @"risk_item",
                                 @"sign_type",
                                 @"user_id",
                                 @"repayment_plan",
                                 @"repayment_no",
                                 @"sms_param"
                                 ];
    }
    NSDictionary *signedOrder =
    [payUtil signedOrderDic:self.orderDic andSignKey:kLLPartnerKey];
    [LLPaySdk sharedSdk].sdkDelegate = self;
    [[UINavigationBar appearance] setBarStyle:UIBarStyleBlack];
    [[LLPaySdk sharedSdk] presentLLPaySignInViewController:self
                                               withPayType:payType
                                             andTraderInfo:signedOrder];
}

#pragma - mark 支付结果 LLPaySdkDelegate
// 订单支付结果返回，主要是异常和成功的不同状态
// TODO: 开发人员需要根据实际业务调整逻辑
- (void)paymentEnd:(LLPayResult)resultCode withResultDic:(NSDictionary *)dic {
    
    NSString *msg = @"异常";
    switch (resultCode) {
        case kLLPayResultSuccess: {
            
            [HttpUrl GET:@"loanOrder/confirmOrder" dict:@{@"orderId":[dataDic[@"id"] description]} hud:self.view isShow:YES WithSuccessBlock:^(id data) {
                NSLog(@"提交订单数据:%@", data);
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                self.returnReloadView();
                if ([data[@"success"] integerValue] == 1) {
                    [HttpUrl GET:@"user/getSignPass" dict:@{@"no_agree":dic[@"no_agree"], @"no_order":no_order, @"user_id":dic[@"user_id"], @"orderId":[dataDic[@"id"] description]} hud:self.view isShow:YES WithSuccessBlock:^(id data) {
                        
                    }];
                } else {
                    [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
                }
            }];
            msg = @"签约成功";
        } break;
        case kLLPayResultFail: {
            msg = @"签约失败";
        } break;
        case kLLPayResultCancel: {
            msg = @"签约取消";
        } break;
        case kLLPayResultInitError: {
            msg = @"连连签约初始化异常";
        } break;
        case kLLPayResultInitParamError: {
            msg = dic[@"ret_msg"];
        } break;
        default:
            break;
    }
    NSLog(@"签约信息:%@", dic);
    //    NSString *showMsg =
    //    [msg stringByAppendingString:[LLPayUtil jsonStringOfObj:dic]];
    
    [JXTAlertView showAlertViewWithTitle:self.resultTitle message:msg cancelButtonTitle:nil buttonIndexBlock:^(NSInteger buttonIndex) {
        userId = @"";
        cardNumber = @"";
        acctName = @"";
        idNumber = @"";
        money = @"";
        orderId = @"";
        [self.navigationController popToRootViewControllerAnimated:YES];
    } otherButtonTitles:@[@"确定"]];
}

- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
