//
//  MessageDetailVC.h
//  MC
//
//  Created by ike on 16/6/28.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageDetailVC : RootViewController

@property (nonatomic, strong) NSString *detailStr;

@property (nonatomic, strong) NSString *titleStr;

@property (nonatomic, assign) NSInteger textFont;

@property (nonatomic, strong) UIColor *theTextColor;

@property (nonatomic, strong) NSAttributedString *attStr;

@property (nonatomic, strong) NSString *longStr;

@end
