//
//  ContractWebVC.m
//  RYJ
//
//  Created by liuyong on 2017/12/19.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "ContractWebVC.h"
#import "YHTTokenManager.h"

@interface ContractWebVC ()<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation ContractWebVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"签署合同";
    self.webView.delegate = self;
    self.webView.scalesPageToFit = YES;
    self.webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self refresh];
}

/**
 *  根据'contractID'、'token'直接展示拼接的 url 地址在’UIWebView‘展示即可，
 *
 **/
- (void)refresh{
    NSMutableString *__urlStr = [NSMutableString stringWithString:@"https://sdk.yunhetong.com/sdk/contract/hView?"];
    NSString * contractId = [NSString stringWithFormat:@"%@",_dict[@"contractId"]];
    
    [__urlStr appendFormat:@"contractId=%lld", [contractId longLongValue]];
    [__urlStr appendFormat:@"&token=%@", _dict[@"token"]];
    
    NSURL  *url = [NSURL URLWithString:__urlStr];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSURLRequest *__request = [NSURLRequest requestWithURL:url
                                               cachePolicy:NSURLRequestReloadIgnoringCacheData
                                           timeoutInterval: 20.0];
    [_webView loadRequest:__request];
    
}
-(void)webViewDidStartLoad:(UIWebView *)webView{
    
}
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
