//
//  BorrowResultsVC.m
//  RYJ
//
//  Created by wyp on 2017/7/21.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "BorrowResultsVC.h"

#import "BorrowDetailVC.h"

#import "HomeVC.h"

@interface BorrowResultsVC () {
    
    __weak IBOutlet UIImageView *topImg;
    __weak IBOutlet UILabel *topLabel;

    __weak IBOutlet UILabel *bottomLabel;
    __weak IBOutlet UIButton *btn;
}

@end

@implementation BorrowResultsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.disableDragBack = YES;
    
    // Do any additional setup after loading the view.
    if ([_idMsg isEqualToString:@"续期成功"]) {
        topLabel.text = @"恭喜您!续期成功";
        bottomLabel.text = @"提前还款信用增值";
        [btn setTitle:@"借款详情" forState:(UIControlStateNormal)];
    } else if ([_idMsg isEqualToString:@"续期失败"]) {
        topImg.image = [UIImage imageNamed:@"shibai"];
        topLabel.text = @"续期扣款失败";
        bottomLabel.text = @"请查看扣款银行卡余额!";
        [btn setTitle:@"借款详情" forState:(UIControlStateNormal)];
    } else if ([_idMsg isEqualToString:@"还款成功"]) {
        topLabel.text = @"恭喜您!还款成功";
        bottomLabel.hidden = YES;
        [btn setTitle:@"再次借款" forState:(UIControlStateNormal)];
    }
}

- (IBAction)pushDetail:(UIButton *)sender {
    if ([sender.titleLabel.text isEqualToString:@"借款详情"]) {
        BorrowDetailVC *vc = [TheGlobalMethod setstoryboard:@"BorrowDetailVC" controller:self];
        vc.orderId = _orderID;
        vc.yuqi = @"0";
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (IBAction)pop:(UIBarButtonItem *)sender {
    if ([_idMsg isEqualToString:@"续期成功"]) {
        
    } else if ([_idMsg isEqualToString:@"续期失败"]) {
        
    } else if ([_idMsg isEqualToString:@"还款成功"]) {
        
    } else if ([_idMsg isEqualToString:@"借款成功"]) {
        for (UIViewController *temp in self.navigationController.viewControllers) {
            if ([temp isKindOfClass:[HomeVC class]]) {
                [self.navigationController popToViewController:temp animated:YES];
            }
        }
    }
    
    kNavPop
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
