//
//  ApplyForBorrowVC.h
//  RYJ
//
//  Created by wyp on 2017/7/20.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "RootViewController.h"

@interface ApplyForBorrowVC : RootViewController

@property (nonatomic ,strong) NSString *borrowMoney;
@property (nonatomic, strong) NSString *limitDays;

@property (nonatomic, strong) void(^returnReloadView)();

@end
