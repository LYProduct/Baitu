//
//  MsgListVC.m
//  XEDK
//
//  Created by wyp on 2017/7/18.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "MsgListVC.h"

#import "MsgCell.h"

#import "MessageDetailVC.h"

@interface MsgListVC ()<UITableViewDelegate, UITableViewDataSource> {

    __weak IBOutlet RootTableView *myTableView;
    NSArray *listArr;
}

@end

@implementation MsgListVC
- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    
    myTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, 12)];
    myTableView.tableHeaderView.backgroundColor = [UIColor clearColor];
    // Do any additional setup after loading the view.
    [self requestNetWorking];
}

- (void)requestNetWorking {

    [HttpUrl GET:@"pushMsgRecord/selectListByUser" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if ([data[@"success"] integerValue] == 1) {
            listArr = data[@"data"];
            NSLog(@"listArr:%@", listArr);
            [myTableView reloadData];
        } else {
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return listArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MsgCell loadingTableViewCell("MsgCell", )
    [Methods dataInText:listArr[indexPath.row][@"title"] InLabel:cell.title defaultStr:@"-"];
    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[listArr[indexPath.row][@"content"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    cell.content.attributedText = attrStr;
//    [Methods dataInText:listArr[indexPath.row][@"content"] InLabel:cell.content defaultStr:@"-"];
    if (![listArr[indexPath.row][@"time"] isEqual:[NSNull null]]) {
        cell.time.text = [TheGlobalMethod dateWithStr:[[listArr[indexPath.row][@"time"] description] substringToIndex:10] Format:@"yyyy-MM-dd HH:mm:ss"];
    }else{
        cell.time.text = @"";
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MessageDetailVC *vc = [TheGlobalMethod setstoryboard:@"MessageDetailVC" controller:self];
//    vc.detailStr = listArr[indexPath.row][@"content"];
    vc.titleStr = listArr[indexPath.row][@"title"];
    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[listArr[indexPath.row][@"content"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    vc.attStr = attrStr;
    [self.navigationController pushViewController:vc animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 158;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
