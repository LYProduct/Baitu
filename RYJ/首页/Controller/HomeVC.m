//
//  HomeVC.m
//  RYJ
//
//  Created by wyp on 2017/7/19.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "HomeVC.h"

#import "ComprehensiveView.h"

#import "ApplyForBorrowVC.h"
#import "MsgListVC.h"
#import "RenewalPayVC.h"

#import "PickerView.h"

#import "MessageDetailVC.h"
#import "SetPasswordVC.h"
#import "RenewalVC.h"

#import <PPBadgeView.h>
#import <JPUSHService.h>

#import <CoreLocation/CoreLocation.h>
#import <ifaddrs.h>
#import <arpa/inet.h>
#import "UIPickerView+malPicker.h"

#import "TheCertificationVC.h"

#import "ChsiLoginVC.h"

#import "ComprehensiveView.h"
#import "AboutUSVC.h"

#import "SocialInsuranceVC.h"
#import "UDIDSafeAuthEngine.h"
#import "UDIDSafeDataDefine.h"
// 目前
#define UDPUBKEY        @"1f6f20c1-7d85-4983-8893-5d240bb86b6b"       // 格式为：@"12121212-1212-1212-1212-121212121212"
#define UDSECURITYKEY   @"ed5185b3-81b7-40b8-9414-14e6a12e9ae3"  // 格式为：@"12121212-1212-1212-1212-121212121212"

@interface HomeVC ()<SDCycleScrollViewDelegate,CLLocationManagerDelegate, UIPickerViewDelegate, UIPickerViewDataSource,UDIDSafeAuthDelegate> {
    
    __weak IBOutlet UIPickerView *leftPickerView;
    __weak IBOutlet UIPickerView *rightPickerView;
    NSArray *leftPickerArr;
    NSArray *rightPickerArr;

    NSInteger selectDayIndex;

    __weak IBOutlet UIImageView *topImg;

    
    __weak IBOutlet UIView *AlsoView;
    __weak IBOutlet UIView *BorrowView;
    
    NSDictionary *dataDic;
    
    NSInteger borrowMoney;
    NSInteger borrowDay;
    
    
    //还款信息
    
    __weak IBOutlet UILabel *needPay;
    __weak IBOutlet UILabel *AlsoDay;
    __weak IBOutlet UILabel *userBorrowMoney;
    __weak IBOutlet UILabel *userApplyDay;
    
    __weak IBOutlet UIButton *alsoBtn;
    
    __weak IBOutlet UIButton *xuqiBtn;
    
    __weak IBOutlet UIButton *statusBtn;
    
    SDCycleScrollView *cycleScrollView;
    SDCycleScrollView *imgCycleScrollView;
    
    NSArray *topImgArr;
    NSString *isCer;
    
    NSDictionary *loginDic;
    
    NSDictionary * _loginUserDict;
    
    NSArray *moneyArr;
    
    
    NSArray * _timeMoneyArr;
    
    __weak IBOutlet UIButton *shenhezhongBtn;
    
    NSMutableArray * leftArr;
    NSMutableArray * rightArr;
    
    NSString * aliPayCode;
    NSString * wechatCode;
    
    NSMutableDictionary * _authDict;
}

@property (nonatomic, strong) CLLocationManager *locationManager;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *leftBarBtn;

@end

@implementation HomeVC
- (IBAction)settingVC:(id)sender {
    AboutUSVC *vc = GETStoryboard(@"AboutUSVC");
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    leftPickerView.delegate = self;
    leftPickerView.delegate = self;
    rightPickerView.delegate = self;
    rightPickerView.dataSource = self;
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
-(void)getCer {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [HttpUrl GET:@"userAuth/selectUserAuth" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

        if (BB_isSuccess) {
            _authDict = [[NSMutableDictionary alloc]initWithCapacity:0];
            
            _authDict = data[@"data"];
           
        }
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    moneyArr = @[@"0",];
    borrowMoney = 0;
    borrowDay = 0;
    [self getCer];
    selectDayIndex = 0;//默认第一个天数
    
    leftPickerArr = @[@"0"];
    rightPickerArr = @[@"0天"];
    
//    [self isCanBorrowMoney];
    leftArr = [[NSMutableArray alloc]initWithCapacity:0];
    rightArr = [[NSMutableArray alloc]initWithCapacity:0];
//
    
    // Do any additional setup after loading the view.
}
// 轮播图
- (void)getBanner {
    [HttpUrl GET:@"eachPicture/selectList" dict:nil hud:self.view isShow:NO WithSuccessBlock:^(id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"轮播图:%@", data);
        if ([data[@"success"] integerValue] == 1) {
            if (![data[@"data"] isEqual:[NSNull null]]) {
                NSMutableArray * arr = [[NSMutableArray alloc]initWithCapacity:0];
                NSArray * daa = data[@"data"];
                for (int i = 0 ; i < daa.count ; i ++ ) {
                    NSString * str = daa[i][@"imgUrl"];
//                    NSString * str1 = [[str substringFromIndex:1] substringToIndex:str.length-1];
                    [arr addObject:[Http_imgIP stringByAppendingString:str]];
                }
                topImgArr = arr;
            }
            [self setImgCycleScrollView];
            imgCycleScrollView.imageURLStringsGroup = topImgArr;
//            topImgArr = data[@"data"];
        } else {
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
}

- (void)push:(NSString *)content alert:(NSString *)alert {
    
    MessageDetailVC *vc = [TheGlobalMethod setstoryboard:@"MessageDetailVC" controller:self];
    vc.detailStr = content;
    vc.titleStr = alert;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
    [TheGlobalMethod insertUserDefault:@"" Key:@"pushContent"];
    [TheGlobalMethod insertUserDefault:@"" Key:@"pushMsg"];
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if (!isStringEmpty(_loginUserDict[@"user"][@"authStatus"])&&[_loginUserDict[@"user"][@"authStatus"] isEqualToString:@"审核成功"]) {
        self.tabBarController.tabBar.hidden = NO;
        UIView *contentView;
        contentView = [self.tabBarController.view.subviews objectAtIndex:0];
        contentView.frame = CGRectMake(contentView.bounds.origin.x, contentView.bounds.origin.y,  contentView.bounds.size.width, contentView.bounds.size.height);
        [_leftBarBtn setImage:[UIImage imageNamed:@""]];
        [_leftBarBtn setTitle:@" "];
        _leftBarBtn.enabled = NO;
    }else{
        UIView *contentView;
        contentView = [self.tabBarController.view.subviews objectAtIndex:0];
        contentView.frame = CGRectMake(contentView.bounds.origin.x,  contentView.bounds.origin.y,  contentView.bounds.size.width, contentView.bounds.size.height + self.tabBarController.tabBar.frame.size.height);    self.tabBarController.tabBar.hidden = YES;
        _leftBarBtn.enabled = YES;
        [_leftBarBtn setTitle:@" "];
        [_leftBarBtn setImage:[UIImage imageNamed:@"shezhi"]];

    }
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (BB_isLogin) {
        [self getLoginUser];
    }
    [self getTimeAndMoney];
    [self getBanner];

    if ([[TheGlobalMethod getUserDefault:@"pushMsg"] length] > 0){
        [self push:[TheGlobalMethod getUserDefault:@"pushContent"] alert:[TheGlobalMethod getUserDefault:@"pushMsg"]];
    }
}
-(void)getTimeAndMoney {
    
    [HttpUrl GET:@"paramSetting/selectIndexMoneyAndDays" dict:nil hud:self.view isShow:NO WithSuccessBlock:^(id data) {
        NSLog(@"借款期限 借款金额 - - %@",data);
        
        if (BB_isSuccess) {
            _timeMoneyArr = data[@"data"][@"moneyAndDays"];
            NSMutableArray * leftArr = [[NSMutableArray alloc]initWithCapacity:0];
            if (_timeMoneyArr.count > 0) {
                for (int i = 0 ; i < _timeMoneyArr.count ; i ++ ) {
                    NSDictionary * dict = _timeMoneyArr[i];
                    [leftArr addObject:[NSString stringWithFormat:@"%@",dict[@"money"]]];
                }
                leftPickerArr = leftArr;
                NSDictionary * dict = _timeMoneyArr[0];
                NSMutableArray * arr = [[NSMutableArray alloc]initWithCapacity:0];
                NSArray * arr1 = dict[@"days"];
                for (int i = 0 ; i < arr1.count ; i ++ ) {
                    [arr addObject:[NSString stringWithFormat:@"%@",arr1[i]]];
                }
                rightPickerArr = arr;
            }
        }
        if (_timeMoneyArr.count > 0) {
            if (!([_loginUserDict[@"user"] isKindOfClass:[NSNull class]])) {
                if (![_loginUserDict[@"user"][@"authStatus"] isEqualToString:@"审核成功"]) {
                    leftPickerArr = @[[data[@"data"][@"maxMoney"] description]];
                    rightPickerArr = @[[data[@"data"][@"maxDay"] description]];
                    leftPickerView.userInteractionEnabled = NO;
                    rightPickerView.userInteractionEnabled = NO;
                }else{
                    leftPickerView.userInteractionEnabled = YES;
                    rightPickerView.userInteractionEnabled = YES;
                }
            }
            
            [leftPickerView reloadAllComponents];
            [rightPickerView reloadAllComponents];
        }
    }];
}
-(void)getLoginUser {
    dispatch_async(dispatch_get_main_queue(), ^{
        [HttpUrl GET:@"user/findLoginUser" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
            if (BB_isSuccess) {
                _loginUserDict = data[@"data"];
                if (!([_loginUserDict[@"user"] isKindOfClass:[NSNull class]])) {
                    if (!isStringEmpty(_loginUserDict[@"user"][@"authStatus"])&& [_loginUserDict[@"user"][@"authStatus"] isEqualToString:@"审核成功"]) {
                        [self showTabBar];

//                        [_loginUserDict[@"user"][@"authStatus"] isEqualToString:@"认证成功"] ||  || [_loginUserDict[@"user"][@"authStatus"] isEqualToString:@"正在审核"]
                        if ([_loginUserDict[@"needPayMoney"] integerValue] == 0) {
                            BorrowView.hidden = NO;
                            AlsoView.hidden = YES;
                        }else{
                            
                            [self getPayWay];
                            [Methods dataInText:[[_loginUserDict[@"gmtDatetime"] description] substringToIndex:10] InLabel:userApplyDay defaultStr:@"-"];
                            
                            [Methods dataInText:_loginUserDict[@"limitPayTime"] InLabel:AlsoDay defaultStr:@"-"];
                            
                            
                            [Methods dataInText:[NSString stringWithFormat:@"%.2f",[_loginUserDict[@"needPayMoney"] floatValue]] InLabel:needPay defaultStr:@"-"];
                            needPay.text = [needPay.text stringByAppendingString:@"元"];
                            [Methods dataInText:[NSString stringWithFormat:@"%.2f",[_loginUserDict[@"needPayMoney"] floatValue]] InLabel:userBorrowMoney defaultStr:@"-"];
                            userBorrowMoney.text = [userBorrowMoney.text stringByAppendingString:@"元"];
                            
                            
                            if ([_loginUserDict[@"orderStatus"] isEqualToString:@"审核中"]) {
                                [statusBtn setTitle:@"审核中" forState:UIControlStateNormal];
                                
                            }else if ([_loginUserDict[@"orderStatus"] isEqualToString:@"打款中"] || [_loginUserDict[@"orderStatus"] isEqualToString:@"待打款"]) {
                                [statusBtn setTitle:@"打款中" forState:UIControlStateNormal];
                                
                            }else{
                                [statusBtn setTitle:@"我要还款" forState:UIControlStateNormal];
                            }
                            BorrowView.hidden = YES;
                            AlsoView.hidden = NO;
                        }
                        
                    }else{
                        [self hideTabBar];
                    }
                }
                
   
            }
        }];
});
    
}

-(void)getPayWay{
    [HttpUrl GET:@"sysConfig/selectSysConfig" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        NSLog(@"支付方式--%@",data);
        leftArr = [[NSMutableArray alloc]initWithCapacity:0];
        rightArr = [[NSMutableArray alloc]initWithCapacity:0];
        if (BB_isSuccess) {
            NSArray * arr = data[@"data"];
            for (int i = 0 ; i < arr.count ; i++) {
                NSDictionary * dict = arr[i];

                if ([dict[@"configKey"] isEqualToString:@"alipay"]) {
                    [leftArr addObject:@"支付宝"];
                    [rightArr addObject:dict[@"configValue"]];

                }
               else if ([dict[@"configKey"] isEqualToString:@"wechatAccount"]) {
                    [leftArr addObject:@"微信"];
                   [rightArr addObject:dict[@"configValue"]];

                }
              else if ([dict[@"configKey"] isEqualToString:@"debitCardNo"]) {
                    [leftArr addObject:@"储蓄卡号"];
                  [rightArr addObject:dict[@"configValue"]];

                }
              else if ([dict[@"configKey"] isEqualToString:@"debitCardName"]) {
                    [leftArr addObject:@"储蓄卡名称"];
                  [rightArr addObject:dict[@"configValue"]];

                }
               else if ([dict[@"configKey"] isEqualToString:@"customServiceQq"]) {
                    [leftArr addObject:@"客服QQ"];
                   [rightArr addObject:dict[@"configValue"]];

                }
               else if ([dict[@"configKey"] isEqualToString:@"alipayCode"]) {
                    [leftArr addObject:@"支付宝付款码"];
                   [rightArr addObject:@"微信收款码"];
                   aliPayCode = dict[@"configValue"];

                }
               if ([dict[@"configKey"] isEqualToString:@"wechatCode"]) {
                    [leftArr addObject:@"*必须标注姓名和手机号码"];
                   [rightArr addObject:@""];
                   wechatCode = dict[@"configValue"];
                }
            }
        }
    }];
}
#pragma mark -隐藏TabBar
- (void)hideTabBar {
    if (self.tabBarController.tabBar.hidden == YES) {
        return;
    }
    UIView *contentView;
    if ( [[self.tabBarController.view.subviews objectAtIndex:0] isKindOfClass:[UITabBar class]] )
        contentView = [self.tabBarController.view.subviews objectAtIndex:1];
    else
        contentView = [self.tabBarController.view.subviews objectAtIndex:0];
    contentView.frame = CGRectMake(contentView.bounds.origin.x,  contentView.bounds.origin.y,  contentView.bounds.size.width, contentView.bounds.size.height + self.tabBarController.tabBar.frame.size.height);
    self.tabBarController.tabBar.hidden = YES;
    
}

#pragma mark -显示TabBar
- (void)showTabBar {
    if (self.tabBarController.tabBar.hidden == NO)
    {
        return;
    }
    UIView *contentView;
    if ([[self.tabBarController.view.subviews objectAtIndex:0] isKindOfClass:[UITabBar class]])
        contentView = [self.tabBarController.view.subviews objectAtIndex:1];
    
    else
        
        contentView = [self.tabBarController.view.subviews objectAtIndex:0];
    contentView.frame = CGRectMake(contentView.bounds.origin.x, contentView.bounds.origin.y,  contentView.bounds.size.width, contentView.bounds.size.height - self.tabBarController.tabBar.frame.size.height);
    self.tabBarController.tabBar.hidden = NO;
    
}

//还款信息
- (void)requestAlsoMoneyMsg {
    [HttpUrl NetErrorGET:@"paramSetting/selectIndexMoneyAndDays" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"还款信息:-> %@", data);
        if ([data[@"success"] integerValue] == 1) {
//            dataDic = data[@"data"];
//            [Methods dataInText:[NSString stringWithFormat:@"%.2f",[dataDic[@"needPayMoney"] floatValue]] InLabel:needPay defaultStr:@"-"];
//            needPay.text = [needPay.text stringByAppendingString:@"元"];
//            [Methods dataInText:[NSString stringWithFormat:@"%.2f",[dataDic[@"borrowMoney"] floatValue]] InLabel:userBorrowMoney defaultStr:@"-"];
//            userBorrowMoney.text = [userBorrowMoney.text stringByAppendingString:@"元"];
//
//            userApplyDay.text = [TheGlobalMethod dateWithStr:[[dataDic[@"gmtDatetime"] description] substringToIndex:10] Format:@"yyyy-MM-dd HH:mm:ss"];
//            AlsoDay.text = [TheGlobalMethod dateWithStr:[[dataDic[@"limitPayTime"] description] substringToIndex:10] Format:@"yyyy-MM-dd HH:mm:ss"];
//            if ([dataDic[@"orderStatus"] integerValue] != 3) {
//                shenhezhongBtn.hidden = NO;
//                if ([dataDic[@"orderStatus"] integerValue] == 2) {
//                    [shenhezhongBtn setTitle:@"待打款" forState:(UIControlStateNormal)];
//                }
//            }
        } else {
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    } WithFailBlock:^(id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

#pragma mark
#pragma mark UIPickerView DataSource Method 数据源方法

//指定pickerview有几个表盘
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;//第一个展示字母、第二个展示数字
}

//指定每个表盘上有几行数据
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView == leftPickerView) {
        return leftPickerArr.count;
    }
    return rightPickerArr.count;
}

#pragma mark UIPickerView Delegate Method 代理方法

//指定每行如何展示数据（此处和tableview类似）
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    if (pickerView == leftPickerView) {
        return leftPickerArr[row];
    }
    return rightPickerArr[row];
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (_timeMoneyArr.count > 0) {
        if (pickerView == leftPickerView) {
            NSDictionary * dict = _timeMoneyArr[row];
            //        rightPickerArr = dict[@"days"];
            NSMutableArray * arr = [[NSMutableArray alloc]initWithCapacity:0];
            NSArray * arr1 = dict[@"days"];
            for (int i = 0 ; i < arr1.count ; i ++ ) {
                [arr addObject:[NSString stringWithFormat:@"%@",arr1[i]]];
            }
            rightPickerArr = arr;
            [rightPickerView reloadAllComponents];
        }
    }
}
-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    [pickerView clearSpearatorLine];//UIPickerView分类方法
    UILabel *lbl = (UILabel *)view;
    
    if (lbl == nil) {
        
        lbl = [[UILabel alloc]init];
        
        //在这里设置字体相关属性
        
        lbl.font = [UIFont systemFontOfSize:30];
        
        lbl.textColor = appDefaultColor;
        lbl.textAlignment = NSTextAlignmentCenter;
        [lbl setBackgroundColor:[UIColor clearColor]];
        
    }
    //重新加载lbl的文字内容
    lbl.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    
    return lbl;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 30;
}

- (IBAction)msgBtn:(UIBarButtonItem *)sender {
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [JPUSHService setBadge:0];
    [self.navigationItem.rightBarButtonItem pp_addBadgeWithNumber:[UIApplication sharedApplication].applicationIconBadgeNumber];
    MsgListVC *vc = [TheGlobalMethod setstoryboard:@"MsgListVC" controller:self];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (NSString *)stringByAppendingString:(NSString *)string {
    return [string stringByAppendingString:@"元"];
}

- (IBAction)apply:(UIButton *)sender {
    if (_timeMoneyArr.count > 0) {
        NSInteger index = [leftPickerView selectedRowInComponent:0];
        NSDictionary * dict = _timeMoneyArr[index];
        
        borrowMoney = [dict[@"money"] integerValue];
        
        NSInteger timeIndex = [rightPickerView selectedRowInComponent:0];
        
        borrowDay = [dict[@"days"][timeIndex] integerValue];
    }
    
    if (!isStringEmpty(_loginUserDict[@"user"][@"authStatus"])&& [_loginUserDict[@"user"][@"authStatus"] isEqualToString:@"审核成功"] ) {
        ApplyForBorrowVC *vc = [TheGlobalMethod setstoryboard:@"ApplyForBorrowVC" controller:self];
        vc.returnReloadView = ^{
//            [self getLoginUser];
        };
        vc.hidesBottomBarWhenPushed = YES;
        vc.borrowMoney = [NSString stringWithFormat:@"%ld", (long)borrowMoney];
        vc.limitDays = [NSString stringWithFormat:@"%ld", (long)borrowDay];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        if ([_loginUserDict[@"user"][@"authStatus"] isEqualToString:@"未认证"] || [_loginUserDict[@"user"][@"authStatus"] isEqualToString:@"审核失败"]) {
            if ([_authDict[@"userAuth"][@"identityAuth"] integerValue] == 3 || [_authDict[@"userAuth"][@"identityAuth"] integerValue] == 2) {
                [TheGlobalMethod selectAlertView:@"请先完成所有认证" controller:self select:^{
                    [self startOCRFlow];
                }];
            }else{
                TheCertificationVC *vc = [[TheCertificationVC alloc] initWithNibName:nil bundle:nil];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
        }else{
            [TheGlobalMethod xianShiAlertView:@"认证信息已提交，请等待审核" controller:self];
        }
        
       
    }
    /*
    [HttpUrl GET:@"user/loginPhone" dict:@{@"phone":[TheGlobalMethod getUserDefault:@"phone"], @"password":[TheGlobalMethod getUserDefault:@"password"]} hud:[UIApplication sharedApplication].keyWindow isShow:YES WithSuccessBlock:^(id data) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        loginDic = data[@"data"];
        if ([data[@"success"] integerValue] == 1) {
            if ([loginDic[@"status"] integerValue] == 4) {
                ShowAlert(@"非常抱歉，您的综合评分不足，请您过段时间再来申请(自被拒绝时间30天之后)", self)
            } else {
                [HttpUrl GET:@"user/userIsBorrow" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
                    if ([[data[@"msg"] description] isEqualToString:@"true"]) {
                        
                            ApplyForBorrowVC *vc = [TheGlobalMethod setstoryboard:@"ApplyForBorrowVC" controller:self];
                        vc.returnReloadView = ^{
                            [self isCanBorrowMoney];
                        };
                            vc.hidesBottomBarWhenPushed = YES;
                            vc.borrowMoney = [NSString stringWithFormat:@"%ld", (long)borrowMoney];
                            vc.limitDays = [NSString stringWithFormat:@"%ld", (long)borrowDay];
                            [self.navigationController pushViewController:vc animated:YES];
                        
                    } else {
                        //                ShowAlert(@"请您完成认证,通过后再次申请", self);
                                        [TheGlobalMethod selectAlertView:@"请先完成所有认证" controller:self select:^{
                                            
                                            [self startOCRFlow];
                                            
                                           
                                        }];
                        
                    }
                }];
            }
        } else {
            
        }
        
        NSLog(@"登录登录登录%@", data);
    }];
    */
    
}
#pragma mark - 还款
- (IBAction)huankuan:(UIButton *)sender {
    
//    [Methods showAlertForPayLeftArr:leftArr rightArr:rightArr bottomStatus:@"3" selfVC:self titleLabelText:@"付款方式(可长按复制)" titleLabelTextColor:appDefaultColor];
    if ([statusBtn.titleLabel.text isEqualToString:@"我要还款"]) {
        [HttpUrl GET:@"loanOrder/moneyBackStatus" dict:@{@"orderId":_loginUserDict[@"orderId"]} hud:self.view isShow:NO WithSuccessBlock:^(id data) {
            
        }];
        [Methods showAlertForPayLeftArr:leftArr rightArr:rightArr bottomStatus:@"3" selfVC:self titleLabelText:@"付款方式(可长按复制)" titleLabelTextColor:appDefaultColor alicode:aliPayCode wechat:wechatCode];

    }
//    [HttpUrl GET:@"user/payPwdIsExist" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
//        NSLog(@"是否设置过交易密码%@", [data[@"msg"] description]);
//        if ([[data[@"msg"] description] isEqualToString:@"true"]) {
//
//            [HttpUrl GET:@"user/userIsNeedPay" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
//                [MBProgressHUD hideHUDForView:self.view animated:YES];
//                NSLog(@"还款判断状态:%@", data);
//                if ([[data[@"code"] description] isEqualToString:@"SUCCESS"]) {
//                    RenewalPayVC *vc = [TheGlobalMethod setstoryboard:@"RenewalPayVC" controller:self];
//                    vc.isBorrow = YES;
//                    vc.hidesBottomBarWhenPushed = YES;
//                    [self.navigationController pushViewController:vc animated:YES];
//                } else {
//                    ShowAlert(data[@"msg"], self)
//                }
//            }];
//        } else {
//            [JXTAlertView showAlertViewWithTitle:@"提示" message:@"请先设置交易密码" cancelButtonTitle:nil buttonIndexBlock:^(NSInteger buttonIndex) {
//                    SetPasswordVC *vc = [TheGlobalMethod setstoryboard:@"SetPasswordVC" controller:self];
//                    vc.isPS = @"0";
//                    vc.hidesBottomBarWhenPushed = YES;
//                    [self.navigationController pushViewController:vc animated:YES];
//            } otherButtonTitles:@[@"去设置"]];
//        }
//    }];
}

- (IBAction)xuqi:(UIButton *)sender {
//    [Methods showAlertForPayLeftArr:leftArr rightArr:rightArr bottomStatus:@"2" selfVC:self titleLabelText:@"付款方式(可长按复制)" titleLabelTextColor:appDefaultColor];
 
//    RenewalVC *vc = [TheGlobalMethod setstoryboard:@"RenewalVC" controller:self];
//    vc.orderID = [dataDic[@"orderId"] description];
//    vc.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:vc animated:YES];
}

- (void)setImgCycleScrollView {
    if (!imgCycleScrollView) {
//        NSMutableArray *urlArr = [NSMutableArray array];
//        for (int i = 0; i <  topImgArr.count; i++) {
//            [urlArr addObject:topImgArr[i][@"imgUrl"]];
//        }
        imgCycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:topImg.bounds imageURLStringsGroup:topImgArr];
        imgCycleScrollView.delegate = self;
        imgCycleScrollView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
        imgCycleScrollView.placeholderImage = [UIImage imageNamed:@"banner"];
        imgCycleScrollView.currentPageDotColor = appDefaultColor;
        imgCycleScrollView.pageDotColor = [UIColor whiteColor];
        imgCycleScrollView.backgroundColor = [UIColor lightGrayColor];
        imgCycleScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleNone;
        //if ([dict[@"imgArr"] count] == 1) {
        //    cycleScrollView.showPageControl = NO;
        //}
        [topImg addSubview:imgCycleScrollView];
    }
    
}

/** 点击图片回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    NSLog(@"%ld", (long)index);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)startOCRFlow {
    /**
     * showInfo（BOOL）: 是否展示身份证扫描详情页
     * mosaicIdName（BOOL）: 姓名是否掩码
     * mosaicIdNumber（BOOL）: 身份证号是否掩码
     * showConfirmIdNumber（BOOL）: 身份证扫描详情页身份证是否掩码
     *
     * pubKey（NSString）: 商户pub_key（开户时通过邮件发送给商户）
     * signTime（NSString）: 签名时间
     * partnerOrderId（NSString）: 商户订单号
     * sign（NSString）: 签名
     * actions（NSArray）: 产品功能列表枚举数组，传入相关产品例如：OCR、活体等，具体枚举详情请参考 UDIDSafeDataDefine.h
     * -()startIdSafeAuthInViewController: 开始流程方法
     **/
    NSString *identityOrderNo = [[self getTimeSp] stringByAppendingString:[[[NSUUID UUID] UUIDString] substringToIndex:8]];
    
    [HttpUrl POST:@"youDunLog/add" dict:@{@"token":[TheGlobalMethod getUserDefault:@"token"], @"identityOrderNo":identityOrderNo} hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        
//        TheCertificationVC *vc = [[TheCertificationVC alloc] initWithNibName:nil bundle:nil];
//        vc.hidesBottomBarWhenPushed = YES;
//        [self.navigationController pushViewController:vc animated:YES];

        UDIDSafeAuthEngine * engine = [[UDIDSafeAuthEngine alloc]init];
        engine.delegate = self;
        engine.authKey = UDPUBKEY;
        engine.notificationUrl = [Http_IP stringByAppendingString:@"userIdentity/userIdentity/back"];
        engine.showInfo = YES;
        engine.isManualOCR = YES;
        //    engine.safeMode = UDIDSafeMode_Medium;
        engine.outOrderId = identityOrderNo;
        [engine startIdSafeFaceAuthInViewController:self];
    }];
    
}

static NSString *signID = @"";
- (void)idSafeAuthFinishedWithResult:(UDIDSafeAuthResult)result UserInfo:(id)userInfo{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:userInfo];
    if (![[dict[@"ret_code"] description] isEqualToString:@"000000"]) {
        
        return;
    }
    if ([[dict[@"result_auth"] description] isEqualToString:@"F"]) {
        ShowAlert(@"认证未通过", self);
    }
    if ([[dict[@"result_auth"] description] isEqualToString:@"T"]) {
        TheCertificationVC *vc = [[TheCertificationVC alloc] initWithNibName:nil bundle:nil];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    });
    
//    [self performSelector:@selector(getUserAuth) withObject:nil afterDelay:5];
}

//- (void)idSafeAuthFinishedWithResult:(UDIDSafeAuthResult)result UserInfo:(id)userInfo{
//    NSMutableDictionary * dict = [[NSMutableDictionary alloc]initWithDictionary:userInfo];
//    if ([dict[@"result_auth"] isEqualToString:@"T"]) {//        NSLog(@"finish");
//        NSDictionary * dict1 = @{@"identityFront":[self getBase64Str:dict[@"url_frontcard"]], @"identityBack":[self getBase64Str:dict[@"url_backcard"]], @"userName":dict[@"id_name"], @"identityNum":dict[@"id_no"], @"faceUrl":[self getBase64Str:dict[@"url_photoliving"]]};//        NSLog(@"身份认证上传 - - %@",dict1);
//        [HttpUrl POST:@"userIdentity/add" dict:dict1 hud:self.view isShow:NO WithSuccessBlock:^(id data) {
//            if ([data[@"success"] integerValue] == 1) {
////                [self getNetWorking];
////                [TheGlobalMethod popAlertView:@"验证成功" controller:self];
//                [TheGlobalMethod selectAlertView:@"继续认证" controller:self select:^{
//                    TheCertificationVC *vc = [[TheCertificationVC alloc] initWithNibName:nil bundle:nil];
//                    vc.hidesBottomBarWhenPushed = YES;
//                    [self.navigationController pushViewController:vc animated:YES];
//                }];
//            } else {
//                [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
//
//            }
//
//        }];
//
//    }
//
//}
-(NSString *)getBase64Str:(NSString *)str{
    NSData *resultData = [NSData dataWithContentsOfURL:[NSURL URLWithString:str]];
    UIImage *img = [UIImage imageWithData:resultData];
    NSData * data = UIImageJPEGRepresentation(img, 1);
    //data:image/jpeg;base64,    前缀
    NSString *encodedImageStr = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    return encodedImageStr;
    
}


// 签名时间方法
- (NSString *)getTimeSp {
    NSString *resultString = nil;
    NSTimeInterval time = [[NSDate date] timeIntervalSince1970] ;
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyyMMddHHMMss"];
    NSDate *datenow = [NSDate dateWithTimeIntervalSince1970:time];
    resultString = [formatter stringFromDate:datenow];
    return resultString;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
