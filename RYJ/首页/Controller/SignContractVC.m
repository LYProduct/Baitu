//
//  SignContractVC.m
//  RYJ
//
//  Created by liuyong on 2017/12/18.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "SignContractVC.h"
#import "ContractWebVC.h"
#import "YHTTokenManager.h"
#import "YHTContractContentViewController.h"
#import "BorrowResultsVC.h"
#import "HomeViewController.h"

@interface SignContractVC ()
{
    NSDictionary * dataDict;
    NSString *contractID;
    
}
@property (weak, nonatomic) IBOutlet UIButton *signBtn;

@end

@implementation SignContractVC
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [self getSignStatus];
}

-(void)getSignStatus {
    [HttpUrl GET:@"yunhetong/getSignYunStatus" dict:@{@"loanOrderId":_returnStr} hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if (BB_isSuccess) {
            if (![data[@"data"] isEqual:[NSNull null]] && [data[@"data"] isEqualToString:@"签署完成"]) {
//                UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
//                HomeViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
//                [self.navigationController pushViewController:vc animated:YES];
//                [TheGlobalMethod xianShiAlertView:@"已签署成功" controller:self];
                [TheGlobalMethod popTAlertView:@"已签署成功" controller:self];
            }
        }else{
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"签署合同";
    
}

- (IBAction)pushSign:(id)sender {
    [HttpUrl GET:@"yunhetong/getToken" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if (BB_isSuccess) {
            dataDict = data[@"data"];
            ContractWebVC * vc = [[ContractWebVC alloc]initWithNibName:@"ContractWebVC" bundle:nil];
            vc.dict = dataDict;
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
            
            
//                    [[YHTTokenManager sharedManager] setTokenWithString:dataDict[@"token"]];
//            contractID = [NSString stringWithFormat:@"%@",dataDict[@"contractId"]];
//                    //查看合同
//            YHTContractContentViewController * vc = [YHTContractContentViewController instanceWithContractID:contractID];
//            vc.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:vc animated:YES];
            
        }else{
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
