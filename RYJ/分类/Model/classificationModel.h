//
//  classificationModel.h
//  RYJ
//
//  Created by wyp on 2018/1/15.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface classificationModel : NSObject
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *picUrl;
@property(nonatomic,strong) NSString *ID;
@end
