//
//  ClassificationDetailsCollectionViewCell.m
//  RYJ
//
//  Created by wyp on 2018/1/15.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import "ClassificationDetailsCollectionViewCell.h"
#import "ClassificationDetailsModel.h"

@interface ClassificationDetailsCollectionViewCell()
@property (weak, nonatomic) IBOutlet UIButton *detailsBT;

@end

@implementation ClassificationDetailsCollectionViewCell

@synthesize phoneLabel;
@synthesize byPrice;
@synthesize phoneImageView;

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor whiteColor];
    [self.detailsBT addTarget:self action:@selector(assessmentBTClick:) forControlEvents:UIControlEventTouchUpInside];
    self.detailsBT.layer.cornerRadius = W(12.5);
    self.detailsBT.layer.masksToBounds = YES;
    [self.detailsBT setTitle:@"  去评估  " forState:UIControlStateNormal];
    self.detailsBT.titleLabel.font = [UIFont systemFontOfSize:14];
    self.detailsBT.layer.borderWidth = 1;
    [self.detailsBT setTitleColor:[[UIColor blueColor] colorWithAlphaComponent:0.5] forState:UIControlStateNormal];
    self.detailsBT.layer.borderColor = [[UIColor blueColor] colorWithAlphaComponent:0.5].CGColor;
}

-(void)assessmentBTClick:(UIButton *)sender
{
    if(self.detailsBlock)
    {
        self.detailsBlock();
    }
}

-(void)setClassificationDetails:(ClassificationDetailsModel *)classificationDetails
{
    _classificationDetails = classificationDetails;
    NSString *imageStr = Http_imgIP;
    phoneLabel.text = classificationDetails.name;
    byPrice.text = classificationDetails.initialPrice;
    
    if(classificationDetails.picUrl)
    {
        [phoneImageView sd_setImageWithURL:[NSURL URLWithString:[imageStr stringByAppendingString:classificationDetails.picUrl]] placeholderImage:placeholderimage];
    }
    else
    {
        phoneImageView.image = placeholderimage;
    }
    
}

@end
