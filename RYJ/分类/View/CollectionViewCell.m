//
//  CollectionViewCell.m
//  RYJ
//
//  Created by wyp on 2018/1/15.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import "CollectionViewCell.h"
#import "classificationModel.h"

@implementation CollectionViewCell

@synthesize phoneImageView;
@synthesize phoneLabel;

- (void)awakeFromNib {
    [super awakeFromNib];
  
    phoneImageView.layer.cornerRadius = 31;
    phoneImageView.layer.masksToBounds = YES;
    
}

-(void)setClassification:(classificationModel *)classification
{
    _classification = classification;
    NSString *imageStr = Http_imgIP;
    
    if(classification.picUrl)
    {
    [phoneImageView sd_setImageWithURL:[NSURL URLWithString:[imageStr stringByAppendingString:classification.picUrl]] placeholderImage:placeholderimage];
    }
    else
    {
        phoneImageView.image = placeholderimage;
    }
    if(classification.name)
    {
        phoneLabel.text = classification.name;
    }
    else
    {
        phoneLabel.text = @"";
    }
}

@end
