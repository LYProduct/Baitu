//
//  CollectionViewCell.h
//  RYJ
//
//  Created by wyp on 2018/1/15.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>
@class classificationModel;

@interface CollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *phoneImageView;

@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;

@property(nonatomic,strong) classificationModel *classification;

@end
