//
//  ClassificationDetailsCollectionViewCell.h
//  RYJ
//
//  Created by wyp on 2018/1/15.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClassificationDetailsModel.h"

@interface ClassificationDetailsCollectionViewCell : UICollectionViewCell

@property(nonatomic,strong) ClassificationDetailsModel *classificationDetails;
@property (weak, nonatomic) IBOutlet UIImageView *phoneImageView;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *byPrice;
@property(strong,nonatomic) void(^detailsBlock)();



@end
