//
//  classificationViewController.m
//  RYJ
//
//  Created by wyp on 2018/1/15.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import "classificationViewController.h"
#import "CollectionViewCell.h"
#import "classificationModel.h"
#import "ClassificationDetailsViewController.h"

@interface classificationViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property(nonatomic,strong) UICollectionView *collectionView;
@property(nonatomic,strong) NSArray *dataSourArray;
@end

@implementation classificationViewController

static NSString *const MKPCell = @"CollCell";

-(NSArray *)dataSourArray
{
    if(!_dataSourArray)
    {
        _dataSourArray = [NSArray array];
    }
    return _dataSourArray;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // 获取轮播图
    [self setupRefresh];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"分类";
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    UICollectionViewFlowLayout *classesLayout = [[UICollectionViewFlowLayout alloc]init];
    classesLayout.itemSize = CGSizeMake(WIDTH_K /3 - W(7), HEIGHT_K / 5 - H(15));
    
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, WIDTH_K, HEIGHT_K) collectionViewLayout:classesLayout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.collectionView];
    [self.collectionView registerNib:[UINib nibWithNibName:@"CollectionViewCell" bundle:nil] forCellWithReuseIdentifier:MKPCell];
    self.collectionView.showsVerticalScrollIndicator = NO;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView.alwaysBounceVertical = YES;

}

#pragma mark - 刷新
-(void)setupRefresh
{
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewx)];
    self.collectionView.mj_header.automaticallyChangeAlpha  = YES;
    [self.collectionView.mj_header beginRefreshing];
}
/**
 * 加载新的数据
 */
-(void)loadNewx
{
    // 结束上拉
    [self.collectionView.mj_footer endRefreshing];
    // 参数

    [HttpUrl GET:@"classify/selectList" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if (BB_isSuccess) {
            self.dataSourArray = [classificationModel  mj_objectArrayWithKeyValuesArray:data[@"data"]];
            [self.collectionView reloadData];
        }else{
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
    
    [self.collectionView.mj_header endRefreshing];
    
}
#pragma mark collectionViewDatasouce
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    //    return self.viewModel.dataArray.count;
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataSourArray.count;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ClassificationDetailsViewController *vc = [[ClassificationDetailsViewController alloc]init];
    vc.model =self.dataSourArray[indexPath.item];
    [self.navigationController pushViewController:vc animated:YES];
    NSLog(@"当前点击的是第%ld个cell",(long)indexPath.item);
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MKPCell forIndexPath:indexPath];
    
    cell.classification = self.dataSourArray[indexPath.row];
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
