//
//  ClassificationDetailsViewController.m
//  RYJ
//
//  Created by wyp on 2018/1/15.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import "ClassificationDetailsViewController.h"
#import "ClassificationDetailsCollectionViewCell.h"
#import "ClassificationDetailsModel.h"

@interface ClassificationDetailsViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
/** collectionView */
@property(nonatomic,strong) UICollectionView *collectionView;
/** 上一次的请求参数 */
@property (nonatomic, strong) NSDictionary *params;
/** 当前页码 */
@property (nonatomic, assign) NSInteger pageNowO;
@property(nonatomic,strong) NSArray *dataSourArray;
@property(nonatomic,strong) NSMutableArray *dataPhoneArray;
/** 图片数据 */
@property(nonatomic,strong) NSMutableArray *pictureArray;

@end

@implementation ClassificationDetailsViewController

static NSString *const MKPCell = @"ClassificationDetailsCell";

-(NSArray *)dataSourArray
{
    if(!_dataSourArray)
    {
        _dataSourArray = [NSArray array];
    }
    return _dataSourArray;
}

-(NSMutableArray *)dataPhoneArray
{
    if(!_dataPhoneArray)
    {
        _dataPhoneArray = [NSMutableArray array];
    }
    return _dataPhoneArray;
}

-(NSMutableArray *)pictureArray
{
    if(!_pictureArray)
    {
        _pictureArray =  [NSMutableArray array];
    }
    return _pictureArray;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // 获取轮播图
    [self setupRefresh];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.model.name;
   [self createView];
    
}
#pragma mark --- 创建视图
-(void)createView
{
    // 布局
    UICollectionViewFlowLayout *whcLayout = [[UICollectionViewFlowLayout alloc]init];
    whcLayout.minimumLineSpacing = W(1);
    whcLayout.minimumInteritemSpacing = W(0);
    whcLayout.itemSize = CGSizeMake((WIDTH_K - W(1))/2, H(235));
    
    self.collectionView  = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, HEIGHT_K) collectionViewLayout:whcLayout];
    self.collectionView.backgroundColor = NAVCOLOR_C(242, 242, 242);
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.view addSubview:self.collectionView];
    [self.collectionView registerNib:[UINib nibWithNibName:@"ClassificationDetailsCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:MKPCell];
    self.collectionView.showsVerticalScrollIndicator = NO;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView.alwaysBounceVertical = YES;
}
#pragma mark - 刷新
-(void)setupRefresh
{
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewx)];
    self.collectionView.mj_header.automaticallyChangeAlpha  = YES;
    [self.collectionView.mj_header beginRefreshing];
    self.collectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];

}
/**
 * 加载新的数据
 */
-(void)loadNewx
{
    // 结束上拉
    [self.collectionView.mj_footer endRefreshing];
    self.pageNowO = 1 ;
    NSString *stringInt = [NSString stringWithFormat:@"%ld",self.pageNowO];
    // 参数
    NSMutableDictionary *parmsO = [NSMutableDictionary dictionary];
    parmsO[@"current"] = stringInt;
    parmsO[@"size"] = @"4";
    parmsO[@"classifyId"] = self.model.ID;
    self.params = parmsO;
    
    [HttpUrl GET:@"goods/selectPage" dict:parmsO hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if (BB_isSuccess) {
            NSDictionary *dic = [NSDictionary dictionary];
            dic = data[@"data"];
            self.dataPhoneArray = [ClassificationDetailsModel  mj_objectArrayWithKeyValuesArray:dic[@"records"]];
            [self.collectionView reloadData];
        }else{
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
    
    [self.collectionView.mj_header endRefreshing];
    
}

-(void)loadMore
{
    // 结束下拉
    [self.collectionView.mj_header endRefreshing];
    NSInteger page = self.pageNowO + 1;
    NSString *stringInt = [NSString stringWithFormat:@"%ld",page];
    // 参数
    NSMutableDictionary *parmsO = [NSMutableDictionary dictionary];
    parmsO[@"current"] = stringInt;
    parmsO[@"size"] = @"4";
    parmsO[@"classifyId"] = self.model.ID;
    self.params = parmsO;
    
    [HttpUrl GET:@"goods/selectPage" dict:parmsO hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if (BB_isSuccess) {
            NSDictionary *dic = [NSDictionary dictionary];
            dic = data[@"data"];
            
            NSArray *newInventory = [ClassificationDetailsModel  mj_objectArrayWithKeyValuesArray:dic[@"records"]];
            [self.dataPhoneArray addObjectsFromArray:newInventory];
            [self.collectionView reloadData];
        }else{
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
    
    self.pageNowO = page;
    [self.collectionView reloadData];
    [self.collectionView.mj_footer endRefreshing];
}

#pragma mark collectionViewDatasouce
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    //    return self.viewModel.dataArray.count;
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataPhoneArray.count;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"当前点击的是第%ld个cell",(long)indexPath.item);
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ClassificationDetailsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MKPCell forIndexPath:indexPath];
    
    cell.classificationDetails = self.dataPhoneArray[indexPath.row];
    cell.detailsBlock = ^{
        self.tabBarController.selectedIndex = 2;
    };
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
