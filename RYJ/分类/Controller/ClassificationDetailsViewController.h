//
//  ClassificationDetailsViewController.h
//  RYJ
//
//  Created by wyp on 2018/1/15.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "classificationModel.h"

@interface ClassificationDetailsViewController : UIViewController
@property(nonatomic,strong) classificationModel *model;
@end
