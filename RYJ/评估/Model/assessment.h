//
//  assessment.h
//  RYJ
//
//  Created by wyp on 2018/1/16.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface assessment : NSObject
@property(nonatomic,strong) NSString *evaluationPrice;
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *color;
@property(nonatomic,strong) NSString *screenShow;
@property(nonatomic,strong) NSString *serviceHistory;

@end
