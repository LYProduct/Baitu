//
//  MKPLeaseBackDetailsOneTableViewCell.m
//  RYJ
//
//  Created by wyp on 2018/1/18.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import "MKPLeaseBackDetailsOneTableViewCell.h"

@implementation MKPLeaseBackDetailsOneTableViewCell

@synthesize leftLabel;

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc indexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        leftLabel.text = @"回租信息";
    }
    if(indexPath.section == 2)
    {
        leftLabel.text = @"优惠劵";
    }
    if(indexPath.section == 3)
    {
        leftLabel.text = @"租金明细";
    }
    if(indexPath.section == 5)
    {
        leftLabel.text = @"交易协议";
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
