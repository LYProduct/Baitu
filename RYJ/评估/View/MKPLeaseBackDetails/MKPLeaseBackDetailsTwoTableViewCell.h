//
//  MKPLeaseBackDetailsTwoTableViewCell.h
//  RYJ
//
//  Created by wyp on 2018/1/18.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MKPLeaseBackDetailsTwoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *leftLabe;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;

@end
