//
//  MKPLeaseBackDetailsTwoTableViewCell.m
//  RYJ
//
//  Created by wyp on 2018/1/18.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import "MKPLeaseBackDetailsTwoTableViewCell.h"

@implementation MKPLeaseBackDetailsTwoTableViewCell

@synthesize leftLabe;
@synthesize rightLabel;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc indexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0)
    {
    if (indexPath.row == 0) {
        [self dealWithOneRow:dict];
    } else if (indexPath.row == 1) {
        [self dealWithTwoRow:dict];
    } else if (indexPath.row == 2) {
        [self dealWithThreeRow:dict];
    } else if (indexPath.row == 3) {
        [self dealWithFourRow:dict];
    } else if (indexPath.row == 4) {
        [self dealWithFiveRow:dict];
    } else if (indexPath.row == 5) {
        [self dealWithSixRow:dict];
    } else if (indexPath.row == 6) {
        [self dealWithSevenRow:dict];
    } else if (indexPath.row == 7) {
        [self dealWithEightRow:dict];
    }
    }
    if(indexPath.section == 4)
    {
        if(indexPath.row == 0)
        {
            [self dealWithOneOneRow:dict];
        }
        if(indexPath.row == 1)
        {
            [self dealWithOneTwoRow:dict];
        }
        if(indexPath.row == 2)
        {
            [self dealWithOneThreeRow:dict];
        }
    }
    if(indexPath.section == 7)
    {
        if(indexPath.row == 2)
        {
            [self dealWithEightOneRow:dict];
        }
    }
}

#pragma mark 第一行处理
- (void)dealWithOneRow:(NSDictionary *)dict {
    leftLabe.text = @"估价机型";
    NSString *nameStr = [NSString stringWithFormat:@"%@",dict[@"name"]];
    if(isStringEmpty(nameStr))
    {
        rightLabel.text = @"";
    }
    else
    {
        rightLabel.text = nameStr;
    }
    
}

#pragma mark 第二行处理
- (void)dealWithTwoRow:(NSDictionary *)dict {
//    evaluationPrice
    leftLabe.text = @"估价价值";
    NSString *valueStr = [NSString stringWithFormat:@"%@元",dict[@"evaluationPrice"]];
    if(isStringEmpty(valueStr))
    {
        rightLabel.text = @"";
    }
    else
    {
        rightLabel.text = valueStr;
    }
}

#pragma mark 第三行处理
- (void)dealWithThreeRow:(NSDictionary *)dict {
    leftLabe.text = @"估价详情";
    NSString *detailsStr = [NSString stringWithFormat:@"%@、%@、%@、%@",dict[@"color"],dict[@"screenShow"],dict[@"serviceHistory"],dict[@"functionQuestion"]];
    if(isStringEmpty(detailsStr))
    {
        rightLabel.text = @"";
    }
    else
    {
        rightLabel.text = detailsStr;
    }
}

#pragma mark 第四行处理
- (void)dealWithFourRow:(NSDictionary *)dict {
    leftLabe.text = @"估价费用";
    NSDictionary *dic = [NSDictionary dictionary];
    dic = dict[@"paramSetting"];
    NSString *assesseStr = [NSString stringWithFormat:@"%@元",dic[@"assessMoney"]];
    if(isStringEmpty(assesseStr))
    {
        rightLabel.text = @"";
    }
    else
    {
        rightLabel.text = assesseStr;
    }
}

#pragma mark 第五行处理
- (void)dealWithFiveRow:(NSDictionary *)dict {
    leftLabe.text = @"回租保证金";
    NSDictionary *dic = [NSDictionary dictionary];
    dic = dict[@"paramSetting"];
    // 估价
    NSString *valueStr = [NSString stringWithFormat:@"%@元",dict[@"evaluationPrice"]];
    NSString *assesseStr = [NSString stringWithFormat:@"%@元",dic[@"cashPledge"]];
    NSInteger a =  [valueStr intValue] * [assesseStr intValue];
    NSString *eitdStr = [NSString stringWithFormat:@"%ld",(long)a];
    NSString * endStr = [NSString stringWithFormat:@"%@元",eitdStr];
    if(isStringEmpty(endStr))
    {
        rightLabel.text = @"";
    }
    else
    {
        rightLabel.text = endStr;
    }
}

#pragma mark 第六行处理
- (void)dealWithSixRow:(NSDictionary *)dict {
    leftLabe.text = @"回租租金";
    NSDictionary *dic = [NSDictionary dictionary];
    dic = dict[@"paramSetting"];
    NSString *assesseStr = [NSString stringWithFormat:@"%@元/天",dic[@"rentDayMoney"]];
    if(isStringEmpty(assesseStr))
    {
        rightLabel.text = @"";
    }
    else
    {
        rightLabel.text = assesseStr;
    }
}

#pragma mark 第七行处理
- (void)dealWithSevenRow:(NSDictionary *)dict {
    leftLabe.text = @"到账金额";
    // 价格
    NSString *valueStr = [NSString stringWithFormat:@"%@元",dict[@"evaluationPrice"]];
    NSInteger a = [valueStr integerValue];
    // 回租保证金
    NSDictionary *dic = [NSDictionary dictionary];
    dic = dict[@"paramSetting"];
    NSString *valueTStr = [NSString stringWithFormat:@"%@元",dict[@"evaluationPrice"]];
    NSString *assesseStr = [NSString stringWithFormat:@"%@元",dic[@"cashPledge"]];
    NSInteger b =  [valueTStr intValue] * [assesseStr intValue];
   // 到账金额(目前先这样,等会还要减去优惠劵
    NSInteger c = a - b;
    NSString *endStr = [NSString stringWithFormat:@"%ld",(long)c];
    if(isStringEmpty(endStr))
    {
        rightLabel.text = @"";
    }
    else
    {
        rightLabel.text = endStr;
    }
}

#pragma mark 第八行处理
- (void)dealWithEightRow:(NSDictionary *)dict {
    leftLabe.text = @"到账银行卡";
    NSDictionary *dic = [NSDictionary dictionary];
    dic = dict[@"bankCard"];
    NSString *assesseStr = [NSString stringWithFormat:@"%@",dic[@"bankCardName"]];
    if(isStringEmpty(assesseStr))
    {
        rightLabel.text = @"";
    }
    else
    {
        rightLabel.text = assesseStr;
    }
}

#pragma mark 第5行数据处理
// 1
-(void)dealWithOneOneRow:(NSDictionary *)dict
{
    leftLabe.text = @"应付租金";
    NSDictionary *dic = [NSDictionary dictionary];
    dic = dict[@"paramSetting"];
    NSString *assesseStr = [NSString stringWithFormat:@"%@",dic[@"rentDayMoney"]];
    if(isStringEmpty(assesseStr))
    {
        rightLabel.text = @"";
    }
    else
    {
        rightLabel.text = assesseStr;
    }
}
// 2
-(void)dealWithOneTwoRow:(NSDictionary *)dict
{
    leftLabe.text = @"优惠劵抵扣";
    rightLabel.text = @"暂无优惠劵";
}
// 3
-(void)dealWithOneThreeRow:(NSDictionary *)dict
{
    leftLabe.text = @"实际付租日";
    rightLabel.text = @"-----";
}
#pragma mark 第8行数据处理
-(void)dealWithEightOneRow:(NSDictionary*)dict
{
    TheGlobalMethod 
//    NSDictionary *dic = [NSDictionary dictionary];
//    dic = dict[@"paramSetting"];
//    NSString *assesseStr = [NSString stringWithFormat:@"%@元",dic[@"rentDayMoney"]];
//    leftLabe.text = @"";
}

@end
