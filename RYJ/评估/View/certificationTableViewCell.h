//
//  certificationTableViewCell.h
//  RYJ
//
//  Created by wyp on 2018/1/10.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface certificationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UIImageView *chooseImageView;

@end
