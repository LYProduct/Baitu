//
//  assessmentViewController.m
//  RYJ
//
//  Created by wyp on 2018/1/3.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import "assessmentViewController.h"
#import "assessmentTableViewCell.h"
#import "PickerView.h"
#import "YZTagViewController.h"
#import "certificationViewController.h"
#import "assessmentDetailsViewController.h"
#import "assessment.h"

@interface assessmentViewController ()<UITableViewDelegate,UITableViewDataSource>
{
   
    __weak IBOutlet RootTableView *myTableView;
}
@property(nonatomic,strong) NSArray *dataSourList;
@property(nonatomic,strong) NSDictionary *dataDic;
@property(strong,nonatomic) NSMutableArray *arrayData;

@end

@implementation assessmentViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"评估";
    myTableView.backgroundColor = [UIColor whiteColor];
    myTableView.delegate = self;
    myTableView.dataSource = self;

    _dataSourList = @[@[@{@"title":@"手机品牌"}, @{@"title":@"手机型号"},@{@"title":@"存储容量"},@{@"title":@"机身颜色"},@{@"title":@"屏幕显示"},@{@"title":@"维修拆机史"},@{@"title":@"功能性问题"}]];
    
}
#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 7;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    assessmentTableViewCell loadingTableViewCell("assessmentTableViewCell", )
    
    [cell reloadCellForData:_dataSourList[indexPath.section][indexPath.row] vc:self indexPath:indexPath];
    if(indexPath.section == 0)
    {
        if(indexPath.row == 6)
        {
            if(_arrayData.count == 0)
            {
                
            }
            else
            {
                for (int i = 0;i< _arrayData.count; i++) {
                    NSString *str = [_arrayData objectAtIndex:i];
                    cell.chooseLabel.text = [ cell.chooseLabel.text stringByAppendingString:[NSString stringWithFormat:@"%@ ",str]];
                }
            }
            
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    assessmentTableViewCell *cell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    assessmentTableViewCell *cellOne = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    assessmentTableViewCell *cellTwo = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    assessmentTableViewCell *cellThree = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
    assessmentTableViewCell *cellFour = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
    assessmentTableViewCell *cellFive = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0]];
    
if(indexPath.section == 0)
{
    if(indexPath.row == 0)
    {
        PickerView *pickerView = [TheGlobalMethod setCell:@"PickerView"];
        pickerView.frame = CGRectMake(0, HEIGHT_K, WIDTH_K, HEIGHT_K);
        [[UIApplication sharedApplication].keyWindow addSubview:pickerView];
        pickerView.title.text = @"手机品牌";
        [pickerView reloadCellForData:@{@"arr":@[@"魅族",@"三星",@"小米",@"iPhone",@"vivo",@"oppo",@"华为"]} vc:self];
        pickerView.ReturnString = ^(NSString *string) {
            cell.chooseLabel.text = string;
        };
    }
    else if (indexPath.row == 1)
    {
if([cell.chooseLabel.text isEqualToString:@"小米"])
{
    PickerView *pickerView = [TheGlobalMethod setCell:@"PickerView"];
    pickerView.frame = CGRectMake(0, HEIGHT_K, WIDTH_K, HEIGHT_K);
    [[UIApplication sharedApplication].keyWindow addSubview:pickerView];
    pickerView.title.text = @"手机型号";
    [pickerView reloadCellForData:@{@"arr":@[@"小米1",@"小米2",@"小米3",@"小米4",@"小米5",@"小米6",@"小米7"]} vc:self];
    pickerView.ReturnString = ^(NSString *string) {
        cellOne.chooseLabel.text = string;
    };
}
        else if ([cell.chooseLabel.text isEqualToString:@"iPhone"])
        {
            PickerView *pickerView = [TheGlobalMethod setCell:@"PickerView"];
            pickerView.frame = CGRectMake(0, HEIGHT_K, WIDTH_K, HEIGHT_K);
            [[UIApplication sharedApplication].keyWindow addSubview:pickerView];
            [pickerView reloadCellForData:@{@"arr":@[@"iPhone 4",@"iPhone 4s",@"iPhone 5",@"iPhone 5s",@"iPhone 6",@"iPhone 6s",@"iPhone 6Plus"]} vc:self];
            pickerView.ReturnString = ^(NSString *string) {
                cellOne.chooseLabel.text = string;
            };
        }
//       else
//       {
//           [TheGlobalMethod xianShiAlertView:@"请先选择手机品牌,谢谢" controller:self];
//       }
    }
    else if (indexPath.row == 2)
    {
        PickerView *pickerView = [TheGlobalMethod setCell:@"PickerView"];
        pickerView.frame = CGRectMake(0, HEIGHT_K, WIDTH_K, HEIGHT_K);
        [[UIApplication sharedApplication].keyWindow addSubview:pickerView];
        pickerView.title.text = @"存储容量";
        [pickerView reloadCellForData:@{@"arr":@[@"8G",@"16G",@"32G",@"64G",@"128G",@"256G"]} vc:self];
        pickerView.ReturnString = ^(NSString *string) {
            cellTwo.chooseLabel.text = string;
        };
    }
    else if (indexPath.row == 3)
    {
        PickerView *pickerView = [TheGlobalMethod setCell:@"PickerView"];
        pickerView.frame = CGRectMake(0, HEIGHT_K, WIDTH_K, HEIGHT_K);
        [[UIApplication sharedApplication].keyWindow addSubview:pickerView];
        pickerView.title.text = @"机身颜色";
        [pickerView reloadCellForData:@{@"arr":@[@"黑色",@"金色",@"银色",@"红色",@"玫瑰金",@"磨砂黑"]} vc:self];
        pickerView.ReturnString = ^(NSString *string) {
            cellThree.chooseLabel.text = string;
        };
    }
    else if (indexPath.row == 4)
    {
        PickerView *pickerView = [TheGlobalMethod setCell:@"PickerView"];
        pickerView.frame = CGRectMake(0, HEIGHT_K, WIDTH_K, HEIGHT_K);
        [[UIApplication sharedApplication].keyWindow addSubview:pickerView];
        pickerView.title.text = @"屏幕显示";
        [pickerView reloadCellForData:@{@"arr":@[@"屏幕无法显示",@"屏幕正常显示",@"亮银色、色差、轻微变黄",@"色斑、漏液、错乱,严重老化"]} vc:self];
        pickerView.ReturnString = ^(NSString *string) {
            cellFour.chooseLabel.text = string;
        };
    }
    else if (indexPath.row == 5)
    {
        PickerView *pickerView = [TheGlobalMethod setCell:@"PickerView"];
        pickerView.frame = CGRectMake(0, HEIGHT_K, WIDTH_K, HEIGHT_K);
        [[UIApplication sharedApplication].keyWindow addSubview:pickerView];
        pickerView.title.text = @"维修拆机史";
        [pickerView reloadCellForData:@{@"arr":@[@"屏幕维修",@"无拆无修",@"主板维修"]} vc:self];
        pickerView.ReturnString = ^(NSString *string) {
            cellFive.chooseLabel.text = string;
        };
    }
    else if (indexPath.row == 6)
    {
        YZTagViewController *vc = [[YZTagViewController alloc]init];
        vc.blockData = ^(NSMutableArray *arrayData) {
            _arrayData = arrayData;
            [tableView reloadData];
        };
        
        [self.navigationController pushViewController:vc animated:YES];
    }

}
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return H(41);

}
- (IBAction)submitBt:(UIButton *)sender {
    
    assessmentTableViewCell *cell = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    assessmentTableViewCell *cellOne = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    assessmentTableViewCell *cellTwo = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    assessmentTableViewCell *cellThree = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
    assessmentTableViewCell *cellFour = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
    assessmentTableViewCell *cellFive = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0]];
    assessmentTableViewCell *cellSix = [myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:6 inSection:0]];
    
    if(cell.chooseLabel.text.length == 0)
    {
        ShowAlert(@"请选择手机品牌", self);
    }
    if(cellOne.chooseLabel.text.length == 0)
    {
        ShowAlert(@"请选择手机型号", self);
    }
    if(cellTwo.chooseLabel.text.length == 0)
    {
        ShowAlert(@"请选择存储容量", self);
    }
    if(cellThree.chooseLabel.text.length == 0)
    {
        ShowAlert(@"请选择机身颜色", self);
    }
    if(cellFour.chooseLabel.text.length == 0)
    {
        ShowAlert(@"请选择显示", self);
    }
    if(cellFive.chooseLabel.text.length == 0)
    {
        ShowAlert(@"请选择维修拆机史", self);
    }
    if(cellSix.chooseLabel.text.length == 0)
    {
        ShowAlert(@"请选择功能性问题", self);
    }
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"name"] = cellOne.chooseLabel.text;
    dic[@"storageCapacity"] = cellTwo.chooseLabel.text;
    dic[@"color"] = cellThree.chooseLabel.text;
    dic[@"screenShow"] = cellFour.chooseLabel.text;
    dic[@"serviceHistory"] = cellFive.chooseLabel.text;
    dic[@"functionQuestion"] = cellSix.chooseLabel.text;

    // evaluation/add
    [HttpUrl POST:@"evaluation/add" dict:dic hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if ([data[@"success"] integerValue] == 1) {
            assessment *model = [assessment mj_objectWithKeyValues:data[@"data"]];
            assessmentDetailsViewController * vc = [[assessmentDetailsViewController alloc]initWithNibName:@"assessmentDetailsViewController" bundle:nil];
            vc.assessmodel = model;
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        } else {
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
