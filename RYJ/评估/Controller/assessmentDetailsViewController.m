//
//  assessmentDetailsViewController.m
//  RYJ
//
//  Created by wyp on 2018/1/17.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import "assessmentDetailsViewController.h"
#import "certificationViewController.h"
#import "MKPLeaseBackDetailsViewController.h"

@interface assessmentDetailsViewController ()
@property(nonatomic,strong) NSString *contractIdStr;
@end

@implementation assessmentDetailsViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getAssessmentURL];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"评估详情";
//    self.view.backgroundColor = NAVCOLOR_C(242, 242, 242);
    [self createView];
//    [self setNav];
}

-(void)createView
{
    // 左上角返回
//        UIButton * addButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        addButton.titleLabel.font = [UIFont systemFontOfSize:18 weight:30];
//        [addButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        addButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
//        [addButton setImage:[UIImage imageNamed:@"fanhui"] forState:UIControlStateNormal];
//        [addButton addTarget:self action:@selector(confirmClick) forControlEvents:UIControlEventTouchUpInside];
//        UIBarButtonItem * left = [[UIBarButtonItem alloc]initWithCustomView:addButton];
//        UIBarButtonItem *fixedButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
//        fixedButton.width = W(-20);
//        self.navigationItem.leftBarButtonItems = @[fixedButton, left];
    // 价格
    [self.piceBT  setTitle:[NSString stringWithFormat:@"￥%@",self.assessmodel.evaluationPrice] forState:UIControlStateNormal];
    self.phoneNameLabel.text = self.assessmodel.name;
    self.textLabel.text = [NSString stringWithFormat:@"%@、%@、%@",self.assessmodel.color,self.assessmodel.screenShow,self.assessmodel.serviceHistory];
    
}

-(void)getAssessmentURL
{
//    evaluation/findMyEvaluation[18]    (null)    @"contractId" : @"1801172034491174"
    [HttpUrl GET:@"evaluation/findMyEvaluation" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if (BB_isSuccess) {
                       NSMutableDictionary *dic = [NSMutableDictionary dictionary];
                        dic = data[@"data"];
                       self.contractIdStr = dic[@"id"];
                    }else{
                        [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
                    }
    }];
    
}

- (IBAction)huizu:(UIButton *)sender {
   
    [HttpUrl NetErrorGET:@"authentication/selectMyAuthentication" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        NSLog(@"获取数据的js字符串==%@",[NSString FromDataToJSString:data]);
        if ([data[@"success"] integerValue] == 1) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic = data[@"data"];
            if( ([dic[@"bankCardAuth"] integerValue] == 1)&&([dic[@"identityAuth"] integerValue] == 1)&&([dic[@"userBaseMsgAuth"] integerValue] == 1)&&([dic[@"phoneRecordAuth"] integerValue] == 1))
            {
                // 暂时走,等会走合同
//                UIStoryboard *stroyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                certificationViewController *infoVC = [stroyboard instantiateViewControllerWithIdentifier:@"certificationViewController"];
//                ATNavigationController *nav = [[ATNavigationController alloc] initWithRootViewController:infoVC];
//                infoVC.contractIdStr = self.contractIdStr;
//                [self presentViewController:nav animated:YES completion:nil];
                
                                UIStoryboard *stroyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                MKPLeaseBackDetailsViewController *vc = [stroyboard instantiateViewControllerWithIdentifier:@"MKPLeaseBackDetailsViewController"];
//                                ATNavigationController *nav = [[ATNavigationController alloc] initWithRootViewController:vc];
                [self.navigationController pushViewController:vc animated:YES];
//                                [self presentViewController:nav animated:YES completion:nil];
                
//                MKPLeaseBackDetailsViewController *vc = [TheGlobalMethod setstoryboard:@"MKPLeaseBackDetailsViewController" controller:self];
//                vc.hidesBottomBarWhenPushed = YES;
//                [self.navigationController pushViewController:vc animated:YES];
            }
            else
            {
                // 认证界面
                UIStoryboard *stroyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                certificationViewController *infoVC = [stroyboard instantiateViewControllerWithIdentifier:@"certificationViewController"];
//                ATNavigationController *nav = [[ATNavigationController alloc] initWithRootViewController:infoVC];
                infoVC.contractIdStr = self.contractIdStr;
                
                [self.navigationController pushViewController:infoVC animated:YES];

//                certificationViewController *vc = [TheGlobalMethod setstoryboard:@"certificationViewController" controller:self];
//                vc.hidesBottomBarWhenPushed = YES;
//                [self.navigationController pushViewController:vc animated:YES];
                
            }
        } else {
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
        
    } WithFailBlock:^(id data) {
        [TheGlobalMethod xianShiAlertView:@"连接网络失败" controller:self];
    }];
    
}

-(void)confirmClick
{
    kNavPop
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
