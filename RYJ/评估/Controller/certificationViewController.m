//
//  certificationViewController.m
//  RYJ
//
//  Created by wyp on 2018/1/10.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import "certificationViewController.h"
#import "certificationTableViewCell.h"
#import "CerPersonMsgVC.h"
#import "CerPeopleIDVC.h"
#import "CerCardVC.h"
#import "CerPhoneVC.h"
// 有盾
#import "SocialInsuranceVC.h"
#import "UDIDSafeAuthEngine.h"
#import "UDIDSafeDataDefine.h"
#import "contractViewController.h"

#import "SignContractVC.h"

@interface certificationViewController ()<UITableViewDelegate,UITableViewDataSource,UDIDSafeAuthDelegate>
{
    __weak IBOutlet UITableView *myTableView;
    NSDictionary *dataDic;
    NSDictionary *authDic;
    
    CerPersonMsgVC *cerPersonMsgVC;
}
@property(nonatomic,strong) NSArray *dataSourList;

@end

@implementation certificationViewController

static NSString *const MKPCell = @"certificationCell";
#define UDPUBKEY        @"80b253b0-9743-49e4-9270-4afa12ef897a"
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupRefresh];
    
}
#pragma mark - 刷新
-(void)setupRefresh
{
    myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(getNetWorking)];
    myTableView.mj_header.automaticallyChangeAlpha = YES;
    [myTableView.mj_header beginRefreshing];
}

- (void)getNetWorking {
    [HttpUrl NetErrorGET:@"authentication/selectMyAuthentication" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        NSLog(@"获取数据的js字符串==%@",[NSString FromDataToJSString:data]);
    
        if ([data[@"success"] integerValue] == 1) {
            dataDic = data[@"data"];
            authDic = dataDic[@"status"];
            [myTableView reloadData];
            myTableView.hidden = NO;
        } else {
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }

        [myTableView.mj_header endRefreshing];
    } WithFailBlock:^(id data) {
        [TheGlobalMethod xianShiAlertView:@"连接网络失败" controller:self];
        [myTableView.mj_header endRefreshing];
    }];

}

- (void)viewDidLoad {
    [super viewDidLoad];

    myTableView.delegate = self;
    myTableView.dataSource = self;
    myTableView.backgroundColor = NAVCOLOR_C(242, 242, 242);
    myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _dataSourList = @[@[@{@"title":@"基本信息认证"},@{@"title":@"身份认证"},@{@"title":@"手机运营商认证"},@{@"title":@"银行卡认证"},@{@"title":@"认证完成"}]];
    
    cerPersonMsgVC = [TheGlobalMethod setstoryboard:@"CerPersonMsgVC" controller:self];
}

#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    if (!dataDic) {
//        return 0;
//    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 5;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    certificationTableViewCell loadingTableViewCell("certificationTableViewCell", )
 
    [cell reloadCellForData:_dataSourList[indexPath.section][indexPath.row] vc:self indexPath:indexPath];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // userBaseMsgAuth  identityAuth  phoneRecordAuth   phoneAddressBookAuth
    if(indexPath.row == 0)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            __block certificationViewController *vc = self;
            if([dataDic[@"userBaseMsgAuth"] integerValue] == 1)
            {
                ShowAlert(@"您已认证成功", self);
            }
            else
            {
                cerPersonMsgVC.reloadTheVC = ^{
                    [vc getNetWorking];
                };
                cerPersonMsgVC.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:cerPersonMsgVC animated:YES];
                
            }
        });
    }
    if(indexPath.row == 1)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([dataDic[@"userBaseMsgAuth"] integerValue] == 0) {
                ShowAlert(@"请先认证个人信息", self);
                return;
            }
            if ([dataDic[@"identityAuth"] integerValue] == 1) {
                ShowAlert(@"您已认证成功", self);
            }

            NSString *userIdStr = [NSString stringWithFormat:@"_%@",[TheGlobalMethod getUserDefault:@"userId"]];
            NSString *identityOrderNo = [[self getTimeSp] stringByAppendingString:userIdStr];                
                UDIDSafeAuthEngine * engine = [[UDIDSafeAuthEngine alloc]init];
                engine.delegate = self;
                engine.authKey = UDPUBKEY;
                engine.notificationUrl = [Http_IP stringByAppendingString:@"userIdentity/userIdentity/back"];
                engine.showInfo = YES;
                engine.isManualOCR = YES;
                //    engine.safeMode = UDIDSafeMode_Medium;
                engine.outOrderId = identityOrderNo;
                [engine startIdSafeFaceAuthInViewController:self];
            
        });
    }
    if(indexPath.row == 2)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([dataDic[@"identityAuth"] integerValue] == 0) {
                ShowAlert(@"请先进行身份认证", self);
                return;
            }
            if ([dataDic[@"phoneRecordAuth"] integerValue] == 1) {
                ShowAlert(@"您已认证成功", self);
            }
            
            CerPhoneVC *vc = [TheGlobalMethod setstoryboard:@"CerPhoneVC" controller:self];
            vc.reloadTheVC = ^{
                [self getNetWorking];
            };
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
            
        });
    }
    if(indexPath.row == 3)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([dataDic[@"phoneRecordAuth"] integerValue] == 0) {
                ShowAlert(@"请先进行运营商认证", self);
                return;
            }
            if ([dataDic[@"bankCardAuth"] integerValue] == 1) {
                ShowAlert(@"您已认证成功", self);
            }
            CerCardVC *vc = [TheGlobalMethod setstoryboard:@"CerCardVC" controller:self];
            vc.reloadTheVC = ^{
                [self getNetWorking];
            };
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        });
    }
    if(indexPath.row == 4)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([dataDic[@"bankCardAuth"] integerValue] == 0) {
                ShowAlert(@"请先进行银行卡认证", self);
                return;
            }
            // 跳转合同
            [self getContract];
        });
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return H(84);
    
}

-(void)getContract
{
//    [HttpUrl GET:@"yunhetong/getToken" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
//        if (BB_isSuccess) {
//           NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//            dic = data[@"data"];
//           NSString *tokenStr = dic[@"token"];
//           NSString *contractIdStr = dic[@"contractId"];
//            NSString *one = @"https://sdk.yunhetong.com/sdk/contract/hView?contractId=";
//            NSString *two = @"&";
//            NSString *three = @"token=";
//            NSString *contractStr = [NSString stringWithFormat:@"%@%@%@%@%@",one,contractIdStr,two,three,tokenStr];
//            contractViewController *vc = [[contractViewController alloc]init];
//            vc.contractStr  = contractStr;
//            [self.navigationController pushViewController:vc animated:YES];
           
//            [self.navigationController pushViewController:vc animated:YES];
//        }else{
//            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
//        }
//    }];
    
            SignContractVC * vc = [[SignContractVC alloc]initWithNibName:@"SignContractVC" bundle:nil];
            vc.returnStr = self.contractIdStr;
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pop:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];

}
// 签名时间方法
- (NSString *)getTimeSp {
    NSString *resultString = nil;
    NSTimeInterval time = [[NSDate date] timeIntervalSince1970] ;
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"yyyyMMddHHMMss"];
    NSDate *datenow = [NSDate dateWithTimeIntervalSince1970:time];
    resultString = [formatter stringFromDate:datenow];
    return resultString;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
