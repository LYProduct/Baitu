//
//  assessmentDetailsViewController.h
//  RYJ
//
//  Created by wyp on 2018/1/17.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "assessment.h"
@interface assessmentDetailsViewController : RootViewController

@property(nonatomic,strong) assessment *assessmodel;
@property (weak, nonatomic) IBOutlet UIButton *piceBT;
@property (weak, nonatomic) IBOutlet UILabel *phoneNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UIButton *huizuBt;

@end
