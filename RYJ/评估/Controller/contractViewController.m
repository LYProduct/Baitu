//
//  contractViewController.m
//  RYJ
//
//  Created by wyp on 2018/1/17.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import "contractViewController.h"

@interface contractViewController ()

@end

@implementation contractViewController

//_contractStr    __NSCFString *    @"https://sdk.yunhetong.com/sdk/contract/hView?contractId=1801171715061009&token=TGT-2029233-stynGJPcsvO2TOpYLdyaFig3Qkak46YJsQSXanpfbQWVsqPX2j-cas01.example.org"    0x00000001c0178240
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = NAVCOLOR_C(242, 242, 242);

    UIWebView * callWebview = [[UIWebView alloc] initWithFrame:self.view.bounds];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    callWebview.scalesPageToFit = YES;
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.contractStr] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval: 20.0];
     [self.view addSubview:callWebview];
    [callWebview loadRequest:request];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
