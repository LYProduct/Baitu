//
//  MKPLeaseBackDetailsViewController.m
//  RYJ
//
//  Created by wyp on 2018/1/18.
//  Copyright © 2018年 RongKe. All rights reserved.
//

#import "MKPLeaseBackDetailsViewController.h"
#import "MKPLeaseBackDetailsOneTableViewCell.h"
#import "MKPLeaseBackDetailsTwoTableViewCell.h"
#import "MKPLeaseBackDetailsThreeTableViewCell.h"
#import "MKPLeaseBackDetailsFourTableViewCell.h"
#import "MKPLeaseBackDetailsFiveTableViewCell.h"

@interface MKPLeaseBackDetailsViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    __weak IBOutlet UITableView *myTableView;
    NSDictionary *dataDic;
}
@end

@implementation MKPLeaseBackDetailsViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupRefresh];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"回租详情";
    self.view.backgroundColor = [UIColor whiteColor];
    
    myTableView.delegate = self;
    myTableView.dataSource = self;
    myTableView.separatorStyle = 0;
    myTableView.backgroundColor = NAVCOLOR_C(242, 242, 242);

}

#pragma mark - 刷新
-(void)setupRefresh
{
    myTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(getNetWorking)];
    myTableView.mj_header.automaticallyChangeAlpha = YES;
    [myTableView.mj_header beginRefreshing];
}

- (void)getNetWorking {
    [HttpUrl GET:@"evaluation/findMyEvaluation" dict:nil hud:self.view isShow:YES WithSuccessBlock:^(id data) {
        if (BB_isSuccess) {
            dataDic = data[@"data"];
            [myTableView reloadData];
        }else{
            [TheGlobalMethod xianShiAlertView:data[@"msg"] controller:self];
        }
    }];
    [myTableView.mj_header endRefreshing];
}

#pragma mark 有几个分区
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (!dataDic) {
        return 0;
    }
    return 8;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else if (section == 1) {
        return 8;
    } else if (section == 2) {
        return 1;
    } else if (section == 3) {
        return 1;
    } else if (section == 4) {
        return 3;
    }else if (section == 5) {
        return 1;
    }else if (section == 6) {
        return 1;
    }else if (section == 7) {
        return 1;
    }
    
    return 3;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0)
    {
        MKPLeaseBackDetailsOneTableViewCell loadingTableViewCell("MKPLeaseBackDetailsOneTableViewCell", )
        [cell reloadCellForData:dataDic vc:self indexPath:indexPath];
        return cell;
    }
    if(indexPath.section == 1)
    {
        MKPLeaseBackDetailsTwoTableViewCell loadingTableViewCell("MKPLeaseBackDetailsTwoTableViewCell", )
        [cell reloadCellForData:dataDic vc:self indexPath:indexPath];
        return cell;
    }
    if(indexPath.section == 2)
    {
        MKPLeaseBackDetailsOneTableViewCell loadingTableViewCell("MKPLeaseBackDetailsOneTableViewCell", )
        [cell reloadCellForData:dataDic vc:self indexPath:indexPath];
        return cell;
    }
    if(indexPath.section == 3)
    {
        MKPLeaseBackDetailsOneTableViewCell loadingTableViewCell("MKPLeaseBackDetailsOneTableViewCell", )
        [cell reloadCellForData:dataDic vc:self indexPath:indexPath];
        return cell;
    }
    if(indexPath.section == 4)
    {
        MKPLeaseBackDetailsOneTableViewCell loadingTableViewCell("MKPLeaseBackDetailsOneTableViewCell", )
        [cell reloadCellForData:dataDic vc:self indexPath:indexPath];
        return cell;
    }
    if(indexPath.section == 5)
    {
        MKPLeaseBackDetailsOneTableViewCell loadingTableViewCell("MKPLeaseBackDetailsOneTableViewCell", )
        [cell reloadCellForData:dataDic vc:self indexPath:indexPath];
        return cell;
    }
    if(indexPath.section == 6)
    {
        MKPLeaseBackDetailsOneTableViewCell loadingTableViewCell("MKPLeaseBackDetailsOneTableViewCell", )
        [cell reloadCellForData:dataDic vc:self indexPath:indexPath];
        return cell;
    }
    if(indexPath.section == 7)
    {
        MKPLeaseBackDetailsOneTableViewCell loadingTableViewCell("MKPLeaseBackDetailsOneTableViewCell", )
        [cell reloadCellForData:dataDic vc:self indexPath:indexPath];
        return cell;
    }
  
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
   if(indexPath.section == 0)
   {
       return H(43);
   }
    if(indexPath.section == 1)
    {
        if(indexPath.row == 0)
        {
           return H(27);
        }
        if(indexPath.row == 1)
        {
            return H(27);
        }
        if(indexPath.row == 2)
        {
            return H(27);
        }
        if(indexPath.row == 3)
        {
            return H(27);
        }
        if(indexPath.row == 4)
        {
            return H(27);
        }
        if(indexPath.row == 5)
        {
            return H(27);
        }
        if(indexPath.row == 6)
        {
            return H(27);
        }
        if(indexPath.row == 7)
        {
            return H(35);
        }
    }
    if(indexPath.section == 2)
    {
        return H(43);
    }
    if(indexPath.section == 3)
    {
        return H(43);
    }
    if(indexPath.section == 4)
    {
        if(indexPath.row == 0)
        {
            return H(27);
        }
        if(indexPath.row == 1)
        {
            return H(27);
        }
        if(indexPath.row == 2)
        {
            return H(35);
        }
    }
    if(indexPath.section == 5)
    {
        return H(43);
    }
    if(indexPath.section == 6)
    {
        return H(43);
    }
    if(indexPath.section == 7)
    {
        return H(43);
    }
    
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, 10)];
    view.backgroundColor = [UIColor clearColor];

    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH_K, 10)];
    view.backgroundColor = [UIColor clearColor];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if(section == 1)
    {
        return H(10);
    }
    if(section == 2)
    {
    return H(10);
    }
    if(section == 4)
    {
      return H(10);
    }
    if(section == 7)
    {
        return H(10);
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        return H(10);
    }

    return 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}
// 确认回租 
- (IBAction)confirmBt:(UIButton *)sender {
    
    
}
- (IBAction)pop:(UIBarButtonItem *)sender {
    kNavPop
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
