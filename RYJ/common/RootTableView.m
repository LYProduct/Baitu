//
//  RootTableView.m
//  MT
//
//  Created by wyp on 17/1/5.
//  Copyright © 2017年 ike. All rights reserved.
//

#import "RootTableView.h"

@implementation RootTableView


- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.showsHorizontalScrollIndicator = NO;
    self.showsVerticalScrollIndicator = NO;
    self.backgroundColor = NAVCOLOR_C(244.0, 244.0, 244.0);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
