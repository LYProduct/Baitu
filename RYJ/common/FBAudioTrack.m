//
//  FBAudioTrack.m
//  RYJ
//
//  Created by 云鹏 on 2017/11/6.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "FBAudioTrack.h"

@implementation FBAudioTrack

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (CGRect)trackRectForBounds:(CGRect)bounds {
    return CGRectMake(0, 0, WIDTH_K-48, 10);
}

@end
