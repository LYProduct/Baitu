//
//  UIViewController+XIBPOP.m
//  MDD
//
//  Created by 云鹏 on 2017/10/12.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "UIViewController+XIBPOP.h"

@implementation UIViewController (XIBPOP)
- (void)popToTopVC {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
