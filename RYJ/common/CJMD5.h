//
//  CJMD5.h
//  BS
//
//  Created by ike on 16/7/18.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CJMD5 : NSObject
+(NSString *)md5HexDigest:(NSString *)input;
@end
