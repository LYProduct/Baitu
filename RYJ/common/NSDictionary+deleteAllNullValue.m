//
//  NSDictionary+deleteAllNullValue.m
//  YunNa
//
//  Created by 云鹏 on 2017/9/16.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "NSDictionary+deleteAllNullValue.h"

@implementation NSDictionary (deleteAllNullValue)

- (NSDictionary *)deleteAllNullValue{
    NSMutableDictionary *mutableDic = [[NSMutableDictionary alloc] init];
    for (NSString *keyStr in self.allKeys) {
        if ([[self objectForKey:keyStr] isEqual:[NSNull null]]) {
            [mutableDic setObject:@"" forKey:keyStr];
        }
        else{
            [mutableDic setObject:[self objectForKey:keyStr] forKey:keyStr];
        }
    }
    return mutableDic;
}

@end
