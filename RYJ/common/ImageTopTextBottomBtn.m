//
//  ImageTopTextBottomBtn.m
//  MDD
//
//  Created by wyp on 2017/6/26.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "ImageTopTextBottomBtn.h"


@implementation ImageTopTextBottomBtn

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
*/

- (void)setHighlighted:(BOOL)highlighted {
    
}

- (void)drawRect:(CGRect)rect {
    // Drawing code
    [super drawRect:rect];
    float  spacing = 5;//图片和文字的上下间距
    
    CGSize imageSize = self.imageView.frame.size;
    CGSize titleSize = self.titleLabel.frame.size;
    CGSize textSize = [self.titleLabel.text sizeWithAttributes:@{NSFontAttributeName : self.titleLabel.font}];
    CGSize frameSize = CGSizeMake(ceilf(textSize.width), ceilf(textSize.height));
    if (titleSize.width + 0.5 < frameSize.width) {
        titleSize.width = frameSize.width;
    }
    CGFloat totalHeight = (imageSize.height + titleSize.height + spacing);
    self.imageEdgeInsets = UIEdgeInsetsMake(- (totalHeight - imageSize.height), 0.0, 0.0, - titleSize.width);
    self.titleEdgeInsets = UIEdgeInsetsMake(0, - imageSize.width, - (totalHeight - titleSize.height), 0);
}

@end
