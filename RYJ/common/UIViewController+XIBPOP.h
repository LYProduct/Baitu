//
//  UIViewController+XIBPOP.h
//  MDD
//
//  Created by 云鹏 on 2017/10/12.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (XIBPOP)
- (void)popToTopVC;
@end
