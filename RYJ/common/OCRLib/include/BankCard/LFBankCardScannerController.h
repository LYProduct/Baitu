//
//  LFBankCardScannerController.h
//  Linkface
//
//  Copyright © 2017-2018 LINKFACE Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LFCaptureController.h"
#import "LFBankCard.h"

typedef NS_OPTIONS(NSInteger, LFBankCardErrorCode) {
    kBankCardCameraAuthorizationFailed = 1,
    kBankCardHandleInitFailed = 2,
};

@protocol LFBankCardScannerControllerDelegate <LFCaptureDelegate>

@optional
//获取银行卡结果
-(void) getCardResult:(LFBankCard *)bankcard;
//获取银行卡结果和截图
-(void) getCardImage:(UIImage *) imgIDCard withCardInfo:(LFBankCard *)bankcard;
//识别出错
-(void) getError: (LFBankCardErrorCode) errorCode;

@end

@interface LFBankCardScannerController : LFCaptureController


@property (nonatomic, weak) id<LFBankCardScannerControllerDelegate> delegate;

- (instancetype)initWithOrientation:(AVCaptureVideoOrientation)orientation licenseName:(NSString *)licenseName isVertical:(BOOL)isVertical shouldFullCard:(BOOL)shouldFullCard;
/*! @brief hideMaskView 控制是否显示内置的取景框和提示
 *
 *  @param bHide
 *          NO ： 不隐藏默认取景框和提示文字 （default）
 *          YES： 隐藏默认的取景框和提示文字，用于开发者自定义取景框界面
 *
 *         注意：
 *              该方法在 present 出了 LFIDCardScannerController 之后或
 *              继承了 LFIDCardScannerController 的 viewDidLoad {} 中调用才能生效
 */
// - (void)hideMaskView:(BOOL) bHide ;

/*! @brief moveWindowVerticalFromCenterWithDelta 调整取景框的位置
 *                  用于开发者自定义取景框，从中央上上下移动是为了在不同屏幕上的适配问题
 *                  参考示例程序 ScannerCMB 观察取景框的变化及记录自定义的取景框偏移量，
 *                  用于和定义的界面保持一致
 *
 *  @param iDeltaY    取景框从中央位置上下移动的偏移量
 *          >0：     取景框从中央位置上移
 *          <0：     取景框从中央位置下移
 */
- (void)moveWindowVerticalFromCenterWithDelta:(int) iDeltaY ;

/*! @brief doRecognitionProcess 设置是否进行视频帧的识别处理
 *
 *  @param bProcessEnabled
 *          YES：    对从设置起后的每帧都进行身份证识别处理 （默认值）
 *          NO：     对从设置起后的每帧都不做识别处理，还保留视频播放
 */
- (void) doRecognitionProcess:(BOOL) bProcessEnabled ;

/*! @brief changeScanWindowForVerticalCard 将扫描框变为扫描竖卡的扫描框
 *
 *  @param isVertical
 *          YES：    扫描框向右旋转
 *          NO：     扫描框向左旋转
 */
- (void)changeScanWindowForVerticalCard:(BOOL)isVertical;


/*! @brief didCancel 程序控制关闭摄像头并取消 LFIDCardScannerController
 *
 *      请使用其他 Controller 调用 LFIDCardScannerController 的 didCancel 来控制摄像头的关闭，如用于超时控制；
 *      也可继承本类，通过重载 didCancel 来控制关闭的方式和时机 。
 */
- (void) didCancel; // 允许其他 Controller 主动控制关闭

- (void) changeScanDirection:(UIButton *)button;

@property (nonatomic, assign)   BOOL    isScanVerticalCard ;


/*! @brief hideChangeScanModeButton 来隐藏控制扫描横竖卡切换的按钮
 *
 *      YES为隐藏，默认不隐藏
 */
@property (nonatomic, assign)   BOOL    showChangeScanModeButton;

/*! @brief snapshotSeconds 是否在扫描的时候获取快照。默认-1，需要则设置时间间隔，单位：秒
 */
@property (nonatomic, assign) NSInteger snapshotSeconds;


/*! @brief callDelegate_getCard: withInformation: 支持自定义扫描成功后的处理流程，支持预览结果和重扫；
 *          v3.6   : 2015.6.11,
 *  @param image 扫描窗区域的截图，包含含有完整身份证边缘的用于识别的图像
 *  @param bankCard 身份证识别的结果，含识别出的文字信息
 *
 *  该方法用于自定义扫描出结果后的流程；
 *  默认是暂停识别处理，给delegate返回识别的图像 image 和识别结果 LFIDCard。
 *  开发者可以继承本类后，重定义识别后的流程，例如给用户展示结果，支持重新扫描或确认结果后结束扫描。
 */
- (void) callDelegate_getCard:(UIImage *)image withInformation:(LFBankCard *)bankCard ;

- (void)receivedError:(NSInteger) errorCode;

- (CGRect)maskWindowRect;
@end
