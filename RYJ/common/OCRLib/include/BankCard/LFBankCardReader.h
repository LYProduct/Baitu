//
//  LFBankCardCaputreReader.h
//  Linkface
//
//  Copyright © 2017-2018 LINKFACE Corporation. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "LFCaptureReader.h"
#import "LFBankCardMaskView.h"

@interface LFBankCardReader : LFCaptureReader

- (instancetype)initWithLiceneseName:(NSString *)liceneseName shouldFullCard:(BOOL)shouldFullCard;

@property (nonatomic, weak) LFBankCardMaskView *maskView ;
@property (nonatomic, strong) UIImage *resultImage;

//@property (nonatomic, assign) BOOL isScanVerticalCard;

/*! @brief bDebug 调试开关(控制NSLog的输出)
 */
@property (nonatomic, assign)   BOOL    bDebug  ;


- (void)moveWindowVerticalFromCenterWithDelta:(int) iDelta ;  //  iDelta == 0 in center , < 0 move up, > 0 move down

- (void)changeScanWindowToVertical:(BOOL)isVertical;

@end
