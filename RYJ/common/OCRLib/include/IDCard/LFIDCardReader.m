//
//  LFIDCardReader.m
//  Linkface
//
//  Copyright © 2017-2018 LINKFACE Corporation. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <libkern/OSAtomic.h>
#import "LFIDCardReader.h"

#import <Endian.h>

#define MAX_Attributes 32
#define kCountTimeOutDefault  1200
#define ST_MAX_RECOGNIZE_THREAD 1    // For Multi-Core Accelerate

enum {
    PHOTOS_EXIF_0ROW_TOP_0COL_LEFT			= 1, //   1  =  0th row is at the top, and 0th column is on the left (THE DEFAULT).
    PHOTOS_EXIF_0ROW_TOP_0COL_RIGHT			= 2, //   2  =  0th row is at the top, and 0th column is on the right.
    PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT      = 3, //   3  =  0th row is at the bottom, and 0th column is on the right.
    PHOTOS_EXIF_0ROW_BOTTOM_0COL_LEFT       = 4, //   4  =  0th row is at the bottom, and 0th column is on the left.
    PHOTOS_EXIF_0ROW_LEFT_0COL_TOP          = 5, //   5  =  0th row is on the left, and 0th column is the top.
    PHOTOS_EXIF_0ROW_RIGHT_0COL_TOP         = 6, //   6  =  0th row is on the right, and 0th column is the top.
    PHOTOS_EXIF_0ROW_RIGHT_0COL_BOTTOM      = 7, //   7  =  0th row is on the right, and 0th column is the bottom.
    PHOTOS_EXIF_0ROW_LEFT_0COL_BOTTOM       = 8  //   8  =  0th row is on the left, and 0th column is the bottom.
};

@interface LFIDCardReader ()
{
    int _iMoveDelta ; //  iDelta == 0 in center , > 0 move up, < 0 move down
    int _iMoveDeltaCopy ; //  copy on each frame, for thread-safe on frame processing
    
    LFIDCardMode _tempCardMode;
    NSDate *_lastsnapshotTime;
}

@property (nonatomic, strong) NSMutableArray *availableIDCards;

@property (nonatomic, assign) bool  bNeedChangeCardMode;


@end

@implementation LFIDCardReader

//@synthesize strPlate ;
@synthesize maskView ;

- (instancetype)initWithLiceneseName:(NSString *)liceneseName shouldFullCard:(BOOL)shouldFullCard
{
    self = [super initWithLiceneseName:liceneseName];
    if (self) {
        //set captureOutput property
        self.captureOutput.videoSettings = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA] forKey:(NSString *)kCVPixelBufferPixelFormatTypeKey];
        self.adjustingFocus = NO;
        
        self.snapshotSeconds = -1;
        _lastsnapshotTime = nil;
        if ( self.bDebug )
            NSLog( @"LFIDCardReader init, LFIDCardScannerController init. " );

        _availableIDCards = [[NSMutableArray alloc] init] ;
        for (int i = 0; i < MIN(ST_MAX_RECOGNIZE_THREAD, [[NSProcessInfo processInfo] activeProcessorCount]); i++) {
            NSString *licensePath = [[NSBundle mainBundle] pathForResource:liceneseName ofType:@"lic"];
            LFIDCard *idcard = [[LFIDCard alloc] initWithLicensePath:licensePath];
            if (idcard) {
                idcard.shouldFullCard = shouldFullCard;
                idcard.bDebug = self.bDebug;
                idcard.iMode = kIDCardSmart;
                [_availableIDCards addObject:idcard];
            }
        }
//        NSLog( @"LFIDCard init version = %@ ", LFIDCard.strVersion );
        self.bProcessEnabled = YES ; // perform OCR on each video frame
    }
    return self;
}

- (void) setBDebug:(BOOL)bDebug
{
    _bDebug = bDebug ;
    for (LFIDCard *idcard in self.availableIDCards) {
        idcard.bDebug = bDebug;
    }
}

- (void)setMode:(LFIDCardMode)mode
{
    UILabel *tempLabel = [[UILabel alloc] init];
    tempLabel.textColor = [UIColor whiteColor];
    [self loopSetIDCardMode:mode];
    switch (mode) {
        case kIDCardSmart:
            tempLabel.text = @"请将身份证放入扫描框内";
            [self.maskView setLabel:tempLabel];
            break;
        case kIDCardFrontal:
            tempLabel.text = @"请将身份证正面放入扫描框内";
            [self.maskView setLabel:tempLabel];
            break;
        case kIDCardBack:
            tempLabel.text = @"请将身份证反面放入扫描框内";
            [self.maskView setLabel:tempLabel];
            break;

        default:
            break;
    }
}

- (void)loopSetIDCardMode:(LFIDCardMode)mode{
    self.bNeedChangeCardMode = YES;
    if ([self.availableIDCards count] > 0) {
        for (LFIDCard *idcard in self.availableIDCards) {
            idcard.iMode = mode;
        }
        self.bNeedChangeCardMode = NO;
    }else{
        _tempCardMode = mode;
    }
    
}

- (void)setHintLabel:(UILabel *)label{
    [self.maskView setLabel:label];
}


- (void) dealloc
{
    if ( self.bDebug )
        NSLog( @"LFIDCardReader dealloc." );
}

- (void)recognizeWithSampleBuffer:(CMSampleBufferRef)sampleBuffer
{
    
    if ( !self.bProcessEnabled )
        return ; 
        
    if (self.adjustingFocus) return;
    
    // 没取到结果前，拍快照
    [self snapshot:sampleBuffer];
    
    if (!CMSampleBufferIsValid(sampleBuffer)) {
        return;
    }
    
    if ([self.availableIDCards count] <= 0) {
        return;
    }
    LFIDCard *idcard = [self.availableIDCards lastObject];
    [self.availableIDCards removeObject:idcard];
    
    CFRetain(sampleBuffer);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        _iMoveDeltaCopy = _iMoveDelta ;
        
//        UIImage *image = [self imageFromSampleBuffer:sampleBuffer];
//        UIImage *test = [self cropImage:image atRect:self.recWindow];
        CVPixelBufferRef pixelBuffer = (CVPixelBufferRef)CMSampleBufferGetImageBuffer(sampleBuffer);
        CVPixelBufferLockBaseAddress(pixelBuffer, 0);
        uint8_t *baseAddress = CVPixelBufferGetBaseAddress(pixelBuffer);
        int width = (int)CVPixelBufferGetWidth(pixelBuffer);
        int height = (int)CVPixelBufferGetHeight(pixelBuffer);
        
        uint8_t *window_buffer = [self transformRGBAtoRGB:baseAddress width:width height:height];
        CGRect rectW = self.recWindow;
        int realWidth  = rectW.size.width;
        int realHeight = rectW.size.height;
        
//        UIImage * image2 = [self imageFromCharRef:window_buffer withWidth:realWidth andHeight:realHeight];
        int iResult = [idcard recognizeCardWithBuffer:window_buffer width:realWidth height:realHeight ] ;
        
        if ( iResult == 2 ) {
            UIImage *fullImage = [self imageFromSampleBuffer:sampleBuffer];
            [idcard setImgOriginCaptured:fullImage];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                if (self.delegate && [self.delegate respondsToSelector:@selector(captureReader:getCardResult:)]) {
                    [self.delegate captureReader:self getCardResult:idcard];
                }
            });
        }
        
        [self activeIDCardInAvailableBankCards:idcard];
        // 释放资源
        free(window_buffer);
        CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
        CFRelease(sampleBuffer);
        
    });

    return ;
    
}

- (UIImage *)cropImage:(UIImage *)image atRect:(CGRect) rect
{
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect drawRect = CGRectMake(-rect.origin.x, -rect.origin.y, image.size.width, image.size.height);
    CGContextClipToRect(context, CGRectMake(0, 0, rect.size.width, rect.size.height));
    [image drawInRect:drawRect];
    UIImage* croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return croppedImage;
}

-(void)snapshot:(CMSampleBufferRef)sampleBuffer{
    if (self.snapshotSeconds <= 0) return;
    if (_lastsnapshotTime == nil) {
        _lastsnapshotTime = [NSDate date];
    }
    NSDate *cur = [NSDate date];
    double deltaTime = [cur timeIntervalSinceDate:_lastsnapshotTime];
    if (deltaTime >= self.snapshotSeconds) {
        _lastsnapshotTime = cur;
        if ([self.delegate respondsToSelector:@selector(captureReader:didSnapshotInProgress:)]) {
            UIImage *image = [self imageFromSampleBuffer:sampleBuffer];
            [self.delegate captureReader:self didSnapshotInProgress:image];
        }
    }
}

- (uint8_t*)transformRGBAtoRGB:(uint8_t*)baseAddress width:(int)width height:(int)height{
    size_t buffer_size = sizeof(unsigned char) * self.recWindow.size.width * self.recWindow.size.height * PIXEL_COMPONENT_NUM;
    uint8_t *window_buffer = malloc(buffer_size);
    int dn = 0;
    int on = 0;
    CGRect rectW = self.recWindow;
    for (int y=0; y < height; y++) {
        for (int x = 0; x< width; x++) {
            if (dn >= buffer_size) {
                break;
            }
            if (y>=(int)rectW.origin.y && y < (int)(rectW.origin.y + rectW.size.height) && x>=(int)rectW.origin.x && x< (int)(rectW.origin.x + rectW.size.width)) {
                window_buffer[dn++] = baseAddress[on++]; // R
                window_buffer[dn++] = baseAddress[on++]; // G
                window_buffer[dn++] = baseAddress[on++]; // B
                on++; // skip alph
            } else {
                on += 4;
            }
        }
        if (dn >= buffer_size) {
            break;
        }
    }
    return window_buffer;
}

- (void)activeIDCardInAvailableBankCards:(id)Object{
    [self.availableIDCards addObject:Object];
    if (self.bNeedChangeCardMode == YES && self.availableIDCards.count > 0) {
        [self loopSetIDCardMode:_tempCardMode];
    }
}

- (void)moveWindowVerticalFromCenterWithDelta:(int) iDelta   //  iDeltaY == 0 in center , < 0 move up, > 0 move down
{
    if (!(self.orientation == AVCaptureVideoOrientationPortrait)) {
        return;
    }
    _iMoveDelta = iDelta;
    
    CGFloat realHeight = SCREEN_HEIGHT;
    
    if ((self.recWindow.origin.y + (CGFloat)_iMoveDelta / realHeight * (CGFloat)self.iVideoHeight) <0) {
        _iMoveDelta = 0 - self.recWindow.origin.y / (CGFloat)self.iVideoHeight * realHeight;
    }
    if ((self.recWindow.origin.y + (CGFloat)_iMoveDelta / realHeight * (CGFloat)self.iVideoHeight + self.recWindow.size.height > (CGFloat)self.iVideoHeight)) {
        _iMoveDelta = realHeight - self.recWindow.origin.y / (CGFloat)self.iVideoHeight * realHeight - self.recWindow.size.height / self.iVideoHeight * realHeight;
    }
    CGRect tmpRect = self.recWindow;
    if (self.iVideoHeight > self.iVideoWidth) {
        tmpRect.origin.y += (CGFloat)_iMoveDelta / realHeight * (CGFloat)self.iVideoHeight;
    }
    self.recWindow = tmpRect;

    [self.maskView moveWindowDeltaY:_iMoveDelta] ;

}

- (void)setVideoOrientation:(AVCaptureVideoOrientation)orientation{
    [super setVideoOrientation:orientation];
}

- (void)setRecognizeItemOption:(LFIDCardItemOption)option {
    for (LFIDCard *card in _availableIDCards) {
        [card setRecognizeItemsOptions:option];
    }
}

@end


