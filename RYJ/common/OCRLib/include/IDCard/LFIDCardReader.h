//
//  LFIDCardReader.h
//  Linkface
//
//  Copyright © 2017-2018 LINKFACE Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LFCaptureReader.h"
#import "LFIDCardMaskView.h"
#import "LFIDCard.h"

@interface LFIDCardReader : LFCaptureReader

- (instancetype)initWithLiceneseName:(NSString *)liceneseName shouldFullCard:(BOOL)shouldFullCard;

@property (nonatomic, weak) LFIDCardMaskView *maskView ;
@property (nonatomic, strong) UIImage *resultImage;

/*! @brief bDebug 调试开关(控制NSLog的输出)
 */
@property (nonatomic, assign) BOOL bDebug  ;

- (void)moveWindowVerticalFromCenterWithDelta:(int) iDelta ;  //  iDelta == 0 in center , < 0 move up, > 0 move down

- (void)setMode:(LFIDCardMode)mode;

- (void)setRecognizeItemOption: (LFIDCardItemOption) option;

- (void)setHintLabel:(UILabel *)label;


@end
