//
//  LFIDCardScannerController ： 身份证扫描控制器
//  Linkface
//
//  Copyright © 2017-2018 LINKFACE Corporation. All rights reserved.

#import "LFCaptureController.h"
#import "LFIDCard.h"

typedef NS_OPTIONS(NSInteger, LFIDCardErrorCode) {
    kIDCardCameraAuthorizationFailed = 1,
    kIDCardHandleInitFailed = 2,
};

@protocol LFIDCardScannerControllerDelegate <LFCaptureDelegate>

@optional
//获取身份证结果和截图
-(void) getCard:(UIImage *) imgSnap withInformation:(LFIDCard *)idCardInformation;
//获取身份证扫描结果
-(void) getCardResult: (LFIDCard *)idcard;
//识别出错
-(void) getError: (LFIDCardErrorCode) errorCode;

@end

/*! @brief LFIDCardScannerController 函数类
 *
 * 该类封装了带有摄像头的全自动身份证检测和识别功能
 */

@interface LFIDCardScannerController : LFCaptureController

@property (nonatomic, weak) id<LFIDCardScannerControllerDelegate> delegate;

/*! @brief hideMaskView 控制是否显示内置的取景框和提示
 *
 *  @param bHide
 *          NO ： 不隐藏默认取景框和提示文字 （default）
 *          YES： 隐藏默认的取景框和提示文字，用于开发者自定义取景框界面
 *
 *         注意：
 *              该方法在 present 出了 LFIDCardScannerController 之后或
 *              继承了 LFIDCardScannerController 的 viewDidLoad {} 中调用才能生效
 */
 - (void)hideMaskView:(BOOL) bHide ;

/*! @brief moveWindowVerticalFromCenterWithDelta 调整取景框的位置
 *                  用于开发者自定义取景框，从中央上上下移动是为了在不同屏幕上的适配问题
 *                  参考示例程序 ScannerCMB 观察取景框的变化及记录自定义的取景框偏移量，
 *                  用于和定义的界面保持一致
 *
 *  @param iDeltaY    取景框从中央位置上下移动的偏移量
 *          >0：     取景框从中央位置上移
 *          <0：     取景框从中央位置下移
 */
- (void)moveWindowVerticalFromCenterWithDelta:(int) iDeltaY ;  

/*! @brief doRecognitionProcess 设置是否进行视频帧的识别处理
 *
 *  @param bProcessEnabled
 *          YES：    对从设置起后的每帧都进行身份证识别处理 （默认值）
 *          NO：     对从设置起后的每帧都不做识别处理，还保留视频播放
 */
- (void) doRecognitionProcess:(BOOL) bProcessEnabled ; 

/*! @brief didCancel 程序控制关闭摄像头并取消 LFIDCardScannerController
 *
 *      请使用其他 Controller 调用 LFIDCardScannerController 的 didCancel 来控制摄像头的关闭，如用于超时控制；
 *      也可继承本类，通过重载 didCancel 来控制关闭的方式和时机 。
 */
- (void) didCancel; // 允许其他 Controller 主动控制关闭 LFIDCardScannerController

/*! @brief cardMode 控制识别身份证哪个面：
 *              0 - kIDCardSmart     - 智能检测 （default）
 *              1 - kIDCardFrontal   - 正面 ,
 *              2 - kIDCardBack      - 反面,
 *
 *         注意：
 *              该方法在 present 出了 LFIDCardScannerController 之后或
 *              继承了 LFIDCardScannerController 的 viewDidLoad {} 中调用才能生效
 */
@property (nonatomic, assign)   LFIDCardMode    cardMode  ;


/*! @brief snapshotSeconds 是否在扫描的时候获取快照。默认-1，需要则设置时间间隔，单位：秒
 */
@property (nonatomic, assign) NSInteger snapshotSeconds;


- (void) callDelegate_getCard:(UIImage *)image withInformation:(LFIDCard *)LFIDCard ;

- (void)receivedError:(NSInteger)errorCode;

- (void)setRecognizeItemOption:(LFIDCardItemOption)option;

- (CGRect)maskWindowRect;

- (void)setHintLabel:(UILabel *)label;

@end
