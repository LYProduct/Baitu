//
//  LFCaptureController.h
//  Linkface
//
//  Copyright © 2017-2018 LINKFACE Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LFCaptureDelegate.h"
#import "LFCaptureMaskView.h"
#import "LFCaptureReaderDelegate.h"
#import "LFCaptureReader.h"
#import "LFCapture.h"


typedef NS_ENUM(NSInteger, DICaptureMode)
{
    kCaptureCard        ,     //2014.11.11 IDCard Detection
    kCaptureCardBack    ,   // 2015.01.13 Capture Card Back
    kCaptureBankCard    ,
};


@protocol LFCaptureDelegate;



@interface LFCaptureController : UIViewController

@property (nonatomic, assign) DICaptureMode iMode;      
@property (nonatomic, weak) id<LFCaptureDelegate> captureDelegate;
@property (nonatomic, strong) LFCaptureReader* captureReader;
@property (nonatomic, strong) LFCaptureMaskView *readerView;
@property (nonatomic, assign) AVCaptureVideoOrientation captureOrientation;
@property (nonatomic, strong) LFCapture *capture;
@property (nonatomic, strong) UIButton *btnChangeScanDirection;
@property (nonatomic, assign) BOOL showAnimation;
@property (nonatomic, strong) UIButton *btnCancel;
@property (nonatomic, copy) NSString *licenseName;


@property (nonatomic, readonly, assign) BOOL shouldFullCard; //是否卡片完整才返回

@property (nonatomic, assign) NSInteger autoCancelTime; //未检测到卡片超时时间，0则不做超时检测。

/**
 captureController init

 @param orientation 方向
 @param licenseName 授权文件名 Linkface.lic 传 Linkface
 @return captureController
 */
- (instancetype)initWithOrientation:(AVCaptureVideoOrientation) orientation licenseName:(NSString *)licenseName shouldFullCard:(BOOL)shouldFullCard;

- (void)stop;

- (void)changeCaptureMode:(NSInteger)iMode;

- (void)captureReader:(LFCaptureReader *)reader didSnapshot:(UIImage *)image;
- (void)hideMaskView:(BOOL) bHidden ;

- (void)didCancel ; // 允许其他 Controller 主动控制关闭 LFIDCardScannerController
- (void) autoCancel; // 超时自动关闭

- (void)receivedErrorNote:(NSNotification *)notification;

- (void)receivedError: (NSInteger)errorCode;

- (void) doRecognitionProcess:(BOOL) bProcessEnabled;

- (void)resetAutoCancelTimer;

/*! @brief bDebug 调试开关(控制NSLog的输出)
 */
@property (nonatomic, assign)   BOOL    bDebug  ;
// The interface to modify the line color.
- (void)setTheScanLineColor:(UIColor *)color;
// The interface to modify the layer color.
- (void)setTheMaskLayerColor:(UIColor *)color andAlpha:(CGFloat)alpha;

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation inDuration:(NSTimeInterval)duration;

@end
