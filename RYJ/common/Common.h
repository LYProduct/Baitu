//
//  Common.h
//  Teaism
//
//  Created by Apple on 15/10/23.
//  Copyright © 2015年 Apple. All rights reserved.
//

#ifndef Common_h
#define Common_h

#define BB_isSuccess [data[@"success"] boolValue]==YES
#define BB_isLogin !isStringEmpty([TheGlobalMethod getUserDefault:@"token"])

#define FONT(A) [UIFont systemFontOfSize:A]

//TableViewCellHeightSelfRows行高
#define TableViewCellHeight -(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{\
UITableViewCell *cell = (UITableViewCell *)[self tableView:myTableView cellForRowAtIndexPath:indexPath];\
return cell.height;\
}


/** tableView返回几个分区 */
#define TableViewNumberOfSectionsInTableView - (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView

//tableView返回几行
#define TableViewNumberOfRowsInSection - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//返回tableView cell
#define TableViewCellForRowAtIndexPath - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//行高
#define TableViewHeightForRowAtIndexPath - (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//header高
#define TableViewHeightForHeaderInSection - (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//footer高
#define TableViewHeightForFooterInSection - (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//返回footer视图
#define TableViewViewForFooterInSection - (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section

//点击方法
#define TableViewDidSelectRowAtIndexPath - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

#define kBackGroudColor [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1]


#define Limit 15
/*-----------------------------TableView------------------------------*/
//CellLabel初始化
#define CellLabelAlloc_K(_Name_) (_Name_) = [[UILabel alloc]init];\
[self addSubview:(_Name_)];
//CellImg
#define CellImage_K(_Img_) (_Img_) = [[UIImageView alloc]init];\
[self addSubview:(_Img_)];
//idCell
#define idCell_K NSString *idCell = [NSString stringWithFormat:@"%ld,%ld",(long)indexPath.section,(long)indexPath.row];
#define initCell(_CellName_)  *cell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"%ld,%ld",(long)indexPath.section,(long)indexPath.row]];\
if (cell == nil) {\
cell= [[[NSBundle mainBundle]loadNibNamed:@(_CellName_) owner:self options:nil]objectAtIndex:0];\
}
//Header Footer View
#define initView(_CellName_) *cell = [[[NSBundle mainBundle]loadNibNamed:@(_CellName_) owner:nil options:nil]objectAtIndex:0];
//TableViewHeightHeader
#define TableViewHeightHeader  -(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//TableViewHeightFooter
#define TableViewHeightFooter  -(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//TableViewNumberSection
#define TableViewNumberSection - (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//TableViewNumberSectionRows
#define TableViewNumberSectionRows - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//TableViewCellHeightRow
#define TableViewCellHeightRow -(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//TableViewSelfCell
#define TableViewSelfCell - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//TableViewSelectSectionRow
#define TableViewSelectSectionRow -(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//TableViewInfoCell
#define TableViewInfoCell(_CellName_) - (UITableViewCell *)(_CellName_)
//NavPop
#define SetPop  [self.navigationController popViewControllerAnimated:YES];

//登录注册请求

//左、右 边视图X坐标  NAV
#define LABLEIMAGE_K 10

//View X坐标
#define ViewFromX_Y 10

//weak self
#define TYPEWEAKSELF __weak typeof(self)weakSelf = self;

//Nav字体颜色
#define LABLECOLOR_K [UIColor whiteColor]
//View颜色
#define RGB_K [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1];

//Blue
#define ColorBlue_k NAVCOLOR_C(49.0, 130.0, 198.0)
//gray
#define GrayColor_K NAVCOLOR_C(173.0, 174.0, 173.0)

//TabBar图片设置
#define TabBarItemImage_K(_TabBarImage_) [[UIImage imageNamed:_TabBarImage_]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];

#define TabBarSelectedImage_K(_SelectedImage_) [[UIImage imageNamed:_SelectedImage_] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];

//RGB
#define NAVCOLOR_C(_RED_,_GREEN_,_BLUE_) [UIColor colorWithRed:(_RED_)/255.0 green:(_GREEN_)/255.0 blue:(_BLUE_)/255.0 alpha:1]
#define COLORPCT_K(_RED_,_GREEN_,_BLUE_,_ALPHA_) [UIColor colorWithRed:(_RED_)/255.0 green:(_GREEN_)/255.0 blue:(_BLUE_)/255.0 alpha:(_ALPHA_)]

//FROM
#define FRAMEMAKE_F(_X_,_Y_,_WIDTH_,_HEIGHT_) CGRectMake(_X_,_Y_, _WIDTH_, _HEIGHT_);

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//获取当前设备的宽、高
#define WIDTH_K [[UIScreen mainScreen] bounds].size.width
#define HEIGHT_K [[UIScreen mainScreen] bounds].size.height

//字典font
#define StringFont_DicK(_Font_) [[NSDictionary alloc]initWithObjectsAndKeys:(_Font_),NSFontAttributeName, nil];

//label 设置
#define LabelSet(_NAME_,_TEXT_,_TEXTCOLOR_,_FONT_,_DIC_,_SIZE_)\
(_NAME_).text = (_TEXT_);\
(_NAME_).textColor = (_TEXTCOLOR_);\
(_NAME_).font = [UIFont systemFontOfSize:(_FONT_)];\
NSDictionary *(_DIC_) = StringFont_DicK((_NAME_).font);\
CGSize (_SIZE_) = [(_NAME_).text sizeWithAttributes:(_DIC_)];


//系统红色
#define REDCOLOR_K NAVCOLOR_C(252.0, 73.0, 72.0)
//系统绿色
#define GREEN_K NAVCOLOR_C(84.0, 160.0, 74.0)
//Image
#define AcquireImage_K(_Image_)[UIImage imageNamed:_Image_];
//请求参数
#define httpUrl_K Network_URL

//网页服务器
#define HTTP_IP @"http://139.224.25.252/api/json/"
//#define HTTP_IP @"http://app.yuexianglintao.com/api/json/"

#endif /* Common_h */
