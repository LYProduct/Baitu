//
//  UIView+shadow.m
//  YunNa
//
//  Created by wyp on 2017/9/7.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import "UIView+shadow.h"

@implementation UIView_shadow

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
}
*/

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.layer.cornerRadius = 5;
        self.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        self.layer.shadowOpacity = 0.8f;
        self.layer.shadowRadius = 5.f;
        self.layer.shadowOffset = CGSizeMake(0,0);
    }
    return self;
}

@end
