//
//  CALayer+XibConfiguration.h
//  TAM
//
//  Created by mc on 16/3/30.
//  Copyright © 2016年 mc. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>


@interface CALayer (XibConfiguration)
@property(nonatomic, assign) UIColor *borderUIColor;
@end
