//
//  NSDictionary+deleteAllNullValue.h
//  YunNa
//
//  Created by 云鹏 on 2017/9/16.
//  Copyright © 2017年 RongKe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (deleteAllNullValue)

- (NSDictionary *)deleteAllNullValue;

@end
