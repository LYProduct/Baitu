//
//  NSString+Category.h
//  CountryLogistics
//
//  Created by Mom on 2017/3/30.
//  Copyright © 2017年 KuBaoInternet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Category)

/**
 *  获取字符串宽度
 *
 *  @param strings    字符串
 *  @param heights 高度
 *  @param fonts   字体大小
 *
 *  @return 宽度
 */
+(CGFloat)getStringWidthFromeString:(NSString *)strings height:(CGFloat)heights font:(UIFont*)fonts;

/**
 *  获取字符串高度
 *
 *  @param strings   字符串
 *  @param widths 宽度
 *  @param fonts  字体大小
 *
 *  @return 宽度
 */
+(CGFloat)getStringHeightFromeString:(NSString *)strings width:(CGFloat)widths font:(UIFont*)fonts;


/**
 *  AttributedString 行间距
 *
 *  @param str 字符串
 *
 *  @return NSMutableAttributedString 字符串
 */
+(NSMutableAttributedString *)setLineSpacingWith:(NSString *)str;

/**
 *  去掉 数字 字符串后面所有的0,保留两位小数
 *
 */
+ (NSString *)getNoZeroSuffixStringWith:(NSString *)str;

/**
 *  根据服务器获取的时间戳转成时间字符串
 *
 *  @param timeStr 服务器获取的时间戳
 *  @param format  时间格式
 *
 *  @return 时间字符串
 */
+(NSString*)TimeFromTimeString:(NSString*)timeStr andFormat:(NSString*)format;

/**
 *  清空字符串中的空白字符
 *
 *  @param string 待处理字符串
 *
 *  @return 去掉空格的字符串
 */
+ (NSString *)DeleteSpaceStringFromStr:(NSString *)string;

/**
 *  将字典或者数组转化为JSON串
 *
 *  @param theData 传入的data
 *
 *  @return js字符串
 */
+ (NSString *)FromDataToJSString:(id)theData;

/**
 *  拼接数组内容成字符串
 *
 *  @param arry 待拼接的数组
 *
 *  @return 拼接好的字符串
 */
+ (NSString *)FromArraryToString:(NSArray *)arry;


/**
 根据连接符连接数组中的字符串
 
 @param arry 待拼接的数组
 @param linkStr 连接符号
 @return 生成的字符串
 */
+ (NSString *)FromArraryToString:(NSMutableArray *)arry linkString:(NSString*)linkStr;

//时间转时间戳
+(NSString*)timeToTimeString;

/**
 时间字符串转时间戳(timeStr和formats格式一样才行)
 
 @param timeStr 时间字符串
 @param formats 时间格式
 @return 生成的时间戳字符串
 */
+(NSString*)timeStrFromShowTimeSting:(NSString*)timeStr format:(NSString*)formats;

+ (NSDate*)timeStringFromShowTimeSting:(NSString*)timeStr fromFormat:(NSString*)fromFormats;

+ (NSString*)stringFromDate:(NSDate*)dates fromFormat:(NSString*)fromFormats;


/**
 电话号码隐藏
 
 @param phone 待处理的电话号码
 @return 生成的字符串
 */
+ (NSString*)phoneNumber:(NSString*)phone;


/**
 银行卡隐藏
 
 @param BankNo 待处理的银行卡号码
 @return 生成的银行卡号码
 */
+ (NSString*)getSecrectStringWithBankNo:(NSString *)BankNo;


/**
 根据给的日期生成星期数

 @param date 待判断日期
 @return 星期数
 */
+ (NSString*)weakNameFrom:(NSDate*)date;

/**
 是否仅为中文
 
 @return bool
 */
- (BOOL) isOnlyChinese;


/**
 是否仅为数字
 
 @return bool
 */
- (BOOL) isOnlyNumber;

/**
 是否为真实姓名
 
 @return bool
 */
- (BOOL) isValidRealName;

@end
