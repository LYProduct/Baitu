//
//  LFLivenessCommon.h
//  LFLivenessController
//
//  Copyright (c) 2017-2018 LINKFACE Corporation. All rights reserved.
//

#ifndef LFLivenessCommon_h
#define LFLivenessCommon_h


#define kSTColorWithRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16)) / 255.0 \
green:((float)((rgbValue & 0xFF00) >> 8)) / 255.0 \
blue:((float)(rgbValue & 0xFF)) / 255.0 alpha:1.0]

#define kLFScreenWidth [UIScreen mainScreen].bounds.size.width
#define kLFScreenHeight [UIScreen mainScreen].bounds.size.height

#endif /* LFLivenessCommon_h */
