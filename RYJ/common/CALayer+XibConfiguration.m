//
//  CALayer+XibConfiguration.m
//  TAM
//
//  Created by mc on 16/3/30.
//  Copyright © 2016年 mc. All rights reserved.
//

#import "CALayer+XibConfiguration.h"

@implementation CALayer (XibConfiguration)

-(void)setBorderUIColor:(UIColor *)color
{
    self.borderColor = color.CGColor;
}
-(UIColor*)borderUIColor
{
    return [UIColor colorWithCGColor:self.borderColor];
}

@end
