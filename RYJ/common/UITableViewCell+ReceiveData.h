//
//  UITableViewCell+ReceiveData.h
//  MT
//
//  Created by wyp on 17/2/8.
//  Copyright © 2017年 ike. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (ReceiveData)

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc;

- (void)reloadCellForData:(NSDictionary *)dict vc:(UIViewController *)vc indexPath:(NSIndexPath *)indexPath;

@end
