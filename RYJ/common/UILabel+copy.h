//
//  UILabel+copy.h
//  WuHuWallet
//
//  Created by 云鹏 on 2017/12/7.
//  Copyright © 2017年 YunLa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (copy)

@property (nonatomic,assign) BOOL isCopyable;

@end
